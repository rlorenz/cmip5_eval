#!/usr/bin/python
'''
File Name : bestXY_models_constrain_weight.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 13-11-2017
Modified:
Purpose: pick best models and choose them for constained ensemble

'''
import numpy as np
import netCDF4 as nc
from netcdftime import utime
import datetime as dt
import glob as glob
from sklearn import linear_model
import math
import os
from os.path import expanduser
home = expanduser("~") # Get users home directory
import sys
sys.path.insert(0, home+'/scripts/plot_scripts/utils/')
from func_read_data import func_read_netcdf
from func_write_netcdf import func_write_netcdf
from calc_RMSE_obs_mod_3D import rmse_3D
import matplotlib
import matplotlib.pyplot as plt
import operator
###
# Define input & output
###
experiment = 'rcp85'
archive = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/'

target_var = 'TXx'
target_file = 'CLIM'
res_name_target = 'ANN'
res_time_target = 'MAX'
target_mask = 'maskT'
freq = 'ann'

diag_var = ['TXx', 'TXx', 'pr']   #, 'tas']
var_file = ['CLIM', 'TREND', 'TREND'] #, 'CLIM']
res_name = ['ANN', 'ANN', 'JJA']   #, 'JJA']
res_time = ['MAX', 'MAX', 'MEAN']
freq_v = ['ann', 'ann', 'mon']
masko = ['maskT', 'maskT', 'maskT']    #, 'maskT']
nvar = len(diag_var)

region = 'CNEU'          #cut data over region?
longname_region = 'Central Europe' # for title plot
obsdata = ['MERRA2']

#path = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/'
outdir = '%sEval_Weight/%s/%s/' %(archive, target_var, region)

syear_hist = 1980
eyear_hist = 2014
syear_fut = 2065
eyear_fut = 2099
syear = 1951
eyear = 2100

# how many models do you want to select?
nr_models = 10

nyears = eyear_hist - syear_hist + 1
ntim_tot = eyear - syear + 1

grid = 'g025'
Fill = 1e+20

if (os.access(outdir, os.F_OK) == False):
    os.makedirs(outdir)
###read model data
print "Read model data"
## find all matching files in archive, loop over all of them
# first count matching files in folder
models_t = list()
model_names = list()
model_names_diag = list()

path_target = '%s/%s/%s/%s/' %(archive, target_var, freq, target_mask)
if region:
    name = '%s%s_%s_*_%s_r?i?p?_%s-%s_%s%s_%s_%s.nc' %(
        path_target, target_var, freq, experiment, syear_fut, eyear_fut,
        res_name_target, res_time_target, target_file, region)
else:
    name = '%s/%s_%s_*_%s_r?i?p?_%s-%s_%s%s_%s.nc' %(
        path_target, target_var, freq, experiment, syear_fut, eyear_fut,
        res_name_target, res_time_target, target_file)

nfiles_targ = len(glob.glob(name))
print str(nfiles_targ) + ' matching files found for target variable'

for filename in glob.glob(name):
    models_t.append(filename.split('_')[4] + ' ' + filename.split('_')[6])

overlap = models_t

for v in xrange(len(diag_var)):
    models_v = list()
    path_var = '%s/%s/%s/%s/' %(archive, diag_var[v], freq_v[v], masko[v])
    if region:
        name_v = '%s%s_%s_*_%s_r?i?p?_%s-%s_%s%s_%s_%s.nc' %(
            path_var, diag_var[v], freq_v[v], experiment, syear_hist,
            eyear_hist, res_name[v], res_time[v], var_file[v], region)
    else:
        name_v = '%s%s_%s_*_%s_r?i?p?_%s-%s_%s%s_%s.nc' %(
            path_var, diag_var[v], freq_v[v], experiment, syear_hist, 
            eyear_hist, res_name[v], res_time[v], var_file[v])

    for filename in glob.glob(name_v):
        models_v.append(filename.split('_')[4] + ' ' +
                        filename.split('_')[6])
    #find overlapping files for all variables
    overlap = list(set(models_v) & set(overlap))
    del models_v, path_var
nfiles = len(overlap)

print str(nfiles) + ' matching files found for all variables'

## read obs data timeseries to plot
for o in obsdata:
    print "Read %s data" %(o)
    if region:
        obsfile_ts = '%s%s/%s/%s/%s_%s_%s_%s-%s_%s%s_%s.nc' %(
            archive, target_var, freq, target_mask, target_var, freq, o,
            syear_hist, eyear_hist, res_name_target, res_time_target, region)
    else:
        obsfile_ts = '%s%s/%s/%s/%s_%s_%s_%s-%s_%s%s.nc' %(
            archive, target_var, freq, target_mask, target_var, freq, o,
            syear_hist, eyear_hist, res_name_target, res_time_target)
    
    fh = nc.Dataset(obsfile_ts, mode = 'r')
    temp_obs_ts = fh.variables[target_var][:]
    lat = fh.variables['lat'][:]
    lon = fh.variables['lon'][:]

    time = fh.variables['time']
    cdftime = utime(time.units, calendar = time.calendar)
    obsdates = cdftime.num2date(time[:])
    obsyears = np.asarray([obsdates[i].year for i in xrange(len(obsdates))])
    fh.close()
    # mask all obs to same mask if available
    if isinstance(temp_obs_ts, np.ma.core.MaskedArray):
        if o == 'Obs':
            maskmiss_ts = temp_obs_ts.mask.copy()
            ma_temp_obs_ts = temp_obs_ts[:]
        else:
            try:
                ma_temp_obs_ts = np.ma.array(temp_obs_ts, mask = maskmiss_ts)
            except (NameError):
                ma_temp_obs_ts = temp_obs_ts[:]
    else:
        ma_temp_obs = np.ma.masked_array(temp_obs_ts, np.isnan(temp_obs_ts))
        try:
            ma_temp_obs_ts = np.ma.array(ma_temp_obs, mask = maskmiss_ts)
        except (NameError):
            ma_temp_obs_ts = ma_temp_obs
    ## calculate area means
    w_lat = np.cos(lat * (4.0 * math.atan(1.0) / 180))
    tmp_latweight = np.ma.average(ma_temp_obs_ts, axis = 1, weights = w_lat)
    if (o == 'MERRA2'):
        merra_ts_areaavg = np.nanmean(tmp_latweight.filled(np.nan), axis = 1)
    elif (o == 'ERAint'):
        era_ts_areaavg = np.nanmean(tmp_latweight.filled(np.nan), axis = 1)
    else:
        obs_ts_areaavg = np.nanmean(tmp_latweight.filled(np.nan), axis = 1)

#Read data
data = np.empty((nvar, nfiles), float, Fill)
data.fill(np.NaN)
obs_areaavg = np.empty((len(obsdata), nvar), float, Fill)
obs_areaavg.fill(np.NaN)
data_unit = list()
for v in xrange(len(diag_var)):
    # Read obs data
    for o in xrange(len(obsdata)):
        print "Read %s data" %(obsdata[o])
        if ((obsdata[o] == 'Obs') and
            ((diag_var[v] == 'rlus') or (diag_var[v] == 'rsds'))):
            name = '%s/%s/%s/%s/%s_%s_%s_2000-%s_%s%s_%s_%s.nc' %(
                archive, diag_var[v], freq_v[v], masko[v], diag_var[v],
                freq_v[v], obsdata[o], eyear_hist, res_name[v], res_time[v],
                var_file[v], region)
        else:
            name = '%s/%s/%s/%s/%s_%s_%s_%s-%s_%s%s_%s_%s.nc' %(
                archive, diag_var[v], freq_v[v], masko[v], diag_var[v],
                freq_v[v], obsdata[o], syear_hist, eyear_hist, res_name[v],
                res_time[v], var_file[v], region)

        fh = nc.Dataset(name, mode = 'r')
        var2_obs = fh.variables[diag_var[v]][:] # global data, time, lat, lon
        lat = fh.variables['lat'][:]
        lon = fh.variables['lon'][:]
        fh.close()
 
        if isinstance(var2_obs, np.ma.core.MaskedArray):
            if obsdata[o] == 'Obs':
                maskmiss = var2_obs.mask.copy()
                ma_var2_obs = var2_obs
            else:
                try:
                    ma_var2_obs = np.ma.array(var2_obs, mask = maskmiss)
                except (NameError):
                    ma_var2_obs = var2_obs
        else:
            ma_tmp = np.ma.masked_array(var2_obs, np.isnan(var2_obs))
            try:
                ma_var2_obs = np.ma.array(ma_tmp, mask = maskmiss)
            except(NameError):
                ma_var2_obs = ma_tmp
        # inistialize arrays
        if ((o == 0) and (v == 0)):
            obs_data = np.empty((len(obsdata), nvar, len(lat), len(lon)), float,
                                Fill)
            obs_data.fill(np.NaN)
        # calculate area average for obs
        tmp_latweight = np.ma.average(np.squeeze(ma_var2_obs), axis = 0,
                                      weights = w_lat)
        obs_data[o, v, :, :] = ma_var2_obs.filled(np.nan)[:]
        obs_areaavg[o, v] = np.nanmean(tmp_latweight.filled(np.nan))

    path_var = '%s/%s/%s/%s/' %(archive, diag_var[v], freq_v[v], masko[v])
    if region:
        name = '%s%s_%s_*_%s_r?i?p?_%s-%s_%s%s_%s_%s.nc' %(
            path_var, diag_var[v], freq_v[v], experiment, syear_hist,
            eyear_hist, res_name[v], res_time[v], var_file[v], region)
    else:
        name = '%s%s_%s_*_%s_r?i?p?_%s-%s_%s%s_%s.nc' %(
            path_var, diag_var[v], freq_v[v], experiment, syear_hist,
            eyear_hist, res_name[v], res_time[v], var_file[v]) 
    f = 0
    for filename in glob.glob(name):
        model_name = filename.split('_')[4] + ' ' + filename.split('_')[6]
        if model_name in overlap:
            #print "Read " + filename + " data"
            fh = nc.Dataset(filename, mode = 'r')
            lon = fh.variables['lon'][:]
            lat = fh.variables['lat'][:]
            temp_mod = fh.variables[diag_var[v]][:] # global data
            #check that time axis and grid is identical for models
            if f != 0:
                if temp_mod0.shape != temp_mod.shape:
                    print('Warning: Dimension for model0 and modelX ' +
                          'is different!')
                    continue
            else:
                temp_mod0 = temp_mod[:]
                tmp = fh.variables[diag_var[v]]
                data_unit.append(tmp.units)
            fh.close()
            model = filename.split('_')[4]
            if model == 'ACCESS1.3':
                model = 'ACCESS1-3'
            elif model == 'FGOALS_g2':
                model = 'FGOALS-g2'
            ens = filename.split('_')[6]
            if (v == 0):
                model_names_diag.append(model + '_' + ens)

            if isinstance(temp_mod, np.ma.core.MaskedArray):
                try:
                    ma_hist = np.ma.array(temp_mod, mask = maskmiss)
                except (NameError):
                    ma_hist = temp_mod
            else:
                ma_tmp = np.ma.masked_array(temp_mod, np.isnan(temp_mod))
                try:
                    ma_hist = np.ma.array(ma_tmp, mask = maskmiss)
                except(NameError):
                    ma_hist = ma_tmp
            if ((v == 0) and (f == 0)):
                data_3D = np.empty((nvar, nfiles, len(lat), len(lon)), float,
                                   Fill)
                data_3D.fill(np.NaN)
            # average over area and save value for each model
            #w_lat = np.cos(lat * (4.0 * math.atan(1.0) / 180))
            tmp2_latweight = np.ma.average(np.squeeze(ma_hist), axis = 0,
                                           weights = w_lat)
            data[v, f] = np.nanmean(tmp2_latweight.filled(np.nan))
            data_3D[v, f, :, :] = ma_hist.filled(np.nan)[:]
            f = f + 1
        else:
            continue

    if data_unit[v] == 'degC':
        degree_sign = u'\N{DEGREE SIGN}'
        data_unit[v] = degree_sign + "C"

target = np.empty((nfiles))
target.fill(np.NaN)
target_ts = dict()
f = 0
if region:
    name = '%s%s_%s_*_%s_r?i?p?_%s-%s_%s%s_%s_%s.nc' %(
        path_target, target_var, freq, experiment, syear_fut, eyear_fut,
        res_name_target, res_time_target, target_file, region)
else:
    name = '%s/%s_%s_*_%s_r?i?p?_%s-%s_%s%s_%s.nc' %(
        path_target, target_var, freq, experiment, syear_fut, eyear_fut,
        res_name_target, res_time_target, target_file)
for filename in glob.glob(name):
    model_name = filename.split('_')[4] + ' ' + filename.split('_')[6]
    if model_name in overlap:
        #print "Read "+filename+" data"
        fh = nc.Dataset(filename, mode = 'r')
        temp_fut = fh.variables[target_var][:] # global data, time, lat, lon
        lat = fh.variables['lat'][:]
        lon = fh.variables['lon'][:]
        tmp = fh.variables[target_var]
        target_unit = tmp.units
        fh.close()
        if (target_file != 'SCALE'):
            path_hist = '%s/%s/%s/%s/' %(archive, target_var, freq, target_mask)
            if region:
                filename_hist = '%s%s_%s_%s_%s_%s_%s-%s_%s%s_%s_%s.nc' %(
                    path_hist, target_var, freq, filename.split('_')[4],
                    experiment, filename.split('_')[6], syear_hist, eyear_hist,
                    res_name_target, res_time_target, target_file, region)
            else:
                filename_hist = '%s/%s_%s_%s_%s_%s_%s-%s_%s%s_%s.nc' %(
                    path_hist, target_var, freq, target_mask,
                    target_var, freq, filename.split('_')[4],
                    experiment, filename.split('_')[6], syear_hist, eyear_hist,
                    res_name_target, res_time_target, target_file)
            fh = nc.Dataset(filename_hist, mode = 'r')
            temp_hist = fh.variables[target_var][:] # global data,time,lat,lon
            fh.close()
            temp_mod = temp_fut - temp_hist
        else:
            temp_mod = temp_fut

        #check that time axis and grid is identical for model0 and modelX
        if f != 0:
            if temp_mod0.shape != temp_mod.shape:
                print 'Warning: Dimension for models are different!'
                continue
        else:
            temp_mod0 = temp_mod[:]

        model = filename.split('_')[4]
        if model == 'ACCESS1.3':
            model = 'ACCESS1-3'
        elif model == 'FGOALS_g2':
            model = 'FGOALS-g2'
        ens = filename.split('_')[6]
        model_names.append(model + '_' + ens)

        if isinstance(temp_mod, np.ma.core.MaskedArray):
            try:
                ma_target = np.ma.array(temp_mod, mask = maskmiss_ts[0,:,:])
            except (NameError):
                ma_target = temp_mod
        else:
            ma_tmp_mod = np.ma.masked_array(temp_mod, np.isnan(temp_mod))
            try:
                ma_target = np.ma.array(ma_tmp_mod, mask = maskmiss_ts[0,:,:])
            except (NameError):
                ma_target = ma_tmp_mod
        if (f == 0):
            target_3D = np.empty((nfiles, len(lat), len(lon)), float, Fill)
            target_3D.fill(np.NaN)
        # average over area and save value for each model
        #w_lat = np.cos(lat * (4.0 * math.atan(1.0) / 180))
        tmp1_latweight = np.ma.average(np.squeeze(ma_target),
                                       axis = 0,
                                       weights = w_lat)
        target[f] = np.nanmean(tmp1_latweight.filled(np.nan))
        target_3D[f, :, :] = ma_target.filled(np.nan)[:]

        # read data over whole timeseries for plotting
        path_ts = '%s/%s/%s/%s/' %(archive, target_var, freq, target_mask)
        if region:
            filename_ts = '%s%s_%s_%s_%s_%s_%s-%s_%s%s_%s.nc' %(
                path_ts, target_var, freq, filename.split('_')[4],
                experiment, filename.split('_')[6], syear, eyear,
                res_name_target, res_time_target, region)
        else:
            filename_ts = '%s/%s_%s_%s_%s_%s_%s-%s_%s%s.nc' %(
                path_ts, target_var, freq, target_mask,
                target_var, freq, filename.split('_')[4],
                experiment, filename.split('_')[6], syear, eyear,
                res_name_target, res_time_target)
        fh = nc.Dataset(filename_ts, mode = 'r')
        temp_ts = fh.variables[target_var][:] # global data,time,lat,lon
        time = fh.variables['time']
        cdftime = utime(time.units, calendar = time.calendar)
        dates = cdftime.num2date(time[:])
        years = np.asarray([dates[i].year for i in xrange(len(dates))])
        fh.close()
        if isinstance(temp_ts, np.ma.core.MaskedArray):
            try:
                ma_temp_ts = np.ma.array(temp_ts,
                                  mask = np.tile(maskmiss_ts[0,:,:],
                                                 (temp_ts.shape[0], 1)))
            except (NameError):
                ma_temp_ts = temp_ts
        else:
            ma_tmp = np.ma.masked_array(temp_ts, np.isnan(temp_ts))
            try:
                ma_temp_ts = np.ma.array(ma_tmp,
                                         mask = np.tile(maskmiss_ts[0,:,:],
                                                        (temp_ts.shape[0], 1)))
            except (NameError):
                ma_temp_ts = ma_tmp

        tmp_ts_latweight = np.ma.average(np.squeeze(ma_temp_ts), axis = 1,
                                         weights = w_lat)
        target_ts[model + '_' + ens] = np.nanmean(tmp_ts_latweight.filled(np.nan), axis = 1)
        f = f + 1
    else:
        continue
if target_unit == 'degC':
    target_unit = degree_sign + "C"

if model_names != model_names_diag:
    print 'Warning: models in diag and target not the same (order).'

# calculate trend over historical period
ols = linear_model.LinearRegression(normalize = True) 
if any(obs == 'Obs' for obs in obsdata):
    ols.fit(obsyears.reshape(-1, 1),
            obs_ts_areaavg.reshape(-1, 1))
    trend_obs = ols.predict(obsyears.reshape(-1, 1))

    z_obs = np.polyfit(obsyears, np.squeeze(trend_obs), 1)
    f_obs = np.poly1d(z_obs)
    extra_x_obs = f_obs(years)
if any(obs == 'ERAint' for obs in obsdata):
    ols.fit(obsyears.reshape(-1, 1),
            era_ts_areaavg.reshape(-1, 1))
    trend_era = ols.predict(obsyears.reshape(-1, 1))

    z_era = np.polyfit(obsyears, np.squeeze(trend_era), 1)
    f_era = np.poly1d(z_era)
    extra_x_era = f_era(years)
if any(obs == 'MERRA2' for obs in obsdata):
    ols.fit(obsyears.reshape(-1, 1),
            merra_ts_areaavg.reshape(-1, 1))
    trend_merra = ols.predict(obsyears.reshape(-1, 1))

    z_merra = np.polyfit(obsyears, np.squeeze(trend_merra), 1)
    f_merra = np.poly1d(z_merra)
    extra_x_merra = f_merra(years)

# read weighted mmm from netcdf
infile = '%s/ncdf/%s%s_%s_%sdiags_%sobs_%s_%s_swu%s_swq%s_wmm_ts.nc' %(
    outdir, target_var, target_file, res_name_target, len(diag_var),
    len(obsdata), region, 'RMSE', '0.6', '0.5')
wmm_file = nc.Dataset(infile, mode = 'r')
wmm = wmm_file.variables['wmm_ts_areaavg'][:]
lower_ts_wmm = wmm_file.variables['lower_ts_wmm'][:]
upper_ts_wmm = wmm_file.variables['upper_ts_wmm'][:]

# calculate RMSE for all diags and obs datasets
RMSE_diag = dict()
rmse = np.empty((nvar, len(obsdata)))
rmse.fill(np.NaN)
rmse_norm = np.empty((nvar, len(obsdata)))
rmse_norm.fill(np.NaN)
rmse_avg = np.empty((nvar))
rmse_avg.fill(np.NaN)
for f in xrange(len(glob.glob(name))):
    for v in xrange(len(diag_var)):
        for o in xrange(len(obsdata)):
            rmse[v, o] = rmse_3D(data_3D[v, f, :, :], obs_data[o, v, :, :],
                                 lat, lon)
            rmse_norm[v, o] = rmse[v, o] / obs_areaavg[o, v]
        rmse_avg[v] = np.mean(rmse_norm[v, :])
    rmse_avgavg = np.mean(rmse_avg)
    RMSE_diag[model_names[f]] = rmse_avgavg

sort_RMSE = sorted(RMSE_diag.items(), key = operator.itemgetter(1))
ranks = {}
def ranks_from_scores(sorted_scores):
    """sorted_scores: a list of tuples (object_id, score),
       return a mapping of object IDs to ranks
    """
    ranks = {}
    previous_score = object()
    for index, (obj_id, score) in enumerate(sorted_scores):
        if score != previous_score:
            previous_score = score
            rank = index + 1
        ranks[obj_id] = rank
    return ranks

sort_ranks = ranks_from_scores(sort_RMSE)

# choose XY best models:
best_models = dict((key, value) for key, value in sort_ranks.items() if value <= nr_models)

tmp = 0
d_best_models_ts = dict()
for key in best_models.keys():
    tmp = tmp + target_ts[key]
    d_best_models_ts[key] = target_ts[key]
target_ts_best = tmp / nr_models

ts_ens_best = np.array([value for key, value in sorted(
    d_best_models_ts.iteritems())], dtype = float)
tmp1 = np.empty(ts_ens_best.shape, dtype = float)
tmp1.fill(np.NaN)
for t in xrange(ts_ens_best.shape[1]):
    tmp1[:, t] = sorted(ts_ens_best[:, t], reverse = True)
dim = np.count_nonzero(tmp1[:, 0])
ind_5 = int(math.ceil(0.05 * dim) - 1)
ind_95 = int(math.floor(0.95 * dim) - 1)
lower_ts = tmp1[ind_5, :]
upper_ts = tmp1[ind_95, :]
del tmp1

# multi-model mean
# first average over initial conditions ensembles per model
models = [x.split('_')[0] for x in target_ts.keys()]
mult_ens = []
seen = set()
for m in models:
    if m not in seen:
        seen.add(m)
    else:
        mult_ens.append(m)
# make sure every model only once in list
list_with_mult_ens = set(mult_ens)
d_avg_ens_ts = dict()
for key, value in target_ts.iteritems():
    if key.split('_')[0] in list_with_mult_ens:
        #find other ensemble members
        ens_mem = [value2 for key2, value2 in sorted(
            target_ts.iteritems()) if key.split('_')[0] in key2]
        d_avg_ens_ts[key.split('_')[0]] = np.nanmean(ens_mem, axis = 0)
    else:
        d_avg_ens_ts[key.split('_')[0]] = value
tmp3 = 0
for key, value in d_avg_ens_ts.iteritems():
    tmp3 = tmp3 + value

target_ts_avg = tmp3 / len(set(models))
del tmp3

ts_ens_areaavg = np.array([value for key, value in sorted(
    d_avg_ens_ts.iteritems())], dtype = float)
tmp21 = np.empty(ts_ens_areaavg.shape, dtype = float)
tmp21.fill(np.NaN)
for t in xrange(ts_ens_areaavg.shape[1]):
    tmp21[:, t] = sorted(ts_ens_areaavg[:, t], reverse = True)
dim = np.count_nonzero(tmp21[:, 0])
ind_5 = int(math.ceil(0.05 * dim) - 1)
ind_95 = int(math.floor(0.95 * dim) - 1)
lower_ts_mm = tmp21[ind_5, :]
upper_ts_mm = tmp21[ind_95, :]
del tmp21

# save data to netcdf for further use
ncdir = '%sncdf/' %outdir
# save timeseries info
outfile = '%sbest%smodels_%s%s_%s_%sdiags_%sobs_%s_%s-%s.nc' %(
    ncdir, nr_models, target_var, target_file, res_name_target, len(diag_var),
        len(obsdata), region, syear, eyear)
nyears = eyear - syear + 1
datesout = [dt.datetime(syear + x, 06, 01, 00) for x in range(0, nyears)]
time = nc.date2num(datesout, units = 'days since %s' %(
    nc.datetime.strftime(dt.datetime(syear, 01, 01, 00), '%Y-%m-%d %H:%M:%S')),
    calendar = 'standard')

fout = nc.Dataset(outfile, mode = 'w')

fout.createDimension('time', len(years))
timeout = fout.createVariable('time', 'f8', ('time'), fill_value = 1e20)
setattr(timeout, 'units',
        'days since %s' %(nc.datetime.strftime(dt.datetime(syear, 01, 01, 00),
                                               '%Y-%m-%d %H:%M:%S')))
fout.createDimension('model', nr_models)
modelout = fout.createVariable('model', str, ('model'), fill_value = 1e20)
models_out = np.array(best_models.keys(), dtype = 'object')

dataout = fout.createVariable('bestM_ts_areaavg', 'f8', ('model', 'time'),
                              fill_value = 1e20)
setattr(dataout, 'Longname', 'area-averaged timeseries for best %s models' %(
    nr_models))
setattr(dataout, 'units', target_unit)
setattr(dataout, 'description', 'timeseries for best %s models' %nr_models)

timeout[:] = time[:]
modelout[:] = models_out
dataout[:] = ts_ens_best[:]

# Set global attributes
setattr(fout, "author", "Ruth Lorenz @IAC, ETH Zurich, Switzerland")
setattr(fout, "contact", "ruth.lorenz@env.ethz.ch")
setattr(fout, "creation date", dt.datetime.today().strftime('%Y-%m-%d'))
setattr(fout, "Script", "bestXY_models_constrain_weight.py")
setattr(fout, "Input files located in:", archive)
fout.close()

## plot data
fig = plt.figure(figsize = (10, 5), dpi = 300)

plt.fill_between(years, lower_ts_mm, upper_ts_mm, facecolor = 'grey',
                 alpha = 0.3)
plt.fill_between(years, lower_ts_wmm, upper_ts_wmm, facecolor = 'crimson', 
                 edgecolor = 'crimson', alpha = 0.3)
plt.fill_between(years, lower_ts, upper_ts, facecolor = 'purple', 
                 edgecolor = 'purple', alpha = 0.3)

plt.plot(years, target_ts_avg, color = 'black', linewidth = 3,
         label = 'MMM (baseline IPCC)')
plt.plot(years, target_ts_best, color = 'purple',
         linewidth = 3, label = 'best %s models' %(nr_models))
plt.plot(years, wmm, color = 'crimson',
         linewidth = 3, label = 'weighted MMM')
if any(obs == 'MERRA2' for obs in obsdata):
    plt.plot(obsyears, merra_ts_areaavg, color = 'royalblue', linewidth = 2,
             label = 'MERRA2')
    plt.plot(years, extra_x_merra, 'royalblue', linewidth = 1.0,
             linestyle = '--')
if any(obs == 'ERAint' for obs in obsdata):
    plt.plot(obsyears, era_ts_areaavg, color = 'steelblue', linewidth = 2,
             label = 'ERAint')
    plt.plot(years, extra_x_era, 'steelblue', linewidth = 1.0, linestyle = '--')
if any(obs == 'Obs' for obs in obsdata):
    plt.plot(obsyears, obs_ts_areaavg, color = 'mediumblue', linewidth = 2,
             label = 'E-Obs')
    plt.plot(years, extra_x_obs, 'mediumblue', label = 'OLS trend', 
             linewidth = 1.0, linestyle = '--')

plt.xlabel('Year')
plt.ylabel('%s %s %s [%s]' %(region, target_var, res_name_target, target_unit))
plt.grid(False)

leg = plt.legend(loc = 'upper left')  ## leg defines legend -> can be modified
leg.draw_frame(False)

plt.savefig('%stimeseries_best%smodels_%s_%s_%s_%s%s_%s_%s_%sobs.pdf' %(
    outdir, nr_models, target_var, target_file, target_mask, len(diag_var),
        diag_var[0], var_file[0], res_name[0], len(obsdata)))

#!/usr/bin/python
'''
File Name : plot_HW_CMIP5_ts.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 25-05-2016
Modified: Wed 25 May 2016 11:55:46 AM CEST
Purpose: plot timeseries of HW from CMIP5 and ERAint in SREX regions


'''
import netCDF4 as nc # to work with NetCDF files
import numpy as np
from scipy import signal
import os # operating system interface
from os.path import expanduser
home = expanduser("~") # Get users home directory
import glob
import sys
sys.path.insert(0,home+'/scripts/plot_scripts/utils/')
#from calc_RMSE_obs_mod_3D import rmse_3D
#from func_read_data import func_read_netcdf
#from func_calc_rmse import func_calc_rmse
#from clim_seas_TLL import clim_mon_TLL, std_mon_TLL, seas_avg_mon_TLL, seas_std_mon_TLL, clim_seas_mon_TLL, ann_avg_mon_TLL
#from calc_trend_TLL import trend_TLL
from point_inside_polygon import point_inside_polygon
import datetime as dt
from netcdftime import utime
#from func_write_netcdf import func_write_netcdf
import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap, addcyclic, shiftgrid
###
# Define input
###
variable = ['HWD_tx90pct']
experiment = 'rcp85'
archive1 = '/net/tropo/climphys/rlorenz/CMIP5_HW/TX90_HW/bp_1961-1990/historical_CMIP5_seasons'
archive2 = '/net/tropo/climphys/rlorenz/CMIP5_HW/TX90_HW/bp_1961-1990/%s_CMIP5_seasons' %(experiment)

outdir = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/TX90_HW/'

#grid = 'g025'
region =['CNA','NAU','MED']
area = ['Central North America','North Australia','Mediterranean']

if (os.access(outdir,os.F_OK)==False):
        os.makedirs(outdir)

syear_eval = 1979
eyear_eval = 2014
syear_eval2 = 1980
eyear_eval2 = 2015

syear_hist = 1951
eyear_hist = 2005
syear_rcp = 2006
eyear_rcp = 2100

#read obs data
print "Read ERAint data"
path = '/net/tropo/climphys/rlorenz/ERA-INT_HW/HW_output/bp_1979_2004/'
obsfile = 'tx90pct_heatwaves__ERA-INT__yearly_summer.nc'

obs = nc.Dataset('%s%s' %(path,obsfile),mode='r')
years = obs.variables['time'][:]
hw_obs = obs.variables[variable[0]]
unit = hw_obs.units

hw_obs = obs.variables[variable[0]][(years>=syear_eval) & (years<=eyear_eval),:,:]
obs_lat = obs.variables['lat'][:]
obs_lon = obs.variables['lon'][:]
obsyears = obs.variables['time'][(years>=syear_eval) & (years<=eyear_eval)]

if isinstance(hw_obs, np.ma.core.MaskedArray):
        hw_obs_nan = hw_obs.filled(np.NaN)
else:
    hw_obs_nan = hw_obs
obs.close()
if (obs_lon[0]>=0.0) and (obs_lon[-1] > 180):
    #flip lon for point_inside_polygon
    hw_obs_nan,obs_lon =  shiftgrid(180., hw_obs_nan, obs_lon,start=False)

print "Read MERRA2 data"
path = '/net/tropo/climphys/rlorenz/MERRA2_HW/HW_output/bp_1980_2004/'
obsfile = 'tx90pct_heatwaves_MERRA2_yearly_summer.nc'

obs = nc.Dataset('%s%s' %(path,obsfile),mode='r')
years = obs.variables['time'][:]
hw_merra = obs.variables[variable[0]]
unit = hw_merra.units

hw_merra = obs.variables[variable[0]][(years>=syear_eval2) & (years<=eyear_eval2),:,:]
merra_lat = obs.variables['lat'][:]
merra_lon = obs.variables['lon'][:]
merrayears = obs.variables['time'][(years>=syear_eval2) & (years<=eyear_eval2)]

if isinstance(hw_merra, np.ma.core.MaskedArray):
        hw_merra_nan = hw_merra.filled(np.NaN)
else:
    hw_merra_nan = hw_merra
obs.close()
if (merra_lon[0]>=0.0) and (merra_lon[-1] > 180):
    #flip lon for point_inside_polygon
    hw_merra_nan,merra_lon =  shiftgrid(180., hw_merra_nan, merra_lon,start=False)

###read model data
print "Read model data"
##find all matching files in archive, loop over all of them
#first count matching files in rcp folder to initialze variable per model:
infile_rcp = 'tx90pct_heatwaves_*_summer.nc'
name = '%s/%s' %(archive2,infile_rcp)
nfiles = len(glob.glob(name))
model_names = []
d_hw_mod = dict()
d_lat = dict()
d_lon = dict()
print str(nfiles)+ ' matching files found'
f = 0
for filename in glob.glob(name):
    print "Read "+filename+" data"
    model_data = nc.Dataset(filename,mode='r')
    years = model_data.variables['time'][:]
    hw_rcp = model_data.variables[variable[0]][(years>=syear_rcp) & (years<=eyear_rcp),:,:]
    lat = model_data.variables['lat'][:]
    lon = model_data.variables['lon'][:]
    years2 = model_data.variables['time'][(years>=syear_rcp) & (years<=eyear_rcp)]
    try:
        model = model_data.model_id
        ens = model_data.parent_experiment_rip
    except (AttributeError):
        continue

    if isinstance(hw_rcp, np.ma.core.MaskedArray):
            hw_rcp_nan = hw_rcp.filled(np.NaN)
    else:
        hw_rcp_nan = hw_rcp
    model_data.close()

    infile_hist = 'tx90pct_heatwaves_%s_historical_%s_yearly_summer.nc' %(model,ens)
    try:
        model_data =nc.Dataset ('%s/%s' %(archive1,infile_hist),mode='r')
    except (RuntimeError):
        continue
    years = model_data.variables['time'][:]
    hw_hist = model_data[variable[0]][(years>=syear_hist) & (years<=eyear_hist),:,:]
    lat2 = model_data.variables['lat'][:]
    lon2 = model_data.variables['lon'][:]
    years1 = model_data.variables['time'][(years>=syear_hist) & (years<=eyear_hist)]
    if isinstance(hw_hist, np.ma.core.MaskedArray):
            hw_hist_nan = hw_hist.filled(np.NaN)
    else:
        hw_hist_nan = hw_hist

    model_data.close()

    if (lat.any() != lat2.any()) or (lon.any() != lon2.any()):
        print 'hist and rcp on different grids, exiting'
        continue
    else:
        del lat2,lon2

    if model == 'ACCESS1.3':
            model = 'ACCESS1-3'
    elif model == 'FGOALS_g2':
            model = 'FGOALS-g2'
    elif (model == 'HadGEM2-ES') or (model == 'HadGEM2-CC'):
        continue #HadGEM models have wrong time in HW files
    model_names.append(model+'_'+ens)

    hw_mod = np.concatenate((hw_hist_nan,hw_rcp_nan),axis=0)
    #print hw_mod.shape
    if (lon[0]>=0.0) and (lon[-1] > 180):
        #flip lon for point_inside_polygon
        hw_mod,lon =  shiftgrid(180., hw_mod, lon,start=False)
    d_hw_mod[model+'_'+ens] = hw_mod
    d_lat[model+'_'+ens] = lat
    d_lon[model+'_'+ens] = lon

years = np.concatenate((years1,years2),axis=0)

#loop over regions
for reg in xrange(len(region)):
    print 'Region is %s' %(region[reg])
    mask=np.loadtxt('/home/rlorenz/scripts/plot_scripts/areas_txt/%s.txt' %(region[reg]))
    hw_obs_reg_nan = np.ndarray((hw_obs_nan.shape))
    hw_merra_reg_nan = np.ndarray((hw_merra_nan.shape))
    for ilat in xrange(len(obs_lat)):
        for ilon in xrange(len(obs_lon)):
            if (point_inside_polygon(obs_lat[ilat],obs_lon[ilon],np.fliplr(mask))):
                hw_obs_reg_nan[:,ilat,ilon] = hw_obs_nan[:,ilat,ilon]
            else:
                hw_obs_reg_nan[:,ilat,ilon] = np.NaN
    for ilat in xrange(len(merra_lat)):
        for ilon in xrange(len(merra_lon)):
            if (point_inside_polygon(merra_lat[ilat],merra_lon[ilon],np.fliplr(mask))):
                hw_merra_reg_nan[:,ilat,ilon] = hw_merra_nan[:,ilat,ilon]
            else:
                hw_merra_reg_nan[:,ilat,ilon] = np.NaN

    #idx_lat = np.where((obs_lat>mask[2,1]) & (obs_lat<mask[0,1]))
    #obs_lat = obs_lat[idx_lat]
    #tmp1_obs_reg = np.empty((len(hw_obs_reg_nan),len(obs_lat),len(obs_lon)))
    #idx_lon = np.where((obs_lon>mask[0,0]) & (obs_lon<mask[2,0]))
    #obs_lon = obs_lon[idx_lon]
    #hw_obs_reg = np.empty((len(hw_obs_reg_nan),len(obs_lat),len(obs_lon)))
    #tmp1_obs_reg = np.squeeze(hw_obs_reg_nan[:,idx_lat,:],1)
    #hw_obs_reg = np.squeeze(tmp1_obs_reg[:,:,idx_lon],2)
    hw_obs_reg_avg =  np.apply_over_axes(np.nanmean, hw_obs_reg_nan, (1, 2))
    hw_merra_reg_avg =  np.apply_over_axes(np.nanmean, hw_merra_reg_nan, (1, 2))

    d_hw_mod_reg_avg = dict()
    sum_hw_mod = 0
    for key, value in d_hw_mod.iteritems():
        hw_mod_reg_nan = np.ndarray((d_hw_mod[key].shape))
        for ilat in xrange(len(d_lat[key])):
            for ilon in xrange(len(d_lon[key])):
                if (point_inside_polygon(d_lat[key][ilat],d_lon[key][ilon],np.fliplr(mask))):
                    hw_mod_reg_nan[:,ilat,ilon] = value[:,ilat,ilon]
                else:
                    hw_mod_reg_nan[:,ilat,ilon] = np.NaN
        #idx_lat = np.where((d_lat[key]>mask[2,1]) & (d_lat[key]<mask[0,1]))
        #d_lat[key] = d_lat[key][idx_lat]
        #tmp1_mod_reg = np.empty((len(hw_mod_reg_nan),len(d_lat[key]),len(d_lon[key])))
        #idx_lon = np.where((d_lon[key]>mask[0,0]) & (d_lon[key]<mask[1,0]))
        #d_lon[key] = d_lon[key][idx_lon]
        #hw_mod_reg = np.empty((len(hw_mod_reg_nan),len(d_lat[key]),len(d_lon[key])))
        #tmp1_mod_reg = np.squeeze(hw_mod_reg_nan[:,idx_lat,:],1)
        #hw_mod_reg = np.squeeze(tmp1_mod_reg[:,:,idx_lon],2)
        d_hw_mod_reg_avg[key] =  np.apply_over_axes(np.nanmean, hw_mod_reg_nan, (1, 2))
        sum_hw_mod = sum_hw_mod + d_hw_mod_reg_avg[key]
    mm_ts = sum_hw_mod/len(d_hw_mod_reg_avg.keys())
    
    # plot area average of annual timeseries
    fig = plt.figure(figsize=(10, 5),dpi=300)
    colors = plt.matplotlib.cm.YlOrRd(np.linspace(0,1,nfiles+20))
    for key, value in d_hw_mod_reg_avg.iteritems():
            plt.plot(years,value[:,0,0],color='grey',linewidth=0.5)
    plt.plot(years,value[:,0,0],color='grey',label="CMIP5",linewidth=0.5)
    plt.plot(years,mm_ts[:,0,0],'k-',label="non-weighted MMM",linewidth=3.0)
    plt.plot(obsyears,hw_obs_reg_avg[:,0,0],color="mediumblue",linestyle='-',label="ERAint",linewidth=3.0)
    plt.plot(merrayears,hw_merra_reg_avg[:,0,0],color="red",linestyle='-',label="MERRA2",linewidth=3.0)
   
    plt.xlabel('Year')
    plt.ylabel(area[reg]+' '+variable[0]+' ['+unit+'] timeseries') #can use LaTeX for subscripts etc
    plt.grid(True)
    leg=plt.legend(loc='upper left') #leg defines legend -> can be modified
    leg.draw_frame(False)

    plt.savefig('%s%s_hist_%s_ERAint_MERRA2_%s_ts.pdf' %(outdir,variable[0],experiment,region[reg]))

#!/usr/bin/python
'''
File Name : weight_mm_beyond_democracy_multObs_delta_flex_cdo.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 08-02-2017
Modified: Fri 01 Dec 2017 12:15:02 PM CET
Purpose: calculate weighted multi model mean based on
         approach from Knutti et al 2017 GRL
         flexible target diagnostic, multiple Obs datasets
'''
import netCDF4 as nc # to work with NetCDF files
import numpy as np
from netcdftime import utime
import datetime as dt
import math
import copy
import os # operating system interface
from os.path import expanduser
home = expanduser("~") # Get users home directory
import glob
import sys
sys.path.insert(0, home + '/scripts/plot_scripts/utils/')
from func_read_data import func_read_netcdf
from func_calc_wu_wq import calc_wu, calc_wq, calc_weights_approx
from func_eval_wmm_nonwmm_error_indexI import error_indexI
from draw_utils import draw
import matplotlib.pyplot as plt
import operator
from random import shuffle
import itertools
from calc_RMSE_obs_mod_3D import rmse_3D
from func_write_netcdf import func_write_netcdf
###
# Define input
###
experiment = 'rcp85'

# TXx CNEU
#target_var = 'TXx'
#target_file = 'CLIM'
#target_res = 'ANN'
#target_mask = 'maskT'
#target_time = 'MAX'
#freq = 'ann'

#eval_var = 'TXx'
#eval_file = 'CLIM'
#eval_res = 'ANN'
#eval_mask = 'maskT'

# multiple variables possible but deltas need to be available
#diag_var = ['TXx', 'TXx', 'pr']
# climatology: CLIM, variability: STD, trend: TREND
#var_file = ['CLIM', 'TREND', 'TREND']
#res_name = ['ANN', 'ANN', 'JJA']
#res_time = ['MAX', 'MAX', 'MEAN']
#masko = ['maskT', 'maskT', 'maskT']
#freq_v = ['ann', 'ann', 'mon']
# weight of individual fields, all equal weight 1 at the moment
#fields_weight = [1, 1, 1]

#hurs CNEU
target_var = 'clt'
target_file = 'CLIM'
target_res = 'JJA'
target_mask = 'maskF'
target_time = 'MEAN'
freq = 'mon'

eval_var = 'clt'
eval_file = 'CLIM'
eval_res = 'JJA'
eval_mask = 'maskF'

# multiple variables possible but deltas need to be available
diag_var = ['clt', 'clt']
# climatology: CLIM, variability: STD, trend: TREND
var_file = ['TREND', 'STD']
res_name = ['JJA', 'JJA']
res_time = ['MEAN', 'MEAN']
masko = ['maskF', 'maskF']
freq_v = ['mon', 'mon']
# weight of individual fields, all equal weight 1 at the moment
fields_weight = [1, 1]

syear_eval = [1980, 1980] #, 1980, 1980
eyear_eval = [2014, 2014] #, 2014, 2014

s_year1 = 1980
e_year1 = 2014
s_year2 = 2065
e_year2 = 2099
syear = 1950
eyear = 2100

# choose region if required
region = 'CNEU'
if region != None:
    area = region
else:
    area = 'GLOBAL'

obsdata = ['ERAint', 'MERRA2'] #, 'ERAint', 'MERRA2'
obsname = ['ERAint', 'MERRA2'] #'E-Obs', 'ERAint', 'MERRA2'
err = 'RMSE' #'perkins_SS', 'RMSE'
err_var = 'rmse' #'SS', 'rmse'

path = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/'
indir = path
outdir = '%s/Eval_Weight/%s/%s/' %(path, target_var, area)
if (os.access(outdir, os.F_OK) == False):
    os.makedirs(outdir)

# contour levels for plotting
lev = np.arange(0.0, 8.0, 0.5)
lev_d = [-1.1, -0.9, -0.7, -0.5, -0.3, -0.1, 0.1, 0.3, 0.5, 0.7, 0.9, 1.1]

## free parameter "radius of similarity" , minimum: internal variability ~0.04
## the larger the more distant models are considered similar
sigma_S2 = 0.6  #NAM: 0.6, CNEU: 0.7, std:0.6 #1.0963
## free parameter "radius of model quality"
## minimum is smallest obs. bias seen in ensemble ~+-1.8**2
## wide: mean intermodel distance in CMIP5
sigma_D2 = 0.5 #std: 0.7 #1.8353, EUR: 0.6. NAM: 0.5

###
# Read data
###
## read rmse for var_file
## first read model names for all var_file from text file
## select all models that occur for all var_files,
## or if testing select number of simulations defined in test
rmse_models = dict()
overlap = list()
rmse_file_target = '%s%s/%s/%s/%s_%s_all_%s_%s-%s' %(
    indir, target_var, freq, target_mask, target_var, target_res,
    experiment, s_year2, e_year2)
overlap = np.genfromtxt(rmse_file_target + '.txt', delimiter = '',
                        dtype = None).tolist()

rmse_file_eval = '%s%s/%s/%s/%s_%s_all_%s_%s-%s' %(
    indir, eval_var, freq, eval_mask, eval_var, eval_res,
    experiment, s_year1, e_year1)
rmse_models[eval_file] = np.genfromtxt(rmse_file_eval + '.txt', delimiter = '',
                                       dtype = None).tolist()
overlap = list(set(rmse_models[eval_file]) & set(overlap))

for v in xrange(len(var_file)):
    rmsefile = '%s%s/%s/%s/%s_%s_%s_all_%s_%s-%s' %(
        indir, diag_var[v], freq_v[v], masko[v], diag_var[v], var_file[v],
        res_name[v], experiment, syear_eval[v], eyear_eval[v])
    if (os.access(rmsefile + '.txt', os.F_OK) == True):
        rmse_models[var_file[v]] = np.genfromtxt(rmsefile + '.txt',
                                                 delimiter = '',
                                                 dtype = None).tolist()
        overlap = list(set(rmse_models[var_file[v]]) & set(overlap))
        try:
            overlap = overlap[0:test]
        except (NameError):
            pass

indices = dict()
rmse_d = np.ndarray((len(var_file), len(obsdata), len(overlap), len(overlap)))
rmse_q = np.ndarray((len(var_file), len(obsdata), len(overlap)))
for v in xrange(len(var_file)):
    rmsepath = '%s%s/%s/%s/' %(indir, diag_var[v], freq_v[v], masko[v])
    rmsefile = '%s%s_%s_%s_all_%s_%s-%s' %(rmsepath, diag_var[v], var_file[v],
                                           res_name[v], experiment,
                                           syear_eval[v], eyear_eval[v])
    ## find indices of model_names in rmse_file
    rmse_models[var_file[v]] = np.genfromtxt(rmsefile + '.txt', delimiter = '',
                                             dtype = None).tolist()
    for m in xrange(len(overlap)):
        indices[overlap[m]] = rmse_models[var_file[v]].index(overlap[m])
    sorted_ind = sorted(indices.items(), key = operator.itemgetter(1))
    ind = [x[1] for x in sorted_ind]
    model_names = [x[0] for x in sorted_ind]

    o = 0
    for obs in obsdata:
        ncfile = '%s%s_%s_%s_all_%s_%s-%s_%s_%s_%s.nc' %(
            rmsepath, diag_var[v], var_file[v], res_name[v], experiment,
            syear_eval[v], eyear_eval[v], area, obs, err)
        if (os.access(ncfile, os.F_OK) == True):
            print err + ' already exist, read from netcdf'
            fh = nc.Dataset(ncfile, mode = 'r')
            rmse_all = fh.variables[err_var]
            rmse_d[v, o, :, :] = rmse_all[ind, ind]
            rmse_q[v, o, :] = rmse_all[ - 1, ind]
            fh.close()
        else:
            print "RMSE delta matrix does not exist yet, exiting"
            sys.exit
        o = o + 1
tmp_delta_u = np.ndarray((len(obsdata), len(model_names), len(model_names)))
delta_u = np.ndarray((len(var_file), len(model_names), len(model_names)))
tmp_delta_q = np.ndarray((len(obsdata), len(model_names)))
delta_q = np.ndarray((len(var_file), len(model_names)))
for v in xrange(len(var_file)):
    o = 0
    for obs in obsdata:
        ## normalize rmse by median AND average over obs
        med = np.nanmedian(rmse_d[v, o, :, :])
        tmp_delta_u[o, :, :] = rmse_d[v, o, :, :] / med
        tmp_delta_q[o, :] = rmse_q[v, o, :] / med
        o = o + 1
    if (len(obsdata) == 1):
        delta_u[v, :, :] = tmp_delta_u[:]
        delta_q[v, :] = tmp_delta_q[:]
    elif (len(obsdata) > 1):
       delta_u[v, :, :] = np.median(tmp_delta_u, axis = 0)
       delta_q[v, :] = np.median(tmp_delta_q, axis = 0)
## average deltas over fields,
## taking field weight into account (all 1 at the moment)
field_w_extend_u = np.reshape(np.repeat(fields_weight,
                                        len(model_names) * len(model_names)),
                              (len(fields_weight), len(model_names),
                               len(model_names)))
delta_u = np.sqrt(np.nansum(field_w_extend_u * delta_u, axis = 0)
                            / np.nansum(fields_weight))
    
field_w_extend_q = np.reshape(np.repeat(fields_weight, len(model_names)),
                              (len(fields_weight), len(model_names)))
delta_q = np.sqrt(np.nansum(field_w_extend_q * delta_q, axis = 0)
                  / np.nansum(fields_weight))

## read obs data
obs_ts_areaavg = dict()
obs_var_areaavg = dict()
d_temp_obs_clim = dict()
d_temp_obs_std = dict()
for obs in obsdata:
    print "Read %s data" %(obs)
    if region:
        obsfile_ts = '%s%s/%s/%s/%s_%s_%s_%s-%s_%s%s_%s.nc' %(
            indir, eval_var, freq, eval_mask, eval_var, freq, obs,
            s_year1, e_year1, eval_res, target_time, region)
    else:
        obsfile_ts = '%s%s/%s/%s/%s_%s_%s_%s-%s_%s%s.nc' %(
            indir, eval_var, freq, eval_mask, eval_var, freq, obs,
            s_year1, e_year1, eval_res, target_time)
    fh = nc.Dataset(obsfile_ts, mode = 'r')
    temp_obs_ts = fh.variables[eval_var][:]
    lat = fh.variables['lat'][:]
    lon = fh.variables['lon'][:]

    time = fh.variables['time']
    cdftime = utime(time.units, calendar = time.calendar)
    obsdates = cdftime.num2date(time[:])
    obsyears = np.asarray([obsdates[i].year for i in xrange(len(obsdates))])
    fh.close()

    if isinstance(temp_obs_ts, np.ma.core.MaskedArray):
        if obs == 'Obs':
            maskmiss_ts = temp_obs_ts.mask.copy()
            ma_temp_obs_ts = temp_obs_ts[:]
        else:
            try:
                ma_temp_obs_ts = np.ma.array(temp_obs_ts,
                                             mask = maskmiss_ts)
            except (NameError):
                ma_temp_obs_ts = temp_obs_ts[:]
    else:
        ma_tmp = np.ma.masked_array(temp_obs_ts, np.isnan(temp_obs_ts))
        try:
            ma_temp_obs_ts = np.ma.array(ma_tmp,
                                         mask = maskmiss_ts)
        except(NameError):
            ma_temp_obs_ts = ma_tmp[:]
        del ma_tmp
    #temp_obs_ts = ma_temp_obs_ts.filled(np.nan)
    del temp_obs_ts
    
    if region:
        obsfile = '%s%s/%s/%s/%s_%s_%s_%s-%s_%s%s_%s_%s.nc' %(
            indir, eval_var, freq, eval_mask, eval_var, freq, obs,
            s_year1, e_year1, eval_res, target_time, 'CLIM', region)
    else:
        obsfile = '%s%s/%s/%s/%s_%s_%s_%s-%s_%s%s_%s.nc' %(
            indir, eval_var, freq, eval_mask, eval_var, freq, obs,
            s_year1, e_year1, eval_res, target_time, 'CLIM')
    fh = nc.Dataset(obsfile, mode = 'r')
    temp_obs_clim = fh.variables[eval_var][:]
    fh.close()

    if isinstance(temp_obs_clim, np.ma.core.MaskedArray):
        if obs == 'Obs':
            maskmiss = temp_obs_clim.mask.copy()
            ma_temp_obs_clim = temp_obs_clim[:]
        try:
            ma_temp_obs_clim = np.ma.array(temp_obs_clim, mask = maskmiss)
        except (NameError):
            ma_temp_obs_clim = temp_obs_clim[:]
    else:
        ma_tmp_clim = np.ma.masked_array(temp_obs_clim, np.isnan(temp_obs_clim))
        try:
            ma_temp_obs_clim = np.ma.array(ma_tmp_clim, mask = maskmiss)
        except(NameError):
            ma_temp_obs_clim = ma_tmp_clim[:]
        del ma_tmp_clim
    d_temp_obs_clim[obs] = ma_temp_obs_clim.filled(np.nan)
    del temp_obs_clim
    
    if region:
        obsfile = '%s%s/%s/%s/%s_%s_%s_%s-%s_%s%s_%s_%s.nc' %(
            indir, eval_var, freq, eval_mask, eval_var, freq, obs,
            s_year1, e_year1, eval_res, target_time, 'STD', region)
    else:
        obsfile = '%s%s/%s/%s/%s_%s_%s_%s-%s_%s%s_%s.nc' %(
            indir, eval_var, freq, eval_mask, eval_var, freq, obs,
            s_year1, e_year1, eval_res, target_time, 'STD')
    fh = nc.Dataset(obsfile, mode = 'r')
    temp_obs_std = fh.variables[eval_var][:]
    fh.close()

    if isinstance(temp_obs_std, np.ma.core.MaskedArray):
        try:
            ma_temp_obs_std = np.ma.array(temp_obs_std, mask = maskmiss)
        except (NameError):
            ma_temp_obs_std = temp_obs_std[:]
    else:
        ma_tmp_std = np.ma.masked_array(temp_obs_std,
                                             np.isnan(temp_obs_std))
        try:
            ma_temp_obs_std = np.ma.array(ma_tmp_std, mask = maskmiss)
        except(NameError):
            ma_temp_obs_std = ma_tmp_std[:]
        del ma_tmp_std
    d_temp_obs_std[obs] = ma_temp_obs_std.filled(np.nan)
    del temp_obs_std
    
    if region:
        obsfile = '%s%s/%s/%s/%s_%s_%s_%s-%s_%s%s_%s_%s.nc' %(
            indir, eval_var, freq, eval_mask, eval_var, freq, obs,
            s_year1, e_year1, eval_res, target_time, eval_file, region)
    else:
        obsfile = '%s%s/%s/%s/%s_%s_%s_%s-%s_%s%s_%s.nc' %(
            indir, eval_var, freq, eval_mask, eval_var, freq,
            obs, s_year1, e_year1, eval_res, target_time, eval_file)
    fh = nc.Dataset(obsfile, mode = 'r')
    temp_obs_var = fh.variables[eval_var][:]
    lat = fh.variables['lat'][:]
    lon = fh.variables['lon'][:]
    fh.close()
    if isinstance(temp_obs_var, np.ma.core.MaskedArray):
        try:
            ma_temp_obs_var = np.ma.array(temp_obs_var, mask = maskmiss)
        except (NameError):
            ma_temp_obs_var = temp_obs_var[:]
    else:
        ma_tmp_var = np.ma.masked_array(temp_obs_var, np.isnan(temp_obs_var))
        try:
            ma_temp_obs_var = np.ma.array(ma_tmp_var, mask = maskmiss)
        except(NameError):
            ma_temp_obs_var = ma_tmp_var[:]
        del ma_tmp_var
    del temp_obs_var
    ## calculate area means
    rad = 4.0 * math.atan(1.0) / 180
    w_lat = np.cos(lat * rad) # weight for latitude differences in area

    tmp_latweight = np.ma.average(ma_temp_obs_ts, axis = 1, weights = w_lat)
    obs_ts_areaavg[obs] = np.nanmean(tmp_latweight.filled(np.nan), axis = 1)

    tmp_latweight = np.ma.average(ma_temp_obs_var[0, :, :], axis = 0,
                                  weights = w_lat)
    obs_var_areaavg[obs] = np.nanmean(tmp_latweight.filled(np.nan), axis = 0)

### read model data
## same models as in rmse file
print "Read model data"
d_temp_mod1 = dict()
d_temp_mod2 = dict()
d_temp_mod = dict()
d_temp_mod_areaavg = dict()
d_target_ts = dict()
nfiles = len(model_names)
print '%s matching files' %(str(nfiles))
for f in xrange(len(model_names)):
    model = model_names[f].split('_', 1)[0]
    ens = model_names[f].split('_', 1)[1]
    if region:
        modfile1 = '%s%s/%s/%s/%s_%s_%s_%s_%s_%s-%s_%s%s_%s_%s.nc' %(
            indir, eval_var, freq, eval_mask, eval_var, freq, model,
            experiment, ens, s_year1, e_year1, eval_res, target_time,
            eval_file, region)
    else:
        modfile1 = '%s%s/%s/%s/%s_%s_%s_%s_%s_%s-%s_%s%s_%s.nc' %(
            indir, eval_var, freq, eval_mask, eval_var, freq, model,
            experiment, ens, s_year1, e_year1, eval_res, target_time, eval_file)
    fh = nc.Dataset(modfile1, mode = 'r')
    temp_mod1 = fh.variables[eval_var][:]
    fh.close()

    if region:
        modfile2 = '%s%s/%s/%s/%s_%s_%s_%s_%s_%s-%s_%s%s_%s_%s.nc' %(
            indir, target_var, freq, target_mask, target_var, freq, model,
            experiment, ens, s_year2, e_year2, target_res, target_time,
            target_file, region)
    else:
        modfile2 = '%s%s/%s/%s/%s_%s_%s_%s_%s_%s-%s_%s%s_%s.nc' %(
            indir, target_var, freq, target_mask, target_var, freq, model,
            experiment, ens, s_year2, e_year2, target_res, target_time,
            target_file)
    fh = nc.Dataset(modfile2, mode = 'r')
    tmp = fh.variables[target_var]
    unit = tmp.units
    temp_mod2 = fh.variables[target_var][:]
    fh.close()
    if isinstance(temp_mod1, np.ma.core.MaskedArray):
        try:
            tmp1 = np.ma.array(temp_mod1, mask = maskmiss)
            d_temp_mod1[model + '_' + ens] = tmp1.filled(np.nan)[:]
        except (NameError):
            d_temp_mod1[model + '_' + ens] = temp_mod1.filled(np.nan)[:]
    else:
        ma_tmp = np.ma.masked_array(temp_mod1, np.isnan(temp_mod1))
        try:
            tmp1 = np.ma.array(ma_tmp, mask = maskmiss)
        except (NameError):
            tmp1 = ma_tmp[:]
        del ma_tmp
        d_temp_mod1[model + '_' + ens] = tmp1.filled(np.nan)[:]

    if isinstance(temp_mod2, np.ma.core.MaskedArray):
        try:
            tmp2 = np.ma.array(temp_mod2, mask = maskmiss)
            d_temp_mod2[model + '_' + ens] = tmp2.filled(np.nan)[:]
        except (NameError):
            d_temp_mod2[model + '_' + ens] = temp_mod2.filled(np.nan)[:]
    else:
        ma_tmp2 = np.ma.masked_array(temp_mod2, np.isnan(temp_mod2))
        try:
            tmp2 = np.ma.array(ma_tmp2, mask = maskmiss)
        except (NameError):
            tmp2 = ma_tmp2
        del ma_tmp2
        d_temp_mod2[model + '_' + ens] = tmp2.filled(np.nan)[:]
    try:
        del tmp1, tmp2
    except (NameError):
        print 'tmp1, tmp2 does not exist'
    if (target_file != 'SCALE'):
        d_temp_mod[model + '_' + ens] = (d_temp_mod2[model + '_' + ens] - 
                                         d_temp_mod1[model + '_' + ens])
    else:
        d_temp_mod[model + '_' + ens] = d_temp_mod2[model + '_' + ens]

    ma_temp_mod = np.ma.masked_array(d_temp_mod[model + '_' + ens], 
                                     np.isnan(d_temp_mod[model + '_' + ens]))
    tmp_latweight = np.ma.average(np.squeeze(ma_temp_mod), axis = 0,
                                  weights = w_lat)
    d_temp_mod_areaavg[model + '_' + ens] = np.nanmean(tmp_latweight.filled(
        np.nan))
    del tmp_latweight, temp_mod1, temp_mod2

    # read data over whole timeseries for plotting
    path_ts = '%s/%s/%s/%s/' %(indir, target_var, freq, target_mask)
    if region:
        filename_ts = '%s%s_%s_%s_%s_%s_%s-%s_%s%s_%s.nc' %(
            path_ts, target_var, freq, model,
            experiment, ens, syear, eyear,
            target_res, target_time, region)
    else:
        filename_ts = '%s/%s_%s_%s_%s_%s_%s-%s_%s%s.nc' %(
            path_ts, target_var, freq, target_mask,
            target_var, freq, model,
            experiment, ens, syear, eyear,
            target_res, target_time)
    fh = nc.Dataset(filename_ts, mode = 'r')
    temp_ts = fh.variables[target_var][:] # global data,time,lat,lon
    time = fh.variables['time']
    cdftime = utime(time.units, calendar = time.calendar)
    dates = cdftime.num2date(time[:])
    years = np.asarray([dates[i].year for i in xrange(len(dates))])
    fh.close()
    if isinstance(temp_ts, np.ma.core.MaskedArray):
        try:
            ma_temp_ts = np.ma.array(temp_ts,
                                     mask = np.tile(maskmiss,
                                                    (temp_ts.shape[0], 1)))
        except (NameError):
            ma_temp_ts = temp_ts
    else:
        ma_tmp = np.ma.masked_array(temp_ts, np.isnan(temp_ts))
        try:
            ma_temp_ts = np.ma.array(ma_tmp,
                                     mask = np.tile(maskmiss,
                                                    (temp_ts.shape[0], 1)))
        except(NameError):
            ma_temp_ts = ma_tmp[:]
        del ma_tmp
    tmp_ts_latweight = np.ma.average(np.squeeze(ma_temp_ts), axis = 1,
                                     weights = w_lat)
    d_target_ts[model + '_' + ens] = np.nanmean(tmp_ts_latweight.filled(np.nan),
                                                axis = 1)

if unit == 'degC':
    degree_sign = u'\N{DEGREE SIGN}'
    target_unit = degree_sign + "C"
else:
    target_unit = unit
###
# Calculate weights
###
print "Calculate weights for model dependence (u:uniqueness) and quality (q)"
wu_end = calc_wu(delta_u, model_names, sigma_S2)
wq_end = calc_wq(delta_q, model_names, sigma_D2)

###
# Calculate weighted multi-model climatologies
###
print "Calculate weighted and non-weighted model means"
approx_wmm = calc_weights_approx(wu_end, wq_end, model_names, d_temp_mod,
                                 var_file = target_file)
approx_wmm_eval = calc_weights_approx(wu_end, wq_end, model_names,
                                      d_temp_mod1, var_file = eval_file)
approx_wmm_ts_areaavg = calc_weights_approx(wu_end, wq_end, model_names,
                                            d_target_ts)

## area average of weighted model mean
ma_approx_wmm = np.ma.masked_array(approx_wmm['approx'],
                                   np.isnan(approx_wmm['approx']))
tmp_latweight = np.ma.average(np.squeeze(ma_approx_wmm), axis = 0,
                              weights = w_lat)
approx_wmm_areaavg = np.nanmean(tmp_latweight.filled(np.nan))

## test if sum of all weights equals one
weights = approx_wmm['weights']
if (type(weights) is dict):
    dims_w = len(weights.values())
    if (dims_w == len(model_names)):
        if (np.sum(weights.values()) != 1.0):
            print "Warning: Sum of all weights does not equal 1 but %s" %(
                str(np.sum(weights.values())))
else:
    dims_w = weights.shape
    if (dims_w[0] == len(model_names)):
        if (np.sum(weights) != 1.0):
            print "Warning: Sum of all weights does not equal 1 but %s" %(
                str(np.sum(weights)))
    else:
        if (np.sum(weights[0, 0, :]) != 1.0):
            print "Warning: Sum of all weights does not equal 1 but %s" %(
                str(np.sum(weights[0, 0, :])))

## calculate spread for timeseries
ts_areaavg = np.array([value for key, value in sorted(
    d_target_ts.iteritems())], dtype = float)
round_weights = np.empty((len(ts_areaavg)))
round_weights.fill(np.NaN)
m = 0
for key, value in sorted(d_target_ts.iteritems()):
    round_weights[m] = int(round(weights[key] * len(model_names) * 1000))
    m = m + 1
ind_end = np.cumsum(round_weights).astype(int)
ind_start = copy.deepcopy(ind_end)
ind_start[0] = 0
ind_start[1:] = copy.deepcopy(ind_end[:len(model_names) - 1] + 1)
tmp = np.empty((len(model_names) * 1000 + 10, ts_areaavg.shape[1]))
tmp.fill(np.NaN)
for m in xrange(len(model_names)):
    if ind_end[m] > 100010:
        ind_end[m] = 100009
    for kk in xrange(ind_start[m], ind_end[m] + 1):
        tmp[kk, :] = ts_areaavg[m, :]
dim = np.count_nonzero(tmp[:, 0])

tmp2 = tmp[:dim, :]
tmp1 = np.empty(tmp2.shape, dtype = float)
tmp1.fill(np.NaN)
for t in xrange(tmp2.shape[1]):
    tmp1[:, t] = sorted(tmp2[:, t])
del tmp2
dim = np.count_nonzero(tmp1[:, 0])

ind_5 = int(math.ceil(0.05 * dim) - 1)
ind_25 = int(math.ceil(0.25 * dim) - 1)
ind_75 = int(math.ceil(0.75 * dim) - 1)
ind_95 = int(math.floor(0.95 * dim) - 1)

lower_ts_wmm = tmp1[ind_5, :]
upper_ts_wmm = tmp1[ind_95, :]
lower25_ts_wmm = tmp1[ind_25, :]
upper75_ts_wmm = tmp1[ind_75, :]
avg_ts_wmm = np.nanmean(tmp1, axis = 0)
avg_ts_wmedmm = np.nanmedian(tmp1, axis = 0)
del tmp
del tmp1

ind_hist1 = np.where(years == s_year1)[0][0]
ind_hist2 = np.where(years == e_year1)[0][0] + 1
ind_fut1 = np.where(years == s_year2)[0][0]
ind_fut2 = np.where(years == e_year2)[0][0] + 1

hist_ts_areaavg = np.mean(ts_areaavg[:, ind_hist1:ind_hist2], axis = 1)
fut_ts_areaavg = np.mean(ts_areaavg[:, ind_fut1:ind_fut2], axis = 1)
delta_ts_areaavg = fut_ts_areaavg - hist_ts_areaavg
tmp = np.empty((len(model_names) * 1000 + 10))
tmp.fill(np.NaN)
for m in xrange(len(model_names)):
    if ind_end[m] > 100010:
        ind_end[m] = 100009
    for kk in xrange(ind_start[m], ind_end[m] + 1):
        tmp[kk] = delta_ts_areaavg[m]
dim = np.count_nonzero(tmp)
tmp2 = tmp[:dim]
tmp1 = sorted(tmp2)
del tmp2
dim = np.count_nonzero(tmp1)

ind_5_delta = int(math.ceil(0.05 * dim) - 1)
ind_25_delta = int(math.ceil(0.25 * dim) - 1)
ind_75_delta = int(math.ceil(0.75 * dim) - 1)
ind_95_delta = int(math.floor(0.95 * dim) - 1)

lower_delta_wmm = tmp1[ind_5_delta]
upper_delta_wmm = tmp1[ind_95_delta]
lower25_delta_wmm = tmp1[ind_25_delta]
upper75_delta_wmm = tmp1[ind_75_delta]
avg_delta_wmm = np.nanmean(tmp1, axis = 0)
avg_delta_wmedmm = np.nanmedian(tmp1, axis = 0)
del tmp
del tmp1

# calculate unweighted mean
# first average over initial conditions ensembles per model
models = [x.split('_')[0] for x in d_temp_mod.keys()]
mult_ens = []
seen = set()
for m in models:
    if m not in seen:
        seen.add(m)
    else:
        mult_ens.append(m)
# make sure every model only once in list
list_with_mult_ens = set(mult_ens)
d_avg_ens = dict()
for key, value in d_temp_mod.iteritems():
    if key.split('_')[0] in list_with_mult_ens:
        #find other ensemble members
        ens_mem = [value2 for key2, value2 in sorted(
            d_temp_mod.iteritems()) if key.split('_')[0] in key2]
        d_avg_ens[key.split('_')[0]] = np.nanmean(ens_mem, axis = 0)
    else:
        d_avg_ens[key.split('_')[0]] = value
        
tmp2 = 0
if (target_file == 'STD'):
    for key, value in d_avg_ens.iteritems():
        tmp_pow = np.power(value, 2)
        tmp2 = tmp2 + tmp_pow
    mm = np.sqrt(tmp2 / len(set(models)))
else: 
    for key, value in d_avg_ens.iteritems():
        tmp2 = tmp2 + value
    mm = tmp2 / len(set(models))
del tmp2
print mm.shape, np.nanmax(mm), np.nanmin(mm)

# multi-model mean ts
# first average over initial conditions ensembles per model
models = [x.split('_')[0] for x in d_target_ts.keys()]
mult_ens = []
seen = set()
for m in models:
    if m not in seen:
        seen.add(m)
    else:
        mult_ens.append(m)
# make sure every model only once in list
list_with_mult_ens = set(mult_ens)
d_avg_ens_ts = dict()
for key, value in d_target_ts.iteritems():
    if key.split('_')[0] in list_with_mult_ens:
        #find other ensemble members
        ens_mem = [value2 for key2, value2 in sorted(
            d_target_ts.iteritems()) if key.split('_')[0] in key2]
        d_avg_ens_ts[key.split('_')[0]] = np.nanmean(ens_mem, axis = 0)
    else:
        d_avg_ens_ts[key.split('_')[0]] = value
tmp3 = 0
for key, value in d_avg_ens_ts.iteritems():
    tmp3 = tmp3 + value

target_ts_avg = tmp3 / len(set(models))
del tmp3

ts_ens_areaavg = np.array([value for key, value in sorted(
    d_avg_ens_ts.iteritems())], dtype = float)
tmp21 = np.empty(ts_ens_areaavg.shape, dtype = float)
tmp21.fill(np.NaN)
for t in xrange(ts_ens_areaavg.shape[1]):
    tmp21[:, t] = sorted(ts_ens_areaavg[:, t])
dim = np.count_nonzero(tmp21[:, 0])
ind_5 = int(math.ceil(0.05 * dim) - 1)
ind_25 = int(math.ceil(0.25 * dim) - 1)
ind_75 = int(math.ceil(0.75 * dim) - 1)
ind_95 = int(math.floor(0.95 * dim) - 1)
lower_ts_mm = tmp21[ind_5, :]
upper_ts_mm = tmp21[ind_95, :]
lower25_ts_mm = tmp21[ind_25, :]
upper75_ts_mm = tmp21[ind_75, :]
del tmp21

# calculate unweighted mean for evaluation period
# first average over initial conditions ensembles per model
models = [x.split('_')[0] for x in d_temp_mod1.keys()]
mult_ens1 = []
seen = set()
for m in models:
    if m not in seen:
        seen.add(m)
    else:
        mult_ens1.append(m)
# make sure every model only once in list
list_with_mult_ens = set(mult_ens1)
d_avg_ens1 = dict()
for key, value in d_temp_mod1.iteritems():
    if key.split('_')[0] in list_with_mult_ens:
        #find other ensemble members
        ens_mem1 = [value2 for key2, value2 in sorted(
            d_temp_mod1.iteritems()) if key.split('_')[0] in key2]
        d_avg_ens1[key.split('_')[0]] = np.nanmean(ens_mem1, axis = 0)
    else:
        d_avg_ens1[key.split('_')[0]] = value

tmp2 = 0
if (target_file == 'STD'):
    for key, value in d_avg_ens1.iteritems():
        tmp_pow = np.power(value, 2)
        tmp2 = tmp2 + tmp_pow
    mm_eval = np.sqrt(tmp2 / len(set(models)))
else: 
    for key, value in d_avg_ens1.iteritems():
        tmp2 = tmp2 + value
    mm_eval = tmp2 / len(set(models))
del tmp2
print mm_eval.shape, np.nanmax(mm_eval), np.nanmin(mm_eval)

# area mean non-weighted
ma_mm = np.ma.masked_array(mm, np.isnan(mm))
if len(mm.shape) == 3:
    tmp_latweight = np.ma.empty((len(mm), len(lon)))
    for ilon in xrange(len(lon)):
        tmp_latweight[:, ilon] = np.ma.average(ma_mm[:, :, ilon],
                                               axis = 1, weights = w_lat)
        mm_areaavg = np.nanmean(tmp_latweight.filled(np.nan), axis = 1)
else:
   tmp_latweight = np.ma.average(ma_mm, axis = 0, weights = w_lat)
   mm_areaavg = np.nanmean(tmp_latweight.filled(np.nan))

## evaluate weighted multi-model mean with error index I^2
d_I2 = dict()
for obs in obsdata:
    d_I2[obs] = error_indexI(np.squeeze(approx_wmm_eval['approx']),
                             np.squeeze(mm_eval),
                             np.squeeze(d_temp_obs_clim[obs]),
                             np.squeeze(d_temp_obs_std[obs]),
                             lat, lon, eval_res)
    print 'I2 = ' + str(round(d_I2[obs], 3))
I2_sum = 0
for key in d_I2:
    I2_sum += d_I2[key]
I2 = I2_sum / len(d_I2.keys())
print 'I2 = ' + str(round(I2, 3)) + ' (average over all obs)'

## calculate RMSE for weighted and unweighted multi-model means
for obs in obsdata:
    rmse_wmm = rmse_3D(approx_wmm_eval['approx'], d_temp_obs_clim[obs], lat, lon)
    rmse_mm = rmse_3D(mm_eval, d_temp_obs_clim[obs], lat, lon)
    diff_rmse = rmse_wmm - rmse_mm
    print 'Diff RMSE WMM-MM = ' + str(round(diff_rmse, 3))

swu_txt = str(np.round(sigma_S2, 3))
swq_txt = str(np.round(sigma_D2, 3))

if len(mm.shape) == 3:
    plot_mm = np.mean(mm, axis = 0)
elif len(mm.shape) == 2:
    plot_mm = mm

if len(approx_wmm['approx'].shape) == 3:
    plot_wmm = np.mean(approx_wmm['approx'], axis = 0)
elif len(approx_wmm['approx'].shape) == 2:
    plot_wmm = approx_wmm['approx']

if len(obsdata) == 1:
    bias_wmm = (np.squeeze(approx_wmm_eval['approx']) - 
                np.squeeze(d_temp_obs_clim[obs]))

    bias_mm = np.squeeze(mm_eval) - np.squeeze(d_temp_obs_clim[obs])

## save data for further plotting and panelling
ncdir = '%s/ncdf/' %outdir
if (os.access('%s' %ncdir, os.F_OK) == False):
    os.makedirs('%s' %ncdir)
    print 'created directory %s' %ncdir

outfile = '%splot_delta_wmm_%s%s_%s_latlon_%s%s%s%s_%sobs_%s_%s_%s_%s.nc' %(
    ncdir, target_var, target_file, target_res, len(diag_var), diag_var[0],
    var_file[0], res_name[0], len(obsdata), area, err, swu_txt, swq_txt)
func_write_netcdf(outfile, plot_wmm, target_var, lon, lat, var_units = unit,
                  Description = '%s %s %s weighted multi-model mean' %(
                      target_var, target_file, target_res),
                  comment = '%s' %(I2))
outfile = '%splot_delta_mm_%s%s_%s_latlon_%s%s%s%s_%sobs_%s_%s_%s_%s.nc' %(
    ncdir, target_var, target_file, target_res, len(diag_var), diag_var[0],
    var_file[0], res_name[0], len(obsdata), area, err, swu_txt, swq_txt)
func_write_netcdf(outfile, plot_mm, target_var, lon, lat, var_units = unit,
                  Description = '%s %s %s equal multi-model mean' %(
                      target_var, target_file, target_res))

if len(obsdata) == 1:
    outfile = '%splot_bias_wmm_%s%s_%s_latlon_%s%s%s%s_%sobs_%s_%s_%s_%s.nc' %(
        ncdir, eval_var, eval_file, eval_res, len(diag_var), diag_var[0],
        var_file[0], res_name[0], len(obsdata), area, err, swu_txt, swq_txt)
    func_write_netcdf(outfile, bias_wmm, eval_var, lon, lat, var_units = unit,
                      Description = '%s %s %s weighted multi-model mean bias' %(
                          eval_var, eval_file, eval_res),
                      comment = '%s' %(I2))
    outfile = '%splot_bias_mm_%s%s_%s_latlon_%s%s%s%s_%sobs_%s_%s_%s_%s.nc' %(
        ncdir, eval_var, eval_var, eval_res, len(diag_var), diag_var[0],
        var_file[0], res_name[0], len(obsdata), area, err, swu_txt, swq_txt)
    func_write_netcdf(outfile, bias_mm, eval_var, lon, lat, var_units = unit,
                      Description = '%s %s %s equal multi-model mean bias' %(
                          eval_var, eval_var, eval_res))

# save timeseries info
outfile = '%s%s%s_%s_%sdiags_%sobs_%s_%s_swu%s_swq%s_wmm_ts.nc' %(
    ncdir, target_var, target_file, target_res, len(diag_var),
    len(obsdata), area, err, swu_txt, swq_txt)
nyears = eyear - syear + 1
datesout = [dt.datetime(syear + x, 06, 01, 00) for x in range(0, nyears)]
time = nc.date2num(datesout, units = 'days since %s' %(
    nc.datetime.strftime(dt.datetime(syear, 01, 01, 00), '%Y-%m-%d %H:%M:%S')),
                   calendar = 'standard')

nyears_obs = eyear_eval[0] - syear_eval[0] + 1
datesout_obs = [dt.datetime(syear_eval[0] + x, 06, 01, 00) for x in range(
    0, nyears_obs)]
obstime = nc.date2num(datesout_obs, units = 'days since %s' %(
    nc.datetime.strftime(dt.datetime(syear_eval[0], 01, 01, 00),
                         '%Y-%m-%d %H:%M:%S')), calendar = 'standard')

fout = nc.Dataset(outfile, mode = 'w')
fout.createDimension('obstime', len(obsyears))
obstimeout = fout.createVariable('obstime', 'f8', ('obstime'),
                                 fill_value = 1e20)
setattr(obstimeout, 'units',
        'days since %s' %(nc.datetime.strftime(dt.datetime(syear_eval[0], 01,
                                                           01, 00),
                                               '%Y-%m-%d %H:%M:%S')))
obsout = dict()
for obs in obsdata:
    obsout[obs] = fout.createVariable('%s_ts_areaavg' %obs, 'f8', ('obstime'),
                                      fill_value = 1e20)
    setattr(obsout[obs],'Longname','area-averaged %s timeseries' %obs)
    setattr(obsout[obs],'units', unit)
    setattr(obsout[obs],'description','')

fout.createDimension('time', len(years))
timeout = fout.createVariable('time', 'f8', ('time'), fill_value = 1e20)
setattr(timeout, 'units',
        'days since %s' %(nc.datetime.strftime(dt.datetime(syear, 01, 01, 00),
                                               '%Y-%m-%d %H:%M:%S')))
mmout = fout.createVariable('mm_ts_areaavg', 'f8', ('time'), fill_value = 1e20)
setattr(mmout, 'Longname', 'area-averaged multi-model mean timeseries')
setattr(mmout, 'units', unit)
setattr(mmout, 'description', '')

lowmmout = fout.createVariable('lower_ts_mm', 'f8', ('time'), fill_value = 1e20)
setattr(lowmmout, 'Longname', '5th percentile of mm')
setattr(lowmmout, 'units', unit)
setattr(lowmmout, 'description',
        'lower 5th percentile of multi-model mean timeseries')

low25mmout = fout.createVariable('lower25_ts_mm', 'f8', ('time'),
                                 fill_value = 1e20)
setattr(low25mmout, 'Longname', '25th percentile of mm')
setattr(low25mmout, 'units', unit)
setattr(low25mmout, 'description',
        'lower 25th percentile of multi-model mean timeseries')

up75mmout = fout.createVariable('upper75_ts_mm', 'f8', ('time'),
                                fill_value = 1e20)
setattr(up75mmout, 'Longname','75th percentile of mm')
setattr(up75mmout, 'units',  unit)
setattr(up75mmout, 'description',
        'upper 75th percentile of multi-model mean timeseries')

upmmout = fout.createVariable('upper_ts_mm', 'f8', ('time'), fill_value = 1e20)
setattr(upmmout, 'Longname','95th percentile of mm')
setattr(upmmout, 'units',  unit)
setattr(upmmout, 'description',
        'upper 95th percentile of multi-model mean timeseries')

wmmout = fout.createVariable('wmm_ts_areaavg', 'f8', ('time'), fill_value =
                             1e20)
setattr(wmmout, 'Longname',
        'area-averaged weighted multi-model mean timeseries')
setattr(wmmout, 'units', unit)
setattr(wmmout, 'description', '')

lowwmmout = fout.createVariable('lower_ts_wmm', 'f8', ('time'),
                                fill_value = 1e20)
setattr(lowwmmout, 'Longname', '5th percentile of wmm')
setattr(lowwmmout, 'units', unit)
setattr(lowwmmout, 'description',
        'lower 5th percentile of weighted multi-model mean timeseries')

low25wmmout = fout.createVariable('lower25_ts_wmm', 'f8', ('time'),
                                fill_value = 1e20)
setattr(low25wmmout, 'Longname', '25th percentile of wmm')
setattr(low25wmmout, 'units', unit)
setattr(low25wmmout, 'description',
        'lower 25th percentile of weighted multi-model mean timeseries')

up75wmmout = fout.createVariable('upper75_ts_wmm', 'f8', ('time'),
                               fill_value = 1e20)
setattr(up75wmmout, 'Longname', '75th percentile of wmm')
setattr(up75wmmout, 'units', unit)
setattr(up75wmmout, 'description',
        'upper 75th percentile of weighted multi-model mean timeseries')

upwmmout = fout.createVariable('upper_ts_wmm', 'f8', ('time'),
                               fill_value = 1e20)
setattr(upwmmout, 'Longname', '95th percentile of wmm')
setattr(upwmmout, 'units', unit)
setattr(upwmmout, 'description',
        'upper 95th percentile of weighted multi-model mean timeseries')

deltawmmout = fout.createVariable('delta_wmm_areaavg', 'f8',
                                  fill_value = 1e20)
setattr(deltawmmout, 'Longname',
        'weighted multi-model mean delta')
setattr(deltawmmout, 'units', unit)
setattr(deltawmmout, 'description', 'weighted multi-model change')

deltawmedmout = fout.createVariable('delta_wmedm_areaavg', 'f8',
                                    fill_value = 1e20)
setattr(deltawmedmout, 'Longname', 'weighted multi-model median delta')
setattr(deltawmedmout, 'units', unit)
setattr(deltawmedmout, 'description', 'weighted multi-model median change')

deltalowwmmout = fout.createVariable('delta_lower5_wmm', 'f8',
                                     fill_value = 1e20)
setattr(deltalowwmmout, 'Longname', '5th percentile of delta wmm')
setattr(deltalowwmmout, 'units', unit)
setattr(deltalowwmmout, 'description',
        'lower 5th percentile of weighted multi-model mean change')

deltalow25wmmout = fout.createVariable('delta_lower25_wmm', 'f8',
                                       fill_value = 1e20)
setattr(deltalow25wmmout, 'Longname', '25th percentile of delta wmm')
setattr(deltalow25wmmout, 'units', unit)
setattr(deltalow25wmmout, 'description',
        'lower 25th percentile of weighted multi-model mean change')

deltaup75wmmout = fout.createVariable('delta_upper75_wmm', 'f8',
                                      fill_value = 1e20)
setattr(deltaup75wmmout, 'Longname', '75th percentile of delta wmm')
setattr(deltaup75wmmout, 'units', unit)
setattr(deltaup75wmmout, 'description',
        'upper 75th percentile of weighted multi-model mean change')

deltaupwmmout = fout.createVariable('delta_upper95_wmm', 'f8',
                                    fill_value = 1e20)
setattr(deltaupwmmout, 'Longname', '95th percentile of delta wmm')
setattr(deltaupwmmout, 'units', unit)
setattr(deltaupwmmout, 'description',
        'upper 95th percentile of weighted multi-model mean change')

obstimeout[:] = obstime[:]
for obs in obsdata:
    obsout[obs][:] = obs_ts_areaavg[obs][:]
timeout[:] = time[:]
mmout[:] = target_ts_avg[:]
lowmmout[:] = lower_ts_mm[:]
upmmout[:] = upper_ts_mm[:]
low25mmout[:] = lower25_ts_mm[:]
up75mmout[:] = upper75_ts_mm[:]

wmmout[:] = approx_wmm_ts_areaavg['approx'][:]
lowwmmout[:] = lower_ts_wmm[:]
upwmmout[:] = upper_ts_wmm[:]
low25wmmout[:] = lower25_ts_wmm[:]
up75wmmout[:] = upper75_ts_wmm[:]

deltawmmout[:] = avg_delta_wmm
deltawmedmout[:] = avg_delta_wmedmm
deltalowwmmout[:] = lower_delta_wmm
deltaupwmmout[:] = upper_delta_wmm
deltalow25wmmout[:] = lower25_delta_wmm
deltaup75wmmout[:] = upper75_delta_wmm

# Set global attributes
setattr(fout, "author", "Ruth Lorenz @IAC, ETH Zurich, Switzerland")
setattr(fout, "contact", "ruth.lorenz@env.ethz.ch")
setattr(fout, "creation date", dt.datetime.today().strftime('%Y-%m-%d'))
setattr(fout, "Script", "weight_mm_beyond_democracy_multObs_delta_flex_cdo.py")
setattr(fout, "Input files located in:", indir)
fout.close()

###
# Plotting
###
print "Plot data"
#plot maps
try:
    draw(plot_wmm, lat, lon,
         title = "%s [%s] weighted multi-model mean %s-%s minus %s-%s" %(
             target_var, unit, s_year2, e_year2, s_year1, e_year2),
         region = region, levels = lev)
except NameError:
    draw(plot_wmm, lat, lon,
         title = "%s [%s] weighted multi-model mean %s-%s minus %s-%s" %(
             target_var, unit, s_year2, e_year2, s_year1, e_year2),
         region = region)
plt.savefig('%s%s_%s_%s_%s%s%s%s_%sobs_%s_%s_swu%s_swq%s_delta_wmm_map.pdf' %(
    outdir, target_var, target_file, target_res, len(diag_var), diag_var[0],
    var_file[0], res_name[0],
    len(obsdata), area, err, swu_txt, swq_txt))

draw(plot_mm, lat, lon,
     title = "%s [%s] multi-model mean %s-%s minus %s-%s" %(
	target_var, unit, s_year2, e_year2, s_year1, e_year2),
     region = region, levels = lev)
plt.savefig('%s%s_%s_%s_%s%s%s%s_%sobs_%s_%s_swu%s_swq%s_delta_mm_map.pdf' %(
    outdir, target_var, target_file, target_res, len(diag_var), diag_var[0],
    var_file[0], res_name[0], len(obsdata),
    area, err, swu_txt, swq_txt))

diff = plot_wmm - plot_mm
try:
    draw(diff, lat, lon,
         title = "Delta %s [%s] weighted - non-weighted, I$^2$ = %s" %(
             target_var, unit, str(round(I2, 2))), levels = lev_d,
         colors = "RdBu_r", region = region)
except NameError:
    min_diff = np.nanmin(diff)
    max_diff = np.nanmax(diff)
    end = np.mean([abs(round(max_diff)), abs(round(min_diff))])
    delta = (end * 2) / 9.0
    if (end == end + delta):
        lev_d = [ - 0.09, - 0.07, - 0.05, - 0.03, - 0.01,
                  0.01, 0.03, 0.05, 0.07, 0.09]
    else:
        lev_d = np.arange( - end, end + delta, delta)       
    draw(diff, lat, lon,
         title = "Delta %s [%s] weighted - non-weighted, I$^2$ = %s" %(
             target_var, unit, str(round(I2, 2))), levels = lev_d,
         colors = "RdBu_r", region = region)
plt.savefig('%sdiff_delta_%s_%s_%s_%s%s%s%s_%sobs_%s_%s_swu%s_swq%s_wmm-mm_map.pdf' %(
    outdir, target_var, target_file, target_res, len(diag_var), diag_var[0],
    var_file[0], res_name[0], len(obsdata),
    area, err, swu_txt, swq_txt))

#plot bias over evaluation period
if len(obsdata) == 1:
    lev_b = [-2.25, -1.75, -1.25, -0.75, -0.25, 0.25, 0.75, 1.25, 1.75, 2.25]

    draw(bias_wmm, lat, lon, title = "Bias %s [%s] weighted - %s, I$^2$ = %s" %
             (eval_var, unit, obsname[0], str(round(I2, 2))), levels = lev_b,
             colors = "RdBu_r", region = region)
    plt.savefig('%sbias_%s_%s_%s_%s%s%s%s_%s_%s_swu%s_swq%s_wmm-%s_%s--%s_map.pdf' %(
        outdir, eval_var, eval_file, eval_res, len(diag_var), diag_var[0],
        var_file[0], res_name[0], area, err, 
        swu_txt, swq_txt, obsdata[0], s_year1, e_year2))

    draw(bias_mm, lat, lon,
         title = "Bias %s [%s] non-weighted - %s, I$^2$ = %s" %
             (eval_var, unit, obsname[0], str(round(I2, 2))), levels = lev_b,
             colors = "RdBu_r", region = region)
    plt.savefig('%sbias_%s_%s_%s_%s%s%s%s_%s_%s_swu%s_swq%s_mm-%s_%s--%s_map.pdf' %(
        outdir, eval_var, eval_file, eval_res, len(diag_var), diag_var[0],
        var_file[0], res_name[0], area, err, 
        swu_txt, swq_txt, obsdata[0], s_year1, e_year2))

# plot time series
fig = plt.figure(figsize = (10, 5), dpi = 300)
plt.fill_between(years, lower_ts_mm, upper_ts_mm, facecolor = 'grey',
                 alpha = 0.3)
plt.plot(years, target_ts_avg, color = 'black', linewidth = 3,
         label = 'MMM (baseline IPCC)')
plt.fill_between(years, lower_ts_wmm, upper_ts_wmm, facecolor = 'crimson',
                 edgecolor = 'crimson', alpha = 0.3)
plt.plot(years, approx_wmm_ts_areaavg['approx'], color = 'crimson',
         linewidth = 3, label = 'weighted MMM')
o = 0
for obs in obsdata:
    plt.plot(obsyears, obs_ts_areaavg[obs], color = 'blue', linewidth = 3,
             label = obsname[o])
    o = o + 1
plt.xlabel('Year')
plt.ylabel('%s %s %s [%s]' %(region, target_var, target_res, target_unit))
plt.grid(False)

leg = plt.legend(loc = 'upper left')  ## leg defines legend -> can be modified
leg.draw_frame(False)
plt.savefig('%stimeseries_%s_%s_%s_%s%s%s%s_%sobs_%s_%s_swu%s_swq%s_wmm_mm.pdf' %(
    outdir, target_var, target_file, target_res, len(diag_var), diag_var[0],
    var_file[0], res_name[0], len(obsdata),
    area, err, swu_txt, swq_txt))

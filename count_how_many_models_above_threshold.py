#!/usr/bin/python
'''
File Name : count_how_many_models_above_threshold.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 14-12-2017
Modified: Thu 14 Dec 2017 08:48:37 AM CET
Purpose: count how many models reach certain threshold at each grid point
         by certain time period in CMIP5 runs

'''
import netCDF4 as nc # to work with NetCDF files
import numpy as np
from netcdftime import utime
import datetime as dt

import os # operating system interface
from os.path import expanduser
home = expanduser("~") # Get users home directory
import glob
import sys
sys.path.insert(0, home + '/scripts/plot_scripts/utils/')

###
# Define input
###
archive = '/net/atmos/data/cmip5-ng'
pathtrop = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/'

variable = 'tas'
experiment = ['rcp85'] #['rcp26', 'rcp45', 'rcp60', 'rcp85']
thresh = [1.5, 2, 3, 4]

syear_ref = 1871
eyear_ref = 1900
syear = 2036
eyear = 2065

grid = 'g025'

outdir = '%s%s/how_many_models_above/' %(pathtrop, variable)

if (os.access(outdir, os.F_OK) == False):
    os.makedirs(outdir)

infile = variable + '_ann'

model_names = []
model_number = 0
d_temp_change = dict()
for exp in experiment:    
    name = '%s/%s/%s_*_%s_*_%s.nc' %(archive, variable, infile, exp, grid)
    nfiles = len(glob.glob(name))
    print str(nfiles) + ' matching files found'
    for filename in glob.glob(name):
        #print "Processing " + filename
        fh = nc.Dataset(filename, mode = 'r')
        model = fh.source_model
        if model == 'ACCESS1.3':
            model = 'ACCESS1-3'
        elif model == 'FGOALS_g2':
            model = 'FGOALS-g2'
        if model not in model_names:
            model_names.append(model)
        else:
            #print '%s already used, not using multiple ensemble members' %model
            fh.close()
            continue

        temp = fh.variables[variable][:]
        time = fh.variables['year']
        cdftime = utime(time.units, calendar = time.calendar)
        dates = cdftime.num2date(time[:])
        years = np.asarray([dates[i].year for i in xrange(len(dates))])

        lat = fh.variables['lat'][:]
        lon = fh.variables['lon'][:]
        fh.close()

        # average over time
        ind_start_ref = np.where(years == syear_ref)[0][0]
        ind_end_ref = np.where(years == eyear_ref)[0][0]
        temp_avg_ref = np.mean(temp[ind_start_ref : ind_end_ref, :, :],
                               axis = 0)

        ind_startf = np.where(years == syear)[0][0]
        ind_endf = np.where(years == eyear)[0][0] + 1
        temp_avg_f = np.mean(temp[ind_startf : ind_endf, :, :], axis = 0)

        d_temp_change[model] = temp_avg_f - temp_avg_ref
        model_number += 1
    
    print model_number
    # TO DO check significance of changes??
    
    d_change_signif = d_temp_change

    def check_thres(data, threshold):
        nr_mod = sum(i > threshold for i in data)
        return nr_mod

    # convert dict to array 
    temp_change = np.array([value for key, value in d_change_signif.iteritems()],
                           dtype = float)
    models_above = np.empty((len(thresh), temp_change.shape[1],
                             temp_change.shape[2]), dtype = float)
    models_above.fill(np.NaN)
    for t in xrange(len(thresh)):
        models_above[t, :, :] = check_thres(temp_change, thresh[t])
        models_above_perc = (models_above[t, :, :] / model_number) * 100
        tmp = np.where(models_above_perc > 50., 1, 0)
        binary = np.where(models_above_perc > 90., 2, tmp)    
        del tmp
    
        outfile = '%s%s_thresh_%s_how_many_models_%s_%s.nc' %(
            outdir, variable, thresh[t], syear, exp)

        fout = nc.Dataset(outfile, mode = 'w')
        fout.createDimension('lat', lat.shape[0])
        fout.createDimension('lon', lon.shape[0])

        latout = fout.createVariable('lat', 'f8', ('lat'), fill_value = 1e20)
        setattr(latout, 'Longname', 'Latitude')
        setattr(latout, 'units', 'degrees_north')

        lonout = fout.createVariable('lon', 'f8', ('lon'), fill_value = 1e20)
        setattr(lonout, 'Longname', 'Longitude')
        setattr(lonout, 'units', 'degrees_east')

        nrthresout = fout.createVariable('nr_modles_above', 'f8', ('lat','lon'),
                                         fill_value = 1e20)
        setattr(nrthresout, 'Longname', 'number of models above threshold')
        
        percout = fout.createVariable('nr_thres', 'f8', ('lat','lon'),
                                      fill_value = 1e20)
        setattr(percout, 'Longname', 'number of models where change above threshold %s' %(str(thresh[t])))
    
        binaryout = fout.createVariable('binary', 'f8', ('lat','lon'),
                                        fill_value = 1e20)
        setattr(binaryout, 'Longname', 'Binary info where change above threshold %s for 50 (1) and 90 (2) of models' %(thresh[t]))
    
        latout[:] = lat[:]
        lonout[:] = lon[:]
        nrthresout[:] = np.squeeze(models_above[t, :, :])
        percout[:] = models_above_perc[:]
        binaryout[:] = binary[:]
    
        # Set global attributes
        setattr(fout, "author", "Ruth Lorenz @IAC, ETH Zurich, Switzerland")
        setattr(fout, "contact", "ruth.lorenz@env.ethz.ch")
        setattr(fout, "creation date", dt.datetime.today().strftime('%Y-%m-%d'))

        setattr(fout, "Script", "count_how_many_models_above_threshold.py")
        setattr(fout, "Input files located in:", archive)
        fout.close()

        del models_above_perc, binary
    del models_above
    model_number = 0
    model_names = []
    d_temp_change = dict()

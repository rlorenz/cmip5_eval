#!/usr/bin/python
'''
File Name : plot_panel_eval_weight_maps_NAM.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 26-04-2017
Modified: Wed 26 Apr 2017 11:25:44 AM CEST
Purpose: plot panel with maps of difference between weighted and unweighted
         model means over NorthAmerica region


'''
# Load modules for this script

import numpy as np
import netCDF4 as nc
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.colors as mc
import matplotlib.gridspec as gridspec
from mpl_toolkits.basemap import Basemap, addcyclic
from pylab import *
from subprocess import call
import os # operating system interface

###
# Define input
###
variable = 'tasmax'
diag = 'CLIM'
region = 'NAM'
obsdata = ['MERRA2', 'ERAint', 'Obs']
diagnum = ['1', '2', '3', '4', '5'] #, '2', '3', '4'

path = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/Eval_Weight'
indir = '%s/%s/%s/ncdf' %(path, variable, region)
outdir = '%s/%s/%s/plots' %(path, variable, region)

if (os.access('%s' %outdir, os.F_OK) == False):
    os.makedirs('%s' %outdir)
    print 'created directory %s' %outdir

ncol = len(obsdata)
nrow = len(diagnum)
plottype= ".eps"

###
# read data
###
data = {}
IError = {}
# loop over diagnostics number
for d in range(0, nrow):
    dnum = str(d + 1)
    for o in range(0, ncol):
        path_mm = '%s/plot_mm_%s%s_JJA_latlon_%s_%s_%s_RMSE_0.8_0.6.nc' %(indir,
                                                                          variable,
                                                                          diag,
                                                                          dnum,
                                                                          obsdata[o],
                                                                          region)
        print path_mm
        ifile = nc.Dataset(path_mm)
        lats = ifile.variables['lat'][:]
        lons = ifile.variables['lon'][:]
        mm = ifile.variables[variable][:]

        path_wmm = '%s/plot_wmm_%s%s_JJA_latlon_%s_%s_%s_RMSE_0.8_0.6.nc' %(indir,
                                                                            variable,
                                                                            diag,
                                                                            dnum,
                                                                            obsdata[o],
                                                                            region)
        print path_wmm
        ifile = nc.Dataset(path_wmm)
        lats = ifile.variables['lat'][:]
        lons = ifile.variables['lon'][:]
        wmm = ifile.variables[variable][:]
        comment = str(round(float(ifile.comment), 2))

        diff = wmm - mm
        data[obsdata[o] +'_'+ dnum] = diff
        IError[obsdata[o] +'_'+ dnum] = comment
del diff

###
# plotting part using gridspec
###
plt.close('all')
if (len(diagnum) == 4):
    diff = [data['%s_1' %(obsdata[0])], data['%s_1' %(obsdata[1])],
            data['%s_1' %(obsdata[2])],
            data['%s_2' %(obsdata[0])], data['%s_2' %(obsdata[1])],
            data['%s_2' %(obsdata[2])],
            data['%s_3' %(obsdata[0])], data['%s_3' %(obsdata[1])],
            data['%s_3' %(obsdata[2])],
            data['%s_4' %(obsdata[0])], data['%s_4' %(obsdata[1])],
            data['%s_4' %(obsdata[2])]]
    titles = ['(a) I$^2$ = %s' %(IError['%s_1' %(obsdata[0])]),
              '(b) I$^2$ = %s' %(IError['%s_1' %(obsdata[1])]),
              '(c) I$^2$ = %s' %(IError['%s_1' %(obsdata[2])]),
              '(d) I$^2$ = %s' %(IError['%s_2' %(obsdata[0])]),
              '(e) I$^2$ = %s' %(IError['%s_2' %(obsdata[1])]),
              '(f) I$^2$ = %s' %(IError['%s_2' %(obsdata[2])]),
              '(g) I$^2$ = %s' %(IError['%s_3' %(obsdata[0])]),
              '(h) I$^2$ = %s' %(IError['%s_3' %(obsdata[1])]),
              '(i) I$^2$ = %s' %(IError['%s_3' %(obsdata[2])]),
              '(j) I$^2$ = %s' %(IError['%s_4' %(obsdata[0])]),
              '(k) I$^2$ = %s' %(IError['%s_4' %(obsdata[1])]),
              '(l) I$^2$ = %s' %(IError['%s_4' %(obsdata[2])])]
    figheight = 10
    heightratios = [0.1, 1, 1, 1, 1, 0.1]
elif (len(diagnum) == 5):
    diff = [data['%s_1' %(obsdata[0])], data['%s_1' %(obsdata[1])],
            data['%s_1' %(obsdata[2])],
            data['%s_2' %(obsdata[0])], data['%s_2' %(obsdata[1])],
            data['%s_2' %(obsdata[2])],
            data['%s_3' %(obsdata[0])], data['%s_3' %(obsdata[1])],
            data['%s_3' %(obsdata[2])],
            data['%s_4' %(obsdata[0])], data['%s_4' %(obsdata[1])],
            data['%s_4' %(obsdata[2])],
            data['%s_5' %(obsdata[0])], data['%s_5' %(obsdata[1])],
            data['%s_5' %(obsdata[2])]]
    titles = ['(a) I$^2$ = %s' %(IError['%s_1' %(obsdata[0])]),
              '(b) I$^2$ = %s' %(IError['%s_1' %(obsdata[1])]),
              '(c) I$^2$ = %s' %(IError['%s_1' %(obsdata[2])]),
              '(d) I$^2$ = %s' %(IError['%s_2' %(obsdata[0])]),
              '(e) I$^2$ = %s' %(IError['%s_2' %(obsdata[1])]),
              '(f) I$^2$ = %s' %(IError['%s_2' %(obsdata[2])]),
              '(g) I$^2$ = %s' %(IError['%s_3' %(obsdata[0])]),
              '(h) I$^2$ = %s' %(IError['%s_3' %(obsdata[1])]),
              '(i) I$^2$ = %s' %(IError['%s_3' %(obsdata[2])]),
              '(j) I$^2$ = %s' %(IError['%s_4' %(obsdata[0])]),
              '(k) I$^2$ = %s' %(IError['%s_4' %(obsdata[1])]),
              '(l) I$^2$ = %s' %(IError['%s_4' %(obsdata[2])]),
              '(m) I$^2$ = %s' %(IError['%s_5' %(obsdata[0])]),
              '(n) I$^2$ = %s' %(IError['%s_5' %(obsdata[1])]),
              '(o) I$^2$ = %s' %(IError['%s_5' %(obsdata[2])])]
    figheight = 12.5
    heightratios = [0.1, 1, 1, 1, 1, 1, 0.1]
elif (len(diagnum) == 1):
    diff = [data['%s_1' %(obsdata[0])], data['%s_1' %(obsdata[1])],
            data['%s_1' %(obsdata[2])]]
    titles = ['(a) I$^2$ = %s' %(IError['%s_1' %(obsdata[0])]),
              '(b) I$^2$ = %s' %(IError['%s_1' %(obsdata[1])]),
              '(c) I$^2$ = %s' %(IError['%s_1' %(obsdata[2])])]
    heightratios = [0.1, 1, 0.1]
    figheight = 4
elif (len(diagnum) == 2):
    diff = [data['%s_1' %(obsdata[0])], data['%s_1' %(obsdata[1])],
            data['%s_1' %(obsdata[2])],
            data['%s_2' %(obsdata[0])], data['%s_2' %(obsdata[1])],
            data['%s_2' %(obsdata[2])]]
    titles = ['(a) I$^2$ = %s' %(IError['%s_1' %(obsdata[0])]),
              '(b) I$^2$ = %s' %(IError['%s_1' %(obsdata[1])]),
              '(c) I$^2$ = %s' %(IError['%s_1' %(obsdata[2])]),
              '(d) I$^2$ = %s' %(IError['%s_2' %(obsdata[0])]),
              '(e) I$^2$ = %s' %(IError['%s_2' %(obsdata[1])]),
              '(f) I$^2$ = %s' %(IError['%s_2' %(obsdata[2])])]
    heightratios = [0.1, 1, 1, 0.1]
    figheight = 5.5
elif (len(diagnum) == 3):
    diff = [data['%s_1' %(obsdata[0])], data['%s_1' %(obsdata[1])],
            data['%s_1' %(obsdata[2])],
            data['%s_2' %(obsdata[0])], data['%s_2' %(obsdata[1])],
            data['%s_2' %(obsdata[2])],
            data['%s_3' %(obsdata[0])], data['%s_3' %(obsdata[1])],
            data['%s_3' %(obsdata[2])]]
    titles = ['(a) I$^2$ = %s' %(IError['%s_1' %(obsdata[0])]),
              '(b) I$^2$ = %s' %(IError['%s_1' %(obsdata[1])]),
              '(c) I$^2$ = %s' %(IError['%s_1' %(obsdata[2])]),
              '(d) I$^2$ = %s' %(IError['%s_2' %(obsdata[0])]),
              '(e) I$^2$ = %s' %(IError['%s_2' %(obsdata[1])]),
              '(f) I$^2$ = %s' %(IError['%s_2' %(obsdata[2])]),
              '(g) I$^2$ = %s' %(IError['%s_3' %(obsdata[0])]),
              '(h) I$^2$ = %s' %(IError['%s_3' %(obsdata[1])]),
              '(i) I$^2$ = %s' %(IError['%s_3' %(obsdata[2])])]
    heightratios = [0.1, 1, 1, 1, 0.1]
    figheight = 7.5
max_lat = np.amax(lats)
min_lat = np.amin(lats)
max_lon = np.amax(lons)
min_lon = np.amin(lons)

colbar='RdBu_r'
levels = [-0.55, -0.45, - 0.35, -0.25, -0.15, -0.05, 0.05, 0.15, 0.25, 0.35, 0.45, 0.55]

fig = plt.figure(figsize = (12, figheight))
plotname = '%s/panel_maps_diff_weighted_%s_%s_%s_%sby%s' %(outdir, variable,
                                                           diag, region, nrow,
                                                           ncol)
gs = gridspec.GridSpec(nrow + 2, ncol + 1,
                       height_ratios = heightratios,
                       width_ratios = [0.2, 1, 1, 1], hspace = 0.1,
                       wspace = 0.1, top = 0.95, right = 0.95, left = 0.05,
                       bottom = 0.05)
for obs in range(0, len(obsdata)):
    ax = plt.subplot(gs[0, obs + 1], frameon = False)
    ax.axes.get_yaxis().set_visible(False)
    ax.axes.get_xaxis().set_visible(False)
    textobs = obsdata[obs]
    ax.text(0., 0, textobs, size = 12)

ax = plt.subplot(gs[1, 0], frameon = False)
ax.axes.get_yaxis().set_visible(False)
ax.axes.get_xaxis().set_visible(False)
text1 = '%s%s' %(variable, diag)
ax.text(0., 0.75, text1, size = 12, rotation = 90, transform=ax.transAxes)

if (len(diagnum) > 1):
    ax = plt.subplot(gs[2, 0], frameon = False)
    ax.axes.get_yaxis().set_visible(False)
    ax.axes.get_xaxis().set_visible(False)
    if (variable == 'tasmax' and diag == 'CLIM'):
        text1 = 'tasmaxCLIM,\n rsdsTREND'
    elif (variable == 'tasmax' and diag == 'STD'):
        text1 = '%s%s,\n prTREND' %(variable, diag)
    ax.text(0., .7, text1, size = 12, rotation = 90, transform=ax.transAxes)
if (len(diagnum) > 2):
    ax = plt.subplot(gs[3, 0], frameon = False)
    ax.axes.get_yaxis().set_visible(False)
    ax.axes.get_xaxis().set_visible(False)
    if (variable == 'tasmax' and diag == 'CLIM'):
        text1 = 'tasmaxCLIM, rsdsTREND,\n tasmaxTREND'
    elif (variable == 'tasmax' and diag == 'STD'):
        text1 = '%s%s,\n prTREND, prSTD' %(variable, diag)
    ax.text(0., 0.85, text1, size = 12, rotation = 90, transform=ax.transAxes)
if (len(diagnum) > 3):
    ax = plt.subplot(gs[4, 0], frameon = False)
    ax.axes.get_yaxis().set_visible(False)
    ax.axes.get_xaxis().set_visible(False)
    if (variable == 'tasmax') and (diag == 'CLIM'):
        text1 = 'tasmaxCLIM, rsdsTREND,\n tasmaxTREND, prCLIM'
    elif (variable == 'tasmax') and (diag == 'STD'):
        text1 = '%s%s,\n prTREND, ,' %(variable, diag)
    ax.text(0., 0.9, text1, size = 12, rotation = 90, transform=ax.transAxes)
if (len(diagnum) > 4):
    ax = plt.subplot(gs[5, 0], frameon = False)
    ax.axes.get_yaxis().set_visible(False)
    ax.axes.get_xaxis().set_visible(False)
    if (variable == 'tasmax') and (diag == 'CLIM'):
        text1 = 'tasmaxCLIM, rsdsTREND,\n tasmaxTREND, prCLIM,\n rsdsCLIM'
    elif (variable == 'tasmax') and (diag == 'STD'):
        text1 = '%s%s,\n prTREND, ,' %(variable, diag)
    ax.text(-0.15, 0.8, text1, size = 12, rotation = 90, transform=ax.transAxes)

pltno = 0
for row in range(1, nrow + 1):
    for col in range(1, ncol + 1):
        #print pltno
        ax = plt.subplot(gs[row, col])

        m = Basemap(projection='cyl', llcrnrlat = min_lat, urcrnrlat = max_lat,
                    llcrnrlon = min_lon, urcrnrlon = max_lon, resolution = 'c')
        m.drawcoastlines()
        if (pltno%ncol == 0):
            dpl = 1
        else:
            dpl = 0
        if (row == nrow):
            dml = 1
        else:
            dml = 0
        m.drawparallels(np.arange(-90., 91., 10.), labels = [dpl, 0, 0, 0])
        m.drawmeridians(np.arange(-180., 181., 20.), labels = [0, 0, 0, dml])

        diff[pltno], lonsnew = addcyclic(diff[pltno], lons)
        lons2d, lats2d = np.meshgrid(lonsnew, lats)
        x, y = m(lons2d, lats2d)

        cmap=plt.get_cmap(colbar)
        norm=mc.BoundaryNorm(levels, cmap.N)

        cs = ax.contourf(x, y, diff[pltno], levels, cmap = cmap, norm = norm,
                         extend = 'both')

        plt.title(titles[pltno], size = 15, ha = 'left', x = 0)
        pltno = pltno + 1

axC = fig.add_subplot(gs[-1, 1 :])
cbar = fig.colorbar(cs, ax = ax, cax = axC, orientation = 'horizontal')
cbar.ax.tick_params(labelsize = 15)

gs.tight_layout(fig)
fig1 = plt.gcf()
plt.show()
plt.draw()
fig1.savefig(plotname + plottype, dpi = fig.dpi)
if (plottype == '.eps'):
    call("epstopdf " + plotname + plottype, shell = True)
    call("pdfcrop " + plotname + ".pdf " + plotname + ".pdf", shell = True)



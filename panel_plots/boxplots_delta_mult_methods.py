#!/usr/bin/python
'''
File Name : boxplots_delta_mult_methods.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 07-12-2017
Modified: Thu 07 Dec 2017 02:11:51 PM CET
Purpose: plot boxplots comparing multiple weighting, constraining
         methods
'''

import numpy as np
import netCDF4 as nc
from netcdftime import utime
import datetime as dt
import os # operating system interface
from os.path import expanduser
home = expanduser("~") # Get users home directory
import sys
sys.path.insert(0, home + '/scripts/plot_scripts/utils/')
from boxplot_custom import boxplot
import matplotlib.pyplot as plt
import pandas as pd
#from scipy import stats
#import statsmodels.api as sm
from sklearn import linear_model
###
# Define input & output
###
experiment = 'rcp85'
target_var = 'TXx'
# climatology: CLIM, variability: STD, trend: TREND
target_file = 'CLIM'
res_name_target = 'ANN'
target_mask = 'maskT'
res_time_target = 'MAX'
degree_sign = u'\N{DEGREE SIGN}'
target_unit = u'\N{DEGREE SIGN}' + 'C'

diag_var = ['TXx', 'TXx', 'pr']
var_file = ['CLIM', 'TREND', 'TREND']
res_name = ['ANN', 'ANN', 'JJA']
res_time = ['MAX', 'MAX', 'MEAN']
freq_v = ['ann', 'ann', 'mon']
masko = ['maskT', 'maskT', 'maskT']
nvar = len(diag_var)

nr_models = 10 # nr of best models in best models

# cut data over region?
region = 'CNEU'
longname_region = 'Central Europe' # for title plot
if region != None:
    area = region
else:
    area = 'GLOBAL'

obsdata = ['MERRA2'] #ERAint, MERRA2, Obs
nobs = len(obsdata)

syear_hist = 1980
eyear_hist = 2014
syear_fut = 2065
eyear_fut = 2099
syear = 1951
eyear = 2100

# define directories
path = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/'
outdir = '%sboxplots_mult_methods/%s/%s/' %(path, target_var, area)
if (os.access(outdir, os.F_OK) == False):
    os.makedirs(outdir)

###
# Read data
###
### read weighted mmm from netcdf
infile = '%s/Eval_Weight/%s/%s/ncdf/%s%s_%s_%sdiags_%sobs_%s_%s_swu%s_swq%s_wmm_ts.nc' %(
    path, target_var, region, target_var, target_file, res_name_target,
    len(diag_var), len(obsdata), region, 'RMSE', '0.6', '0.5')
wmm_file = nc.Dataset(infile, mode = 'r')

wmm = wmm_file.variables['wmm_ts_areaavg'][:]
lower_ts_wmm = wmm_file.variables['lower_ts_wmm'][:]
upper_ts_wmm = wmm_file.variables['upper_ts_wmm'][:]
lower25_ts_wmm = wmm_file.variables['lower25_ts_wmm'][:]
upper75_ts_wmm = wmm_file.variables['upper75_ts_wmm'][:]

delta_wmm = wmm_file.variables['delta_wmm_areaavg'][:]
delta_wmedm = wmm_file.variables['delta_wmedm_areaavg'][:]
delta_lower5_wmm = wmm_file.variables['delta_lower5_wmm'][:]
delta_lower25_wmm = wmm_file.variables['delta_lower25_wmm'][:]
delta_upper75_wmm = wmm_file.variables['delta_upper75_wmm'][:]
delta_upper95_wmm = wmm_file.variables['delta_upper95_wmm'][:]

wmm_file.close()

### read bestXY models from netcdf
infile = '%sEval_Weight/%s/%s/ncdf/best%smodels_%s%s_%s_%sdiags_%sobs_%s_%s-%s.nc' %(
    path, target_var, region, nr_models, target_var, target_file,
    res_name_target, len(diag_var), len(obsdata), region, syear, eyear)

best_file = nc.Dataset(infile, mode = 'r')
bestm = best_file.variables['bestM_ts_areaavg'][:]
time = best_file.variables['time']
cdftime = utime(time.units, calendar = 'standard')
dates = cdftime.num2date(time[:])
years = np.asarray([dates[i].year for i in xrange(len(dates))])

bestm_mean = np.mean(bestm, axis = 0)
bestm_med = np.median(bestm, axis = 0)
bestm_5 = np.percentile(bestm, 5, axis = 0)
bestm_25 = np.percentile(bestm, 25, axis = 0)
bestm_75 = np.percentile(bestm, 75, axis = 0)
bestm_95 = np.percentile(bestm, 95, axis = 0)

best_file.close()

### read constrained models from netcdf
infile = '%semerg_const/%s/%s/ncdf/%s_%s_%s_%sdiags_%sobs_%s_cmm.nc' %(
    path, target_var, region, target_var, target_file, res_name_target,
    len(diag_var), len(obsdata), region)
const_file = nc.Dataset(infile, mode = 'r')

const_ens =  const_file.variables['cens_ts_areaavg'][:]
constmm_mean = np.mean(const_ens, axis = 0)
constmm_med = np.median(const_ens, axis = 0)
constmm_5 = np.percentile(const_ens, 5, axis = 0)
constmm_25 = np.percentile(const_ens, 25, axis = 0)
constmm_75 = np.percentile(const_ens, 75, axis = 0)
constmm_95 = np.percentile(const_ens, 95, axis = 0)

### read  unconstrained models from same netcdf
ens = const_file.variables['ens_ts_areaavg'][:]
mm = np.mean(ens, axis = 0)
medm = np.median(ens, axis = 0)
lower_ts_mm = np.percentile(ens, 5, axis = 0)
upper_ts_mm = np.percentile(ens, 95, axis = 0)
lower25_ts_mm = np.percentile(ens, 25, axis = 0)
upper75_ts_mm = np.percentile(ens, 75, axis = 0)

### read obs data
obs_ts = const_file.variables['obs_ts_areaavg'][:]
obstime = const_file.variables['obstime']
cdftimeobs = utime(obstime.units, calendar = 'standard')
dates_obs = cdftimeobs.num2date(obstime[:])
years_obs = np.asarray([dates_obs[i].year for i in xrange(len(dates_obs))])

era_ts = const_file.variables['era_ts_areaavg'][:]
merra_ts = const_file.variables['merra_ts_areaavg'][:]

const_file.close()

if isinstance(obs_ts, np.ma.core.MaskedArray):
    obs_ts = obs_ts.filled(np.nan)
if isinstance(era_ts, np.ma.core.MaskedArray):
    era_ts = era_ts.filled(np.nan)
if isinstance(merra_ts, np.ma.core.MaskedArray):
    merra_ts = merra_ts.filled(np.nan)

###
# Calculate delta changes and obs lin reg
###

# calculate linear regression for obs
ols = linear_model.LinearRegression(normalize = True)

try:
    ols.fit(years_obs.reshape(-1, 1), obs_ts.reshape(-1, 1))
    trend_obs = ols.predict(years_obs.reshape(-1, 1))
    z_obs = np.polyfit(years_obs, np.squeeze(trend_obs), 1)
    f_obs = np.poly1d(z_obs)
    extra_x_obs = f_obs(years)
except (ValueError):
    extra_x_obs = np.nan

try:
    ols.fit(years_obs.reshape(-1, 1), era_ts.reshape(-1, 1))
    trend_era = ols.predict(years_obs.reshape(-1, 1))
    z_era = np.polyfit(years_obs, np.squeeze(trend_era), 1)
    f_era = np.poly1d(z_era)
    extra_x_era = f_era(years)
except (ValueError):
    extra_x_era = np.nan

try:
    ols.fit(years_obs.reshape(-1, 1), merra_ts.reshape(-1, 1))
    trend_merra = ols.predict(years_obs.reshape(-1, 1))
    z_merra = np.polyfit(years_obs, np.squeeze(trend_merra), 1)
    f_merra = np.poly1d(z_merra)
    extra_x_merra = f_merra(years)
except (ValueError):
    extra_x_merra = np.nan
### calculate data for delta change
ind_hist1 = np.where(years == syear_hist)[0][0]
ind_hist2 = np.where(years == eyear_hist)[0][0] + 1
bestm_hist = np.mean(bestm[:, ind_hist1 : ind_hist2], axis = 1)

ind_fut1 = np.where(years == syear_fut)[0][0]
ind_fut2 = np.where(years == eyear_fut)[0][0] + 1
bestm_fut = np.mean(bestm[:, ind_fut1 : ind_fut2], axis = 1)

delta_best_mean = np.mean(bestm_fut - bestm_hist)
delta_best_median = np.median(bestm_fut - bestm_hist)
delta_best5 = np.percentile((bestm_fut - bestm_hist), 5, axis = 0)
delta_best25 = np.percentile((bestm_fut - bestm_hist), 25, axis = 0)
delta_best75 = np.percentile((bestm_fut - bestm_hist), 75, axis = 0)
delta_best95 = np.percentile((bestm_fut - bestm_hist), 95, axis = 0)

const_hist = np.mean(const_ens[:, ind_hist1 : ind_hist2], axis = 1)
const_fut = np.mean(const_ens[:, ind_fut1 : ind_fut2], axis = 1)
delta_const_mean = np.mean(const_fut - const_hist)
delta_const_median = np.median(const_fut - const_hist)
delta_const5 = np.percentile((const_fut - const_hist), 5, axis = 0)
delta_const25 = np.percentile((const_fut - const_hist), 25, axis = 0)
delta_const75 = np.percentile((const_fut - const_hist), 75, axis = 0)
delta_const95 = np.percentile((const_fut - const_hist), 95, axis = 0)

hist = np.mean(ens[:, ind_hist1 : ind_hist2], axis = 1)
fut = np.mean(ens[:, ind_fut1 : ind_fut2], axis = 1)
delta_mean = np.mean(fut - hist)
delta_median = np.median(fut - hist)
delta5 = np.percentile((fut - hist), 5, axis = 0)
delta25 = np.percentile((fut - hist), 25, axis = 0)
delta75 = np.percentile((fut - hist), 75, axis = 0)
delta95 = np.percentile((fut - hist), 95, axis = 0)

try:
    obs_hist = np.mean(extra_x_obs[ind_hist1 : ind_hist2])
    obs_fut = np.mean(extra_x_obs[ind_fut1 : ind_fut2])
    delta_obs = obs_fut - obs_hist
except(TypeError):
    obs_hist = np.nan
    obs_fut = np.nan
    delta_obs = np.nan
try:
    era_hist = np.mean(extra_x_era[ind_hist1 : ind_hist2])
    era_fut = np.mean(extra_x_era[ind_fut1 : ind_fut2])
    delta_era = era_fut - era_hist
except(TypeError):
    era_hist = np.nan
    era_fut = np.nan
    delta_era = np.nan
try:
    merra_hist = np.mean(extra_x_merra[ind_hist1 : ind_hist2])
    merra_fut = np.mean(extra_x_merra[ind_fut1 : ind_fut2])
    delta_merra = merra_fut - merra_hist
except(TypeError):
    merra_hist = np.nan
    merra_fut = np.nan
    delta_merra = np.nan

###
# Plotting
###
### plot boxplot end of century
percentiles = [{'mean': np.mean(mm[ind_fut1:ind_fut2]),
                'median' : np.mean(mm[ind_fut1:ind_fut2]),
                5: np.mean(lower_ts_mm[ind_fut1:ind_fut2]),
                25: np.mean(lower25_ts_mm[ind_fut1:ind_fut2]),
                75: np.mean(upper75_ts_mm[ind_fut1:ind_fut2]),
                95: np.mean(upper_ts_mm[ind_fut1:ind_fut2])},
               {'mean': np.mean(wmm[ind_fut1:ind_fut2]),
                'median' : np.mean(wmm[ind_fut1:ind_fut2]),
                5: np.mean(lower_ts_wmm[ind_fut1:ind_fut2]),
                25: np.mean(lower25_ts_wmm[ind_fut1:ind_fut2]),
                75: np.mean(upper75_ts_wmm[ind_fut1:ind_fut2]),
                95: np.mean(upper_ts_wmm[ind_fut1:ind_fut2])},
               {'mean': np.mean(bestm_mean[ind_fut1:ind_fut2]),
                'median': np.mean(bestm_med[ind_fut1:ind_fut2]),
                5: np.mean(bestm_5[ind_fut1:ind_fut2]),
                25: np.mean(bestm_25[ind_fut1:ind_fut2]),
                75: np.mean(bestm_75[ind_fut1:ind_fut2]),
                95: np.mean(bestm_95[ind_fut1:ind_fut2])},
               {'mean': np.mean(constmm_mean[ind_fut1:ind_fut2]),
                'median': np.mean(constmm_med[ind_fut1:ind_fut2]),
                5: np.mean(constmm_5[ind_fut1:ind_fut2]),
                25: np.mean(constmm_25[ind_fut1:ind_fut2]),
                75: np.mean(constmm_75[ind_fut1:ind_fut2]),
                95: np.mean(constmm_95[ind_fut1:ind_fut2])},
               {'mean': obs_fut,
                'median': np.nan,
                5: np.nan,
                25: np.nan,
                75: np.nan,
                95: np.nan},
               {'mean': era_fut,
                'median': np.nan,
                5: np.nan,
                25: np.nan,
                75: np.nan,
                95: np.nan},
               {'mean': merra_fut,
                'median': np.nan,
                5: np.nan,
                25: np.nan,
                75: np.nan,
                95: np.nan}]
df = pd.DataFrame(percentiles)

out = boxplot(df, color = (0, 0, 0))

# adding horizontal grid lines
a = plt.gca()
a.yaxis.grid(True)
a.set_xlim(-1, len(df))
a.set_title('%s TXx 2065-2099' %region)
a.set_xticklabels(['', 'IPCC', 'weighted', 'best',
                   'constrained', 'E-Obs', 'ERAint', 'MERRA2'])

plt.ylabel('%s %s [%s]' %(target_var, res_name_target, target_unit))

# save figure
plt.savefig('%sboxplot_%s_%s_%s_%s%s_%s_%s_%sobs.pdf' %(
    outdir, target_var, target_file, target_mask, len(diag_var),
    diag_var[0], var_file[0], res_name[0], nobs))

### plot boxplot for delta
percentiles_delta = [{'mean': delta_mean,
                      'median' : delta_median,
                      5: delta5,
                      25: delta25,
                      75: delta75,
                      95: delta95},
                     {'mean': delta_wmm,
                      'median' : delta_wmedm,
                      5: delta_lower5_wmm,
                      25: delta_lower25_wmm,
                      75: delta_upper75_wmm,
                      95: delta_upper95_wmm},
                     {'mean': delta_best_mean,
                      'median' : delta_best_median,
                      5: delta_best5,
                      25: delta_best25,
                      75: delta_best75,
                      95: delta_best95},
                     {'mean': delta_const_mean,
                      'median' : delta_const_median,
                      5: delta_const5,
                      25: delta_const25,
                      75: delta_const75,
                      95: delta_const95}]
#                     {'mean': delta_obs,
#                      'median': np.nan,
#                      5: np.nan,
#                      25: np.nan,
#                      75: np.nan,
#                      95: np.nan},
#                     {'mean': delta_era,
#                      'median': np.nan,
#                      5: np.nan,
#                      25: np.nan,
#                      75: np.nan,
#                      95: np.nan},
#                     {'mean': delta_merra,
#                      'median': np.nan,
#                      5: np.nan,
#                      25: np.nan,
#                      75: np.nan,
#                      95: np.nan}]
df_delta = pd.DataFrame(percentiles_delta)

out_delta = boxplot(df_delta, color = (0, 0, 0))

# adding horizontal grid lines
a = plt.gca()
a.yaxis.grid(True)
a.set_xlim(-1, len(df_delta))
a.set_title('%s TXx %s-%s - %s-%s' %(region, syear_fut, eyear_fut, syear_hist,
                                     eyear_hist))
a.set_xticklabels(['', 'IPCC', 'weighted', 'best', 'constrained'])
#, 'E-Obs', 'ERAint', 'MERRA2'])

plt.ylabel('$\Delta$%s %s [%s]' %(target_var, res_name_target, target_unit))

# save figure
plt.savefig('%sboxplot_delta_%s_%s_%s_%s%s_%s_%s_%sobs.pdf' %(
    outdir, target_var, target_file, target_mask, len(diag_var),
    diag_var[0], var_file[0], res_name[0], nobs))

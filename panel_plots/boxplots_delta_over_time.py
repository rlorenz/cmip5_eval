#!/usr/bin/python
'''
File Name : boxplots_delta_over_time.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 08-01-2018
Modified: Mon 08 Jan 2018 05:08:06 PM CET
Purpose: plot boxplots for difference between several future estimates
         minus historical, change over time in perfect model test 

'''
import numpy as np
import netCDF4 as nc
import os # operating system interface
from os.path import expanduser
home = expanduser("~") # Get users home directory
import sys
sys.path.insert(0, home + '/scripts/plot_scripts/utils/')
from boxplot_custom import boxplot
import matplotlib.pyplot as plt

###
# Define input & output
###
experiment = 'rcp85'

#target_var = 'TXx'
## climatology: CLIM, variability: STD, trend: TREND
#target_file = 'CLIM'
#res_name_target = 'ANN'
#target_mask = 'maskT'
#res_time_target = 'MAX'
#degree_sign = u'\N{DEGREE SIGN}'
#target_unit = u'\N{DEGREE SIGN}' + 'C'

#diag_var = ['TXx', 'TXx']
#var_file = ['CLIM', 'TREND']
#res_name = ['ANN', 'ANN']
#res_time = ['MAX', 'MAX']
#freq_v = ['ann', 'ann']
#masko = ['maskT', 'maskT']

target_var = 'hurs'
# climatology: CLIM, variability: STD, trend: TREND
target_file = 'CLIM'
res_name_target = 'JJA'
target_mask = 'maskF'
res_time_target = 'MEAN'
degree_sign = u'\N{DEGREE SIGN}'
target_unit = '%'

diag_var = ['hurs', 'hurs']
var_file = ['TREND', 'STD']
res_name = ['JJA', 'JJA']
res_time = ['MEAN', 'MEAN']
freq_v = ['mon', 'mon']
masko = ['maskF', 'maskF']

nvar = len(diag_var)

# cut data over region?
region = 'CNEU'
longname_region = 'Central Europe' # for title plot
if region != None:
    area = region
else:
    area = 'GLOBAL'

obsdata = ['Obs'] #ERAint, MERRA2, Obs
nobs = len(obsdata)

syear_hist = 1950
eyear_hist = 1999
syear_fut = [1975, 2000, 2025, 2050]
eyear_fut = [2024, 2049, 2074, 2099]
syear = 1950
eyear = 2100

# define directories
path = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/'
indir = '%sweight_perf_mod_test/%s/%s/ncdf/' %(path, target_var, area)
outdir = '%sboxplots_over_time/%s/%s/' %(path, target_var, area)
if (os.access(outdir, os.F_OK) == False):
    os.makedirs(outdir)

###
# Read data
###
delta_lin = list()
delta_mm = list()
delta_wmm = list()
delta_best = list()

for period in xrange(len(syear_fut)):
    infile = '%sdata_boxplot_%s_%s_%s_%s%s_%s_%s_%s_%s-%s.nc' %(
        indir,target_var, target_file, target_mask, len(diag_var), diag_var[0],
        var_file[0], res_name[0], area, syear_fut[period], eyear_fut[period])

    ncfile = nc.Dataset(infile, mode = 'r')

    delta_lin.append(ncfile.variables['delta_lin_true'][:])
    delta_mm.append(ncfile.variables['delta_mm_true'][:])
    delta_wmm.append(ncfile.variables['delta_wmm_true'][:])
    delta_best.append(ncfile.variables['delta_best_true'][:])
    ncfile.close()

## plot boxplot for change from hist to future linear regresssion
fig = plt.figure(figsize = (7, 7), dpi = 300)
plt.boxplot([delta_lin[0], delta_lin[1], delta_lin[2], delta_lin[3]],
            showmeans = True,
            whis = [5, 95], vert = True,
            labels = ['%s-%s' %(syear_fut[0], eyear_fut[0]), 
                      '%s-%s' %(syear_fut[1], eyear_fut[1]),
                      '%s-%s' %(syear_fut[2], eyear_fut[2]),
                      '%s-%s' %(syear_fut[3], eyear_fut[3])])
# adding horizontal grid lines
ax = plt.gca()
ax.yaxis.grid(True)
ax.axhline(y = 0.0, color = 'k', linestyle = '-')
ax.set_title('Linear regression') 
plt.ylim([-8, 8])
plt.ylabel('%s $\Delta$%s %s [%s]' %(region, target_var, res_name_target,
                                     target_unit))
plt.xlabel('$\Delta$lin - $\Delta$true')
plt.savefig('%sboxplot_delta_lin_reg_%s_%s_%s_%s%s_%s_%s_%s-%s_%speriods.pdf' %(
    outdir, target_var, target_file, target_mask, len(diag_var), diag_var[0], 
    var_file[0], res_name[0], syear_hist, eyear_hist, len(syear_fut)))

## plot boxplot for change from hist to future arithmetric multi-model mean
fig = plt.figure(figsize = (7, 7), dpi = 300)
plt.boxplot([delta_mm[0], delta_mm[1], delta_mm[2], delta_mm[3]],
            showmeans = True,
            whis = [5, 95], vert = True,
            labels = ['%s-%s' %(syear_fut[0], eyear_fut[0]), 
                      '%s-%s' %(syear_fut[1], eyear_fut[1]),
                      '%s-%s' %(syear_fut[2], eyear_fut[2]),
                      '%s-%s' %(syear_fut[3], eyear_fut[3])])
# adding horizontal grid lines
ax = plt.gca()
ax.yaxis.grid(True)
ax.axhline(y = 0.0, color = 'k', linestyle = '-')
ax.set_title('Arithmetric multi-model mean') 
plt.ylim([-8, 8])
plt.ylabel('%s $\Delta$%s %s [%s]' %(region, target_var, res_name_target,
                                     target_unit))
plt.xlabel('$\Delta$mm - $\Delta$true')
plt.savefig('%sboxplot_delta_mm_%s_%s_%s_%s%s_%s_%s_%s-%s_%speriods.pdf' %(
    outdir, target_var, target_file, target_mask, len(diag_var), diag_var[0], 
    var_file[0], res_name[0], syear_hist, eyear_hist, len(syear_fut)))

## plot boxplot for change from hist to future weighted multi-model mean
fig = plt.figure(figsize = (7, 7), dpi = 300)
plt.boxplot([delta_wmm[0], delta_wmm[1], delta_wmm[2], delta_wmm[3]],
            showmeans = True,
            whis = [5, 95], vert = True,
            labels = ['%s-%s' %(syear_fut[0], eyear_fut[0]), 
                      '%s-%s' %(syear_fut[1], eyear_fut[1]),
                      '%s-%s' %(syear_fut[2], eyear_fut[2]),
                      '%s-%s' %(syear_fut[3], eyear_fut[3])])
# adding horizontal grid lines
ax = plt.gca()
ax.yaxis.grid(True)
ax.axhline(y = 0.0, color = 'k', linestyle = '-')
ax.set_title('Weighted multi-model mean') 
plt.ylim([-8, 8])
plt.ylabel('%s $\Delta$%s %s [%s]' %(region, target_var, res_name_target,
                                     target_unit))
plt.xlabel('$\Delta$wmm - $\Delta$true')
plt.savefig('%sboxplot_delta_wmm_%s_%s_%s_%s%s_%s_%s_%s-%s_%speriods.pdf' %(
    outdir, target_var, target_file, target_mask, len(diag_var), diag_var[0], 
    var_file[0], res_name[0], syear_hist, eyear_hist, len(syear_fut)))

## plot boxplot for change from hist to future best models
fig = plt.figure(figsize = (7, 7), dpi = 300)
plt.boxplot([delta_best[0], delta_best[1], delta_best[2], delta_best[3]],
            showmeans = True,
            whis = [5, 95], vert = True,
            labels = ['%s-%s' %(syear_fut[0], eyear_fut[0]), 
                      '%s-%s' %(syear_fut[1], eyear_fut[1]),
                      '%s-%s' %(syear_fut[2], eyear_fut[2]),
                      '%s-%s' %(syear_fut[3], eyear_fut[3])])
# adding horizontal grid lines
ax = plt.gca()
ax.yaxis.grid(True)
ax.axhline(y = 0.0, color = 'k', linestyle = '-')
ax.set_title('Best models') 
plt.ylim([-8, 8])
plt.ylabel('%s $\Delta$%s %s [%s]' %(region, target_var, res_name_target,
                                     target_unit))
plt.xlabel('$\Delta$best - $\Delta$true')
plt.savefig('%sboxplot_delta_best_%s_%s_%s_%s%s_%s_%s_%s-%s_%speriods.pdf' %(
    outdir, target_var, target_file, target_mask, len(diag_var), diag_var[0], 
    var_file[0], res_name[0], syear_hist, eyear_hist, len(syear_fut)))

## plot boxplot for change from hist to future
fig = plt.figure(figsize = (12, 7), dpi = 300)

# function for setting the colors of the box plots
def setBoxColors(bp):
    plt.setp(bp['boxes'][0], color = 'k')
    plt.setp(bp['caps'][0], color = 'k')
    plt.setp(bp['caps'][1], color = 'k')
    plt.setp(bp['whiskers'][0], color = 'k')
    plt.setp(bp['whiskers'][1], color = 'k')
    plt.setp(bp['fliers'][0], color = 'k', markeredgecolor = 'k')
    plt.setp(bp['medians'][0], color = 'k')
    plt.setp(bp['means'][0], markerfacecolor = 'k', markeredgecolor = 'k')

    plt.setp(bp['boxes'][1], color = 'blue')
    plt.setp(bp['caps'][2], color = 'blue')
    plt.setp(bp['caps'][3], color = 'blue')
    plt.setp(bp['whiskers'][2], color = 'blue')
    plt.setp(bp['whiskers'][3], color = 'blue')
    plt.setp(bp['fliers'][1], color = 'blue', markeredgecolor = 'blue')
    plt.setp(bp['medians'][1], color = 'blue')
    plt.setp(bp['means'][1], markerfacecolor = 'blue', markeredgecolor = 'blue')

    plt.setp(bp['boxes'][2], color = 'green')
    plt.setp(bp['caps'][4], color = 'green')
    plt.setp(bp['caps'][5], color = 'green')
    plt.setp(bp['whiskers'][4], color = 'green')
    plt.setp(bp['whiskers'][5], color = 'green')
    plt.setp(bp['fliers'][2], color = 'green', markeredgecolor = 'green')
    plt.setp(bp['medians'][2], color = 'green')
    plt.setp(bp['means'][2], markerfacecolor = 'green',
             markeredgecolor = 'green')

    plt.setp(bp['boxes'][3], color = 'm')
    plt.setp(bp['caps'][6], color = 'm')
    plt.setp(bp['caps'][7], color = 'm')
    plt.setp(bp['whiskers'][6], color = 'm')
    plt.setp(bp['whiskers'][7], color = 'm')
    plt.setp(bp['fliers'][3], color = 'm', markeredgecolor = 'm')
    plt.setp(bp['medians'][3], color = 'm')
    plt.setp(bp['means'][3], markerfacecolor = 'm', markeredgecolor = 'm')

ax = plt.gca()
ax.axhline(y = 0.0, color = 'k', linestyle = '-')
plt.hold(True)
#1st period
bp = plt.boxplot([[delta_lin[0]], [delta_mm[0]], [delta_wmm[0]],
                  [delta_best[0]]],
                 showmeans = True,
                 whis = [5, 95], vert = True,
                 positions = [1, 2, 3, 4], widths = 0.6)
setBoxColors(bp)
#2nd period
bp = plt.boxplot([[delta_lin[1]], [delta_mm[1]], [delta_wmm[1]],
                  [delta_best[1]]],
                 showmeans = True, whis = [5, 95], vert = True,
                 positions = [6, 7, 8, 9], widths = 0.6)
setBoxColors(bp)
#3rd period
bp = plt.boxplot([[delta_lin[2]], [delta_mm[2]], [delta_wmm[2]],
                  [delta_best[2]]], showmeans = True, whis = [5, 95],
                 vert = True, positions = [11, 12, 13, 14], widths = 0.6)
setBoxColors(bp)
#4th period
bp = plt.boxplot([[delta_lin[3]], [delta_mm[3]], [delta_wmm[3]],
                  [delta_best[3]]], showmeans = True, whis = [5, 95],
                 vert = True, positions = [16, 17, 18, 19], widths = 0.6)
setBoxColors(bp)
# set axes limits and labels
plt.xlim(0, 20)
#plt.ylim([-9, 4])
ax.set_xticklabels(['1975-2024', '2000-2049', '2025-2074', '2050-2099'])
ax.set_xticks([2.5, 7.5, 12.5, 17.5])

#ax.yaxis.grid(True)
ax.set_title('Difference $\Delta$%s to "truth" for different methods and periods' %(target_var)) 

plt.ylabel('%s $\Delta$%s %s [%s]' %(region, target_var, res_name_target, target_unit))
plt.xlabel('Time period')

hK, = plt.plot([1,1],'k-')
hB, = plt.plot([1,1],'b-')
hG, = plt.plot([1,1],'g-')
hM, = plt.plot([1,1],'m-')
plt.legend((hK, hB, hG, hM),('lin reg', 'mm', 'wmm', 'best'),
           loc = 'lower left', fontsize = 12)
hK.set_visible(False)
hB.set_visible(False)
hG.set_visible(False)
hM.set_visible(False)

plt.savefig('%sboxplot_delta_all_%s_%s_%s_%s%s_%s_%s_%s-%s_%speriods.pdf' %(
    outdir, target_var, target_file, target_mask, len(diag_var), diag_var[0], 
    var_file[0], res_name[0], syear_hist, eyear_hist, len(syear_fut)))

# plot means from all methods in one figure over time
data_lin = np.mean(delta_lin, axis = 1)
data_mm = np.mean(delta_mm, axis = 1)
data_wmm = np.mean(delta_wmm, axis = 1)
data_best = np.mean(delta_best, axis = 1)
time = range(len(syear_fut))

lower_lin = np.percentile(delta_lin, 25, axis = 1)
upper_lin = np.percentile(delta_lin, 75, axis = 1)
lower_mm = np.percentile(delta_mm, 25, axis = 1)
upper_mm = np.percentile(delta_mm, 75, axis = 1)
lower_wmm = np.percentile(delta_wmm, 25, axis = 1)
upper_wmm = np.percentile(delta_wmm, 75, axis = 1)
lower_best = np.percentile(delta_best, 25, axis = 1)
upper_best = np.percentile(delta_best, 75, axis = 1)

fig = plt.figure(figsize = (10, 7), dpi = 300)

plt.plot(time, data_lin, color = 'k', linestyle = ':', label = 'lin reg')
plt.plot(time, data_mm, color = 'b', linestyle = '--', label = 'mm')
plt.plot(time, data_wmm, color = 'g', linestyle = '-.', label = 'wmm')
plt.plot(time, data_best, color = 'm', linestyle = '-', label = 'best')

plt.fill_between(time, lower_lin, upper_lin, facecolor = 'grey',
                 alpha = 0.3)
plt.fill_between(time, lower_mm, upper_mm, facecolor = 'b',
                 edgecolor = 'b', alpha = 0.3)
plt.fill_between(time, lower_wmm, upper_wmm, facecolor = 'g',
                 edgecolor = 'g', alpha = 0.3)
plt.fill_between(time, lower_best, upper_best, facecolor = 'm',
                 edgecolor = 'm', alpha = 0.3)

ax = plt.gca()
ax.set_title('Mean delta over time') 
labels = ['%s-%s' %(syear_fut[0], eyear_fut[0]), '',
          '%s-%s' %(syear_fut[1], eyear_fut[1]), '', 
          '%s-%s' %(syear_fut[2], eyear_fut[2]), '', 
          '%s-%s' %(syear_fut[3], eyear_fut[3])]
ax.set_xticklabels(labels)

plt.ylabel('%s $\Delta$%s %s [%s]' %(region, target_var, res_name_target, target_unit))
plt.xlabel('time period')
leg = plt.legend(loc = 'lower left', fontsize = 12)
leg.draw_frame(False)

plt.savefig('%sdelta_all_%s_%s_%s_%s%s_%s_%s_%s-%s_%speriods.pdf' %(
    outdir, target_var, target_file, target_mask, len(diag_var), diag_var[0], 
    var_file[0], res_name[0], syear_hist, eyear_hist, len(syear_fut)))

#!/usr/bin/python
'''
File Name : lin_reg_hist_TREND_to_fut_CMIP5_cdo.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 13-11-2017
Modified:
Purpose: Calculate linear regression for CMIP5 data
         to predict future based on historical TREND
	 using sklearn python packages (http://scikit-learn.org)
         Scikit-learn: Machine Learning in Python, Pedregosa et al.,
                       JMLR 12, pp. 2825-2830, 2011.
'''
import numpy as np
import netCDF4 as nc
from netcdftime import utime
import datetime as dt
import glob as glob
from sklearn import linear_model
from sklearn.cross_validation import cross_val_score, train_test_split
import os
from os.path import expanduser
home = expanduser("~") # Get users home directory
import sys
import math
sys.path.insert(0, home+'/scripts/plot_scripts/utils/')
from func_read_data import func_read_netcdf
from func_write_netcdf import func_write_netcdf
import matplotlib
import matplotlib.pyplot as plt

###
# Define input & output
###
experiment = 'rcp85'
archive = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/'
res_name_target = 'ANN'
freq = 'ann'

target_var = 'TXx'
target_file = 'CLIM'
res_time_target = 'MAX'
target_mask = 'maskT'

diag_var = ['TXx']
var_file = ['TREND']
res_name = ['ANN']
res_time = ['MAX']
freq_v = ['ann']
masko = ['maskT']
nvar = len(diag_var)

region = 'CNEU'          #cut data over region?
longname_region = 'Central Europe' # for title plot
obsnames = ['Obs']

path = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/'
outdir = '%s%s/Lin_Reg/%s/' %(path, target_var, region)

syear_hist = 1950
eyear_hist = 1999
syear_fut = 2050
eyear_fut = 2099
syear = 1950
eyear = 2100

nyears = eyear_hist - syear_hist + 1
ntim_tot = eyear - syear + 1

grid = 'g025'

ols = linear_model.LinearRegression(normalize = True) 
Labelols = "OLS"
tsr = linear_model.TheilSenRegressor(random_state = 50, copy_X = True)
Labeltsr = "Theil-Sen"
ridgeCV = linear_model.RidgeCV(normalize = True)
Labelridge = "RidgeCV"

if (os.access(outdir, os.F_OK) == False):
        os.makedirs(outdir)
###read model data
print "Read model data"
## find all matching files in archive, loop over all of them
# first count matching files in folder
models_t = list()
model_names = list()

path_target = '%s/%s/%s/%s/' %(archive, target_var, freq, target_mask)
if region:
    name = '%s%s_%s_*_%s_r?i?p?_%s-%s_%s%s_%s_%s.nc' %(
        path_target, target_var, freq, experiment, syear_fut, eyear_fut,
        res_name_target, res_time_target, target_file, region)
else:
    name = '%s/%s_%s_*_%s_r?i?p?_%s-%s_%s%s_%s.nc' %(
        path_target, target_var, freq, experiment, syear_fut, eyear_fut,
        res_name_target, res_time_target, target_file)

nfiles_targ = len(glob.glob(name))
print str(nfiles_targ) + ' matching files found for target variable'

for filename in glob.glob(name):
    models_t.append(filename.split('_')[4] + ' ' + filename.split('_')[6])

overlap = models_t

for v in xrange(len(diag_var)):
    models_v = list()
    path_var = '%s/%s/%s/%s/' %(archive, diag_var[v], freq_v[v], masko[v])
    if region:
        name_v = '%s%s_%s_*_%s_r?i?p?_%s-%s_%s%s_%s_%s.nc' %(
            path_var, diag_var[v], freq_v[v], experiment, syear_hist,
            eyear_hist, res_name[v], res_time[v], var_file[v], region)
    else:
        name_v = '%s%s_%s_*_%s_r?i?p?_%s-%s_%s%s_%s.nc' %(
            path_var, diag_var[v], freq_v[v], experiment, syear_hist,
            eyear_hist, res_name[v], res_time[v], var_file[v])

    for filename in glob.glob(name_v):
        models_v.append(filename.split('_')[4] + ' ' +
                        filename.split('_')[6])
    #find overlapping files for all variables
    overlap = list(set(models_v) & set(overlap))
    del models_v, path_var
nfiles = len(overlap)

print str(nfiles) + ' matching files found for all variables'

target = np.empty((nfiles))
target_ts = dict() #np.empty((ntim_tot, nfiles))
f = 0
for filename in glob.glob(name):
    model_name = filename.split('_')[4] + ' ' + filename.split('_')[6]
    if model_name in overlap:
        #print "Read "+filename+" data"
        fh = nc.Dataset(filename, mode = 'r')
        temp_fut = fh.variables[target_var][:] # global data, time, lat, lon
        lat = fh.variables['lat'][:]
        lon = fh.variables['lon'][:]
        tmp = fh.variables[target_var]
        target_unit = tmp.units
        try:
            Fill = tmp._FillValue
        except AttributeError:
            Fill = 1e+20
        fh.close()
        if (target_file != 'SCALE'):
            path_hist = '%s/%s/%s/%s/' %(archive, target_var, freq, target_mask)
            if region:
                filename_hist = '%s%s_%s_%s_%s_%s_%s-%s_%s%s_%s_%s.nc' %(
                    path_hist, target_var, freq, filename.split('_')[4],
                    experiment, filename.split('_')[6], syear_hist, eyear_hist,
                    res_name_target, res_time_target, target_file, region)
            else:
                filename_hist = '%s/%s_%s_%s_%s_%s_%s-%s_%s%s_%s.nc' %(
                    path_hist, target_var, freq, target_mask,
                    target_var, freq, filename.split('_')[4],
                    experiment, filename.split('_')[6], syear_hist, eyear_hist,
                    res_name_target, res_time_target, target_file)
            fh = nc.Dataset(filename_hist, mode = 'r')
            temp_hist = fh.variables[target_var][:] # global data,time,lat,lon
            fh.close()
            temp_mod = temp_fut - temp_hist
        else:
            temp_mod = temp_fut

        #check that time axis and grid is identical for model0 and modelX
        if f != 0:
            if temp_mod0.shape != temp_mod.shape:
                print 'Warning: Dimension for models are different!'
                continue
        else:
            temp_mod0 = temp_mod[:]

        model = filename.split('_')[4]
        if model == 'ACCESS1.3':
            model = 'ACCESS1-3'
        elif model == 'FGOALS_g2':
            model = 'FGOALS-g2'
        ens = filename.split('_')[6]
        model_names.append(model + '_' + ens)

        if isinstance(temp_mod, np.ma.core.MaskedArray):
            #print type(temp_mod), temp_mod.shape
            temp_mod = temp_mod.filled(np.NaN)

        if (target_var == 'sic') and  (model == "EC-EARTH"):
            with np.errstate(invalid='ignore'):
                temp_mod[temp_mod < 0.0] = np.NaN
        # average over area and save value for each model
        w_lat = np.cos(lat * (4.0 * math.atan(1.0) / 180))
        ma_target = np.ma.masked_array(temp_mod,
                                       np.isnan(temp_mod))
        tmp1_latweight = np.ma.average(np.squeeze(ma_target),
                                       axis = 0,
                                       weights = w_lat)
        target[f] = np.nanmean(tmp1_latweight.filled(np.nan))

        # read data over whole timeseries for plotting
        path_ts = '%s/%s/%s/%s/' %(archive, target_var, freq, target_mask)
        if region:
            filename_ts = '%s%s_%s_%s_%s_%s_%s-%s_%s%s_%s.nc' %(
                path_ts, target_var, freq, filename.split('_')[4],
                experiment, filename.split('_')[6], syear, eyear,
                res_name_target, res_time_target, region)
        else:
            filename_ts = '%s/%s_%s_%s_%s_%s_%s-%s_%s%s.nc' %(
                path_ts, target_var, freq, target_mask, filename.split('_')[4],
                experiment, filename.split('_')[6], syear, eyear,
                res_name_target, res_time_target)
        fh = nc.Dataset(filename_ts, mode = 'r')
        temp_ts = fh.variables[target_var][:] # global data,time,lat,lon
        time = fh.variables['time']
        cdftime = utime(time.units, calendar = time.calendar)
        dates = cdftime.num2date(time[:])
        years = np.asarray([dates[i].year for i in xrange(len(dates))])
        fh.close()
        tmp_ts_latweight = np.ma.average(np.squeeze(temp_ts), axis = 1,
                                         weights = w_lat)
        target_ts[model + '_' + ens] = np.nanmean(tmp_ts_latweight.filled(np.nan), axis = 1)
        f = f + 1
    else:
        continue
if target_unit == 'degC':
    degree_sign = u'\N{DEGREE SIGN}'
    target_unit = degree_sign + "C"

data = np.empty((nfiles, nvar), float, Fill)
data_unit = list()
for v in xrange(len(diag_var)):
    path_var = '%s/%s/%s/%s/' %(archive, diag_var[v], freq_v[v], masko[v])
    if region:
        name = '%s%s_%s_*_%s_r?i?p?_%s-%s_%s%s_%s_%s.nc' %(
            path_var, diag_var[v], freq_v[v], experiment, syear_hist, 
            eyear_hist, res_name[v], res_time[v], var_file[v], region)
    else:
        name = '%s%s_%s_*_%s_r?i?p?_%s-%s_%s%s_%s.nc' %(
            path_var, diag_var[v], freq_v[v], experiment, syear_hist,
            eyear_hist, res_name[v], res_time[v], var_file[v]) 
    f = 0
    for filename in glob.glob(name):
        model_name = filename.split('_')[4] + ' ' + filename.split('_')[6]
        if model_name in overlap:
            #print "Read " + filename + " data"
            fh = nc.Dataset(filename, mode = 'r')
            lon = fh.variables['lon'][:]
            lat = fh.variables['lat'][:]
            temp_mod = fh.variables[diag_var[v]][:] # global data
            #check that time axis and grid is identical for models
            if f != 0:
                if temp_mod0.shape != temp_mod.shape:
                    print('Warning: Dimension for model0 and modelX ' +
                          'is different!')
                    continue
            else:
                temp_mod0 = temp_mod[:]
                tmp = fh.variables[diag_var[v]]
                data_unit.append(tmp.units)
            fh.close()

            if isinstance(temp_mod, np.ma.core.MaskedArray):
                #print type(temp_mod), temp_mod.shape
                temp_mod = temp_mod.filled(np.NaN)
                #print type(temp_mod), temp_mod.shape

            if (diag_var[v] == 'sic') and  (model == "EC-EARTH"):
                with np.errstate(invalid = 'ignore'):
                    temp_mod[temp_mod < 0.0] = np.NaN
            # average over area and save value for each model
            w_lat = np.cos(lat * (4.0 * math.atan(1.0) / 180))
            ma_hist = np.ma.masked_array(temp_mod,
                                         np.isnan(temp_mod))
            tmp2_latweight = np.ma.average(np.squeeze(ma_hist),
                                           axis = 0,
                                           weights = w_lat)
            data[f, v] = np.nanmean(tmp2_latweight.filled(np.nan))
            f = f + 1
        else:
            continue

    # Read obs data
    for obsdata in obsnames:
        print "Read %s data" %(obsdata)
        if ((obsdata == 'Obs') and
            ((diag_var[v] == 'rlus') or (diag_var[v] == 'rsds'))):
            name = '%s/%s/%s/%s/%s_%s_%s_2000-%s_%s%s_%s_%s.nc' %(
                archive, diag_var[v], freq_v[v], masko[v], diag_var[v],
                freq_v[v], obsdata, eyear_hist, res_name[v], res_time[v],
                var_file[v], region)
        else:
            name = '%s/%s/%s/%s/%s_%s_%s_%s-%s_%s%s_%s_%s.nc' %(
                archive, diag_var[v], freq_v[v], masko[v], diag_var[v],
                freq_v[v], obsdata, syear_hist, eyear_hist, res_name[v],
                res_time[v], var_file[v], region)

        fh = nc.Dataset(name, mode = 'r')
        var2_obs = fh.variables[diag_var[v]][:] # global data, time, lat, lon
        if isinstance(var2_obs, np.ma.core.MaskedArray):
                var2_obs = var2_obs.filled(np.NaN)

        # calculate area average for obs
        ma_var2_obs = np.ma.masked_array(var2_obs, np.isnan(var2_obs))
        tmp_latweight = np.ma.average(np.squeeze(ma_var2_obs), axis = 0,
                                      weights = w_lat)
        if (obsdata == 'MERRA2'):
            m2_obs_areaavg = np.nanmean(tmp_latweight.filled(np.nan))
        if (obsdata == 'ERAint'):
            era_obs_areaavg = np.nanmean(tmp_latweight.filled(np.nan))
        else:
            obs_areaavg = np.nanmean(tmp_latweight.filled(np.nan))
if data_unit[0] == 'degC':
    data_unit[0] = degree_sign + "C"

# Split the dataset in two equal parts
X_train, X_test, y_train, y_test = train_test_split(
    data, np.squeeze(target.reshape(-1, 1)), test_size = 0.5, random_state = 10)

# fit regressions
ols.fit(X_train, y_train)
ols_lin_pred = ols.predict(X_test)

tsr.fit(X_train, y_train)
tsr_lin_pred = tsr.predict(X_test)

ridgeCV.fit(X_train, y_train)
ridgeCV_lin_pred = ridgeCV.predict(X_test)

print("The model is trained on the full training set (train_test_split).")
print("The scores are computed on the full test set.")
print()
#OLS
print('OLS')
# The coefficients
#print('Coefficients OLS: \n', ols.coef_)
# The mean square error
print("Residual sum of squares OLS: %.2f"
      % np.mean((ols_lin_pred - y_test) ** 2))
# Explained variance score: 1 is perfect prediction
print('Variance score OLS: %.2f' % ols.score(X_test, y_test))
#CrossValidation
cv = 5
scoresOLS = cross_val_score(ols, X_test, y_test, cv = cv)
print("Accuracy OLS: %0.2f (+/- %0.2f)" % (scoresOLS.mean(),
                                           scoresOLS.std() * 2))
print()
print('Theil-Sen:')
print('Variance score: %.2f' % tsr.score(X_test, y_test))
print()
print('Ridge CV:')
print('Variance score: %.2f' % ridgeCV.score(X_test, y_test))
print()
corr = np.corrcoef(target, np.squeeze(data))[0, 1]
print ('Correlation: %.2f' %corr)

# plot scatter with linear regressions
fig = plt.figure(figsize = (10, 7), dpi = 300)
ax = fig.add_subplot(111)

#plt.scatter(X_test, y_test, marker = 'o', color = 'black', edgecolors = 'none',
#            s = 40)
plt.scatter(np.squeeze(data), target, marker = 'o', color = 'black',
            edgecolors = 'none', s = 40)
plt.plot(X_test, ols_lin_pred, color = 'blue', linewidth = 3,
         label = '%s, R$^2$: %0.2f' %(Labelols, ols.score(X_test, y_test)) )
plt.plot(X_test, tsr_lin_pred, color = 'purple', linewidth = 3,
         label = '%s, R$^2$: %0.2f' %(Labeltsr, tsr.score(X_test, y_test)))
plt.plot(X_test, ridgeCV_lin_pred, color = 'seagreen', linewidth = 3,
         label = '%s, R$^2$: %0.2f' %(Labelridge,
                                      ridgeCV.score(X_test, y_test)))
try:
    plt.axvline(x = obs_areaavg, color = 'darkgrey', linewidth = 2,
                label = 'Obs')
except(NameError):
    print 'Warning: no Obs data'
try:
    plt.axvline(x = m2_obs_areaavg, color = 'darkgrey', linewidth = 2)
except(NameError):
    print 'Warning: no MERRA2 data'
try:
    plt.axvline(x = era_obs_areaavg, color = 'darkgrey', linewidth = 2)
except(NameError):
    print 'Warning: no ERAint data'
plt.xlabel('%s %s %s [%s]' %(diag_var[0], var_file[0], res_name[0],
                             data_unit[0]))
plt.ylabel('$\Delta$%s %s %s [%s]' %(target_var, target_file, res_name_target,
                                     target_unit))
plt.legend(loc = 'upper left', frameon = False)
plt.text(0.1, 0.73, 'Corr = %s' %(round(corr, 2)), fontsize = 15,
         ha = 'center', va = 'center', transform = ax.transAxes)
plt.title(longname_region + ' ' + res_name_target)
plt.savefig('%slin_reg_hist_%s_%s_%s_%s_%s_%s.pdf' %(
        outdir, target_var, target_file, target_mask, diag_var[0],
        var_file[0], res_name[0]))

# plot timeseries
#target_ts_mmm = np.nanmean(target_ts, axis = 1)
# calculate unweighted mean for evaluation period
# first average over initial conditions ensembles per model
models = [x.split('_')[0] for x in target_ts.keys()]
mult_ens1 = []
seen = set()
for m in models:
    if m not in seen:
        seen.add(m)
    else:
        mult_ens1.append(m)
# make sure every model only once in list
list_with_mult_ens = set(mult_ens1)
d_avg_ens1 = dict()
for key, value in target_ts.iteritems():
    if key.split('_')[0] in list_with_mult_ens:
        #find other ensemble members
        ens_mem1 = [value2 for key2, value2 in sorted(
            target_ts.iteritems()) if key.split('_')[0] in key2]
        d_avg_ens1[key.split('_')[0]] = np.nanmean(ens_mem1, axis = 0)
    else:
        d_avg_ens1[key.split('_')[0]] = value

tmp2 = 0
for key, value in d_avg_ens1.iteritems():
    tmp2 = tmp2 + value
target_ts_ens1_mmm = tmp2 / len(set(models))
del tmp2
avg_ens1 = np.array(d_avg_ens1.values(), dtype = float)
target_std_ens1 = np.ndarray((len(years)))
for yr in xrange(len(years)):
    target_std_ens1[yr] = np.std(avg_ens1[:, yr]) 

# calculate trend over historical period
y1 = np.where(years == syear_hist)[0][0]
y2 = np.where(years == eyear_hist)[0][0]
ols.fit(years[y1 : y2 + 1].reshape(-1, 1),
        target_ts_ens1_mmm[y1 : y2 + 1].reshape(-1, 1))
trend = ols.predict(years[y1 : y2 + 1].reshape(-1, 1))

z = np.polyfit(years[y1 : y2 + 1], np.squeeze(trend), 1)
f = np.poly1d(z)
extra_x = f(years)

fig = plt.figure(figsize = (10, 5), dpi = 300)
for key, value in target_ts.iteritems():
    plt.plot(years, value, color = 'grey', linewidth = 0.5)
plt.plot(years, target_ts_ens1_mmm, 'black',
         label = "non-weighted MMM", linewidth = 3.0)
plt.fill_between(years, target_ts_ens1_mmm + target_std_ens1,
                 target_ts_ens1_mmm - target_std_ens1, facecolor = 'grey',
                 alpha = 0.4)

plt.plot(years[y1 : y2 + 1], trend, 'blue', linewidth = 1.0)
plt.plot(years, extra_x, 'blue', label = 'OLS trend', linewidth = 2.0)
plt.xlabel('Year')
plt.ylabel('%s %s %s [%s]' %(region, target_var, res_name_target, target_unit))
plt.grid(False)
leg = plt.legend(loc = 'upper left')  ## leg defines legend -> can be modified
leg.draw_frame(False)

plt.savefig('%stimeseries_lin_reg_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s.pdf' %(
        outdir, target_var, target_file, target_mask, diag_var[0],
        var_file[0], res_name[0], syear_hist, eyear_hist, syear_fut, eyear_fut, len(years)))

## save data to netcdf
outfile = '%sncdf/timeseries_lin_reg_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s.nc' %(
        outdir, target_var, target_file, target_mask, diag_var[0],
        var_file[0], res_name[0], syear_hist, eyear_hist, syear_fut, eyear_fut, len(years))
if (os.access(outdir + 'ncdf/', os.F_OK) == False):
        os.makedirs(outdir + 'ncdf/')

datesout = [dt.datetime(syear + x, 06, 01, 00) for x in range(0, ntim_tot)]
time = nc.date2num(datesout, units = 'days since %s' %(
        nc.datetime.strftime(dt.datetime(syear, 01, 01, 00),
                             '%Y-%m-%d_%H:%M:%S')), calendar = 'standard')

fout = nc.Dataset(outfile, mode = 'w')
fout.createDimension('time', len(years))
timeout = fout.createVariable('time', 'f8', ('time'), fill_value = 1e20)
setattr(timeout, 'units',
        'days since %s' %(
                nc.datetime.strftime(dt.datetime(syear, 01, 01, 00),
                                     '%Y-%m-%d_%H:%M:%S')))
mmout = fout.createVariable('mm_ts_areaavg', 'f8', ('time'), fill_value = 1e20)
setattr(mmout, 'Longname', 'multi-model mean timeseries')
setattr(mmout, 'units', target_unit)
setattr(mmout, 'description', 'area-averaged multi-model mean timeseries')

stdout = fout.createVariable('mm_std_areaavg', 'f8', ('time'),
                             fill_value = 1e20)
setattr(mmout, 'Longname', 'standard deviation')
setattr(mmout, 'units', '-')
setattr(mmout, 'description', 'area-averaged standard deviation over multiple models over time')

linout = fout.createVariable('linreg_ts_areaavg', 'f8', ('time'), fill_value = 1e20)
setattr(linout, 'Longname', 'linear regression based on TREND')
setattr(linout, 'units', '')
setattr(linout, 'description', 'linear regression based on TREND over %s-%s' %(
        syear_hist, eyear_hist))

mmout[:] = target_ts_ens1_mmm[:]
stdout[:] = target_std_ens1[:]
linout[:] = extra_x[:]
timeout[:] = time[:]

# Set global attributes
setattr(fout, "author", "Ruth Lorenz @IAC, ETH Zurich, Switzerland")
setattr(fout, "contact", "ruth.lorenz@env.ethz.ch")
setattr(fout, "creation date", dt.datetime.today().strftime('%Y-%m-%d'))
setattr(fout, "Script", "lin_reg_hist_TREND_to_fut_CMIP5_cdo.py")
setattr(fout, "Input files located in:", archive)
fout.close()

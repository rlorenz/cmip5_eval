#!/usr/bin/python
'''
File Name : mder_CMIP5_sklearn.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 14-02-2017
Modified: Mon 13 Feb 2017 09:14:59 CET
Purpose: Calculate randomized lasso for CMIP5 data
	 using sklearn python packages (http://scikit-learn.org)
         Scikit-learn: Machine Learning in Python, Pedregosa et al.,
                       JMLR 12, pp. 2825-2830, 2011.

'''
import numpy as np
import netCDF4 as nc
import glob as glob
import os
import math
from sklearn import linear_model
from sklearn.metrics import r2_score
#from sklearn.svm import SVC
from sklearn.model_selection import train_test_split
from sklearn.cross_validation import cross_val_score
from sklearn.feature_selection import RFE
from sklearn.linear_model import RandomizedLasso
from os.path import expanduser
home = expanduser("~") # Get users home directory
import sys
sys.path.insert(0,home+'/scripts/plot_scripts/utils/')
from func_read_data import func_read_netcdf
from func_write_netcdf import func_write_netcdf
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec

###
# Define input & output
###
experiment = 'rcp85'
archive = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/'
res_name_target = 'JJA'
freq = 'mon'

target_var = 'tasmax'
target_file = 'CLIM'
diag_var = ['tasmax', 'pr', 'rlus', 'rsds', 'huss', 'psl', 'tasmax', 'pr', 'rsds', 'hfls', 'huss', 'psl', 'tasmax', 'pr', 'rsds', 'hfls', 'huss', 'psl']  # NAM tasmax clim
#diag_var = ['tasmax', 'pr', 'huss', 'psl', 'tasmax', 'rsds', 'psl', 'tasmax', 'pr', 'rsds', 'psl', 'hfls']  # NAM tasmax clim
var_file = ['CLIM', 'CLIM', 'CLIM', 'CLIM','CLIM','CLIM','STD', 'STD', 'STD', 'STD', 'STD', 'STD', 'TREND', 'TREND', 'TREND', 'TREND', 'TREND', 'TREND'] #,'clim'
#var_file = ['CLIM', 'CLIM', 'CLIM', 'CLIM','STD', 'STD', 'STD', 'TREND', 'TREND', 'TREND', 'TREND', 'TREND']
res_name = ['JJA', 'JJA', 'JJA', 'JJA', 'JJA', 'JJA', 'JJA','JJA', 'JJA','JJA', 'JJA', 'JJA','JJA', 'JJA','JJA', 'JJA','JJA','JJA']
#res_name = ['JJA', 'JJA', 'JJA', 'JJA', 'JJA', 'JJA', 'JJA','JJA', 'JJA','JJA', 'JJA', 'JJA']

#diag_var = ['tasmax', 'rsds', 'rlus', 'pr', 'pr', 'psl', 'hfls'] # CNEU tasmax clim
#var_file = ['CLIM', 'CLIM','CLIM', 'CLIM', 'CLIM', 'STD', 'TREND'] #

#target_var = 'tasmax'
#target_file = 'STD'
#diag_var = ['tasmax', 'pr', 'rlus', 'rsds', 'hfls', 'huss','tasmax', 'pr', 'rlus', 'rsds', 'huss', 'tasmax', 'pr', 'rsds', 'hfls', 'huss'] # NAM tasmax std
#var_file = ['STD', 'STD', 'STD', 'STD', 'STD', 'STD', 'CLIM', 'CLIM', 'CLIM', 'CLIM','CLIM', 'TREND', 'TREND', 'TREND', 'TREND', 'TREND'] #,'clim'
#res_name = ['JJA', 'JJA', 'JJA', 'JJA', 'JJA', 'JJA', 'JJA', 'JJA','JJA', 'JJA', 'JJA', 'JJA', 'JJA', 'JJA', 'JJA', 'JJA']
#diag_var = ['tasmax', 'rlus', 'pr',  'hfls', 'rsds', 'hfls', 'pr', 'huss'] # CNEU tasmax std
#var_file = ['STD', 'STD', 'CLIM', 'CLIM', 'STD', 'STD', 'STD', 'STD'] #
#var_kind = ['seas', 'seas', 'seas', 'seas', 'seas', 'seas', 'seas', 'seas']

nvar = len(diag_var)

outdir = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/%s/Mult_Var_Lin_Reg/' %(target_var)
region = 'NAM'          #cut data over region?

syear_hist = 1980
eyear_hist = 2014
syear_fut = 2065
eyear_fut = 2099

nyears = eyear_hist - syear_hist + 1
grid = 'g025'

if (os.access(outdir, os.F_OK) == False):
        os.makedirs(outdir)

###read model data
print "Read model data"
##find all matching files in archive, loop over all of them
#first count matching files in folder
models_t = list()
model_names = list()
name = '%s/%s/%s/%s_%s_*_%s_r?i?p?_%s-%s_%sMEAN_%s_%s.nc' %(archive, target_var,
                                                            freq, target_var,
                                                            freq, experiment,
                                                            syear_fut,eyear_fut,
                                                            res_name_target,
                                                            target_file,
                                                            region)
nfiles_targ = len(glob.glob(name))
print str(nfiles_targ) + ' matching files found for target variable'
for filename in glob.glob(name):
        models_t.append(filename.split('_')[4] + ' ' + filename.split('_')[6])
overlap = models_t
for v in xrange(len(diag_var)):
        models_v = list()
        name_v = '%s/%s/%s/%s_%s_*_%s_r?i?p?_%s-%s_%sMEAN_%s_%s.nc' %(archive,
            diag_var[v], freq, diag_var[v],freq, experiment, syear_hist,
            eyear_hist, res_name[v], var_file[v], region)

        for filename in glob.glob(name_v):
                models_v.append(filename.split('_')[4] + ' ' +
                                filename.split('_')[6])
        #find overlapping files for all variables
        overlap = list(set(models_v) & set(overlap))
        del models_v
nfiles = len(overlap)
print str(nfiles) + ' matching files found for all variables'
target = np.empty((nfiles))
f = 0
for filename in glob.glob(name):
    model_name = filename.split('_')[4] + ' ' + filename.split('_')[6]
    if model_name in overlap:
            #print "Read "+filename+" data"
            fh = nc.Dataset(filename, mode = 'r')
            temp_fut = fh.variables[target_var][:] # global data, time, lat, lon
            lat = fh.variables['lat'][:]
            lon = fh.variables['lon'][:]
            tmp = fh.variables[target_var]
            target_unit = tmp.units
            try:
                Fill = tmp._FillValue
            except AttributeError:
                Fill = 1e+20
            fh.close()
            filename_hist = '%s/%s/%s/%s_%s_%s_%s_%s_%s-%s_%sMEAN_%s_%s.nc' %(archive, target_var, freq, target_var, freq, filename.split('_')[4], experiment, filename.split('_')[6], syear_hist, eyear_hist, res_name[v], target_file, region)
            fh = nc.Dataset(filename_hist, mode = 'r')
            temp_hist = fh.variables[target_var][:] # global data,time,lat,lon
            fh.close()

            temp_mod = temp_fut - temp_hist

            #check that time axis and grid is identical for model0 and modelX
            if f != 0:
                if temp_mod0.shape != temp_mod.shape:
                    print 'Warning: Dimension for models are different!'
                    continue
            else:
                temp_mod0 = temp_mod[:]

            model = filename.split('_')[5]
            if model == 'ACCESS1.3':
                    model = 'ACCESS1-3'
            elif model == 'FGOALS_g2':
                    model = 'FGOALS-g2'
            ens = filename.split('_')[6]
            model_names.append(model + '_' + ens)

            if isinstance(temp_mod, np.ma.core.MaskedArray):
                    #print type(temp_mod), temp_mod.shape
                    temp_mod = temp_mod.filled(np.NaN)

            if (target_var == 'sic') and  (model == "EC-EARTH"):
                    with np.errstate(invalid='ignore'):
                            temp_mod[temp_mod < 0.0] = np.NaN
            # average over area and save value for each model
            w_lat = np.cos(lat * (4.0 * math.atan(1.0) / 180))
            ma_target = np.ma.masked_array(temp_mod,
                                           np.isnan(temp_mod))
            tmp1_latweight = np.ma.average(np.squeeze(ma_target),
                                           axis = 0,
                                           weights = w_lat)
            target[f] = np.nanmean(tmp1_latweight.filled(np.nan))
            f = f + 1
    else:
            continue
 
data = np.empty((nfiles, nvar), float, Fill)
data_unit = list()
for v in xrange(len(diag_var)):
    name = '%s/%s/%s/%s_%s_*_%s_r?i?p?_%s-%s_%sMEAN_%s_%s.nc' %(archive,
                                                                diag_var[v],
                                                                freq,
                                                                diag_var[v],
                                                                freq,
                                                                experiment,
                                                                syear_hist,
                                                                eyear_hist,
                                                                res_name[v],
                                                                var_file[v],
                                                                region)
    f = 0
    for filename in glob.glob(name):
            model_name = filename.split('_')[4] + ' ' + filename.split('_')[6]
            if model_name in overlap:
                    #print "Read " + filename + " data"
                    fh = nc.Dataset(filename, mode = 'r')
                    lon = fh.variables['lon'][:]
                    lat = fh.variables['lat'][:]
                    temp_mod = fh.variables[diag_var[v]][:] # global data
                    #check that time axis and grid is identical for models
                    if f != 0:
                        if temp_mod0.shape != temp_mod.shape:
                            print 'Warning: Dimension for model0 and modelX is different!'
                            continue
                    else:
                        temp_mod0 = temp_mod[:]
                        tmp = fh.variables[diag_var[v]]
                        data_unit.append(tmp.units)
                    fh.close()
                    model = filename.split('_')[5]
                    if model == 'ACCESS1.3':
                            model = 'ACCESS1-3'
                    elif model == 'FGOALS_g2':
                            model = 'FGOALS-g2'
                    ens = filename.split('_')[6]
                    model_names.append(model + '_' + ens)

                    if isinstance(temp_mod, np.ma.core.MaskedArray):
                            print type(temp_mod), temp_mod.shape
                            temp_mod = temp_mod.filled(np.NaN)
                            #print type(temp_mod), temp_mod.shape

                    if (diag_var[v] == 'sic') and  (model == "EC-EARTH"):
                            with np.errstate(invalid = 'ignore'):
                                    temp_mod[temp_mod < 0.0] = np.NaN
                    # average over area and save value for each model
                    ma_hist = np.ma.masked_array(temp_mod,
                                                 np.isnan(temp_mod))
                    tmp2_latweight = np.ma.average(np.squeeze(ma_hist),
                                                   axis = 0,
                                                   weights = w_lat)
                    data[f, v] = np.nanmean(tmp2_latweight.filled(np.nan))
                    f = f + 1
            else:
                    continue

rlasso = RandomizedLasso(alpha = 0.05)
Labelrl = "RandomizedLasso"

rlasso.fit(data, target)

importances = rlasso.scores_
indices = np.argsort(importances)[::-1]

# Print the feature ranking
print("Feature ranking:")
for f in range(data.shape[1]):
    print("%d. feature %d (%s %s %f)" % (f + 1, indices[f], diag_var[indices[f]], var_file[indices[f]], importances[indices[f]]))

# Plot the feature importances of the forest
plt.figure()
plt.title("Feature importances")
plt.bar(range(data.shape[1]), importances[indices],
       color = "r", align = "center")
plt.xticks(range(data.shape[1]), indices)
plt.xlim([-1, data.shape[1]])
plt.xlabel('Feature')
plt.savefig('%sfeature_importance_rlasso_%s_change_%s_%s_%s_%s.pdf' %(outdir, Labelrl, target_var, target_file, res_name[0], region))  

del rlasso

rlasso = RandomizedLasso(alpha = 0.05)
rlasso.fit(data[:, 1:], data[:, 0])

importances = rlasso.scores_
indices = np.argsort(importances)[::-1]

# Print the feature ranking
print("Feature ranking:")
for f in range(data[:, 1:].shape[1]):
    print("%d. feature %d (%s %s %f)" % (f + 1, indices[f], diag_var[indices[f] + 1], var_file[indices[f] + 1], importances[indices[f]]))

# Plot the feature importances of the forest
plt.figure()
plt.title("Feature importances")
plt.bar(range(data[:, 1:].shape[1]), importances[indices],
       color = "r", align = "center")
plt.xticks(range(data[:, 1:].shape[1]), indices)
plt.xlim([-1, data[:, 1:].shape[1]])
plt.xlabel('Feature')
plt.savefig('%sfeature_importance_rlasso_%s_%s_%s_%s_%s.pdf' %(outdir, Labelrl, target_var, target_file, res_name[0], region))  

#!/usr/bin/python
'''
File Name : extrp_mult_diag_reg_hist_fut_CMIP5.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 15-01-2018
Modified: Mon 15 Jan 2018 10:47:33 AM CET
Purpose: calculate multiple diagnostics regression (linear, quadratic etc.)
         model over historical period and extrapolate into future.
         diagnostics calulated with cdo 
'''
import numpy as np
import netCDF4 as nc
from netcdftime import utime
import datetime as dt
import glob as glob
import os
import math

from sklearn import linear_model
from sklearn.cross_validation import cross_val_score, train_test_split
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import PolynomialFeatures

import matplotlib
import matplotlib.pyplot as plt

###
# Define input & output
###
experiment = 'rcp85'
archive = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/'
res_name_target = 'ANN'
freq = 'ann'

target_var = 'TXx'
target_file = 'TREND'
res_time_target = 'MAX'
target_mask = 'maskT'

diag_var = ['TXx']
var_file = ['TREND']
res_name = ['ANN']
res_time = ['MAX']
freq_v = ['ann']
masko = ['maskT']
nvar = len(diag_var)
pol_deg = 2

region = 'CNEU'          #cut data over region?
longname_region = 'Central Europe' # for title plot

path = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/'
outdir = '%s%s/Extrp_Reg/%s/' %(path, target_var, region)

syear_hist = 1950
eyear_hist = 1999
syear_fut = 2050
eyear_fut = 2099
syear = 1950
eyear = 2100
nyears = eyear_hist - syear_hist + 1
ntim_tot = eyear - syear + 1

grid = 'g025'
Fill = 1e+20

if (os.access(outdir, os.F_OK) == False):
        os.makedirs(outdir)

###read model data
print "Read model data"
## find all matching files in archive, loop over all of them

# first count matching files in folder
models_t = list()
model_names = list()

path_target = '%s/%s/%s/%s/' %(archive, target_var, freq, target_mask)
if region:
    name = '%s%s_%s_*_%s_r?i?p?_%s-%s_%s%s_%s_%s.nc' %(
        path_target, target_var, freq, experiment, syear_fut, eyear_fut,
        res_name_target, res_time_target, target_file, region)
else:
    name = '%s/%s_%s_*_%s_r?i?p?_%s-%s_%s%s_%s.nc' %(
        path_target, target_var, freq, experiment, syear_fut, eyear_fut,
        res_name_target, res_time_target, target_file)

nfiles_targ = len(glob.glob(name))
print str(nfiles_targ) + ' matching files found for target variable'

for filename in glob.glob(name):
    models_t.append(filename.split('_')[4] + ' ' + filename.split('_')[6])

overlap = models_t

for v in xrange(len(diag_var)):
    models_v = list()
    path_var = '%s/%s/%s/%s/' %(archive, diag_var[v], freq_v[v], masko[v])
    if region:
        name_v = '%s%s_%s_*_%s_r?i?p?_%s-%s_%s%s_%s_%s.nc' %(
            path_var, diag_var[v], freq_v[v], experiment, syear_hist,
            eyear_hist, res_name[v], res_time[v], var_file[v], region)
    else:
        name_v = '%s%s_%s_*_%s_r?i?p?_%s-%s_%s%s_%s.nc' %(
            path_var, diag_var[v], freq_v[v], experiment, syear_hist,
            eyear_hist, res_name[v], res_time[v], var_file[v])

    for filename in glob.glob(name_v):
        models_v.append(filename.split('_')[4] + ' ' +
                        filename.split('_')[6])
    #find overlapping files for all variables
    overlap = list(set(models_v) & set(overlap))
    del models_v, path_var
nfiles = len(overlap)
print str(nfiles) + ' matching files found for all variables'

target = np.empty((nfiles))
target_hist = np.empty((nfiles))
target_ts = np.empty((nfiles, ntim_tot), float, Fill)
f = 0
for filename in glob.glob(name):
    model_name = filename.split('_')[4] + ' ' + filename.split('_')[6]
    if model_name in overlap:
        #print "Read "+filename+" data"
        fh = nc.Dataset(filename, mode = 'r')
        temp_fut = fh.variables[target_var][:] # global data, time, lat, lon
        lat = fh.variables['lat'][:]
        lon = fh.variables['lon'][:]
        tmp = fh.variables[target_var]
        target_unit = tmp.units
        fh.close()
        if (target_file != 'SCALE'):
            path_hist = '%s/%s/%s/%s/' %(archive, target_var, freq, target_mask)
            if region:
                filename_hist = '%s%s_%s_%s_%s_%s_%s-%s_%s%s_%s_%s.nc' %(
                    path_hist, target_var, freq, filename.split('_')[4],
                    experiment, filename.split('_')[6], syear_hist, eyear_hist,
                    res_name_target, res_time_target, target_file, region)
            else:
                filename_hist = '%s/%s_%s_%s_%s_%s_%s-%s_%s%s_%s.nc' %(
                    path_hist, target_var, freq, target_mask,
                    target_var, freq, filename.split('_')[4],
                    experiment, filename.split('_')[6], syear_hist, eyear_hist,
                    res_name_target, res_time_target, target_file)
            fh = nc.Dataset(filename_hist, mode = 'r')
            temp_hist = fh.variables[target_var][:] # global data,time,lat,lon
            fh.close()
            temp_mod = temp_fut - temp_hist
        else:
            temp_mod = temp_fut

        #check that time axis and grid is identical for model0 and modelX
        if f != 0:
            if temp_mod0.shape != temp_mod.shape:
                print 'Warning: Dimension for models are different!'
                continue
        else:
            temp_mod0 = temp_mod[:]

        model = filename.split('_')[4]
        if model == 'ACCESS1.3':
            model = 'ACCESS1-3'
        elif model == 'FGOALS_g2':
            model = 'FGOALS-g2'
        ens = filename.split('_')[6]
        model_names.append(model + '_' + ens)

        if isinstance(temp_mod, np.ma.core.MaskedArray):
            temp_mod = temp_mod.filled(np.NaN)

        # average over area and save value for each model
        w_lat = np.cos(lat * (4.0 * math.atan(1.0) / 180))
        ma_target = np.ma.masked_array(temp_mod,
                                       np.isnan(temp_mod))
        tmp1_latweight = np.ma.average(np.squeeze(ma_target),
                                       axis = 0,
                                       weights = w_lat)
        target[f] = np.nanmean(tmp1_latweight.filled(np.nan))


        # read data over whole timeseries for plotting
        path_ts = '%s/%s/%s/%s/' %(archive, target_var, freq, target_mask)
        if region:
            filename_ts = '%s%s_%s_%s_%s_%s_%s-%s_%s%s_%s.nc' %(
                path_ts, target_var, freq, filename.split('_')[4],
                experiment, filename.split('_')[6], syear, eyear,
                res_name_target, res_time_target, region)
        else:
            filename_ts = '%s/%s_%s_%s_%s_%s_%s-%s_%s%s.nc' %(
                path_ts, target_var, freq, target_mask,
                target_var, freq, filename.split('_')[4],
                experiment, filename.split('_')[6], syear, eyear,
                res_name_target, res_time_target)
        fh = nc.Dataset(filename_ts, mode = 'r')
        temp_ts = fh.variables[target_var][:] # global data,time,lat,lon
        time = fh.variables['time']
        cdftime = utime(time.units, calendar = time.calendar)
        dates = cdftime.num2date(time[:])
        years = np.asarray([dates[i].year for i in xrange(len(dates))])
        fh.close()
        tmp_ts_latweight = np.ma.average(np.squeeze(temp_ts), axis = 1,
                                         weights = w_lat)
        target_ts[f, :] = np.nanmean(tmp_ts_latweight.filled(np.nan), axis = 1)
        del tmp_ts_latweight
        f = f + 1
    else:
        continue
if target_unit == 'degC':
    degree_sign = u'\N{DEGREE SIGN}'
    target_unit = degree_sign + "C"

data = np.empty((nfiles, nvar), float, Fill)
data_ts = np.empty((nfiles, nvar, ntim_tot), float, Fill)
data_unit = list()
for v in xrange(len(diag_var)):
    path_var = '%s/%s/%s/%s/' %(archive, diag_var[v], freq_v[v], masko[v])
    if region:
        name = '%s%s_%s_*_%s_r?i?p?_%s-%s_%s%s_%s_%s.nc' %(
            path_var, diag_var[v], freq_v[v], experiment, syear_hist, 
            eyear_hist, res_name[v], res_time[v], var_file[v], region)
    else:
        name = '%s%s_%s_*_%s_r?i?p?_%s-%s_%s%s_%s.nc' %(
            path_var, diag_var[v], freq_v[v], experiment, syear_hist,
            eyear_hist, res_name[v], res_time[v], var_file[v]) 
    f = 0
    for filename in glob.glob(name):
        model_name = filename.split('_')[4] + ' ' + filename.split('_')[6]
        model_key = filename.split('_')[4] + '_' + filename.split('_')[6]
        if model_name in overlap:
            #print "Read " + filename + " data"
            fh = nc.Dataset(filename, mode = 'r')
            lon = fh.variables['lon'][:]
            lat = fh.variables['lat'][:]
            temp_mod = fh.variables[diag_var[v]][:] # global data
            #check that time axis and grid is identical for models
            if f != 0:
                if temp_mod0.shape != temp_mod.shape:
                    print('Warning: Dimension for model0 and modelX ' +
                          'is different!')
                    continue
            else:
                temp_mod0 = temp_mod[:]
                tmp = fh.variables[diag_var[v]]
                data_unit.append(tmp.units)
            fh.close()

            if isinstance(temp_mod, np.ma.core.MaskedArray):
                #print type(temp_mod), temp_mod.shape
                temp_mod = temp_mod.filled(np.NaN)

            # average over area and save value for each model
            w_lat = np.cos(lat * (4.0 * math.atan(1.0) / 180))
            ma_hist = np.ma.masked_array(temp_mod,
                                         np.isnan(temp_mod))
            tmp2_latweight = np.ma.average(np.squeeze(ma_hist), axis = 0,
                                           weights = w_lat)
            data[f, v] = np.nanmean(tmp2_latweight.filled(np.nan))

            # read data over whole timeseries for plotting
            path_ts = '%s/%s/%s/%s/' %(archive, diag_var[v], freq_v[v],
                                       masko[v])
            if region:
                filename_ts = '%s%s_%s_%s_%s_%s_%s-%s_%s%s_%s.nc' %(
                    path_ts, diag_var[v], freq_v[v], filename.split('_')[4],
                    experiment, filename.split('_')[6], syear, eyear,
                    res_name[v], res_time[v], region)
            else:
                filename_ts = '%s%s_%s_%s_%s_%s_%s-%s_%s%s.nc' %(
                    path_ts, diag_var[v], freq_v[v], filename.split('_')[4],
                    experiment, filename.split('_')[6], syear, eyear,
                    res_name[v], res_time[v])
            fh = nc.Dataset(filename_ts, mode = 'r')
            temp_ts = fh.variables[diag_var[v]][:] # global data,time,lat,lon
            time = fh.variables['time']
            cdftime = utime(time.units, calendar = time.calendar)
            dates = cdftime.num2date(time[:])
            years = np.asarray([dates[i].year for i in xrange(len(dates))])
            fh.close()
            tmp_ts_latweight = np.ma.average(np.squeeze(temp_ts), axis = 1,
                                             weights = w_lat)
            data_ts[f, v, :] = np.nanmean(tmp_ts_latweight.filled(np.nan),
                                          axis = 1)
            f = f + 1
        else:
            continue

if data_unit[0] == 'degC':
    data_unit[0] = degree_sign + "C"

# Split the dataset in two parts, historical for training, future for testing
target_ts_hist = np.empty((nfiles, nyears), float, Fill)
target_ts_fut = np.empty((nfiles, nyears), float, Fill)
data_ts_hist = np.empty((nfiles, nvar, nyears), float, Fill)
data_ts_fut = np.empty((nfiles, nvar, nyears), float, Fill)

y1 = np.where(years == syear_hist)[0][0]
y2 = np.where(years == eyear_hist)[0][0]
y3 = np.where(years == syear_fut)[0][0]
y4 = np.where(years == eyear_fut)[0][0]

for f in xrange(nfiles):
    target_ts_hist[f, :] = target_ts[f, y1 : y2 + 1]
    target_ts_fut[f, :] = target_ts[f, y3 : y4 + 1]
    for v in xrange(len(diag_var)):
        data_ts_hist[f, v, :] = data_ts[f, v, y1 : y2 + 1]
        data_ts_fut[f, v, :] = data_ts[f, v, y3 : y4 + 1]

X_train = years[y1 : y2 + 1].reshape(-1, 1)
X_test = years[y3 : y4 + 1].reshape(-1, 1)
y_train = np.transpose(target_ts_hist)
y_test = np.transpose(target_ts_fut)

# fit regressions
ols = linear_model.LinearRegression(normalize = True)
#ols.fit(X_train, y_train)
#ols_lin_pred = ols.predict(X_test)
polynomial_features = PolynomialFeatures(degree = pol_deg, include_bias = False)
pipeline = Pipeline([("polynomial_features", polynomial_features),
                     ("linear_regression", ols)])
pipeline.fit(X_train, y_train)
ols_lin_pred = pipeline.predict(X_test)

print("The model is trained on the full training set (historical period).")
print("The scores are computed on the full test set (future time period).")
print()
#OLS
print('OLS')
# The coefficients
#print('Coefficients OLS: \n', ols.coef_)
# The mean square error
print("Residual sum of squares OLS: %.2f"
      % np.mean((ols_lin_pred - y_test) ** 2))
# Explained variance score: 1 is perfect prediction
if pol_deg == 1:
    print('Variance score OLS: %.2f' % ols.score(X_test, y_test))
#CrossValidation
cv = 5
scoresOLS = cross_val_score(ols, X_test, y_test, cv = cv)
print("Accuracy OLS: %0.2f (+/- %0.2f)" % (scoresOLS.mean(),
                                           scoresOLS.std() * 2))
for mod in xrange(len(model_names)):
    plt.scatter(X_train, y_train[:, mod])
plt.plot(X_test, ols_lin_pred)
plt.show()

#z = np.polyfit(years[y1 : y2 + 1], np.squeeze(ols_lin_pred), 1)
#f = np.poly1d(z)
#extra_x = f(years)

## mult var lin reg for diag_var (not ts)
X_train, X_test, y_train, y_test = train_test_split(
    data, np.squeeze(target.reshape(-1, 1)), test_size = 0.5, random_state = 10)
pipeline.fit(X_train, y_train)
ols_lin_pred = pipeline.predict(X_test)

print("The model is trained on the full training set (train_test_split).")
print("The scores are computed on the full test set.")
print()
#OLS
print('OLS')
# The coefficients
#print('Coefficients OLS: \n', ols.coef_)
# The mean square error
print("Residual sum of squares OLS: %.2f"
      % np.mean((ols_lin_pred - y_test) ** 2))
if pol_deg == 1:
# Explained variance score: 1 is perfect prediction
    print('Variance score OLS: %.2f' % ols.score(X_test, y_test))
#CrossValidation
cv = 5
scoresOLS = cross_val_score(ols, X_test, y_test, cv = cv)
print("Accuracy OLS: %0.2f (+/- %0.2f)" % (scoresOLS.mean(),
                                           scoresOLS.std() * 2))
print()
plt.scatter(data[:, 0], target, color = 'k')
plt.scatter(X_train[:, 0], y_train)
plt.plot(X_test, ols_lin_pred)
plt.show()

#!/usr/bin/python
'''
File Name : mder_CMIP5_sklearn.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 01-07-2016
Modified: Mon 12 Dec 2016 22:26:08 CET
Purpose: Calculate multivariate linear regression for CMIP5 data
	 using sklearn python packages (http://scikit-learn.org)
         Scikit-learn: Machine Learning in Python, Pedregosa et al.,
                       JMLR 12, pp. 2825-2830, 2011.

'''
import numpy as np
import netCDF4 as nc
import glob as glob
import os
from sklearn import linear_model
from sklearn.metrics import r2_score
#from sklearn.svm import SVC
from sklearn.cross_validation import cross_val_score
from sklearn.feature_selection import RFECV
from os.path import expanduser
home = expanduser("~") # Get users home directory
import sys
sys.path.insert(0,home+'/scripts/plot_scripts/utils/')
from func_read_data import func_read_netcdf
from func_write_netcdf import func_write_netcdf
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from matplotlib import rc

rc('text', usetex = True)
rc('text.latex', preamble = '\usepackage{color}')
###
# Define input & output
###
experiment = 'rcp85'
archive = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/'
target_var = 'tasmax'
target_file = 'clim'
target_kind = 'seas'
diag_var = ['tasmax', 'tasmax', 'hfls', 'rlus', 'rlus', 'rsds', 'pr', 'pr']  # NAM tasmax clim
var_file = ['clim', 'trnd', 'clim', 'clim', 'trnd', 'clim', 'clim', 'std'] #,'clim'
var_kind = ['seas', 'seas', 'seas', 'seas', 'seas', 'seas', 'seas', 'seas'] #,'seas'
#diag_var = ['tasmax', 'rsds', 'rlus', 'pr', 'pr', 'psl', 'hfls'] # CNEU tasmax clim
#var_file = ['clim', 'clim','clim', 'clim', 'std', 'std', 'trnd'] #
#var_kind = ['seas', 'seas','seas','seas','seas','seas','seas']

#target_var = 'tasmax'
#target_file = 'std'
#target_kind = 'seas'
#diag_var = ['tasmax','hfls', 'rlus','rlus', 'rsds', 'pr', 'psl', 'huss'] # NAM tasmax std
#var_file = ['std','std','clim', 'std','clim', 'clim', 'std', 'std'] #,'clim'
#var_kind = ['seas','seas','seas','seas','seas','seas','seas', 'seas'] #,'seas'
#diag_var = ['tasmax', 'rlus', 'pr',  'hfls', 'rsds', 'hfls', 'pr', 'huss'] # CNEU tasmax std
#var_file = ['std', 'std', 'clim', 'clim', 'std', 'std', 'std', 'std'] #
#var_kind = ['seas', 'seas', 'seas', 'seas', 'seas', 'seas', 'seas', 'seas']

nvar = len(diag_var)

outdir = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/%s/Mult_Var_Lin_Reg/' %(target_var)
region = 'NAM'          #cut data over region?

syear_hist = 1980
eyear_hist = 2014
syear_fut = 2065
eyear_fut = 2099

nyears = eyear_hist - syear_hist + 1
grid = 'g025'

if (os.access(outdir, os.F_OK) == False):
        os.makedirs(outdir)

###read model data
print "Read model data"
##find all matching files in archive, loop over all of them
#first count matching files in folder
models_t = list()
model_names = list()
name = '%s/%s/%s_%s_%s_*_r?i?p?_%s_%s_%s-%s.nc' %(archive, target_var,
                                                  target_var, target_file,
                                                  target_kind, region,
                                                  experiment, syear_fut,
                                                  eyear_fut)
nfiles_targ = len(glob.glob(name))
print str(nfiles_targ) + ' matching files found for target variable'
for filename in glob.glob(name):
        models_t.append(filename.split('_')[5] + ' ' + filename.split('_')[6])
overlap = models_t
for v in xrange(len(diag_var)):
        models_v = list()
        name_v = '%s/%s/%s_%s_%s_*_r?i?p?_%s_%s_%s-%s.nc' %(archive,
                                                            diag_var[v],
                                                            diag_var[v],
                                                            var_file[v],
                                                            var_kind[v],
                                                            region, experiment,
                                                            syear_hist,
                                                            eyear_hist)
        for filename in glob.glob(name_v):
                models_v.append(filename.split('_')[5] + ' ' +
                                filename.split('_')[6])
        #find overlapping files for all variables
        overlap = list(set(models_v) & set(overlap))
        del models_v
nfiles = len(overlap)
print str(nfiles) + ' matching files found for all variables'
target = np.empty((nfiles))
f = 0
for filename in glob.glob(name):
    model_name = filename.split('_')[5] + ' ' + filename.split('_')[6]
    if model_name in overlap:
            #print "Read "+filename+" data"
            fh = nc.Dataset(filename, mode = 'r')
            temp_fut = fh.variables[target_var][:] # global data, time, lat, lon
            lat = fh.variables['lat']
            lon = fh.variables['lon']
            tmp = fh.variables[target_var]
            target_unit = tmp.units
            Fill = tmp._FillValue
            fh.close()

            filename_hist = '%s/%s/%s_%s_%s_%s_%s_%s_%s_%s-%s.nc' %(archive,
                                                                    target_var,
                                                                    target_var,
                                                                    target_file,
                                                                    target_kind,
                                                                    filename.split('_')[5],
                                                                    filename.split('_')[6],
                                                                    region,
                                                                    experiment,
                                                                    syear_hist,
                                                                    eyear_hist)
            fh = nc.Dataset(filename_hist, mode = 'r')
            temp_hist = fh.variables[target_var][:] # global data,time,lat,lon
            fh.close()

            temp_mod = temp_fut - temp_hist

            #check that time axis and grid is identical for model0 and modelX
            if f != 0:
                if temp_mod0.shape != temp_mod.shape:
                    print 'Warning: Dimension for models are different!'
                    continue
            else:
                temp_mod0 = temp_mod[:]

            model = filename.split('_')[5]
            if model == 'ACCESS1.3':
                    model = 'ACCESS1-3'
            elif model == 'FGOALS_g2':
                    model = 'FGOALS-g2'
            ens = filename.split('_')[6]
            model_names.append(model + '_' + ens)

            if isinstance(temp_mod, np.ma.core.MaskedArray):
                    #print type(temp_mod), temp_mod.shape
                    temp_mod = temp_mod.filled(np.NaN)

            if (target_var == 'sic') and  (model == "EC-EARTH"):
                    with np.errstate(invalid='ignore'):
                            temp_mod[temp_mod < 0.0] = np.NaN
            # average over area and save value for each model
            target[f] = np.nanmean(temp_mod)
            f = f + 1
    else:
            continue

data = np.empty((nfiles, nvar), float, Fill)
data_unit = list()
for v in xrange(len(diag_var)):
    name = '%s/%s/%s_%s_%s_*_r?i?p?_%s_%s_%s-%s.nc' %(archive, diag_var[v],
                                                      diag_var[v], var_file[v],
                                                      var_kind[v], region,
                                                      experiment, syear_hist,
                                                      eyear_hist)
    f = 0
    for filename in glob.glob(name):
            model_name = filename.split('_')[5] + ' ' + filename.split('_')[6]
            if model_name in overlap:
                    #print "Read " + filename + " data"
                    fh = nc.Dataset(filename, mode = 'r')
                    lon = fh.variables['lon'][:]
                    lat = fh.variables['lat'][:]
                    temp_mod = fh.variables[diag_var[v]][:] # global data
                    #check that time axis and grid is identical for models
                    if f != 0:
                        if temp_mod0.shape != temp_mod.shape:
                            print 'Warning: Dimension for model0 and modelX is different!'
                            continue
                    else:
                        temp_mod0 = temp_mod[:]
                        tmp = fh.variables[diag_var[v]]
                        data_unit.append(tmp.units)
                    fh.close()
                    model = filename.split('_')[5]
                    if model == 'ACCESS1.3':
                            model = 'ACCESS1-3'
                    elif model == 'FGOALS_g2':
                            model = 'FGOALS-g2'
                    ens = filename.split('_')[6]
                    model_names.append(model + '_' + ens)

                    if isinstance(temp_mod, np.ma.core.MaskedArray):
                            print type(temp_mod), temp_mod.shape
                            temp_mod = temp_mod.filled(np.NaN)
                            #print type(temp_mod), temp_mod.shape

                    if (diag_var[v] == 'sic') and  (model == "EC-EARTH"):
                            with np.errstate(invalid = 'ignore'):
                                    temp_mod[temp_mod < 0.0] = np.NaN
                    # average over area and save value for each model
                    data[f, v] = np.nanmean(temp_mod)
                    f = f + 1
            else:
                    continue

ols = linear_model.LinearRegression(normalize = True) 
Labelols = "OLS"
#clf = linear_model.Ridge(normalize = True)
#Labelclf = "Ridge"
#clf = linear_model.RidgeCV(normalize = True)
#Labelclf = "RidgeCV"
clf = linear_model.BayesianRidge(compute_score = True)
Labelclf = "BayesianRidge"

colors = ['purple', 'royalblue', 'seagreen', 'darkorange', 'teal',
          'deepskyblue', 'deeppink', 'darkgrey', 'gold', 'grey']
fig = plt.figure(figsize = (25, 25))
gs = gridspec.GridSpec(nvar, nvar)
#keep same fonts for LaTeX R^2
matplotlib.rcParams['mathtext.fontset'] = 'custom'
matplotlib.rcParams['mathtext.rm'] = 'Bitstream Vera Sans'
matplotlib.rcParams['mathtext.it'] = 'Bitstream Vera Sans:italic'
matplotlib.rcParams['mathtext.bf'] = 'Bitstream Vera Sans:bold'
matplotlib.rcParams['xtick.labelsize'] = 10
matplotlib.rcParams['ytick.labelsize'] = 10
for v in range(nvar):
        clf.fit(data[:, v].reshape(-1, 1), target.reshape(-1, 1))
        r2_clf = r2_score(target.reshape(-1, 1),
                      clf.predict(data[:, v].reshape(-1, 1)))
        ols.fit(data[:, v].reshape(-1, 1), target.reshape(-1, 1))
        r2_ols = r2_score(target.reshape(-1, 1),
                      ols.predict(data[:, v].reshape(-1, 1)))
        # The coefficients
        print('Coefficients: \n', clf.coef_)
        # The mean square error
        print("Residual sum of squares: %.2f"
              % np.mean((clf.predict(data[:,v].reshape(-1, 1)) - target.reshape(-1, 1)) ** 2))
        print("R^2: %.2f" %(r2_clf))
        # Explained variance score: 1 is perfect prediction
        print('Variance score: %.2f' % clf.score(data[:,v].reshape(-1, 1), target.reshape(-1, 1)))

        # Plot outputs
        ax = plt.subplot(gs[0, v])
        ax.scatter(data[:, v], target,  color = colors[v], label = 'True')
        ax.plot(data[:, v].reshape(-1, 1),
                ols.predict(data[:, v].reshape(-1, 1)), color = 'gray',
                linewidth = 3, label = 'Predicted')
        ax.plot(data[:, v].reshape(-1, 1),
                clf.predict(data[:, v].reshape(-1, 1)), color = 'black',
                linewidth = 3, label = 'Predicted')

        ax.text(0.3, 1.048, 'R$^2$: %.2f,' %(r2_clf),
                horizontalalignment = 'center', verticalalignment = 'center',
                transform=ax.transAxes, fontsize = 12)
        ax.text(0.7, 1.034, '{R$_{OLS}^{2}$: %.2f}' %(r2_ols),
                horizontalalignment = 'center', verticalalignment = 'center',
                transform=ax.transAxes, fontsize = 12, color = 'gray')
        ax.set_xlabel(diag_var[v] + ' ' + var_file[v] + ' ' + var_kind[v] +
                      ' [' + data_unit[v] + ']')
        ax.set_ylabel(target_var + ' ' +target_file + ' ' + target_kind +
                      ' [' + target_unit + ']')
        #plt.xticks(())
        #plt.yticks(())
        #plt.show()

        for x in range(nvar - (v + 1)):
                print v, x
                clf.fit(data[:, x + v + 1].reshape(-1, 1),
                        data[:, v].reshape(-1, 1))
                ols.fit(data[:, x + v + 1].reshape(-1, 1),
                        data[:, v].reshape(-1, 1))
                r2_clf = r2_score(data[:, v].reshape(-1, 1),
                                  clf.predict(data[:, x + v + 1].reshape(-1, 1)))
                r2_ols = r2_score(data[:, v].reshape(-1, 1),
                                  ols.predict(data[:, x + v + 1].reshape(-1, 1)))                
                # The coefficients
                print('Coefficients: \n', clf.coef_)
                # The mean square error
                print("Residual sum of squares: %.2f"
                      % np.mean((clf.predict(data[:, x + v + 1].reshape(-1, 1)) 
                                 - data[:, v].reshape(-1, 1)) ** 2))
                print("R^2: %.2f" %(r2_clf))
                # Explained variance score: 1 is perfect prediction
                print('Variance score: %.2f' % clf.score(data[:, x + v + 1].reshape(-1, 1), data[:, v].reshape(-1, 1)))

                # Plot outputs
                ax2 = plt.subplot(gs[v + 1, x + v + 1])
                ax2.scatter(data[:, x + v + 1], data[:, v],  color = colors[v],
                            label = 'True')
                ax2.plot(data[:, x + v + 1].reshape(-1, 1),
                         ols.predict(data[:, x + v + 1].reshape(-1, 1)),
                         color = 'gray',
                         linewidth = 3, label = 'Predicted')
                ax2.plot(data[:, x + v + 1].reshape(-1, 1),
                         clf.predict(data[:, x + v + 1].reshape(-1, 1)),
                         color = 'black', linewidth = 3, label = 'Predicted')

                #ax2.set_title(r"R$^2$: %.2f" % (r2_clf), fontsize = 12)
                ax2.text(0.3, 1.048, 'R$^2$: %.2f,' %(r2_clf),
                        horizontalalignment = 'center',
                        verticalalignment = 'center',
                        transform=ax2.transAxes, fontsize = 12)
                ax2.text(0.7, 1.034, '{R$_{OLS}^{2}$: %.2f}' %(r2_ols),
                        horizontalalignment = 'center',
                        verticalalignment = 'center',
                        transform=ax2.transAxes, fontsize = 12, color = 'gray')

                ax2.set_xlabel(diag_var[x + v + 1] + ' ' + var_file[x + v + 1]
                               + ' ' + var_kind[x + v + 1] + ' [' +
                               data_unit[x + v + 1] + ']')
                ax2.set_ylabel(diag_var[v] + ' ' + var_file[v] + ' ' +
                               var_kind[v] + ' [' + data_unit[v] + ']')
                #plt.xticks(())
                #plt.yticks(())
#plt.legend(loc='best')
plt.tight_layout()
#plt.show()
plt.savefig('%spanel_regressions_%s_change_%s_%s_%s_%s.pdf' %(outdir, Labelclf,
                                                              target_var,
                                                              target_file,
                                                              target_kind,
                                                              region))     

ols.fit(data, target.reshape(-1, 1))
clf.fit(data, target.reshape(-1, 1))

# The coefficients
print('Coefficients OLS: \n', ols.coef_)
print('Coefficients: \n', clf.coef_)
# The mean square error
print("Residual sum of squares OLS: %.2f"
      % np.mean((ols.predict(data) - target.reshape(-1, 1)) ** 2))
print("Residual sum of squares: %.2f"
      % np.mean((clf.predict(data) - target.reshape(-1, 1)) ** 2))
# Explained variance score: 1 is perfect prediction
print('Variance score OLS: %.2f' % ols.score(data, target.reshape(-1, 1)))
print('Variance score: %.2f' % clf.score(data, target.reshape(-1, 1)))


# Plot outputs
for v in range(nvar):
        plt.scatter(data[:, v], target,  color = colors[v])
plt.plot(data, clf.predict(data), color = 'blue',
         linewidth = 3)

#plt.xticks(())
#plt.yticks(())
plt.show()

#CrossValidation
cv = 5
scoresOLS = cross_val_score(ols, data, target, cv = cv)
print("Accuracy OLS: %0.2f (+/- %0.2f)" % (scoresOLS.mean(),
                                           scoresOLS.std() * 2))
scores = cross_val_score(clf, data, target, cv = cv)
print("Accuracy: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))

rfecv = RFECV(estimator = ols, step = 1)
rfecv.fit(data, target)

print("Optimal number of features OLS: %d" % rfecv.n_features_)

rfecv = RFECV(estimator = clf, step = 1)
rfecv.fit(data, target)

print("Optimal number of features : %d" % rfecv.n_features_)

plt.figure()
plt.xlabel("Number of features selected")
plt.ylabel("Cross validation score (nb of correct classifications)")
plt.plot(range(1, len(rfecv.grid_scores_) + 1), rfecv.grid_scores_)
#plt.show()
plt.savefig('%sscatter_%s_change_%s_%s_%s_%s.pdf' %(outdir, Labelclf,
                                                    target_var, target_file,
                                                    target_kind, region))  

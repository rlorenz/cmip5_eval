#!/usr/bin/python
'''
File Name : calc_rmse_for_weight_beyond_democracy.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 06-04-2016
Modified: Thu 15 Dec 2016 00:01:24 CET
Purpose: Script calculating rmse for further use in
         weight_mm_beyond_democracy_cmip5-ng.py
'''
import netCDF4 as nc # to work with NetCDF files
import numpy as np
from scipy import signal
import os # operating system interface
from os.path import expanduser
home = expanduser("~") # Get users home directory
import glob
import sys
sys.path.insert(0,home+'/scripts/plot_scripts/utils/')
#from calc_RMSE_obs_mod_3D import rmse_3D
from func_read_data import func_read_netcdf
from func_calc_rmse import func_calc_rmse
from perkins_skill import perkins_skill
from clim_seas_TLL import clim_mon_TLL, std_mon_TLL, seas_avg_mon_TLL, seas_std_mon_TLL, clim_seas_mon_TLL, ann_avg_mon_TLL
from calc_trend_TLL import trend_TLL
from point_inside_polygon import point_inside_polygon
import datetime as dt
from netcdftime import utime
from func_write_netcdf import func_write_netcdf
from mpl_toolkits.basemap import shiftgrid
###
# Define input
###
## calculate variables separately,
## weighting can then be done based on multiple variables
variable = ['tasmax', 'tasmax', 'tasmax']
## climatology:clim, variability:std, trend:trnd
var_file = ['clim', 'std', 'trnd']
## kind is cyc: annual cycle, mon: monthly means, seas: seasonal means
var_kind = ['seas', 'seas', 'seas']
## weight of individual fields, all equal weight 1 at the moment     
fields_weight = [1, 1, 1]
## choose data for particular month or season?
res = [2, 2, 2]
## cut data over region?        
region = 'EUR'

if (variable[0] != 'ef') and (variable[0] != 'lwnet'):
    archive = '/net/atmos/data/cmip5-ng'
else:
    archive = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/%s/mon/' %(variable[0])
outdir = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/%s/' %(variable[0])
infile = variable[0] + '_mon'
syear = 1980
eyear = 2014
nyears = eyear - syear + 1

experiment = 'rcp85'
grid = 'g025'


obsdata = 'MERRA2' # ERAint, MERRA2, Obs (depends on variable e.g. HadGHCND etc)

if (os.access(outdir, os.F_OK) == False):
        os.makedirs(outdir)

## read obs data
if (obsdata == 'ERAint'):
        print "Read ERAint data"
        path = '/net/tropo/climphys/rlorenz/Datasets/ERAint/ERAint_mon'

        if (variable[0] == 'tas'):
            temp = func_read_netcdf('%s/T2M_monthly_ERAint_197901-201601_%s.nc' 
                                    %(path, grid), 'T2M', var_units = True,
                                    syear = syear, eyear = eyear)
            temp_obs = temp['data']
            obs_unit = temp['unit']
        elif (variable[0] == 'pr'):
            temp = func_read_netcdf('%s/RTOT_monthly_ERAint_197901-201412_%s.nc'
                                    %(path, grid), 'RTOT', var_units = True,
                                    syear = syear, eyear = eyear)
            temp_obs = temp['data']
            obs_unit = temp['unit']
            if (obs_unit == 'kg m-2 s-1'):
                ## convert to mm/day
                tmp_obs = temp_obs * (24 * 60 * 60)
                temp_obs = tmp_obs[:]
                del tmp_obs
                obs_unit = 'mm/day'
        elif (variable[0] == 'sic'):
            temp = func_read_netcdf('%s/ci_monthly_ERAint_197901-201601_%s.nc'
                                    %(path, grid), 'ci', syear = syear,
                                    eyear = eyear)
            ## ERAint data is in fraction -> convert to percent as in CMIP5
            temp_obs = temp['data'] * 100
            obs_unit = 'percent'
        elif (variable[0] == 'hfls'):
            temp = func_read_netcdf('%s/slhf_monthly_ERAint_197901-201512_%s.nc'
                                    %(path, grid), 'slhf', var_units = True,
                                    syear = syear, eyear = eyear)
            temp_obs = -temp['data']
            obs_unit = temp['unit']
        elif (variable[0] == 'tasmax'):
            temp = func_read_netcdf('%s/mx2t_monmean_ERAint_197901-201512_%s.nc'
                                    %(path, grid), 'mx2t', var_units = True,
                                    syear = syear, eyear = eyear)
            temp_obs = temp['data']
            obs_unit = temp['unit']
        elif (variable[0] == 'ef'):
            temp = func_read_netcdf('%s/EF_monthly_ERAint_19790101-20151231_%s.nc'
                                    %(path, grid), 'EF', var_units = True,
                                    syear = syear, eyear = eyear)
            temp_obs = temp['data']
            obs_unit = temp['unit']
        elif (variable[0] == 'rsus'):
            temp = func_read_netcdf('%s/ssru_monthly_ERAint_197901-201512_%s.nc'
                                    %(path, grid), 'ssru', var_units = True,
                                    syear = syear, eyear = eyear)
            temp_obs = temp['data']
            obs_unit = temp['unit']
        elif (variable[0] == 'rlus'):
            temp = func_read_netcdf('%s/stru_monthly_ERAint_197901-201512_%s.nc'
                                    %(path, grid), 'stru', var_units = True,
                                    syear = syear, eyear = eyear)
            temp_obs = temp['data']
            obs_unit = temp['unit']
        elif (variable[0] == 'huss'):
            temp = func_read_netcdf('%s/q_monthly_ERAint_197901-201512_%s.nc'
                                    %(path, grid), 'q', var_units = True,
                                    syear = syear, eyear = eyear)
            temp_obs = temp['data']
            obs_unit = temp['unit']
        else:
            print "Warning: no ERAint data for this variable, exiting"
            sys.exit
elif (obsdata == 'MERRA2'):
        print "Read MERRA2 data"
        path = '/net/tropo/climphys/rlorenz/Datasets/MERRA2/monthly'

        if (variable[0] == 'tas'):
            temp = func_read_netcdf(
                   '%s/%s.tavgmon_2d_slv_Nx.T2MMEAN.1980-2015_remapbil_%s.nc'
                   %(path, obsdata, grid), 'T2MMEAN', var_units = True,
                   syear = syear, eyear = eyear)
            temp_obs = temp['data']
            obs_unit = temp['unit']
        elif (variable[0] == 'tasmax'):
            temp = func_read_netcdf(
                   '%s/%s.tavgmon_2d_slv_Nx.T2MMAX.1980-2015_remapbil_%s.nc'
                   %(path, obsdata, grid), 'T2MMAX', var_units = True,
                   syear = syear, eyear = eyear)
            temp_obs = temp['data']
            obs_unit = temp['unit']
        elif (variable[0] == 'pr'):
            temp = func_read_netcdf(
                   '%s/%s.tavgmon_2d_flx_Nx.PRECTOTCORR.1980-2015_remapcon2_%s.nc'
                   %(path, obsdata, grid), 'PRECTOTCORR', var_units = True,
                   syear = syear, eyear = eyear)
            temp_obs = temp['data']
            obs_unit = temp['unit']
            if (obs_unit == 'kg m-2 s-1'):
                ## convert to mm/day
                tmp_obs = temp_obs * (24 * 60 * 60)
                temp_obs = tmp_obs[:]
                del tmp_obs
                obs_unit = 'mm/day'
        elif (variable[0] == 'hfls'):
            temp = func_read_netcdf(
                   '%s/%s.tavgmon_2d_flx_Nx.EFLUX.1980-2015_remapbil_%s.nc'
                   %(path, obsdata, grid), 'EFLUX', var_units = True,
                   syear = syear, eyear = eyear)
            temp_obs = temp['data']
            obs_unit = temp['unit']
        elif (variable[0] == 'psl'):
            temp = func_read_netcdf(
                   '%s/%s.tavgmon_2d_slv_Nx.SLP.1980-2015_remapbil_%s.nc'
                   %(path, obsdata, grid), 'SLP', var_units = True,
                   syear = syear, eyear = eyear)
            temp_obs = temp['data']
            obs_unit = temp['unit']
        elif (variable[0] == 'ef'):
            temp = func_read_netcdf(
                   '%s/%s.tavgmon_2d_flx_Nx.EF.1980-2015_remapbil_%s.nc'
                   %(path, obsdata, grid), 'EFLUX', var_units = True,
                   syear = syear, eyear = eyear)
            temp_obs = temp['data']
            obs_unit = temp['unit']
        elif (variable[0] == 'rsus'):
            temp = func_read_netcdf(
                    '%s/%s.tavgmon_2d_rad_Nx.SWGEM.1980-2015_remapbil_%s.nc'
                    %(path, obsdata, grid), 'SWGEM', var_units = True,
                    syear = syear, eyear = eyear)
            temp_obs = temp['data']
            obs_unit = temp['unit']
        elif (variable[0] == 'rsds'):
            temp = func_read_netcdf(
                    '%s/%s.tavgmon_2d_rad_Nx.SWGDN.1980-2015_remapbil_%s.nc'
                    %(path, obsdata, grid), 'SWGDN', var_units = True,
                    syear = syear, eyear = eyear)
            temp_obs = temp['data']
            obs_unit = temp['unit']
        elif (variable[0] == 'rlus'):
            temp = func_read_netcdf(
                    '%s/%s.tavgmon_2d_rad_Nx.LWGEM.1980-2015_remapbil_%s.nc'
                    %(path, obsdata, grid), 'LWGEM', var_units = True,
                    syear = syear, eyear = eyear)
            temp_obs = temp['data']
            obs_unit = temp['unit']
        elif (variable[0] == 'huss'):
            temp = func_read_netcdf(
                    '%s/%s.tavgmon_2d_slv_Nx.QV2M.1980-2015_remapbil_%s.nc'
                    %(path, obsdata, grid), 'QV2M', var_units = True,
                    syear = syear, eyear = eyear)
            temp_obs = temp['data']
            obs_unit = temp['unit']
        else:
            print "Warning: no MERRA data for this variable, exiting"
            sys.exit
elif (obsdata == 'Obs'):
        print "Read Obs data"
        path = '/net/tropo/climphys/rlorenz/Datasets/'

        if (variable[0] == 'tas'):
            temp = func_read_netcdf(
                   '%s/BerkeleyEarth/Complete_TAVG_LatLong_%s.nc'
                   %(path,grid), 'TAVG', var_units = True,
                   syear = syear, eyear = eyear)
            temp_obs = temp['data']
            obs_unit = temp['unit']
        elif (variable[0] == 'tasmax'):
            temp = func_read_netcdf(
                   '%s/HadGHCND/monthly/HadGHCND_TXTN_avg_monthly_acts_195001-201412_remapbil_%s.nc'
                   %(path, grid), 'tmax', var_units = True,
                   syear = syear, eyear = eyear)
            temp_obs = temp['data']
            obs_unit = temp['unit']
            if ((obs_unit == 'decC') or (obs_unit == 'C') or (obs_unit == 'degrees C')):
                ## convert to K
                tmp_obs = temp_obs + 273.15
                temp_obs = tmp_obs[:]
                del tmp_obs
                obs_unit = 'K'
        elif (variable[0] == 'pr'):
            temp = func_read_netcdf(
                   '%s/GPCP/precip.mon.mean.197901-201607.v2.3.nc' %(
                   path), 'precip', var_units = True, syear = syear,
                   eyear = eyear)
            temp_obs = temp['data']
            obs_unit = temp['unit']
            if (obs_unit == 'kg m-2 s-1'):
                ## convert to mm/day
                tmp_obs = temp_obs * (24 * 60 * 60)
                temp_obs = tmp_obs[:]
                del tmp_obs
                obs_unit = 'mm/day'    
        elif (variable[0] == 'hfls'):
            temp = func_read_netcdf(
                   '%s/GLEAM/monthly/GLEAM_E_monthly_v3.0a_1980-2014_%s.nc' %(
                   path, grid), 'E', var_units = True, syear = syear,
                   eyear = eyear)
            temp_obs = temp['data']
            obs_unit = temp['unit']
            #tmp_file = '%s/LandFluxEVAL/LandFluxEVAL.merged.89-05.monthly.diagnostic.%s.nc' %(path, grid)
            #ifile = nc.Dataset(tmp_file, mode = 'r')
            #obs_unit = ifile.variables['ET_mean'].units
            #time = ifile.variables['time'][:]
            #time_str = [str(i) for i in time]
            #orig_years = np.asarray([int(y[0:4]) for y in time_str])
            #months = np.asarray([int(y[4:6]) for y in time_str])
            #temp_obs = ifile.variables['E'][(orig_years >= syear) &
            #                                (orig_years <= eyear) , :, :]
            #obsmonths = months[(orig_years >= syear) & (orig_years <= eyear)]
            #obsyears = orig_years[(orig_years >= syear) & (orig_years <= eyear)]
            #obslat = ifile.variables['lat'][:]
            #obslon = ifile.variables['lon'][:]
            #ifile.close()
            #del orig_years
            if ((obs_unit == 'mm/d') or (obs_unit == 'mm/day')):
                ## convert to W m^-2
                factor = 2.5 * 10 ** 6 / (24 * 60 * 60)
                tmp_obs = np.multiply(temp_obs, factor)
                temp_obs = tmp_obs[:]
                del tmp_obs
                obs_unit = 'Wm-2'
        elif (variable[0] == 'psl'):
            print "Warning: no obs data for this variable, exiting"
            sys.exit
        elif (variable[0] == 'ef'):
            print "Warning: no obs data for this variable, exiting"
            sys.exit
        elif (variable[0] == 'rsus'):
            temp = func_read_netcdf(
                    '%s/CERES/CERES_EBAF-sfc_sw_up_all_mon_Ed2.8_Subset_200003-201511_%s.nc'
                    %(path, grid), 'sfc_sw_up_all_mon', var_units = True,
                    syear = syear, eyear = eyear)
            temp_obs = temp['data']
            obs_unit = temp['unit']
        elif (variable[0] == 'rsds'):
            temp = func_read_netcdf(
                    '%s/CERES/CERES_EBAF-sfc_sw_down_all_mon_Ed2.8_Subset_200003-201511_%s.nc'
                    %(path, grid), 'sfc_sw_down_all_mon', var_units = True,
                    syear = syear, eyear = eyear)
            temp_obs = temp['data']
            obs_unit = temp['unit']
        elif (variable[0] == 'rlus'):
            temp = func_read_netcdf(
                    '%s/CERES/CERES_EBAF-sfc_lw_up_all_mon_Ed2.8_Subset_200003-201511_%s.nc'
                    %(path, grid), 'sfc_lw_up_all_mon', var_units = True,
                    syear = syear, eyear = eyear)
            temp_obs = temp['data']
            obs_unit = temp['unit']
        else:
            print "Warning: no obs data for this variable, exiting"
            sys.exit
else:
        print'Wrong obsdata specified, not available, exit'
        sys.exit
try:    
    obslat = temp['lat']
    obslon = temp['lon']    
    obsdates = temp['dates']
    obsmonths = np.asarray([obsdates[i].month for i in xrange(len(obsdates))])
    obsyears = np.asarray([obsdates[i].year for i in xrange(len(obsdates))])
except NameError:
    obsdates = time_str

if isinstance(temp_obs, np.ma.core.MaskedArray):
    temp_obs = temp_obs.filled(np.NaN)

## shiftgrid to -180 to 180 if necessary (to be compatible with area mask)
if (obslon[0] >= 0.0) and (obslon[-1] > 180):
    obslons_orig = obslon[:]
    temp_obs, obslon = shiftgrid(180., temp_obs, obslon, start = False)

## if any region defined, read region extent from file and cut data into region
if region != None:
    print 'Region is %s' %(region)
    area = region
    mask = np.loadtxt('/home/rlorenz/scripts/plot_scripts/areas_txt/%s.txt'
                      %(region))                    
    temp_obs_reg_nan = np.ndarray((temp_obs.shape))
    for ilat in xrange(len(obslat)):
        for ilon in xrange(len(obslon)):
            if (point_inside_polygon(obslat[ilat], obslon[ilon],
                np.fliplr(mask))):
                temp_obs_reg_nan[:, ilat, ilon] = temp_obs[:, ilat, ilon]
            else:
                temp_obs_reg_nan[:, ilat, ilon] = np.NaN
    idx_lat = np.where((obslat > np.min(mask[:, 1])) &
                       (obslat < np.max(mask[:, 1])))
    obslat_orig = obslat
    obslat = obslat[idx_lat]
    tmp1_obs_reg = np.empty((len(temp_obs_reg_nan), len(obslat), len(obslon)))
    tmp1_obs_reg.fill(np.NaN)
    idx_lon = np.where((obslon > np.min(mask[:, 0])) &
                       (obslon < np.max(mask[:, 0])))
    obslon_orig = obslon
    obslon = obslon[idx_lon]
    temp_obs_reg = np.empty((len(temp_obs_reg_nan), len(obslat), len(obslon)))
    temp_obs_reg.fill(np.NaN)
    tmp1_obs_reg = np.squeeze(temp_obs_reg_nan[:, idx_lat, :], 1)
    temp_obs_reg = np.squeeze(tmp1_obs_reg[:, :, idx_lon], 2)

    #del temp_obs
else:
    area = 'GLOBAL'
    obslat_orig = obslat
    obslon_orig = obslon
    temp_obs_reg = temp_obs

## detrend ts before calculating variability etc.
if np.isnan(temp_obs_reg).any():
    temp_obs_reg_dtr = np.empty((temp_obs_reg.shape))
    temp_obs_reg_dtr.fill(np.NaN)
    for ilat in xrange(len(obslat)):
        for ilon in xrange(len(obslon)):
            tmp = temp_obs_reg[:, ilat, ilon]
            if np.isnan(tmp).any():
                temp_obs_reg_dtr[:, ilat, ilon] = np.NaN
            else:
                temp_obs_reg_dtr[:, ilat, ilon] = signal.detrend(tmp, axis = 0,
                                                                 type = 'linear'
                                                                 , bp = 0)
else:
    temp_obs_reg_dtr = signal.detrend(temp_obs_reg, axis = 0, type = 'linear',
                                      bp = 0)

## calculate obs climatology, variability, trend
print "Calculate obs climatology, variability, trend"
if (res[0] == None) and (var_kind[0] == 'cyc'):
    temp_obs_ts = ann_avg_mon_TLL(temp_obs_reg, obsyears)
    try:
        time_out = temp['time'][0::12]
    except NameError:
        tmp_out = obsdates[0::12]
elif (res[0] == None) and (var_kind[0] != 'cyc'):
    temp_obs_ts = temp_obs_reg
    try:
        time_out = temp['time']
    except NameError:
        tmp_out = obsdates
else:
    if var_kind[0] == 'mon':
        ind = np.where(obsmonths == res[0])
        temp_obs_ts = np.squeeze(temp_obs_reg[ind, :, :])
        try:
            time_out = temp['time'][ind]
        except NameError:
            tmp_out = obsdates[ind]
    elif var_kind[0] == 'seas':
        temp_obs_seas_ts = clim_seas_mon_TLL(temp_obs_reg, obsmonths, obsyears)
        temp_obs_ts = temp_obs_seas_ts[res[0], :, :, :]
        try:
            time_out = temp['time'][0::12]
        except NameError:
            tmp_out = obsdates[0::12]
try:
    calendar = temp['tcal']
    tunit = temp['tunit']
except NameError:
    new_dates = []
    for n in range(len(tmp_out)):
        y = int(tmp_out[n][0:4])
        m = int(tmp_out[n][4:6])
        new_dates.append(dt.datetime(y, m, 15))
    calendar = 'gregorian'
    tunit = 'days since %s-01-15 00:00:00' %(syear)
    time_out = nc.date2num(new_dates, units = tunit, calendar = calendar)
    del tmp_out
    del new_dates
## save ts data
outfile = '%s%s_%s_%s_%s_%s_%s_%s-%s.nc' %(outdir, variable[0], 'ts',
                                           var_kind[0], obsdata, area,
                                           experiment, syear, eyear)
func_write_netcdf(outfile, temp_obs_ts, variable[0], obslon, obslat,
                  timevar = time_out, time_unit = tunit,
                  time_cal = calendar, var_units = obs_unit,
                  Description = '%s timeseries over %s %s' %(variable[0],
                  var_kind[0], res[0]))

t_ind = var_file.index('clim')
if res[t_ind] == None:
    temp_obs_clim = clim_mon_TLL(temp_obs_reg, obsmonths)
    time_out = range(0, 12)
elif var_kind[t_ind] == 'mon':
    ind = np.where(obsmonths == res[t_ind])
    temp_obs_clim = np.nanmean(np.squeeze(temp_obs_reg[ind, :, :]), axis=0,
                               keepdims = True)
    time_out = [1]
elif var_kind[t_ind] == 'seas':
    temp_obs_clim = np.empty((1, len(obslat), len(obslon)))
    temp_obs_clim.fill(np.NaN)
    temp_obs_clim_seas = seas_avg_mon_TLL(temp_obs_reg, obsmonths)
    temp_obs_clim[0, :, :] = temp_obs_clim_seas[res[t_ind], :, :]
    time_out = [1]

## save clim data
outfile = '%s%s_%s_%s_%s_%s_%s_%s-%s.nc' %(outdir, variable[t_ind], 'clim',
                                           var_kind[t_ind], obsdata, area,
                                           experiment, syear, eyear)
if len(time_out) > 1:
    func_write_netcdf(outfile, temp_obs_clim, variable[t_ind], obslon,
                      obslat, timevar = time_out, time_unit = 'month',
                      var_units = obs_unit,
                      Description = '%s climatology over %s %s'
                      %(variable[t_ind], var_kind[t_ind], res[t_ind]))
else:
    func_write_netcdf(outfile, temp_obs_clim, variable[t_ind], obslon,
                      obslat, var_units = obs_unit, Description =
                      '%s climatology over %s %s' %(variable[t_ind],
                      var_kind[t_ind], res[t_ind]))

t_ind = var_file.index('std')
if res[t_ind] == None:
    temp_obs_std = std_mon_TLL(temp_obs_reg_dtr, obsmonths)
elif var_kind[t_ind] == 'mon':
    ind = np.where(obsmonths == res[t_ind])
    temp_obs_std = np.nanstd(np.squeeze(temp_obs_reg_dtr[ind, :, :]),
                             axis = 0, keepdims = True)
elif var_kind[t_ind] == 'seas':
    temp_obs_std = np.empty((1, len(obslat), len(obslon)))
    temp_obs_std.fill(np.NaN)
    temp_obs_std_seas = seas_std_mon_TLL(temp_obs_reg_dtr, obsmonths, obsyears)
    temp_obs_std[0, :, :] = temp_obs_std_seas[res[t_ind], :, :]

## save std data
outfile = '%s%s_%s_%s_%s_%s_%s_%s-%s.nc' %(outdir, variable[t_ind], 'std',
                                           var_kind[t_ind], obsdata, area,
                                           experiment, syear, eyear)
if len(time_out) > 1:
    func_write_netcdf(outfile, temp_obs_std, variable[t_ind], obslon,
                      obslat, timevar = time_out, time_unit = 'month',
                      var_units = obs_unit, Description =
                      '%s variability over %s %s' %(variable[t_ind],
                      var_kind[t_ind], res[t_ind]))
else:
    func_write_netcdf(outfile, temp_obs_std, variable[t_ind], obslon,
                      obslat, var_units = obs_unit, Description =
                      '%s variability over %s %s' %(variable[t_ind],
                      var_kind[t_ind], res[t_ind]))

## calculate trend over time defined in res
if any('trnd' in s for s in var_file):
    t_ind = var_file.index('trnd')
    if res[t_ind] == None:
        temp_obs_trnd = trend_TLL(temp_obs_ts, drop = False)
    else:
        if (var_kind[t_ind] == 'mon'):
            ind = np.where(obsmonths == res[t_ind])
            temp_obs_trnd = trend_TLL(np.squeeze(temp_obs_reg[ind, :, :]),
                                      drop=False)
        elif (var_kind[t_ind] == 'seas'):
            temp_obs_trnd = trend_TLL(temp_obs_seas_ts[res[t_ind], :, :, :],
                                      drop=False)
    ## save trend data
    outfile = '%s%s_%s_%s_%s_%s_%s_%s-%s.nc' %(outdir, variable[t_ind], 'trnd',
                                               var_kind[t_ind], obsdata, area,
                                               experiment, syear, eyear)
    if len(time_out) > 1:
        func_write_netcdf(outfile, temp_obs_trnd, variable[t_ind], obslon,
                          obslat, timevar = time_out, time_unit = 'month',
                          var_units = obs_unit, Description =
                          '%s trend over %s %s' %(variable[t_ind],
                          var_kind[t_ind], res))
    else:
        func_write_netcdf(outfile, temp_obs_trnd, variable[t_ind], obslon,
                          obslat, var_units = obs_unit, Description =
                          '%s trend over %s %s' %(variable[t_ind], 
                                                  var_kind[t_ind], res[t_ind]))

#print temp_obs_clim.shape
### read model data
print "Read model data"
## find all matching files in archive, loop over all of them
## first count matching files in folder to initialze variable rmse per model:
if variable[0] != 'ef':
    name = '%s/%s/%s_*_%s_*_%s.nc' %(archive, variable[0], infile, experiment, grid)
else:
    name = '%s/%s_*_%s_*_%s.nc' %(archive, infile, experiment, grid)
nfiles = len(glob.glob(name))
model_names = []
d_temp_mod_ts = dict()
d_temp_mod_clim = {}
d_temp_mod_std = {}
d_temp_mod_trnd = {}
print str(nfiles)+ ' matching files found'
f = 0
for filename in glob.glob(name):
    print "Read " + filename + " data"
    model_data = func_read_netcdf(filename, variable[0], var_units = True,
                                  syear = syear, eyear = eyear)
    temp_mod = model_data['data']
    lat = model_data['lat']
    lon = model_data['lon']
    mod_unit = model_data['unit']
    dates = model_data['dates']
    months = np.asarray([dates[i].month for i in xrange(len(dates))])
    years = np.asarray([dates[i].year for i in xrange(len(dates))])

    # shiftgrid to -180 to 180 if necessary (to be compatible with area mask)
    if (lon[0] >= 0.0) and (lon[-1] > 180):
        lons_orig = lon[:]
        temp_mod, lon = shiftgrid(180., temp_mod, lon, start = False)

    ## check that time axis and grid is identical for model0 and modelX and obs
    if temp_obs.shape != temp_mod.shape:
        print 'Warning: Dimension for model and Obs is different!'
        #continue
    if any(lon) != any(obslon_orig):
        print 'Warning: Longitudes for model and Obs are different!'
        continue
    if any(lat) != any(obslat_orig):
        print 'Warning: Latitudes for model and Obs are different!'
        continue
    if ((dates[0].year != obsyears[0]) or
        (dates[0].month != obsmonths[0])):
        print 'Warning: time axis for model and Obs is not identical!'
        print 'model starts %s, while Obs starts %s' %(dates[0], obsdates[0])
        #sys.exit
    
    if f != 0:
        if temp_mod0.shape != temp_mod.shape:
            print 'Warning: Dimension for model0 and modelX is different!'
            continue
        if ((dates[0].year != dates0[0].year) or
            (dates[0].month != dates0[0].month)):
            print 'time axis for model0 and modelX is not identical, exiting!'
            print 'model0 starts %s, while modelX starts %s' %(dates0[0],
                                                               dates[0])
            sys.exit
    else:
        dates0 = dates[:]
        temp_mod0 = temp_mod[:]

    fh = nc.Dataset(filename, mode = 'r')
    model = fh.source_model
    if model == 'ACCESS1.3':
        model = 'ACCESS1-3'
    elif model == 'FGOALS_g2':
        model = 'FGOALS-g2'
    ens = fh.source_ensemble
    model_names.append(model + '_' + ens)

    if isinstance(temp_mod, np.ma.core.MaskedArray):
        temp_mod = temp_mod.filled(np.NaN)

    if (variable[0] == 'pr') and (mod_unit == 'kg m-2 s-1'):
        print 'convert pr to mm/day'
        tmp_mod = temp_mod * (24 * 60 * 60)
        temp_mod = tmp_mod[:]
        del tmp_mod
        mod_unit = 'mm/day'
       
    if (variable[0] == 'sic') and  (model == "EC-EARTH"):
        with np.errstate(invalid = 'ignore'):
            temp_mod[temp_mod < 0.0] = np.NaN

    ## if any region defined, cut data into region
    if region != None:   
        temp_mod_reg_nan = np.ndarray((temp_mod.shape))
        for ilat in xrange(len(lat)):
            for ilon in xrange(len(lon)):
                if (point_inside_polygon(lat[ilat], lon[ilon],
                    np.fliplr(mask))):
                    temp_mod_reg_nan[:, ilat, ilon] = temp_mod[:, ilat, ilon]
                else:
                    temp_mod_reg_nan[:, ilat, ilon] = np.NaN
        idx_lat = np.where((lat > mask[2, 1]) & (lat < mask[0, 1]))
        lat = lat[idx_lat]
        tmp1_mod_reg = np.empty((len(temp_mod_reg_nan), len(lat), len(lon)))
        tmp1_mod_reg.fill(np.NaN)
        idx_lon = np.where((lon > mask[0, 0]) & (lon < mask[1, 0]))
        lon = lon[idx_lon]
        temp_mod_reg = np.empty((len(temp_mod_reg_nan), len(lat), len(lon)))
        temp_mod_reg.fill(np.NaN)
        tmp1_mod_reg = np.squeeze(temp_mod_reg_nan[:, idx_lat, :], 1)
        temp_mod_reg = np.squeeze(tmp1_mod_reg[:, :, idx_lon], 2)
        del temp_mod
    else:
        temp_mod_reg = temp_mod

    ## calculate timeseries and save for each model
    if (res[0] == None) and (var_kind[0] == 'cyc'):
        temp_mod_ts = ann_avg_mon_TLL(temp_mod_reg, years)
        d_temp_mod_ts[model + '_' + ens] = temp_mod_ts
        time_out = model_data['time'][0::12]
    elif (res[0] == None) and (var_kind[0] != 'cyc'):
        temp_mod_ts = temp_mod_reg
        d_temp_mod_ts[model + '_' + ens] = temp_mod_ts
        time_out = model_data['time']
    else:
        if var_kind[0] == 'mon':
            ind = np.where(months == res[0])
            temp_mod_ts = np.squeeze(temp_mod_reg[ind, :, :])
            d_temp_mod_ts[model + '_' + ens] = temp_mod_ts
            time_out = model_data['time'][ind]
        elif var_kind[0] == 'seas':
            temp_mod_clim = np.empty((1, len(lat), len(lon)))
            temp_mod_clim.fill(np.NaN)
            temp_mod_clim_seas = seas_avg_mon_TLL(temp_mod_reg, months)
            temp_mod_ts = clim_seas_mon_TLL(temp_mod_reg, months, years)[res[0], :, :, :]
            time_out = model_data['time'][0::12]
            d_temp_mod_ts[model + '_' + ens] = temp_mod_ts    
    #save ts data
    outfile = '%s%s_%s_%s_%s_%s_%s_%s_%s-%s.nc' %(outdir, variable[0], 'ts',
                                                  var_kind[0], model, ens, area,
                                                  experiment, syear, eyear)
    if len(time_out) > 1:
        func_write_netcdf(outfile, temp_mod_ts, variable[0], lon, lat,
                          timevar = time_out, time_unit = model_data['tunit'],
                          time_cal = model_data['tcal'],
                          var_units = mod_unit,
                          Description = '%s timeseries over %s %s' %(
                                         variable[0], var_kind[0],res))
    else:
        func_write_netcdf(outfile, temp_mod_ts, variable[0], lon, lat,
                          var_units = mod_unit,
                          Description = '%s timeseries over %s %s' %(
                                         variable[0], var_kind[0], res))

    #calculate climatology
    if any('clim' in s for s in var_file):
        t_ind = var_file.index('clim')
        if res[t_ind] == None:
            time_out = range(0, 12)
            temp_mod_clim = clim_mon_TLL(temp_mod_reg, months)
        elif var_kind[t_ind] == 'mon':
            ind=np.where(months == res[t_ind])
            time_out = [1]
            temp_mod_clim = np.nanmean(np.squeeze(temp_mod_reg[ind, :, :]),
                                       axis = 0, keepdims = True)
        elif var_kind[t_ind] == 'seas':
            temp_mod_clim = np.empty((1, len(lat), len(lon)))
            temp_mod_clim.fill(np.NaN)
            time_out = [1]
            temp_mod_clim_seas = seas_avg_mon_TLL(temp_mod_reg, months)
            temp_mod_clim[0, :, :] = temp_mod_clim_seas[res[t_ind], :, :]
        d_temp_mod_clim[model + '_' + ens] = temp_mod_clim
        ## save climatology data
        outfile = '%s%s_%s_%s_%s_%s_%s_%s_%s-%s.nc' %(outdir, variable[t_ind],
                                                      var_file[t_ind],
                                                      var_kind[t_ind], model,
                                                      ens, area, experiment,
                                                      syear, eyear)
        if len(time_out) > 1:
            func_write_netcdf(outfile, temp_mod_clim, variable[t_ind], lon, lat,
                              timevar = time_out, time_unit = 'month',
                              var_units = mod_unit,
                              Description = '%s climatology over %s %s' %(
                                            variable[t_ind], var_kind[t_ind],
                                            res))
        else:
            func_write_netcdf(outfile, temp_mod_clim, variable[t_ind], lon,lat,
                              var_units = mod_unit,
                              Description = '%s climatology over %s %s' %(
                                            variable[t_ind], var_kind[t_ind],
                                            res))

    #detrend ts before calculating variability
    if any('std' in s for s in var_file):
        if np.isnan(temp_mod_reg).any():
            temp_mod_reg_dtr = np.empty((temp_mod_reg.shape))
            temp_mod_reg_dtr.fill(np.NaN)
            for ilat in xrange(len(lat)):
                for ilon in xrange(len(lon)):
                    tmp = temp_mod_reg[:, ilat, ilon]
                    if np.isnan(tmp).any():
                        temp_mod_reg_dtr[:, ilat, ilon] = np.NaN
                    else:
                        temp_mod_reg_dtr[:, ilat, ilon] = signal.detrend(tmp,
                                                          axis=0, type='linear',
                                                          bp=0)
        else:
            temp_mod_reg_dtr = signal.detrend(temp_mod_reg, axis=0)
        t_ind = var_file.index('std')
        if res[t_ind] == None:
            time_out = range(0, 12)
            temp_mod_std = std_mon_TLL(temp_mod_reg_dtr, months)
        elif var_kind[t_ind] == 'mon':
            ind=np.where(months == res[t_ind])
            time_out = [1]
            temp_mod_std = np.nanstd(np.squeeze(temp_mod_reg_dtr[ind, :, :]),
                                     axis = 0, keepdims = True)
        elif var_kind[t_ind] == 'seas':
            temp_mod_std = np.empty((1, len(lat), len(lon)))
            temp_mod_std.fill(np.NaN)
            time_out = [1]
            temp_mod_std_seas = seas_std_mon_TLL(temp_mod_reg_dtr, months,
                                                 years)
            temp_mod_std[0, :, :] = temp_mod_std_seas[res[t_ind], :, :]
        d_temp_mod_std[model + '_' + ens] = temp_mod_std
        ## save variability data
        outfile = '%s%s_%s_%s_%s_%s_%s_%s_%s-%s.nc' %(outdir, variable[t_ind],
                                                      var_file[t_ind],
                                                      var_kind[t_ind], model,
                                                      ens,area,experiment,
                                                      syear,eyear)
        if len(time_out) > 1:
            func_write_netcdf(outfile, temp_mod_std, variable[t_ind], lon, lat,
                              timevar = time_out, time_unit = 'month',
                              var_units = mod_unit,
                              Description = '%s variability over %s %s' %(
                                            variable[t_ind], var_kind[t_ind],
                                            res))
        else:
            func_write_netcdf(outfile, temp_mod_std, variable[t_ind], lon, lat,
                              var_units = mod_unit,
                              Description = '%s variability over %s %s' %(
                                             variable[t_ind], var_kind[t_ind],
                                             res))

    ## calculate trend over time defined in res
    if any('trnd' in s for s in var_file):
        t_ind = var_file.index('trnd')
        if res[t_ind] != None:
            if (var_kind[t_ind] == 'mon'):    
                ind=np.where(months == res[t_ind])
                time_out = [1]
                temp_mod_trnd = trend_TLL(np.squeeze(temp_mod_reg[ind, :, :]),
                                          drop = False)
            elif (var_kind[t_ind] == 'seas'):
                time_out = [1]
                temp_mod_seas = clim_seas_mon_TLL(temp_mod_reg, months, years)
                temp_mod_trnd = trend_TLL(np.squeeze(
                                          d_temp_mod_ts[model + '_' + ens]),
                                          drop = False)
        else:
            temp_mod_trnd = trend_TLL(np.squeeze(temp_mod_ts), drop = False)
            time_out = [1]
        d_temp_mod_trnd[model + '_' + ens] = temp_mod_trnd
        ## save trend data
        outfile = '%s%s_%s_%s_%s_%s_%s_%s_%s-%s.nc' %(outdir, variable[t_ind],
                                                      var_file[t_ind],
                                                      var_kind[t_ind], model,
                                                      ens, area, experiment,
                                                      syear, eyear)
        if len(time_out) > 1:
            func_write_netcdf(outfile, temp_mod_trnd, variable[t_ind], lon, lat,
                              timevar = time_out, time_unit = 'month',
                              var_units = mod_unit,
                              Description = '%s trend over %s %s' %(
                              variable[t_ind], var_kind[t_ind], res))
        else:
            func_write_netcdf(outfile, temp_mod_trnd, variable[t_ind], lon, lat,
                              var_units = mod_unit,
                              Description = '%s trend over %s %s' %(
                              variable[t_ind], var_kind[t_ind], res))
    f = f + 1
    fh.close()

model_names.append("Obs")
del temp_obs

## calculate rmse for var_file
for v in range(len(var_file)):
    #loop over all models and calculate rmse compared to each other model
    print "Calculate RMSE for model dependence and quality for %s %s %s" %(
            variable[v], var_file[v], var_kind[v])
    #if any region defined, read region extent from file                
    if (var_file[v] == 'clim'):
        rmse_all = func_calc_rmse(d_temp_mod_clim, temp_obs_clim, lat, lon,
                                  model_names[:-1], var_kind[v])
    elif (var_file[v] == 'std'):
        rmse_all = func_calc_rmse(d_temp_mod_std, temp_obs_std, lat, lon,
                                  model_names[:-1], var_kind[v])
    elif (var_file[v] == 'trnd'):
        rmse_all = func_calc_rmse(d_temp_mod_trnd, temp_obs_trnd, lat, lon,
                                  model_names[:-1])

    print "Save deltas to netcdf"
    fout = nc.Dataset('%s%s_%s_%s_all_%s_%s-%s_%s_%s_RMSE.nc' 
                      %(outdir, variable[v], var_file[v], var_kind[v], 
                        experiment, syear, eyear, area, obsdata), mode = 'w')
    fout.createDimension('x', len(model_names))
    fout.createDimension('y', len(model_names))

    xout = fout.createVariable('x', 'f8', ('x'), fill_value = 1e20)
    setattr(xout, 'Longname', 'ModelNames')
    yout = fout.createVariable('y', 'f8', ('y'), fill_value = 1e20)
    setattr(yout, 'Longname', 'ModelNames')

    rmseout = fout.createVariable('rmse', 'f8', ('x', 'y'), fill_value = 1e20)
    setattr(rmseout, 'Longname', 'Root Mean Squared Error')
    setattr(rmseout, 'units', '-')
    setattr(rmseout, 'description',
            'Root mean squared error between models and models and reference') 

    xout[:] = range(len(model_names))[:]
    yout[:] = range(len(model_names))[:]
    rmseout[:] = rmse_all[:]

    # Set global attributes
    setattr(fout, "author", "Ruth Lorenz @IAC, ETH Zurich, Switzerland")
    setattr(fout, "contact", "ruth.lorenz@env.ethz.ch")
    setattr(fout, "creation date", dt.datetime.today().strftime('%Y-%m-%d'))
    setattr(fout, "comment", "")

    setattr(fout, "Script", "calc_rmse_for_weight_beyond_democracy.py")
    setattr(fout, "Input files located in:", archive)
    fout.close()

    #save model names into separate text file
    with open('%s%s_%s_%s_all_%s_%s-%s.txt' %(outdir, variable[v], var_file[v],
                                              var_kind[v], experiment, syear,
                                              eyear), "w") as text_file:
        for m in range(len(model_names)):
            text_file.write(model_names[m] + "\n")

## calculate perkins skill score for var_file
for v in range(len(var_file)):
    ## loop over all models and calculate ss compared to each other model
    SS_all = np.empty((len(model_names), len(model_names)), dtype = float)
    SS_all.fill(np.NaN)
    print "Calculate perkins SS for model dependence and quality for " + variable[v] + " " + var_file[v] + " " + var_kind[v]
    if (var_file[v] == 'clim'):
        bin_min = np.minimum(np.nanmin(temp_obs_clim),
                             np.nanmin(d_temp_mod_clim.values()))
        bin_max = np.minimum(np.nanmax(temp_obs_clim),
                             np.nanmax(d_temp_mod_clim.values()))
        hist_obs, bin_edges_obs = np.histogram(temp_obs_clim.flatten(),
                                               bins = np.arange(bin_min,
                                                                bin_max, 0.5),
                                               density = True)
        pdf_obs = hist_obs * np.diff(bin_edges_obs)
        pdf_mod = dict()
        for key, value in d_temp_mod_clim.iteritems():
            hist_mod, bin_edges_mod = np.histogram(value.flatten(),
                                                   bins = np.arange(bin_min,
                                                                    bin_max,
                                                                    0.5),
                                                   density = True)
            pdf_mod[key] = hist_mod * np.diff(bin_edges_mod)
    elif (var_file[v] == 'std'):
        bin_min = np.minimum(np.nanmin(temp_obs_std),
                             np.nanmin(d_temp_mod_std.values()))
        bin_max = np.minimum(np.nanmax(temp_obs_std),
                             np.nanmax(d_temp_mod_std.values()))
        hist_obs, bin_edges_obs = np.histogram(temp_obs_std.flatten(),
                                               bins = np.arange(bin_min,
                                                                bin_max,
                                                                0.01),
                                               density = True)
        pdf_obs = hist_obs * np.diff(bin_edges_obs)
        pdf_mod = dict()
        for key, value in d_temp_mod_std.iteritems():
            hist_mod, bin_edges_mod = np.histogram(value.flatten(),
                                                   bins = np.arange(bin_min,
                                                                    bin_max,
                                                                    0.01),
                                                   density = True)
            pdf_mod[key] = hist_mod * np.diff(bin_edges_mod)
    elif (var_file[v] == 'trnd'):
        bin_min = np.minimum(np.nanmin(temp_obs_trnd),
                             np.nanmin(d_temp_mod_trnd.values()))
        bin_max = np.minimum(np.nanmax(temp_obs_trnd),
                             np.nanmax(d_temp_mod_trnd.values()))
        hist_obs, bin_edges_obs = np.histogram(temp_obs_trnd.flatten(),
                                               bins = np.arange(bin_min,
                                                                bin_max,
                                                                0.01),
                                               density = True)
        pdf_obs = hist_obs * np.diff(bin_edges_obs)
        pdf_mod = dict()
        for key, value in d_temp_mod_trnd.iteritems():
            hist_mod, bin_edges_mod = np.histogram(value.flatten(),
                                                   bins = np.arange(bin_min,
                                                                    bin_max,
                                                                    0.01),
                                                   density = True)
            pdf_mod[key] = hist_mod * np.diff(bin_edges_mod)
    for j in xrange(len(model_names)):
        if (j < len(model_names) - 1):
            compare = model_names[j]
        for jj in xrange(len(model_names)):
            if (jj < len(model_names) - 1):
                ref = model_names[jj]
            if (j == jj):
                SS_all[j, jj] = 1.0
            elif (j == len(model_names) - 1):
                SS_all[j, jj] = perkins_skill(pdf_obs, pdf_mod[ref])
            elif (jj == len(model_names) - 1):
                SS_all[j, jj] = perkins_skill(pdf_mod[compare], pdf_obs)
            else:
                SS_all[j, jj] = perkins_skill(pdf_mod[compare], pdf_mod[ref])

    #SS_all_ar = np.array(SS_all.items(), dtype=type(temp_obs_clim)) #dict to array
    print SS_all.shape

    print "Save perkins SS to netcdf"
    fout = nc.Dataset('%s%s_%s_%s_all_%s_%s-%s_%s_%s_perkins_SS.nc' %(
            outdir, variable[v], var_file[v], var_kind[v], experiment, syear,
            eyear, area, obsdata), mode = 'w')
    fout.createDimension('x', len(model_names))
    fout.createDimension('y', len(model_names))

    xout = fout.createVariable('x', 'f8', ('x'), fill_value = 1e20)
    setattr(xout, 'Longname', 'ModelNames')
    yout = fout.createVariable('y', 'f8', ('y'), fill_value = 1e20)
    setattr(yout, 'Longname', 'ModelNames')

    SSout = fout.createVariable('SS', 'f8', ('x', 'y'), fill_value = 1e20)
    setattr(SSout, 'Longname', 'Perkins Skill Score')
    setattr(SSout, 'units', '-')
    setattr(SSout, 'description', 'Perkins pdf skill score between models and '
            + 'models and reference')

    xout[:] = range(len(model_names))[:] 
    yout[:] = range(len(model_names))[:]
    SSout[:] = 1 - SS_all[:]

    # Set global attributes
    setattr(fout, "author", "Ruth Lorenz @IAC, ETH Zurich, Switzerland")
    setattr(fout, "contact", "ruth.lorenz@env.ethz.ch")
    setattr(fout, "creation date", dt.datetime.today().strftime('%Y-%m-%d'))
    setattr(fout, "comment", "")

    setattr(fout, "Script", "calc_pdf_perkins_ss.py")
    setattr(fout, "Input files located in:", archive)
    fout.close()

#!/usr/bin/python
'''
File Name : eval_map_plot.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 08-08-2016
Modified: Fri 04 Nov 2016 11:26:53 AM CET
Purpose: plots maps global or region to compare CMIP5models-obs


'''
import netCDF4 as nc # to work with NetCDF files
import numpy as np
import glob as glob
import os # operating system interface
from os.path import expanduser
home = expanduser("~") # Get users home directory
import sys
sys.path.insert(0,home+'/scripts/plot_scripts/utils/')
from func_read_data import func_read_netcdf
from clim_seas_TLL import seas_avg_mon_TLL, clim_mon_TLL, seas_std_mon_TLL
from point_inside_polygon import point_inside_polygon
from draw_utils import draw
from draw_panel_utils import draw_panel_2x2_h
from subprocess import call
import matplotlib.pyplot as plt
from mpl_toolkits.basemap import shiftgrid
import math
###
# Define input
###
variable = 'tasmax'
obsdata = 'MERRA2'
obsdir = '/net/tropo/climphys/rlorenz/Datasets/%s' %(obsdata)
moddir = '/net/atmos/data/cmip5-ng'
freq = 'mon'
experiment = 'rcp85'
grid = 'g025'

###
# Define analysis and output
###
syear = 1980
eyear = 2015
region = 'NAM'
outfreq = 'seas_std'
lev_d = np.arange(-1.3, 1.5, 0.2)    # seas: np.arange(-6.5, 7.5, 1)
                                   # seas_std: np.arange(-3.25, 3.75, 0.5)
                                   # seas_std: np.arange(-1.3, 1.5, 0.2)
colbar_d = "RdBu_r"
lev = np.arange(0, 3, 0.3)  # seas: np.arange(260, 326, 2)
                         # seas_std: np.arange(0, 10, 1), np.arange(0, 2, 0.2)
colbar = "inferno_r"
plotall = True  # plot data for each model or only ensemble mean?
outdir = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/%s/%s/' %(
        variable, outfreq)

if (os.access(outdir,os.F_OK) == False):
    os.makedirs(outdir)

if outfreq == 'mon':
    ntim = 12
    month = [
            'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul' ,
            'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
elif (outfreq == 'seas') or (outfreq == 'seas_std'):
    ntim = 4
    season = ['DJF', 'MAM', 'JJA', 'SON']

if region is None:
    figuresize = (12, 8)
elif (region == 'CNA_flip'):
    figuresize = (9, 11)
else:
    figuresize = (12, 8)
#read obs data
if (obsdata == 'ERAint'):
    print "Read ERAint data"
    path = '%s/ERAint_mon' %(obsdir)

    if (variable=='tas'):
        temp = func_read_netcdf(
                    '%s/T2M_monthly_ERAint_197901-201601_%s.nc'
                    %(path, grid), 'T2M', var_units = True,
                    syear = syear, eyear = eyear)
        temp_obs = temp['data']
        obs_unit = temp['unit']
    elif (variable == 'pr'):
        temp = func_read_netcdf(
                    '%s/RTOT_monthly_ERAint_197901-201412_%s.nc'
                    %(path,grid), 'RTOT', var_units = True,
                    syear = syear, eyear = eyear)
        temp_obs = temp['data']
        obs_unit = temp['unit']
    elif (variable == 'sic'):
        temp = func_read_netcdf(
                    '%s/ci_monthly_ERAint_197901-201601_%s.nc'
                    %(path, grid), 'ci', syear = syear, eyear = eyear)
        # ERAint data is in fraction -> convert to percent as in CMIP5
        temp_obs = temp['data'] * 100
        obs_unit = 'percent'
    elif (variable == 'hfls'):
        temp = func_read_netcdf(
                    '%s/slhf_monthly_ERAint_197901-201512_%s.nc'
                    %(path,grid), 'slhf', var_units = True,
                    syear = syear, eyear = eyear)
        temp_obs = - temp['data']
        obs_unit = temp['unit']
    elif (variable == 'tasmax'):
        temp = func_read_netcdf(
                    '%s/mx2t_monmean_ERAint_197901-201512_%s.nc'
                    %(path, grid), 'mx2t', var_units = True,
                    syear = syear, eyear = eyear)
        temp_obs = temp['data']
        obs_unit = temp['unit']
elif (obsdata == 'MERRA2'):
    print "Read MERRA2 data"
    path = '%s/monthly' %(obsdir)
    if (variable == 'tas'):
        temp = func_read_netcdf(
                   '%s/MERRA2.tavgmon_2d_slv_Nx.T2MMEAN.1980-2015_remapbil_%s.nc'
                    %(path, grid), 'T2MMEAN', var_units = True,
                    syear = syear, eyear = eyear)
    elif (variable == 'tasmax'):
        temp = func_read_netcdf(
                    '%s/MERRA2.tavgmon_2d_slv_Nx.T2MMAX.1980-2015_remapbil_%s.nc'
                    %(path, grid), 'T2MMAX', var_units = True,
                    syear = syear, eyear = eyear)
    elif (variable == 'tasmin'):
        temp = func_read_netcdf(
                    '%s/MERRA2.tavgmon_2d_slv_Nx.T2MMIN.1980-2015_remapbil_%s.nc'
                    %(path, grid), 'T2MMIN', var_units = True,
                    syear = syear, eyear = eyear)
    elif (variable == 'hfls'):
        temp = func_read_netcdf(
                    '%s/MERRA2.tavgmon_2d_flx_Nx.EFLUX.1980-2015_remapbil_%s.nc'
                    %(path, grid), 'EFLUX', var_units = True,
                    syear = syear, eyear = eyear)
    elif (variable == 'hfss'):
        temp = func_read_netcdf(
                    '%s/MERRA2.tavgmon_2d_flx_Nx.HFLUX.1980-2015_remapbil_%s.nc'
                    %(path,grid), 'HFLUX', var_units = True,
                    syear = syear, eyear = eyear)
    elif (variable == 'slp'):
        temp = func_read_netcdf(
                    '%s/MERRA2.tavgmon_2d_slv_Nx.SLP.1980-2015_remapbil_%s.nc'
                    %(path, grid), 'SLP', var_units = True,
                    syear = syear, eyear = eyear)
    temp_obs = temp['data']
    obs_unit = temp['unit']

elif (obsdata == 'HadGHCND'):
    print "Read HadGHCND data"
    path = '%s/monthly' %(obsdir)

    if (variable == 'tasmax'):
        temp = func_read_netcdf(
                '%s/HadGHCND_TXTN_avg_monthly_acts_195001-201412_remapbil_%s.nc'
                                %(path, grid), 'tmax',
                                var_units = True, syear = syear, eyear = eyear)
    elif (variable == 'tasmin'):
        temp = func_read_netcdf(
                '%s/HadGHCND_TXTN_avg_monthly_acts_195001-201412_remapbil_%s.nc'
                                %(path ,grid), 'tmin',
                                var_units = True, syear = syear, eyear = eyear)
    temp_obs = temp['data']
    obs_unit = temp['unit']
else:
    print'Wrong obsdata specified, not available, exit'
    sys.exit

if (obs_unit == 'degC'):
    temp_obs = temp_obs + 273.15
    obs_unit = 'K'

obslat = temp['lat']
obslon = temp['lon']
lat_orig = obslat[:]
lon_orig = obslon[:]
obsdates = temp['dates']
obsmonths = np.asarray([obsdates[i].month for i in xrange(len(obsdates))])
obsyears = np.asarray([obsdates[i].year for i in xrange(len(obsdates))])

# shiftgrid to -180 to 180 if necessary (to be compatible with area mask)
if (obslon[0] >= 0.0) and (obslon[-1] > 180):
    obslons_orig = obslon[:]
    temp_obs, obslon = shiftgrid(180., temp_obs, obslon, start = False)

if isinstance(temp_obs, np.ma.core.MaskedArray):
    temp_obs = temp_obs.filled(np.NaN)

## if any region defined, read region extent from file and cut data into region
if region != None:
    print 'Region is %s' %(region)
    area = region
    mask=np.loadtxt(
            '/home/rlorenz/scripts/plot_scripts/areas_txt/%s.txt'
            %(region))
    temp_obs_reg_nan = np.ndarray((temp_obs.shape))
    for ilat in xrange(len(obslat)):
        for ilon in xrange(len(obslon)):
            if (point_inside_polygon(
                            obslat[ilat], obslon[ilon], np.fliplr(mask))):
                temp_obs_reg_nan[:, ilat, ilon] = temp_obs[:, ilat, ilon]
            else:
                temp_obs_reg_nan[:, ilat, ilon] = np.NaN
    idx_lat = np.where((obslat > np.min(mask[:, 1])) & (obslat < np.max(mask[:, 1])))
    obslat = obslat[idx_lat]
    tmp1_obs_reg = np.empty((len(temp_obs_reg_nan), len(obslat), len(obslon)))
    idx_lon = np.where((obslon > np.min(mask[:, 0])) & (obslon < np.max(mask[:, 0])))
    obslon = obslon[idx_lon]
    temp_obs_reg = np.empty((len(temp_obs_reg_nan), len(obslat), len(obslon)))
    tmp1_obs_reg = np.squeeze(temp_obs_reg_nan[:, idx_lat, :], 1)
    temp_obs_reg = np.squeeze(tmp1_obs_reg[:, :, idx_lon], 2)
    #del temp_obs
else:
    area = 'GLOBAL'
    temp_obs_reg = temp_obs

#calculate climatology
if (outfreq == 'seas'):
    obs_avg = seas_avg_mon_TLL(temp_obs_reg, obsmonths)
elif (outfreq == 'seas_std'):
    obs_avg = seas_std_mon_TLL(temp_obs_reg, obsmonths, obsyears)
elif (outfreq == 'mon'):
    obs_avg = clim_mon_TLL(temp_obs_reg, obsmonths)
else:
    print 'Error on output frequency, not known, exiting'
    sys.exit

# Read model data
name = '%s/%s/%s_%s_*_%s_r1i1p1_%s.nc' %(
        moddir, variable, variable, freq, experiment, grid)
nfiles = len(glob.glob(name))
model_names = []
f = 0
for filename in glob.glob(name):
    fh = nc.Dataset(filename, mode='r')
    model = fh.source_model
    if model == 'ACCESS1.3':
        model = 'ACCESS1-3'
    elif model == 'FGOALS_g2':
        model = 'FGOALS-g2'

    ## if we only want to analyze certain models:
    if not model in ['bcc-csm1-1-m', 'CanESM2', 'CSIRO-Mk3-6-0', 'CMCC-CM', 'GFDL-CM3', 'GISS-E2-R', 'IPSL-CM5A-LR', 'FGOALS-g2', 'MIROC5', 'HadGEM2-AO', 'MPI-ESM-MR', 'MRI-ESM-MR', 'MRI-CGCM3', 'CCSM4', 'NorESM1-M']:
        continue
    ens = fh.source_ensemble
    model_names.append(model+'_'+ens)

    print "Read "+filename+" data"
    model_data = func_read_netcdf(filename, variable, var_units = True,
                                  syear = syear, eyear = eyear)
    temp_mod = model_data['data']
    lat = model_data['lat']
    lon = model_data['lon']
    dates = model_data['dates']
    months = np.asarray([dates[i].month for i in xrange(len(dates))])
    years = np.asarray([dates[i].year for i in xrange(len(dates))])

    # shiftgrid to -180 to 180 if necessary (to be compatible with area mask)
    if (lon[0] >= 0.0) and (lon[-1] > 180):
        lons_orig = lon[:]
        temp_mod, lon = shiftgrid(180., temp_mod, lon, start = False)

    #check that time axis and grid is identical for model0 and modelX and obs
    if temp_obs.shape != temp_mod.shape:
        print 'Warning: Dimension for model and Obs is different!'
        continue
    if any(lon) != any(obslon):
        print 'Warning: Longitudes for model and Obs are different!'
        continue
    if any(lat) != any(obslat):
        print 'Warning: Latitudes for model and Obs are different!'
        continue
    if ((dates[0].year != obsdates[0].year)
        or (dates[0].month != obsdates[0].month)):
        print 'time axis for model and Obs is not identical, exiting!'
        print 'model starts ' + dates[0] + ', while Obs starts ' + obsdates[0]
        sys.exit

    if f == 0:
        mod_bias = np.empty((nfiles, ntim, len(lat), len(lon)))
        dates0 = dates[:]
        temp_mod0 = temp_mod[:]
    else:
        if temp_mod0.shape != temp_mod.shape:
            print 'Warning: Dimension for model0 and modelX is different!'
            continue
        if ((dates[0].year != dates0[0].year)
            or (dates[0].month != dates0[0].month)):
            print 'time axis for model0 and modelX is not identical, exiting!'
            print 'model0 starts %s, while modelX starts %s'  %(
                    dates0[0], dates[0])
            sys.exit

    if isinstance(temp_mod, np.ma.core.MaskedArray):
        temp_mod = temp_mod.filled(np.NaN)

    if (variable == 'sic') and  (model == "EC-EARTH"):
        with np.errstate(invalid = 'ignore'):
            temp_mod[temp_mod < 0.0] = np.NaN
 
    ## if any region defined, cut data into region
    if region != None:
        temp_mod_reg_nan = np.ndarray((temp_mod.shape))
        for ilat in xrange(len(lat)):
            for ilon in xrange(len(lon)):
                if (point_inside_polygon(
                    lat[ilat], lon[ilon], np.fliplr(mask))):
                    temp_mod_reg_nan[:, ilat, ilon] = temp_mod[:, ilat, ilon]
                else:
                    temp_mod_reg_nan[:, ilat, ilon] = np.NaN
        idx_lat = np.where((lat > np.min(mask[:, 1])) & (lat < np.max(mask[:, 1])))
        lat = lat[idx_lat]
        tmp1_mod_reg = np.empty((len(temp_mod_reg_nan), len(lat), len(lon)))
        idx_lon = np.where((lon > np.min(mask[:, 0])) & (lon < np.max(mask[:, 0])))
        lon = lon[idx_lon]
        temp_mod_reg = np.empty((len(temp_mod_reg_nan), len(lat), len(lon)))
        tmp1_mod_reg = np.squeeze(temp_mod_reg_nan[:, idx_lat, :], 1)
        temp_mod_reg = np.squeeze(tmp1_mod_reg[:, :, idx_lon], 2)
        del temp_mod
    else:
        temp_mod_reg = temp_mod

    print np.min(temp_mod_reg), np.max(temp_mod_reg)
    if (f == 0):
        mod_avg = np.empty((nfiles, ntim, len(lat), len(lon)))
        mod_bias = np.empty((nfiles, ntim, len(lat), len(lon)))

    ## calculate climatolotgy
    if (outfreq == 'seas'):
        mod_avg[f, :, :, :] = seas_avg_mon_TLL(temp_mod_reg, months)
    elif (outfreq == 'seas_std'):
        mod_avg[f, :, :, :] = seas_std_mon_TLL(temp_mod_reg, months, years)
    elif (outfreq == 'mon'):
        mod_avg[f, :, :, :] = clim_mon_TLL(temp_mod_reg, months)
    else:
        print 'Error on output frequency, not known, exiting'
        sys.exit
    mod_bias[f, :, :, :] = mod_avg[f, :, :, :] - obs_avg

    ## plot data
    if (plotall == True):
        print 'Plot data for model ' + model + ens
        ## plot seasonal data in panel using gridspec
        if (outfreq == 'seas') or (outfreq == 'seas_std'):
            plotname = '%sdiff_%s_%s_%s_%s_%s_%s_%s_%s_%s.pdf' %(
                       outdir, variable, model, ens, obsdata, outfreq,
                       str(syear), str(eyear), area, grid)
            title =  '%s bias %s - %s'  %(variable, model, obsdata)
            subtitles = ['a) DJF', 'b) MAM', 'c) JJA', 'd) SON'] 
            draw_panel_2x2_h(mod_bias[f, :, :, :], lat, lon,
                             plotname = plotname, title = title,
                             subtitles = subtitles, levels = lev_d,
                             colors = colbar_d, masko = False,
                             region = region, figsize = figuresize)
            call("pdfcrop " + plotname + " " + plotname, shell = True)
        elif (outfreq == 'mon'):
            for m in xrange(0, 11):
                plotname =  '%sdiff_%s_%s_%s_%s_%s_%s_%s_%s_%s.pdf' %(
                            outdir, variable, model, ens, obsdata,
                            month[m], str(syear), str(eyear), area, grid)
                title = '%s bias %s - %s %s' %(
                        variable, model, obsdata, month[m])
                draw(mod_bias[f, :, :, :], lat, lon, title = title,
                     levels = lev_d, colors = colbar_d)
                #plt.show()
                plt.savefig(plotname)
                call("pdfcrop " + plotname + " " + plotname, shell = True)

    f = f + 1

#calculate multi-model mean
if (outfreq == 'seas_std'):
    tmp_pow = np.power(mod_avg, 2)
    mod_avg_mmm = np.sqrt(np.nanmean(tmp_pow, axis = 0))
    tmp_pow = np.power(mod_bias, 2)
    mod_bias_mmm = np.sqrt(np.nanmean(tmp_pow, axis = 0))
else:
    mod_avg_mmm = np.nanmean(mod_avg, axis = 0)
    mod_bias_mmm = np.nanmean(mod_bias, axis = 0)
print np.min(mod_avg_mmm), np.max(mod_avg_mmm)
print np.min(obs_avg), np.max(obs_avg)
print np.min(mod_bias_mmm), np.max(mod_bias_mmm)
#calculate bias
#mmm_bias = mod_avg_mmm - obs_avg
mmm_bias = mod_bias_mmm
print np.min(mmm_bias), np.max(mmm_bias)
## plot data
print 'Plot data multi model mean'
if (outfreq == 'seas') or (outfreq == 'seas_std'):
    ## plot seasonal bias data in panel using gridspec
    plotname =  '%sdiff_%s_mmm_%s_%s_%s_%s_%s_%s.pdf' %(
                outdir, variable, obsdata, outfreq,
                str(syear), str(eyear), area, grid)
    title =  '%s bias mmm - %s %s - %s'  %(
            variable, obsdata, str(syear), str(eyear))
    subtitles = ['a) DJF', 'b) MAM', 'c) JJA', 'd) SON'] 
    draw_panel_2x2_h(mmm_bias, lat, lon, plotname = plotname,
                     title = title, subtitles = subtitles, 
                     levels = lev_d, colors = colbar_d, masko = False,
                     region = region, figsize = figuresize)
    call("pdfcrop " + plotname + " " + plotname, shell = True)

    ## plot seasonal absolute data in panel using gridspec
    plotname = '%s%s_mmm_%s_%s_%s_%s_%s.pdf' %(
               outdir, variable, outfreq, str(syear), str(eyear), area, grid)
    title = '%s mmm %s - %s' %(variable, str(syear), str(eyear))
    draw_panel_2x2_h(mod_avg_mmm, lat, lon, plotname = plotname,
                     title = title, subtitles = subtitles,
                     levels = lev, colors = colbar, masko = False,
                     region = region, figsize = figuresize)
    call("pdfcrop " + plotname + " " + plotname, shell = True)

    plotname = '%s%s_%s_%s_%s_%s_%s_%s.pdf' %(
               outdir, variable, obsdata, outfreq,
               str(syear), str(eyear), area, grid)
    title = '%s %s %s - %s' %(variable, obsdata, str(syear), str(eyear))
    draw_panel_2x2_h(obs_avg, lat, lon, plotname = plotname,
                     title = title, subtitles = subtitles,
                     levels = lev, colors = colbar, masko = False,
                     region = region, figsize = figuresize)
    call("pdfcrop " + plotname + " " + plotname, shell=True)

elif (outfreq == 'mon'):
    for m in xrange(0, 11):
        plotname = '%sdiff_%s_mmm_%s_%s_%s_%s_%s_%s.pdf' %(
                   outdir, variable, obsdata, month[m],
                   str(syear), str(eyear), area, grid)
        title = '%s bias mmm - %s %s %s - %s' %(
                variable, obsdata, month[m], str(syear), str(eyear))
        draw(mmm_bias, lat, lon, title = title,
             levels = lev_d, colors = colbar_d)
        #plt.show()
        plt.savefig(plotname)
        call("pdfcrop " + plotname + " " + plotname, shell = True)

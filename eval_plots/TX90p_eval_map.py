#!/usr/bin/python
'''
File Name : TX90p_eval_map.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 01-09-2016
Modified: Thu 01 Sep 2016 05:17:23 PM CEST
Purpose: plot TX90p heat wave aspects from CMIP5 models
	and obs as maps, global or regional


'''
import netCDF4 as nc # to work with NetCDF files
import numpy as np
import glob as glob
import os # operating system interface
from os.path import expanduser
home = expanduser("~") # Get users home directory
import sys
sys.path.insert(0, home + '/scripts/plot_scripts/utils/')
from func_read_data import func_read_netcdf
from point_inside_polygon import point_inside_polygon
from draw_utils import draw
from draw_panel_utils import draw_panel_2x2_h
from subprocess import call
import matplotlib.pyplot as plt
from draw_utils import draw
from mpl_toolkits.basemap import shiftgrid
###
# Define input
###
variable = 'HWA_tx90pct'
experiment = 'rcp85'
archive1 = '/net/tropo/climphys/rlorenz/CMIP5_HW/TX90_HW/bp_1961-1990/historical_CMIP5_seasons'
archive2 = '/net/tropo/climphys/rlorenz/CMIP5_HW/TX90_HW/bp_1961-1990/%s_CMIP5_seasons' %(experiment)

obsdata = 'ERAint'
grid = 'g025'

syear_eval = 1979
eyear_eval = 2014

syear_hist = 1979
eyear_hist = 2005
syear_rcp = 2006
eyear_rcp = 2014

region = 'CNA_flip'
lev_d = np.arange(-50, 50, 5)  #  np.arange(-6.5, 7.5, 1)
                                   #  np.arange(-3.25, 3.75, 0.5)
colbar_d = "RdBu_r"
lev = np.arange(0, 100, 10)  # np.arange(260, 326, 2)
                         #  np.arange(0, 10, 1)
colbar = "inferno_r"
plotall = False  # plot data for each model or only ensemble mean?
outdir = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/TX90_HW/%s/' %(
        variable)
if (os.access(outdir,os.F_OK)==False):
        os.makedirs(outdir)

#read obs data
if (obsdata == 'ERAint'):
    print "Read ERAint data"
    path = '/net/tropo/climphys/rlorenz/ERA-INT_HW/HW_output/bp_1979_2004/'
    obsfile = 'tx90pct_heatwaves_ERA-INT_yearly_summer_remapcon2_%s.nc' %(grid)

    obs = nc.Dataset('%s%s' %(path, obsfile), mode='r')
    years = obs.variables['time'][:]
    hw_obs = obs.variables[variable]
    unit = hw_obs.units

    hw_obs = obs.variables[variable][
            (years >= syear_eval) & (years <= eyear_eval), :, :]
    obs_lat = obs.variables['lat'][:]
    obs_lon = obs.variables['lon'][:]
    obsyears = obs.variables['time'][
            (years >= syear_eval) & (years <= eyear_eval)]

    if isinstance(hw_obs, np.ma.core.MaskedArray):
        hw_obs_nan = hw_obs.filled(np.NaN)
    else:
        hw_obs_nan = hw_obs
    obs.close()
elif (obsdata == 'MERRA2'):
    print "Read MERRA2 data"
    path = '/net/tropo/climphys/rlorenz/MERRA2_HW/HW_output/bp_1980_2004/'
    obsfile = 'tx90pct_heatwaves_MERRA2_yearly_summer_remapcon2_%s.nc' %(grid)

    obs = nc.Dataset('%s%s' %(path, obsfile), mode='r')
    years = obs.variables['time'][:]
    hw_obs = obs.variables[variable]
    unit = hw_merra.units

    hw_obs = obs.variables[variable][
            (years >= syear_eval) & (years <= eyear_eval), :, :]
    obs_lat = obs.variables['lat'][:]
    obs_lon = obs.variables['lon'][:]
    obsyears = obs.variables['time'][
            (years >= syear_eval) & (years <= eyear_eval)]

    if isinstance(hw_obs, np.ma.core.MaskedArray):
        hw_obs_nan = hw_obs.filled(np.NaN)
    else:
        hw_obs_nan = hw_obs
    obs.close()
##if (obs_lon[0] >= 0.0) and (obs_lon[-1] > 180):
    #flip lon for point_inside_polygon
    #hw_obs_nan, obs_lon =  shiftgrid(180., hw_obs_nan, obs_lon,start=False)

if region != None:
    print 'Region is %s' %(region)
    area = region
    mask = np.loadtxt('/home/rlorenz/scripts/plot_scripts/areas_txt/%s.txt' %(
            region))
    hw_obs_reg_nan = np.ndarray((hw_obs_nan.shape))
    for ilat in xrange(len(obs_lat)):
        for ilon in xrange(len(obs_lon)):
            if (point_inside_polygon(
                            obs_lat[ilat], obs_lon[ilon], np.fliplr(mask))):
                hw_obs_reg_nan[:, ilat, ilon] = hw_obs_nan[:, ilat, ilon]
            else:
                hw_obs_reg_nan[:, ilat, ilon] = np.NaN

    idx_lat = np.where((obs_lat > mask[2,1]) & (obs_lat < mask[0,1]))
    obs_lat = obs_lat[idx_lat]
    tmp1_obs_reg = np.empty((len(hw_obs_reg_nan), len(obs_lat), len(obs_lon)))
    idx_lon = np.where((obs_lon>mask[0, 0]) & (obs_lon<mask[2, 0]))
    obs_lon = obs_lon[idx_lon]
    hw_obs_reg = np.empty((len(hw_obs_reg_nan), len(obs_lat), len(obs_lon)))
    tmp1_obs_reg = np.squeeze(hw_obs_reg_nan[:, idx_lat, :], 1)
    hw_obs_reg = np.squeeze(tmp1_obs_reg[:, :, idx_lon], 2)
    ## calculate average over time
    hw_obs_avg = np.nanmean(hw_obs_reg, axis = 0)
else:
    area = 'GLOBAL'
    ## calculate average over time
    hw_obs_avg = np.nanmean(hw_obs_nan, axis = 0)

## read model data
print "Read model data"
## find all matching files in archive, loop over all of them
## first count matching files in rcp folder to initialze variable per model:
infile_rcp = 'tx90pct_heatwaves_*_summer_remapcon2_%s.nc' %(grid)
name = '%s/%s' %(archive2, infile_rcp)
nfiles = len(glob.glob(name))
model_names = []
#d_hw_mod_avg = dict()
print str(nfiles)+ ' matching files found'
f = 0
for filename in glob.glob(name):
    print "Read " + filename + " data"
    model_data = nc.Dataset(filename, mode='r')
    years = model_data.variables['time'][:]
    hw_rcp = model_data.variables[variable][
            (years >= syear_rcp) & (years <= eyear_rcp), :, :]
    lat = model_data.variables['lat'][:]
    lon = model_data.variables['lon'][:]
    years2 = model_data.variables['time'][
            (years >= syear_rcp) & (years <= eyear_rcp)]
    try:
        model = model_data.model_id
        ens = model_data.parent_experiment_rip
    except (AttributeError):
        continue

    if isinstance(hw_rcp, np.ma.core.MaskedArray):
            hw_rcp_nan = hw_rcp.filled(np.NaN)
    else:
        hw_rcp_nan = hw_rcp
    model_data.close()

    infile_hist = 'tx90pct_heatwaves_%s_historical_%s_yearly_summer_remapcon2_%s.nc' %(
            model, ens, grid)
    try:
        model_data =nc.Dataset('%s/%s' %(archive1, infile_hist), mode='r')
    except (RuntimeError):
        continue
    years = model_data.variables['time'][:]
    hw_hist = model_data[variable][
            (years >= syear_hist) & (years <= eyear_hist), :, :]
    lat2 = model_data.variables['lat'][:]
    lon2 = model_data.variables['lon'][:]
    years1 = model_data.variables['time'][
            (years >= syear_hist) & (years <= eyear_hist)]
    if isinstance(hw_hist, np.ma.core.MaskedArray):
            hw_hist_nan = hw_hist.filled(np.NaN)
    else:
        hw_hist_nan = hw_hist

    model_data.close()

    if (lat.any() != lat2.any()) or (lon.any() != lon2.any()):
        print 'hist and rcp on different grids, exiting'
        continue
    else:
        del lat2, lon2

    if model == 'ACCESS1.3':
            model = 'ACCESS1-3'
    elif model == 'FGOALS_g2':
            model = 'FGOALS-g2'
    elif (model == 'HadGEM2-ES') or (model == 'HadGEM2-CC'):
        continue #HadGEM models have wrong time in HW files
    model_names.append(model + '_' + ens)

    hw_mod = np.concatenate((hw_hist_nan, hw_rcp_nan), axis = 0)

    ## print hw_mod.shape
    #if (lon[0] >= 0.0) and (lon[-1] > 180):
        ## flip lon for point_inside_polygon
        #hw_mod, lon =  shiftgrid(180., hw_mod, lon, start = False)

    years = np.concatenate((years1, years2), axis = 0)

    ## if any region defined, read region extent from file and
    ## cut data into region
    if region != None:
        hw_mod_reg_nan = np.ndarray((hw_mod.shape))
        for ilat in xrange(len(lat)):
            for ilon in xrange(len(lon)):
                if (point_inside_polygon(
                    lat[ilat], lon[ilon], np.fliplr(mask))):
                    hw_mod_reg_nan[:, ilat, ilon] = hw_mod[:, ilat, ilon]
                else:
                    hw_mod_reg_nan[:, ilat, ilon] = np.NaN
        idx_lat = np.where((lat > mask[2, 1]) &
                           (lat < mask[0, 1]))
        lat = lat[idx_lat]
        tmp1_mod_reg = np.empty(
                (len(hw_mod_reg_nan), len(lat), len(lon)))
        idx_lon = np.where((lon > mask[0,0]) &
                           (lon < mask[1,0]))
        lon = lon[idx_lon]
        hw_mod_reg = np.empty(
                (len(hw_mod_reg_nan), len(lat), len(lon)))
        tmp1_mod_reg = np.squeeze(hw_mod_reg_nan[:, idx_lat, :], 1)
        hw_mod_reg = np.squeeze(tmp1_mod_reg[:, :, idx_lon], 2)
    else:
        hw_mod_reg = hw_mod

    if (f == 0):
        hw_mod_avg = np.empty((nfiles, len(lat), len(lon)))
        hw_mod_bias = np.empty((nfiles, len(lat), len(lon)))
    hw_mod_avg[f, :, :] = np.nanmean(hw_mod_reg, axis = 0)
    ## calculate bias
    hw_mod_bias[f, :, :] = hw_mod_avg[f, :, :] - hw_obs_avg

    if (plotall == True):
        ## plot bias for each model
        plotname =  '%sdiff_%s_%s_%s_%s_ANN_%s-%s_%s_%s.pdf ' %(
                    outdir, variable, model, ens, obsdata, str(syear_eval),
                    str(eyear_eval), area, grid)
        title = '%s bias %s - %s %s - %s' %(
                variable, model, obsdata, str(syear_eval), str(eyear_eval))
        if area != 'GLOBAL':
            draw(hw_mod_bias[f, :, :], lat, lon,
                 title = title, levels = lev_d, colors = colbar_d, region = area)
        else:
            draw(hw_mod_bias[f, :, :], lat, lon,
                 title = title, levels = lev_d, colors = colbar_d)
        plt.savefig(plotname)
        call("pdfcrop " + plotname + " " + plotname, shell = True)
    f = f + 1

## calculate multi-model mean
mod_avg_mmm = np.nanmean(hw_mod_avg, axis = 0)
#calculate bias
mmm_bias = mod_avg_mmm - hw_obs_avg

## plot mmm
print 'Plot data multi model mean'
plotname = '%sdiff_%s_mmm_%s_ANN_%s-%s_%s_%s.pdf' %(
           outdir, variable, obsdata,
           str(syear_eval), str(eyear_eval), area, grid)
title = '%s bias mmm - %s ANN %s - %s' %(
        variable, obsdata, str(syear_eval), str(eyear_eval))
if area != 'GLOBAL':
        draw(mmm_bias, lat, lon, title = title, levels = lev_d, colors = colbar_d, region = area)
else:
        draw(mmm_bias, lat, lon, title = title, levels = lev_d, colors = colbar_d)
plt.savefig(plotname)
call("pdfcrop " + plotname + " " + plotname, shell = True)

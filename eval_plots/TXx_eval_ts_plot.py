#!/usr/bin/python
'''
File Name : TXx_eval_ts_plot.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 25-05-2016
Modified: Wed 25 May 2016 11:55:46 AM CEST
Purpose: plot timeseries of TXx from CMIP5, HadGHCND, ERAint and MERRA2 Global or in SREX regions


'''
import netCDF4 as nc # to work with NetCDF files
import numpy as np
from scipy import signal
import os # operating system interface
from os.path import expanduser
home = expanduser("~") # Get users home directory
import glob
import sys
sys.path.insert(0,home+'/scripts/plot_scripts/utils/')
from point_inside_polygon import point_inside_polygon
from func_read_data import func_read_netcdf
import datetime as dt
from netcdftime import utime
#from func_write_netcdf import func_write_netcdf
import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap, addcyclic, shiftgrid
###
# Define input
###
variable = 'TXx'
experiment = 'rcp85'
archive = '/net/bio/climphys2/fischeer/CMIP5/EXTREMES/STATS/'
freq = 'mon'
grid = 'native'
###
# Define analysis and output
###
syear = 1949
eyear = 2100
region =['NAM']
area = ['North America']
outfreq = 'ann'
season = 0
outdir = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/%s/%s/' %(variable,outfreq)

if (os.access(outdir,os.F_OK)==False):
        os.makedirs(outdir)

syear_eval = 1979
eyear_eval = 2015
syear_eval2 = 1980
eyear_eval2 = 2015
syear_eval3 = 1949
eyear_eval3 = 2011

#read obs data
print "Read ERAint data"
path = '/net/tropo/climphys/rlorenz/ERAint_EXTREMES/'

temp = func_read_netcdf('%s/mx2t_ERAint_1979-2015.YEARMAX.nc'
                            %(path),'mx2t',var_units=True,
                            syear=syear_eval, eyear=eyear_eval)
temp_obs = temp['data']
era_unit = temp['unit']
if (era_unit == 'K'):
        temp_obs = temp_obs-273.15
        era_unit = 'degC'
obs_lat = temp['lat']
obs_lon = temp['lon']
obsdates = temp['dates']
obsmonths = np.asarray([obsdates[i].month for i in xrange(len(obsdates))])
obsyears = np.asarray([obsdates[i].year for i in xrange(len(obsdates))])

if isinstance(temp_obs, np.ma.core.MaskedArray):
        temp_obs_nan = temp_obs.filled(np.NaN)
else:
    temp_obs_nan = temp_obs

if (obs_lon[0] >= 0.0) and (obs_lon[-1] > 180):
    #flip lon for point_inside_polygon
    temp_obs_nan, obs_lon =  shiftgrid(180., temp_obs_nan, obs_lon,start=False)

print "Read MERRA2 data"
path = '/net/tropo/climphys/rlorenz/MERRA2_EXTREMES/'

temp = func_read_netcdf('%s/T2MMAX_MERRA2_1980-2015.YEARMAX.nc'
                            %(path), 'T2MMAX', var_units = True,
                            syear = syear_eval2, eyear = eyear_eval2)

temp_merra = temp['data']
merra_unit = temp['unit']
if (merra_unit == 'K'):
        temp_merra = temp_merra - 273.15
        merra_unit = 'degC'
merra_lat = temp['lat']
merra_lon = temp['lon']
merradates = temp['dates']
merramonths = np.asarray([merradates[i].month for i in xrange(len(merradates))])
merrayears = np.asarray([merradates[i].year for i in xrange(len(merradates))])

if isinstance(temp_merra, np.ma.core.MaskedArray):
        temp_merra_nan = temp_merra.filled(np.NaN)
else:
    temp_merra_nan = temp_merra

if (merra_lon[0] >= 0.0) and (merra_lon[-1] > 180):
    #flip lon for point_inside_polygon
    temp_merra_nan, merra_lon =  shiftgrid(180., temp_merra_nan, merra_lon,start=False)

print "Read HadGHCND data"
path = '/net/tropo/climphys/rlorenz/Datasets/HadGHCND/'
ifile = nc.Dataset('%s/HadGHCND_%s-%s_TXx.nc' %(path, syear_eval3, eyear_eval3))

HadG_lat = ifile.variables['lat'][:]
HadG_lon = ifile.variables['lon'][:]
temp_HadG = ifile.variables['Annual'][:]
HadG_unit = 'degC'

if isinstance(temp_HadG, np.ma.core.MaskedArray):
        temp_HadG_nan = temp_HadG.filled(np.NaN)
else:
    temp_HadG_nan = temp_HadG

if (HadG_lon[0] >= 0.0) and (HadG_lon[-1] > 180):
    #flip lon for point_inside_polygon
    temp_HadG_nan, HadG_lon =  shiftgrid(180., temp_HadG_nan, HadG_lon,
                                        start = False)

###read model data
print "Read model data"
##find all matching files in archive, loop over all of them
name = '%s/tasmax_*_%s_*_2006-2100.YEARMAX.nc' %(archive, experiment)
nfiles = len(glob.glob(name))
model_names = []
d_temp_mod = dict()
d_lat = dict()
d_lon = dict()
print str(nfiles)+ ' matching files found'
f = 0
for filename in glob.glob(name):
    print "Read "+filename+" data"
    fh = nc.Dataset(filename,mode='r')
    model = filename.split('_')[1]
    if model == 'CESM-ETH':
            continue
    ens = filename.split('_')[3]

    filename0 = '%s/tasmax_%s_historical_%s_1901-2005.YEARMAX.nc' %(archive,
                                                                    model, ens)
    try:
            ifile = nc.MFDataset([filename0, filename])
    except RuntimeError:
            continue
    lat = ifile.variables['lat'][:]
    lon = ifile.variables['lon'][:]
    nctime = ifile.variables['time']
    try:
            nctime = nc.MFTime(nctime)
    except AttributeError: 
            pass
    except ValueError: 
            pass
    print "Create dates from netcdf time"
    nctime_units = nctime.units
    nctime_cal = nctime.calendar
    cdftime = utime(nctime_units, calendar = nctime_cal)
    dates=cdftime.num2date(nctime[:])
    years=np.asarray([dates[i].year for i in xrange(len(dates))])

    dates=dates[(years >= syear) & (years <= eyear)]
    months=np.asarray([dates[i].month for i in xrange(len(dates))])
    days=np.asarray([dates[i].day for i in xrange(len(dates))])

    temp_mod = ifile.variables['tasmax'][(years >= syear) & (years <= eyear), :, :]
    try:
            Fill = ifile.variables['tasmax']._FillValue
    except AttributeError:
            Fill = 1e20
    years = np.asarray([dates[i].year for i in xrange(len(dates))])
    ifile.close()
    model_names.append(model + '_' + ens)

    #print len(temp_mod)
    if f != 0:
        if (len(temp_mod) != len(temp_mod0)):
                print 'Warning: Dimension for model0 and modelX is different!'
                continue
        if (dates[0].year != dates0[0].year) or (dates[0].month != dates0[0].month):
            print 'time axis for model0 and modelX is not identical, exiting!'
            print 'model0 starts '+ str(dates0[0]) + ', while modelX starts ' + str(dates[0])
            sys.exit
    else:
        dates0 = dates[:]
        temp_mod0 = temp_mod[:]


    if isinstance(temp_mod, np.ma.core.MaskedArray):
            temp_mod = temp_mod.filled(np.NaN)
            #print type(temp_mod), temp_mod.shape

    if (variable == 'sic') and  (model == "EC-EARTH"):
            with np.errstate(invalid = 'ignore'):
                    temp_mod[temp_mod < 0.0] = np.NaN
    temp_mod = temp_mod - 273.15
    unit = 'degC'
    if (lon[0] >= 0.0) and (lon[-1] > 180):
        #flip lon for point_inside_polygon
        temp_mod, lon =  shiftgrid(180., temp_mod, lon, start = False)
    if (len(temp_mod) == len(temp_mod0)):
            d_temp_mod[model+'_'+ens] = temp_mod
            d_lat[model+'_'+ens] = lat
            d_lon[model+'_'+ens] = lon
    f = f + 1

#loop over regions
for reg in xrange(len(region)):
    print 'Region is %s' %(region[reg])
    mask = np.loadtxt('/home/rlorenz/scripts/plot_scripts/areas_txt/%s.txt' %(region[reg]))
    #if ((mask[:,0]).any > 180):
    #    mask[:, 0] = mask[:,0] - 180  
    temp_obs_reg_nan = np.ndarray((temp_obs_nan.shape))
    temp_merra_reg_nan = np.ndarray((temp_merra_nan.shape))
    temp_HadG_reg_nan = np.ndarray((temp_HadG_nan.shape))
    for ilat in xrange(len(obs_lat)):
        for ilon in xrange(len(obs_lon)):
            if (point_inside_polygon(obs_lat[ilat], obs_lon[ilon], np.fliplr(mask))):
                temp_obs_reg_nan[:,ilat,ilon] = temp_obs_nan[:, ilat, ilon]
            else:
                temp_obs_reg_nan[:,ilat,ilon] = np.NaN
    for ilat in xrange(len(merra_lat)):
        for ilon in xrange(len(merra_lon)):
            if (point_inside_polygon(merra_lat[ilat], merra_lon[ilon], np.fliplr(mask))):
                temp_merra_reg_nan[:,ilat,ilon] = temp_merra_nan[:, ilat, ilon]
            else:
                temp_merra_reg_nan[:,ilat,ilon] = np.NaN
    for ilat in xrange(len(HadG_lat)):
        for ilon in xrange(len(HadG_lon)):
            if (point_inside_polygon(HadG_lat[ilat], HadG_lon[ilon], np.fliplr(mask))):
                temp_HadG_reg_nan[:,ilat,ilon] = temp_HadG_nan[:, ilat, ilon]
            else:
                temp_HadG_reg_nan[:,ilat,ilon] = np.NaN

    temp_obs_reg_avg =  np.apply_over_axes(np.nanmean, temp_obs_reg_nan, (1, 2))
    temp_merra_reg_avg =  np.apply_over_axes(np.nanmean, temp_merra_reg_nan, (1, 2))
    temp_HadG_reg_avg =  np.apply_over_axes(np.nanmean, temp_HadG_reg_nan, (1, 2))

    d_temp_mod_reg_avg = dict()
    sum_temp_mod = 0
    for key, value in d_temp_mod.iteritems():
        temp_mod_reg_nan = np.ndarray((d_temp_mod[key].shape))
        for ilat in xrange(len(d_lat[key])):
            for ilon in xrange(len(d_lon[key])):
                if (point_inside_polygon(d_lat[key][ilat],d_lon[key][ilon],np.fliplr(mask))):
                    temp_mod_reg_nan[:,ilat,ilon] = value[:,ilat,ilon]
                else:
                    temp_mod_reg_nan[:,ilat,ilon] = np.NaN

        d_temp_mod_reg_avg[key] =  np.apply_over_axes(np.nanmean, temp_mod_reg_nan, (1, 2))
        sum_temp_mod = sum_temp_mod + d_temp_mod_reg_avg[key]
    mm_ts = sum_temp_mod/len(d_temp_mod_reg_avg.keys())
    
    # plot area average of annual timeseries
    fig = plt.figure(figsize=(10, 5), dpi = 300)
    colors = plt.matplotlib.cm.YlOrRd(np.linspace(0, 1, nfiles + 20))
    for key, value in d_temp_mod_reg_avg.iteritems():
            plt.plot(range(syear, eyear + 1), value[:, 0, 0], color = 'grey',
                     linewidth=0.5)
    plt.plot(range(syear,eyear + 1), value[:, 0, 0], color='grey',
             label = "CMIP5", linewidth = 0.5)
    plt.plot(range(syear, eyear + 1), mm_ts[:, 0, 0], 'k-',
             label = "non-weighted MMM", linewidth = 3.0)
    plt.plot(obsyears, temp_obs_reg_avg[:, 0, 0], color = "mediumblue",
             linestyle = '-',label = "ERAint", linewidth = 3.0)
    plt.plot(merrayears, temp_merra_reg_avg[:, 0, 0], color = "red",
             linestyle = '-', label = "MERRA2", linewidth = 3.0)
    plt.plot(range(syear_eval3, eyear_eval3 + 1), temp_HadG_reg_avg[:, 0, 0],
             color = "purple", linestyle = '-', label = "HadGHCND",
             linewidth = 3.0)
   
    plt.xlabel('Year')
    plt.ylabel(area[reg] + ' ' + variable + ' [' + unit + '] timeseries') #can use LaTeX for subscripts etc
    plt.grid(True)
    leg=plt.legend(loc = 'upper left') #leg defines legend -> can be modified
    leg.draw_frame(False)

    plt.savefig('%s%s_hist_%s_ERAint_MERRA2_HadGHCND_%s_%s_ts.pdf' %(outdir,
                                                                     variable,
                                                                     experiment,
                                                                     region[reg],
                                                                     outfreq))

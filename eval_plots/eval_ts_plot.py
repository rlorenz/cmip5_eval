#!/usr/bin/python
'''
File Name : eval_ts_plot.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 25-05-2016
Modified: Thu 23 Feb 2017 15:22:11 CET
Purpose: plot timeseries from CMIP5, ERAint and MERRA2 Global or in SREX regions

'''
import netCDF4 as nc # to work with NetCDF files
import numpy as np
from scipy import signal
import math
import os # operating system interface
from os.path import expanduser
home = expanduser("~") # Get users home directory
import glob
import sys
sys.path.insert(0,home+'/scripts/plot_scripts/utils/')
#from calc_RMSE_obs_mod_3D import rmse_3D
from func_read_data import func_read_netcdf
#from func_calc_rmse import func_calc_rmse
from clim_seas_TLL import clim_seas_mon_TLL
#from calc_trend_TLL import trend_TLL
from point_inside_polygon import point_inside_polygon
import datetime as dt
from netcdftime import utime
#from func_write_netcdf import func_write_netcdf
import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap, addcyclic, shiftgrid
###
# Define input
###
variable = 'tas'
experiment = 'rcp85'
archive = '/net/atmos/data/cmip5-ng'
freq = 'mon'
grid = 'native'
###
# Define analysis and output
###
syear = 1951
eyear = 2100
region =['NAM']
area = ['North America']
outfreq = 'seas'
season = 2
outdir = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/%s/%s/' %(variable, outfreq)

if (os.access(outdir, os.F_OK) == False):
        os.makedirs(outdir)

syear_eval = 1979
eyear_eval = 2015
syear_eval2 = 1980
eyear_eval2 = 2015
if (variable == 'tasmax' or 'tasmin'):
        HadG = True
        syear_eval3 = 1950
        eyear_eval3 = 2014

#read obs data
print "Read ERAint data"
path = '/net/tropo/climphys/rlorenz/Datasets/ERAint/ERAint_mon/'
if (variable == 'tas'):
    temp = func_read_netcdf('%s/T2M_monthly_ERAint_197901-201601_g025.nc'
                            %(path), 'T2M', var_units = True,
                            syear = syear_eval, eyear = eyear_eval)
    temp_obs = temp['data']
    obs_unit = temp['unit']
elif (variable == 'pr'):
    temp = func_read_netcdf('%s/RTOT_monthly_ERAint_197901-201412.nc'
                            %(path), 'RTOT', var_units = True,
                            syear = syear_eval, eyear = eyear_eval)
    temp_obs = temp['data']
    obs_unit = temp['unit']
elif (variable == 'sic'):
    temp = func_read_netcdf('%s/ci_monthly_ERAint_197901-201601.nc'
                            %(path), 'ci', syear = syear_eval, eyear = eyear_eval)
    #ERAint data is in fraction -> convert to percent as in CMIP5
    temp_obs = temp['data'] * 100
    obs_unit = 'percent'
elif (variable == 'hfls'):
    temp = func_read_netcdf('%s/slhf_monthly_ERAint_197901-201512.nc'
                            %(path), 'slhf', var_units = True,
                            syear = syear_eval, eyear = eyear_eval)
    temp_obs = -temp['data']
    obs_unit = temp['unit']
elif (variable == 'tasmax'):
    temp = func_read_netcdf('%s/mx2t_monmean_ERAint_197901-201512.nc'
                            %(path), 'mx2t', var_units = True,
                            syear = syear_eval, eyear = eyear_eval)
    temp_obs = temp['data']
    obs_unit = temp['unit']
if (obs_unit == 'K'):
        temp_obs = temp_obs - 273.15
        obs_unit = 'degC'
obs_lat = temp['lat']
obs_lon = temp['lon']
obsdates = temp['dates']
obsmonths = np.asarray([obsdates[i].month for i in xrange(len(obsdates))])
obsyears = np.asarray([obsdates[i].year for i in xrange(len(obsdates))])

if isinstance(temp_obs, np.ma.core.MaskedArray):
        temp_obs_nan = temp_obs.filled(np.NaN)
else:
    temp_obs_nan = temp_obs

if (obs_lon[0] >= 0.0) and (obs_lon[-1] > 180):
    #flip lon for point_inside_polygon
    temp_obs_nan, obs_lon =  shiftgrid(180., temp_obs_nan, obs_lon, start = False)

print "Read MERRA2 data"
path = '/net/tropo/climphys/rlorenz/Datasets/MERRA2/monthly'
if (variable == 'tas'):
    temp = func_read_netcdf('%s/MERRA2.tavgmon_2d_slv_Nx.T2MMEAN.1980-2015.nc'
                            %(path), 'T2MMEAN', var_units = True,
                            syear = syear_eval2, eyear = eyear_eval2)
elif (variable == 'tasmax'):
    temp = func_read_netcdf('%s/MERRA2.tavgmon_2d_slv_Nx.T2MMAX.1980-2015.nc' %(path)
                            , 'T2MMAX', var_units = True,
                            syear = syear_eval2, eyear = eyear_eval2)
elif (variable == 'tasmin'):
    temp = func_read_netcdf('%s/MERRA2.tavgmon_2d_slv_Nx.T2MMIN.1980-2015.nc'
                            %(path), 'T2MMIN', var_units = True,
                            syear = syear_eval2, eyear = eyear_eval2)
elif (variable == 'hfls'):
    temp = func_read_netcdf('%s/MERRA2.tavgmon_2d_flx_Nx.EFLUX.1980-2015.nc'
                            %(path), 'EFLUX', var_units = True,
                            syear = syear_eval2, eyear = eyear_eval2)
elif (variable == 'hfss'):
    temp = func_read_netcdf('%s/MERRA2.tavgmon_2d_flx_Nx.HFLUX.1980-2015.nc'
                            %(path), 'HFLUX', var_units = True,
                            syear = syear_eval2, eyear = eyear_eval2)
elif (variable == 'slp'):
    temp = func_read_netcdf('%s/MERRA2.tavgmon_2d_slv_Nx.SLP.1980-2015.nc'
                            %(path), 'SLP', var_units = True,
                            syear = syear_eval2, eyear = eyear_eval2)
temp_merra = temp['data']
merra_unit = temp['unit']
if (merra_unit == 'K'):
        temp_merra = temp_merra - 273.15
        merra_unit = 'degC'
merra_lat = temp['lat']
merra_lon = temp['lon']
merradates = temp['dates']
merramonths = np.asarray([merradates[i].month for i in xrange(len(merradates))])
merrayears = np.asarray([merradates[i].year for i in xrange(len(merradates))])

if isinstance(temp_merra, np.ma.core.MaskedArray):
        temp_merra_nan = temp_merra.filled(np.NaN)
else:
    temp_merra_nan = temp_merra

if (merra_lon[0] >= 0.0) and (merra_lon[-1] > 180):
    #flip lon for point_inside_polygon
    temp_merra_nan, merra_lon =  shiftgrid(180., temp_merra_nan, merra_lon,
                                           start = False)

if HadG:
    print "Read HadGHCND data"
    path = '/net/tropo/climphys/rlorenz/Datasets/HadGHCND/monthly'
    if (variable == 'tasmax'):
            temp = func_read_netcdf('%s/HadGHCND_TXTN_avg_monthly_acts_195001-201412.nc' 
                                    %(path), 'tmax', lonvar = 'longitude', latvar = 'latitude', var_units = True,
                                    syear = syear_eval3, eyear = eyear_eval3)
    elif (variable == 'tasmin'):
            temp = func_read_netcdf('%s/HadGHCND_TXTN_avg_monthly_acts_195001-201412.nc'
                                    %(path), 'tmin', lonvar = 'longitude', latvar = 'latitude', var_units = True,
                                    syear = syear_eval3, eyear = eyear_eval3)
    temp_hadg = temp['data']
    hadg_unit = temp['unit']
    if (hadg_unit == 'K'):
        temp_hadg = temp_hadg - 273.15
        hadg_unit = 'degC'
    hadg_lat = temp['lat']
    hadg_lon = temp['lon']
    hadgdates = temp['dates']
    hadgmonths = np.asarray([hadgdates[i].month for i in xrange(len(hadgdates))])
    hadgyears = np.asarray([hadgdates[i].year for i in xrange(len(hadgdates))])

    if isinstance(temp_hadg, np.ma.core.MaskedArray):
        temp_hadg_nan = temp_hadg.filled(np.NaN)
    else:
        temp_hadg_nan = temp_hadg

    if (hadg_lon[0] >= 0.0) and (hadg_lon[-1] > 180):
        #flip lon for point_inside_polygon
        temp_hadg_nan, hadg_lon =  shiftgrid(180., temp_hadg_nan, hadg_lon, start = False)

###read model data
print "Read model data"
##find all matching files in archive, loop over all of them
name = '%s/%s/%s_%s_*_%s_*_%s.nc' %(archive, variable, variable, freq, experiment, grid)
nfiles = len(glob.glob(name))
model_names = []
d_temp_mod = dict()
d_lat = dict()
d_lon = dict()
print str(nfiles)+ ' matching files found'
f = 0
for filename in glob.glob(name):
    print "Read " + filename + " data"
    model_data = func_read_netcdf(filename, variable, var_units = True,
                                  syear = syear, eyear = eyear)
    temp_mod = model_data['data']
    lat = model_data['lat']
    lon = model_data['lon']
    dates = model_data['dates']
    months = np.asarray([dates[i].month for i in xrange(len(dates))])
    years = np.asarray([dates[i].year for i in xrange(len(dates))])

    # shiftgrid to -180 to 180 if necessary (to be compatible with area mask)
    if (lon[0] >= 0.0) and (lon[-1] > 180):
        lons_orig = lon[:]
        temp_mod, lon = shiftgrid(180., temp_mod, lon, start = False)
    
    #print len(temp_mod)
    if f != 0:
        if (len(temp_mod) != len(temp_mod0)):
                print 'Warning: Dimension for model0 and modelX is different!'
                continue
        if (dates[0].year != dates0[0].year) or (dates[0].month != dates0[0].month):
            print 'time axis for model0 and modelX is not identical, exiting!'
            print 'model0 starts '+str(dates0[0])+ ', while modelX starts '+str(dates[0])
            sys.exit
    else:
        dates0 = dates[:]
        temp_mod0 = temp_mod[:]

    fh = nc.Dataset(filename,mode='r')
    model = fh.source_model
    if model == 'ACCESS1.3':
            model = 'ACCESS1-3'
    elif model == 'FGOALS_g2':
            model = 'FGOALS-g2'
    ens = fh.source_ensemble
    model_names.append(model+'_'+ens)

    if isinstance(temp_mod, np.ma.core.MaskedArray):
        temp_mod = temp_mod.filled(np.NaN)
        #print type(temp_mod), temp_mod.shape

    if (variable == 'sic') and  (model=="EC-EARTH"):
        with np.errstate(invalid='ignore'):
            temp_mod[temp_mod < 0.0] = np.NaN
    if (variable == 'tas') or (variable == 'tasmax') or  (variable == 'tasmin'):
            temp_mod = temp_mod - 273.15
            unit = 'degC'
    if (lon[0]>=0.0) and (lon[-1] > 180):
        #flip lon for point_inside_polygon
        temp_mod,lon =  shiftgrid(180., temp_mod, lon,start=False)
    if (len(temp_mod) == len(temp_mod0)):
            d_temp_mod[model+'_'+ens] = temp_mod
            d_lat[model+'_'+ens] = lat
            d_lon[model+'_'+ens] = lon
    f = f + 1

#loop over regions
for reg in xrange(len(region)):
    print 'Region is %s' %(region[reg])
    mask = np.loadtxt('/home/rlorenz/scripts/plot_scripts/areas_txt/%s.txt' %(region[reg]))
    temp_obs_reg_nan = np.ndarray((temp_obs_nan.shape))
    temp_merra_reg_nan = np.ndarray((temp_merra_nan.shape))
    for ilat in xrange(len(obs_lat)):
        for ilon in xrange(len(obs_lon)):
            if (point_inside_polygon(obs_lat[ilat], obs_lon[ilon], np.fliplr(mask))):
                temp_obs_reg_nan[:, ilat, ilon] = temp_obs_nan[:, ilat, ilon]
            else:
                temp_obs_reg_nan[:, ilat, ilon] = np.NaN
    for ilat in xrange(len(merra_lat)):
        for ilon in xrange(len(merra_lon)):
            if (point_inside_polygon(merra_lat[ilat], merra_lon[ilon], np.fliplr(mask))):
                temp_merra_reg_nan[:, ilat, ilon] = temp_merra_nan[:, ilat, ilon]
            else:
                temp_merra_reg_nan[:, ilat, ilon] = np.NaN
    if HadG:
        temp_hadg_reg_nan = np.ndarray((temp_hadg_nan.shape))
        for ilat in xrange(len(hadg_lat)):
            for ilon in xrange(len(hadg_lon)):
                if (point_inside_polygon(hadg_lat[ilat], hadg_lon[ ilon], np.fliplr(mask))):
                    temp_hadg_reg_nan[:, ilat, ilon] = temp_hadg_nan[:, ilat, ilon]
                else:
                    temp_hadg_reg_nan[:, ilat, ilon] = np.NaN

    #idx_lat = np.where((obs_lat > mask[2, 1]) & (obs_lat < mask[0, 1]))
    #obs_lat = obs_lat[idx_lat]
    #tmp1_obs_reg = np.empty((len(temp_obs_reg_nan), len(obs_lat), len(obs_lon)))
    #idx_lon = np.where((obs_lon > mask[0, 0]) & (obs_lon < mask[2, 0]))
    #obs_lon = obs_lon[idx_lon]
    #temp_obs_reg = np.empty((len(temp_obs_reg_nan), len(obs_lat), len(obs_lon)))
    #tmp1_obs_reg = np.squeeze(temp_obs_reg_nan[:, idx_lat, :], 1)
    #temp_obs_reg = np.squeeze(tmp1_obs_reg[:, :, idx_lon], 2)
    temp_obs_seas = clim_seas_mon_TLL(temp_obs_reg_nan, obsmonths, obsyears)
    temp_merra_seas = clim_seas_mon_TLL(temp_merra_reg_nan, merramonths, merrayears)
    if HadG:
            temp_hadg_seas = clim_seas_mon_TLL(temp_hadg_reg_nan, hadgmonths,
                                               hadgyears)
    temp_obs_reg_avg =  np.apply_over_axes(np.nanmean, temp_obs_seas[season, :, :, :], (1, 2))
    temp_merra_reg_avg =  np.apply_over_axes(np.nanmean, temp_merra_seas[season, :, :, :], (1, 2))
    if HadG:
            temp_hadg_reg_avg =  np.apply_over_axes(np.nanmean, temp_hadg_seas[season, :, :, :], (1, 2))

    d_temp_mod_reg_avg = dict()
    sum_temp_mod = 0
    for key, value in d_temp_mod.iteritems():
        temp_mod_reg_nan = np.ndarray((d_temp_mod[key].shape))
        for ilat in xrange(len(d_lat[key])):
            for ilon in xrange(len(d_lon[key])):
                if (point_inside_polygon(d_lat[key][ilat], d_lon[key][ilon], np.fliplr(mask))):
                    temp_mod_reg_nan[:, ilat, ilon] = value[:, ilat, ilon]
                else:
                    temp_mod_reg_nan[:, ilat, ilon] = np.NaN
        #idx_lat = np.where((d_lat[key] > mask[2, 1]) & (d_lat[key] < mask[0, 1]))
        #d_lat[key] = d_lat[key][idx_lat]
        #tmp1_mod_reg = np.empty((len(temp_mod_reg_nan), len(d_lat[key]), len(d_lon[key])))
        #idx_lon = np.where((d_lon[key] > mask[0, 0]) & (d_lon[key] < mask[1, 0]))
        #d_lon[key] = d_lon[key][idx_lon]
        #temp_mod_reg = np.empty((len(temp_mod_reg_nan), len(d_lat[key]), len(d_lon[key])))
        #tmp1_mod_reg = np.squeeze(temp_mod_reg_nan[:, idx_lat, :], 1)
        #temp_mod_reg = np.squeeze(tmp1_mod_reg[:, :, idx_lon], 2)
        temp_mod_seas = clim_seas_mon_TLL(temp_mod_reg_nan, months, years)
	# weighted area average
	rad = 4.0 * math.atan(1.0) / 180
	w_lat = np.cos(lat * rad) #weight for latitude differences in area

        ma_temp_mod = np.ma.masked_array(temp_mod_seas, np.isnan(value))
        tmp_latweight = np.ma.average(np.squeeze(ma_temp_mod), axis = 0,
                                      weights = w_lat)
        d_temp_mod_reg_avg[key] = np.nanmean(tmp_latweight.filled(np.nan))
        del tmp_latweight

        sum_temp_mod = sum_temp_mod + d_temp_mod_reg_avg[key]

    mm_ts = sum_temp_mod / len(d_temp_mod_reg_avg.keys())
    
    # plot area average of annual timeseries
    fig = plt.figure(figsize=(10, 5), dpi = 300)
    colors = plt.matplotlib.cm.YlOrRd(np.linspace(0, 1, nfiles + 20))
    for key, value in d_temp_mod_reg_avg.iteritems():
        plt.plot(range(syear, eyear + 1), value[:, 0, 0], color = 'grey',
                 linewidth = 0.5)
    plt.plot(range(syear, eyear + 1), value[:, 0, 0], color = 'grey',
             label = "CMIP5",linewidth = 0.5)
    plt.plot(range(syear, eyear + 1), mm_ts[:, 0, 0], 'k-',
             label = "non-weighted MMM", linewidth = 3.0)
    plt.plot(range(syear_eval, eyear_eval + 1), temp_obs_reg_avg[:, 0, 0],
             color = "mediumblue", linestyle = '-', label = "ERAint",
             linewidth = 3.0)
    plt.plot(range(syear_eval2, eyear_eval2 + 1), temp_merra_reg_avg[:, 0, 0],
             color = "red", linestyle = '-', label = "MERRA2", linewidth = 3.0)
    if HadG:
        plt.plot(range(syear_eval3, eyear_eval3 + 1),
                 temp_hadg_reg_avg[:, 0, 0], color = "purple", linestyle = '-',
                 label = "HadGHCND", linewidth = 3.0)
    plt.xlabel('Year')
    plt.ylabel(area[reg] + ' ' + variable + ' [' + unit + '] timeseries') #can use LaTeX for subscripts etc
    plt.grid(True)
    leg=plt.legend(loc = 'upper left') #leg defines legend -> can be modified
    leg.draw_frame(False)
    if HadG:
            plt.savefig('%s%s_hist_%s_ERAint_MERRA2_HadGHCND_%s_%s%s_ts.pdf' %(outdir, variable, experiment, region[reg], outfreq, season))
    else:
            plt.savefig('%s%s_hist_%s_ERAint_MERRA2_%s_%s%s_ts.pdf' %(outdir, variable, experiment, region[reg], outfreq, season))

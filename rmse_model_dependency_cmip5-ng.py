#!/usr/bin/python
'''
File Name : rmse_model_dependency_cmip5-ng.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 29-02-2016
Modified: Mon 29 Feb 2016 02:24:35 PM CET
Purpose: Calculate RMSE between models to determine interdependency


'''
import netCDF4 as nc # to work with NetCDF files
import numpy as np
import datetime as dt
from math import atan
from netcdftime import utime
import calendar
import matplotlib.pyplot as plt
import os # operating system interface
from os.path import expanduser
home = expanduser("~") # Get users home directory
import glob
import sys
sys.path.insert(0,home+'/scripts/plot_scripts/utils/')
from calc_RMSE_obs_mod_3D import rmse_3D
###
# Define input
###
variable = 'tas'

archive = '/net/atmos/data/cmip5-ng'
#workdir = '/net/tropo/climphys/rlorenz/cmip5-ng/work/'
outdir = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/%s/' %(variable)
infile = variable+'_mon'
syear = 1979
eyear = 2005
nyears = eyear - syear + 1

experiment = 'historicalGHG'
grid = 'g025'
#ens = 'r1i1p1'

# free parameter "radius of similarity" , minimum: internal variability ~0.04
# the larger the more distant models are considered similar
sigma_S2 = 0.1

if (os.access(outdir,os.F_OK)==False):
        os.makedirs(outdir)

###read model data
##find all matching files in archive, loop over all of them
#first count matching files in folder to initialze variable rmse per model:
name = '%s/%s/%s_*_%s_*_%s.nc' %(archive,variable,infile,experiment,grid)
nfiles = len(glob.glob(name))
model_names = []
d_temp_mod_clim = {}

print str(nfiles)+ ' matching files found'
f = 0
for filename in glob.glob(name):
    fh = nc.Dataset(filename,mode='r')
    lon = fh.variables['lon'][:]
    lat = fh.variables['lat'][:]
    time = fh.variables['time']
    print "Create dates from netcdf time for model"
    nccalendar = time.calendar
    cdftime = utime(time.units,calendar=time.calendar)
    dates=cdftime.num2date(time[:])
    years=np.asarray([dates[i].year for i in xrange(len(dates))])
    dates=dates[(years>=syear) & (years<=eyear)]
    months = np.asarray([dates[i].month for i in xrange(len(dates))])
    print "Read "+filename+" data"
    temp_mod= fh.variables[variable][(years>=syear) & (years<=eyear),:,:]

    #check that time axis and grid is identical for model0 and modelX
    if f != 0:
        if temp_mod0.shape != temp_mod.shape:
            print 'Warning: Dimension for model0 and modelX is different!'
            continue
        if (dates[0].year != dates0[0].year) or (dates[0].month != dates0[0].month) or (dates[0].day != dates0[0].day):
            print 'time axis for model0 and modelX is not identical, exiting!'
            print 'model0 starts '+dates0[0]+ ', while modelX starts '+dates[0]
            sys.exit
    else:
        dates0 = dates[:]
        temp_mod0 = temp_mod[:]

    model = fh.source_model
    ens = fh.source_ensemble
    model_names.append(model+'_'+ens)

    temp_mod_clim = np.ndarray((12,len(lat),len(lon)))
    #calculate climatology
    for m in range(1,13):
        M = m-1
        temp_mod_clim[M,:,:] = np.mean(temp_mod[months==m,:,:],axis = 0)

    f = f+1
    d_temp_mod_clim[model+'_'+ens] = temp_mod_clim
    fh.close()

d_S = {} 
#loop over all models and calculate rmse compared to each other model
for m in range(len(model_names)):
    model = model_names[m]
    #tmp_mod0 = d_temp_mod_clim[model]
    #print tmp_mod0.shape
    S_all = 0
    for compare in model_names:
        if (model == compare):
            continue
        else:
            rmse = rmse_3D(d_temp_mod_clim[compare],d_temp_mod_clim[model],lat,lon,annual=True)
            S =  np.exp(-(rmse/sigma_S2))
            S_all = S_all + S
    d_S[model] = S_all

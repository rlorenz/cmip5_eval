#!/usr/bin/python
'''
File Name : plot_percent_perf_mod_test.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 07-03-2018
Modified: Wed 07 Mar 2018 05:43:01 PM CET
Purpose: plot percent models within 90% at certain sigma_s value for all sigma_d


'''
import numpy as np
import netCDF4 as nc
import matplotlib.pyplot as plt
###
# Define input & output
###
target_var = 'tasmax'
target_file = 'CLIM'
target_seas = 'JJA'
region = 'NAM'
obsdata = 'ERAint'
diags = ['1', '2', '3', '4', '5', '6']
###
# Read data
###
test = dict()
d2 = dict()
for i in diags:
    infile = 'percent_%s_%s_%s_%s_%s_RMSE_%s.nc' %(target_var, target_file,
                                                   target_seas, i, 
                                                   region, obsdata)
    ifile = nc.Dataset(infile, mode = 'r')
    d2[i] = ifile.variables['D2'][:]
    test[i] = ifile.variables['test'][11, :]
    ifile.close()


###
# Plot data
###
fig = plt.figure(figsize = (10, 8), dpi = 300)
plt.tick_params(axis = 'both', which = 'major', labelsize = 15)
plt.axhline(y = 0.8, color = 'k', linewidth = 1)

plt.plot(d2['1'], test['1'], color = 'purple', linewidth = 3, label = '1 diag')
plt.plot(d2['2'], test['2'], color = 'seagreen', linewidth = 3, label = '2 diags')
plt.plot(d2['3'], test['3'], color = 'firebrick', linewidth = 3, label = '3 diags')
plt.plot(d2['4'], test['4'], color = 'b', linewidth = 3, label = '4 diags')
plt.plot(d2['5'], test['5'], color = 'orange', linewidth = 3, label = '5 diags')
plt.plot(d2['6'], test['6'], color = 'grey', linewidth = 3, label = '6 diags')

plt.axvline(x = 0.5, color = 'k', linewidth = 2)

plt.title('Perfect model test in %s for %s at $\sigma_S$ $\simeq$ 0.6' %(region, obsdata), fontsize = 18)
plt.xlabel('Parameter for weighting model performance $\sigma_D$', fontsize = 18)
plt.ylabel('Fraction of perfect model tests within 10-90%', fontsize = 18)

leg = plt.legend(loc = 'lower right', fontsize = 18)  # leg defines legend -> can be modified
leg.draw_frame(False)

plt.savefig('percent_perf_mod_test_%s_%s_%s_%s_%s.pdf' %(
    target_var, target_file, target_seas, region, obsdata))

#!/usr/bin/python
'''
File Name : plot_ts_constrained_ensemble.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 22-11-2017
Modified: Wed 22 Nov 2017 10:10:08 AM CET
Purpose: plot timeseries of constrained ensemble determined with
         em_const_lin_reg_hist_to_fut_CMIP5_cdo.py

'''
import numpy as np
import netCDF4 as nc
from netcdftime import utime
import datetime as dt
import math
import os
from os.path import expanduser
home = expanduser("~") # Get users home directory
import sys
sys.path.insert(0, home+'/scripts/plot_scripts/utils/')
from func_read_data import func_read_netcdf
from func_write_netcdf import func_write_netcdf
import matplotlib
import matplotlib.pyplot as plt

###
# Define input & output
###
experiment = 'rcp85'
archive = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/'

target_var = 'TXx'
target_file = 'CLIM'
target_mask = 'maskT'
res_name_target = 'ANN'
res_time_target = 'MAX'
freq = 'ann'

diag_var = ['TXx', 'TXx', 'pr'] #]
var_file = ['TREND', 'CLIM', 'TREND'] #
res_name = ['ANN', 'ANN', 'JJA'] #
res_time = ['MAX', 'MAX', 'MEAN']
freq_v = ['ann', 'ann', 'mon']
masko = ['maskT', 'maskT', 'maskT'] #
nvar = len(diag_var)

region = 'CNEU'          #cut data over region?
longname_region = 'Central Europe' # for title plot
obsdata = ['MERRA2'] #'Obs', 'MERRA2', 
nobs = len(obsdata)

path = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/'
outdir = '%semerg_const/%s/%s/' %(path, target_var, region)

syear_eval = 1980
eyear_eval = 2014
syear = 1951
eyear = 2100

if (os.access(outdir, os.F_OK) == False):
        os.makedirs(outdir)

###
# Read data
###
## first read model names for all var_file from text file
## select all models that occur for all var_files,
models = dict()
overlap = list()
for v in xrange(len(var_file)):
    filename = '%s%s/%s/%s/%s_%s_%s_all_%s_%s-%s' %(
        path, diag_var[v], freq_v[v], masko[v], diag_var[v], var_file[v],
        res_name[v], experiment, syear_eval, eyear_eval)
    if (os.access(filename + '.txt', os.F_OK) == True):
        models[var_file[v]] = np.genfromtxt(filename + '.txt',
                                            delimiter = '',
                                            dtype = None).tolist()[: - 1]
        if v > 0:
            overlap = list(set(models[var_file[v]]) & set(overlap))
        else:
            overlap = models[var_file[v]]

## read obs data
obs_ts_areaavg = np.nan
era_ts_areaavg = np.nan
merra_ts_areaavg = np.nan
for o in obsdata:
    print "Read %s data" %(o)
    if region:
        obsfile_ts = '%s%s/%s/%s/%s_%s_%s_%s-%s_%s%s_%s.nc' %(
            path, target_var, freq, target_mask, target_var, freq, o,
            syear_eval, eyear_eval, res_name_target, res_time_target, region)
    else:
        obsfile_ts = '%s%s/%s/%s/%s_%s_%s_%s-%s_%s%s.nc' %(
            path, target_var, freq, target_mask, target_var, freq, o,
            syear_eval, eyear_eval, res_name_target, res_time_target)
    
    fh = nc.Dataset(obsfile_ts, mode = 'r')
    temp_obs_ts = fh.variables[target_var][:]
    lat = fh.variables['lat'][:]
    lon = fh.variables['lon'][:]

    time = fh.variables['time']
    cdftime = utime(time.units, calendar = time.calendar)
    obsdates = cdftime.num2date(time[:])
    obsyears = np.asarray([obsdates[i].year for i in xrange(len(obsdates))])
    fh.close()
    # mask data to obs mask if available
    if isinstance(temp_obs_ts, np.ma.core.MaskedArray):
        if o == 'Obs':
            maskmiss = temp_obs_ts.mask.copy()
            ma_temp_obs_ts = temp_obs_ts
        else:
            try:
                ma_temp_obs_ts = np.ma.array(temp_obs_ts, mask = maskmiss)
            except(NameError):
                ma_temp_obs_ts = temp_obs_ts
    else:
        ma_tmp = np.ma.masked_array(temp_obs_ts, np.isnan(temp_obs_ts))
        try:
            ma_temp_obs_ts = np.ma.array(temp_obs_ts, mask = maskmiss)
        except (NameError):
            ma_temp_obs_ts = ma_tmp
        
    ## calculate area means
    rad = 4.0 * math.atan(1.0) / 180
    w_lat = np.cos(lat * rad) # weight for latitude differences in area
    tmp_latweight = np.ma.empty((len(obsyears), len(lon)))
    for ilon in xrange(len(lon)):
        tmp_latweight[:, ilon] = np.ma.average(ma_temp_obs_ts[:, :, ilon],
                                               axis = 1, weights = w_lat)
    if (o == 'MERRA2'):
        merra_ts_areaavg = np.nanmean(tmp_latweight.filled(np.nan), axis = 1)
    elif (o == 'ERAint'):
        era_ts_areaavg = np.nanmean(tmp_latweight.filled(np.nan), axis = 1)
    else:
        obs_ts_areaavg = np.nanmean(tmp_latweight.filled(np.nan), axis = 1)

###read model data
print "Read model data"
d_temp_mod_ts = dict()
d_temp_mod_ts_areaavg = dict()
nfiles = len(overlap)
print '%s matching files' %(str(nfiles))
for f in xrange(len(overlap)):
    model = overlap[f].split('_', 1)[0]
    ens = overlap[f].split('_', 1)[1]
    if region:
        modfile_ts = '%s%s/%s/%s/%s_%s_%s_%s_%s_%s-%s_%s%s_%s.nc' %(
            path, target_var, freq, target_mask, target_var, freq, model,
            experiment, ens, syear, eyear, res_name_target, res_time_target,
            region)
    else:
        modfile_ts = '%s%s/%s/%s/%s_%s_%s_%s_%s_%s-%s_%s%s.nc' %(
            path, target_var, freq, target_mask, target_var, freq, model,
            experiment, ens, syear, eyear, res_name_target, res_time_target)

    fh = nc.Dataset(modfile_ts, mode = 'r')
    tmp = fh.variables[target_var]
    unit = tmp.units
    temp_mod_ts = fh.variables[target_var][:]
    lat = fh.variables['lat'][:]
    lon = fh.variables['lon'][:]
    time = fh.variables['time']
    cdftime = utime(time.units, calendar = time.calendar)
    dates = cdftime.num2date(time[:])
    years = np.asarray([dates[i].year for i in xrange(len(dates))])
    fh.close()
    if isinstance(temp_mod_ts, np.ma.core.MaskedArray):
        try:
            tmp_ts = np.ma.array(temp_mod_ts,
                                 mask = np.tile(maskmiss[0, :, :],
                                                (temp_mod_ts.shape[0], 1)))
            d_temp_mod_ts[model + '_' + ens] = tmp_ts.filled(np.nan)
        except (NameError):
            d_temp_mod_ts[model + '_' + ens] = temp_mod_ts[:]
    else:
        ma_tmp = np.ma.masked_array(temp_mod_ts, np.isnan(temp_mod_ts))
        try:
            tmp_ts = np.ma.array(ma_tmp,
                                 mask = np.tile(maskmiss[0, :, :],
                                                (temp_mod_ts.shape[0], 1)))
            d_temp_mod_ts[model + '_' + ens] = tmp_ts.filled(np.nan)
        except (NameError):
            d_temp_mod_ts[model + '_' + ens] = temp_mod_ts[:]
    ## calculate weighted area average
    ma_temp_mod_ts = np.ma.masked_array(d_temp_mod_ts[model + '_' + ens],
                                        np.isnan(d_temp_mod_ts[model + '_' +
                                                               ens]))
    tmp_latweight = np.ma.empty((len(years), len(lon)))
    for ilon in xrange(len(lon)):
        tmp_latweight[:, ilon] = np.ma.average(ma_temp_mod_ts[:, :, ilon],
                                               axis = 1, weights = w_lat)
    d_temp_mod_ts_areaavg[model + '_' + ens] = np.nanmean(tmp_latweight.filled(
        np.nan), axis = 1)

# calculate unweighted mean
# first average over initial conditions ensembles per model
models = [x.split('_')[0] for x in d_temp_mod_ts.keys()]
mult_ens = []
seen = set()
for m in models:
    if m not in seen:
        seen.add(m)
    else:
        mult_ens.append(m)
# make sure every model only once in list
list_with_mult_ens = set(mult_ens)
d_avg_ens_ts = dict()
for key, value in d_temp_mod_ts_areaavg.iteritems():
    if key.split('_')[0] in list_with_mult_ens:
        #find other ensemble members
        ens_mem = [value2 for key2, value2 in sorted(
            d_temp_mod_ts_areaavg.iteritems()) if key.split('_')[0] in key2]
        d_avg_ens_ts[key.split('_')[0]] = np.nanmean(ens_mem, axis = 0)
    else:
        d_avg_ens_ts[key.split('_')[0]] = value
tmp3 = 0
for key, value in d_avg_ens_ts.iteritems():
    tmp3 = tmp3 + value

mm_ts_areaavg = tmp3 / len(set(models))
print 'Size of area averaged mm: %s' %mm_ts_areaavg.shape
print 'max, min: %s, %s' %(np.nanmax(mm_ts_areaavg), np.nanmin(mm_ts_areaavg))

## read list with constrained ensemble
files_const = '%s/constrained_models_%s_%s_%s_%s_%sdiags_%sobs.txt' %(
        outdir, target_var, target_file, res_name_target,
        target_mask, nvar, nobs)
if (os.access(files_const, os.F_OK) == True):
    models_const = np.genfromtxt(files_const, delimiter = '',
                                 dtype = None).tolist()
d_temp_mod_ts_areaavg_const = dict((k, d_temp_mod_ts_areaavg[k]) for k in models_const)

models_new = [x.split('_')[0] for x in d_temp_mod_ts_areaavg_const.keys()]
mult_ens_new = []
seen_new = set()
for m in models_new:
    if m not in seen_new:
        seen_new.add(m)
    else:
        mult_ens_new.append(m)
# make sure every model only once in list
list_with_mult_ens_const = set(mult_ens_new)
d_avg_ens_ts_const = dict()
for key, value in d_temp_mod_ts_areaavg_const.iteritems():
    if key.split('_')[0] in list_with_mult_ens_const:
        #find other ensemble members
        ens_mem = [value2 for key2, value2 in sorted(
            d_temp_mod_ts_areaavg_const.iteritems()) if key.split('_')[0] in key2]
        d_avg_ens_ts_const[key.split('_')[0]] = np.nanmean(ens_mem, axis = 0)
    else:
        d_avg_ens_ts_const[key.split('_')[0]] = value
tmp3 = 0
for key, value in d_avg_ens_ts_const.iteritems():
    tmp3 = tmp3 + value

const_mm_ts_areaavg = tmp3 / len(set(models_new))
print 'Size of area averaged constrained mm: %s' %const_mm_ts_areaavg.shape
print 'max, min: %s, %s' %(np.nanmax(const_mm_ts_areaavg),
                           np.nanmin(const_mm_ts_areaavg))

## calculate spread
ts_ens_areaavg = np.array([value for key, value in sorted(
    d_avg_ens_ts.iteritems())], dtype = float)
tmp21 = np.empty(ts_ens_areaavg.shape, dtype = float)
for t in xrange(ts_ens_areaavg.shape[1]):
    tmp21[:, t] = sorted(ts_ens_areaavg[:, t], reverse = True)
dim = np.count_nonzero(tmp21[:, 0])
print dim
ind_5 = int(math.ceil(0.05 * dim) - 1)
ind_95 = int(math.floor(0.95 * dim) - 1)
lower_ts_mm = tmp21[ind_5, :]
print ind_5
upper_ts_mm = tmp21[ind_95, :]
print ind_95
avg_ts_mm = np.nanmean(tmp21, axis = 0)
del tmp21

ts_ens_areaavg_const = np.array([value for key, value in sorted(
    d_avg_ens_ts_const.iteritems())], dtype = float)
tmp21 = np.empty(ts_ens_areaavg_const.shape, dtype = float)
for t in xrange(ts_ens_areaavg_const.shape[1]):
    tmp21[:, t] = sorted(ts_ens_areaavg_const[:, t], reverse = True)
dim_const = np.count_nonzero(tmp21[:, 0])
print dim_const
ind_5 = int(math.ceil(0.05 * dim_const) - 1)
ind_95 = int(math.floor(0.95 * dim_const) - 1)
ind_25 = int(math.ceil(0.25 * dim_const) - 1)
ind_75 = int(math.floor(0.75 * dim_const) - 1)

lower_ts_cmm = tmp21[ind_5, :]
print ind_5
upper_ts_cmm = tmp21[ind_95, :]
print ind_95
lower25_ts_cmm = tmp21[ind_25, :]
upper75_ts_cmm = tmp21[ind_75, :]

avg_ts_cmm = np.nanmean(tmp21, axis = 0)
median_ts_cmm = np.nanmedian(tmp21, axis = 0)
del tmp21

#calculate change in spread
spread_change = round(np.mean(lower_ts_cmm[-20:] - upper_ts_cmm[-20:]) -
                      np.mean(lower_ts_mm[-20:] - upper_ts_mm[-20:]), 1)

## plot timeseries
## plot area average of annual timeseries for obs, mm and wmm
## with uncertainty spread (5-95%)
fig = plt.figure(figsize = (10, 5), dpi = 300)
ax = fig.add_subplot(111)
plt.plot(obsyears, merra_ts_areaavg, color = 'blue',
         linewidth = 3.0)
plt.plot(obsyears, era_ts_areaavg, color = 'blue',
         linewidth = 3.0)
plt.plot(obsyears, obs_ts_areaavg, color = 'mediumblue', label = 'Obs',
         linewidth = 3.0)
plt.plot(years, mm_ts_areaavg, 'black',
         label = "multi-model mean (MMM)", linewidth = 3.0)
#plt.plot(years, lower_ts_mm, color = "grey")
#plt.plot(years, upper_ts_mm, color = "grey")
plt.fill_between(years, lower_ts_mm, upper_ts_mm, facecolor = 'grey',
                 alpha = 0.4)
plt.plot(years, avg_ts_cmm, color = "red", linestyle = '-',
         label = "constrained MMM", linewidth = 3.0)
#plt.plot(years, lower_ts_cmm, color = "red")
#plt.plot(years, upper_ts_cmm, color = "red")
plt.fill_between(years, lower_ts_cmm, upper_ts_cmm, facecolor = 'red',
                 alpha = 0.4, edgecolor = "red")
ax.text(0.15, 0.7, 'spread change: %.1f %s' %(spread_change, unit),
        ha = 'center', va = 'center', transform = ax.transAxes)
plt.xlabel('Year')
plt.ylabel('%s %s %s [%s]' %(region, target_var, res_name_target, unit))
plt.grid(True)
leg = plt.legend(loc = 'upper left')  # leg defines legend -> can be modified
leg.draw_frame(False)

plt.savefig('%s%s_%s_%s_%sdiags_%sobs_%s_cmm_spread.pdf' %(
    outdir, target_var, target_file, res_name_target, len(diag_var), nobs,
    region))

## save data for further plotting and panelling
if (os.access('%s/ncdf/' %outdir, os.F_OK) == False):
    os.makedirs('%s/ncdf/' %outdir)
    print 'created directory %s/ncdf/' %outdir
outfile = '%s/ncdf/%s_%s_%s_%sdiags_%sobs_%s_cmm.nc' %(
    outdir, target_var, target_file, res_name_target, len(diag_var), nobs,
    region)
nyears = eyear - syear + 1
datesout = [dt.datetime(syear + x, 06, 01, 00) for x in range(0, nyears)]
time = nc.date2num(datesout, units = 'days since %s' %(
    nc.datetime.strftime(dt.datetime(syear, 01, 01, 00), '%Y-%m-%d %H:%M:%S')),
                   calendar = 'standard')

nyears_obs = eyear_eval - syear_eval + 1
datesout_obs = [dt.datetime(syear_eval + x, 06, 01, 00) for x in range(
    0, nyears_obs)]
obstime = nc.date2num(datesout_obs, units = 'days since %s' %(
    nc.datetime.strftime(dt.datetime(syear_eval, 01, 01, 00),
                         '%Y-%m-%d %H:%M:%S')), calendar = 'standard')

fout = nc.Dataset(outfile, mode = 'w')

fout.createDimension('obstime', len(obsyears))
obstimeout = fout.createVariable('obstime', 'f8', ('obstime'),
                                 fill_value = 1e20)
setattr(obstimeout, 'units',
        'days since %s' %(nc.datetime.strftime(dt.datetime(syear_eval, 01,
                                                           01, 00),
                                               '%Y-%m-%d %H:%M:%S')))

obsout = fout.createVariable('obs_ts_areaavg', 'f8', ('obstime'),
                             fill_value = 1e20)
setattr(obsout,'Longname','area-averaged observational timeseries')
setattr(obsout,'units', unit)
setattr(obsout,'description','')

eraout = fout.createVariable('era_ts_areaavg', 'f8', ('obstime'),
                             fill_value = 1e20)
setattr(eraout,'Longname','area-averaged ERAinterim timeseries')
setattr(eraout,'units', unit)
setattr(eraout,'description','')

merraout = fout.createVariable('merra_ts_areaavg', 'f8', ('obstime'),
                             fill_value = 1e20)
setattr(merraout,'Longname','area-averaged MERRA2 timeseries')
setattr(merraout,'units', unit)
setattr(merraout,'description','')

fout.createDimension('time', len(years))
timeout = fout.createVariable('time', 'f8', ('time'), fill_value = 1e20)
setattr(timeout, 'units',
        'days since %s' %(nc.datetime.strftime(dt.datetime(syear, 01, 01, 00),
                                               '%Y-%m-%d_%H:%M:%S')))
fout.createDimension('model', len(set(models)))
modelout = fout.createVariable('model', str, ('model'), fill_value = 1e20)
models_out = np.array(list(set(models)), dtype = 'object')

ensout = fout.createVariable('ens_ts_areaavg', 'f8', ('model', 'time'),
                              fill_value = 1e20)
setattr(ensout, 'Longname', 'ensemble timeseries')
setattr(ensout, 'units', unit)
setattr(ensout, 'description', 'area-averaged timeseries for all models in ensemble')

mmout = fout.createVariable('mm_ts_areaavg', 'f8', ('time'), fill_value = 1e20)
setattr(mmout, 'Longname', 'area-averaged multi-model mean timeseries')
setattr(mmout, 'units', unit)
setattr(mmout, 'description', '')

lowmmout = fout.createVariable('lower_ts_mm', 'f8', ('time'), fill_value = 1e20)
setattr(lowmmout, 'Longname', '5th percentile of mm')
setattr(lowmmout, 'units', unit)
setattr(lowmmout, 'description',
        'lower 5th percentile of multi-model mean timeseries')

upmmout = fout.createVariable('upper_ts_mm', 'f8', ('time'), fill_value = 1e20)
setattr(upmmout, 'Longname','95th percentile of mm')
setattr(upmmout, 'units',  unit)
setattr(upmmout, 'description',
        'upper 95th percentile of multi-model mean timeseries')

fout.createDimension('model_const', len(set(models_new)))
modelconstout = fout.createVariable('model_const', str, ('model_const'), fill_value = 1e20)
modelsconst_out = np.array(list(set(models_new)), dtype = 'object')

censout = fout.createVariable('cens_ts_areaavg', 'f8', ('model_const', 'time'),
                              fill_value = 1e20)
setattr(censout, 'Longname', 'constrained ensemble timeseries')
setattr(censout, 'units', unit)
setattr(censout, 'description', 'area-averaged timeseries for all models in constrained ensemble')

cmmout = fout.createVariable('cmm_ts_areaavg', 'f8', ('time'), fill_value =
                             1e20)
setattr(cmmout, 'Longname', 'constrained multi-model mean timeseries')
setattr(cmmout, 'units', unit)
setattr(cmmout, 'description', '')

cmedmout = fout.createVariable('cmedm_ts_areaavg', 'f8', ('time'), fill_value =
                               1e20)
setattr(cmedmout, 'Longname', 'constrained multi-model median timeseries')
setattr(cmedmout, 'units', unit)
setattr(cmedmout, 'description', '')

lowcmmout = fout.createVariable('lower_ts_cmm', 'f8', ('time'),
                                fill_value = 1e20)
setattr(lowcmmout, 'Longname', '5th percentile of cmm')
setattr(lowcmmout, 'units', unit)
setattr(lowcmmout, 'description',
        'lower 5th percentile of constrained multi-model timeseries')

low25cmmout = fout.createVariable('lower25_ts_cmm', 'f8', ('time'),
                                fill_value = 1e20)
setattr(low25cmmout, 'Longname', '25th percentile of cmm')
setattr(low25cmmout, 'units', unit)
setattr(low25cmmout, 'description',
        'lower 25th percentile of constrained multi-model timeseries')

up75cmmout = fout.createVariable('upper75_ts_cmm', 'f8', ('time'),
                               fill_value = 1e20)
setattr(up75cmmout, 'Longname', '75th percentile of cmm')
setattr(up75cmmout, 'units', unit)
setattr(up75cmmout, 'description',
        'upper 75th percentile of constrained multi-model timeseries')

upcmmout = fout.createVariable('upper_ts_cmm', 'f8', ('time'),
                               fill_value = 1e20)
setattr(upcmmout, 'Longname', '95th percentile of cmm')
setattr(upcmmout, 'units', unit)
setattr(upcmmout, 'description',
        'upper 95th percentile of constrained multi-model timeseries')

obstimeout[:] = obstime[:]
obsout[:] = obs_ts_areaavg[:]
eraout[:] = era_ts_areaavg[:]
merraout[:] = merra_ts_areaavg[:]

timeout[:] = time[:]

ensout[:] = ts_ens_areaavg[:]
mmout[:] = mm_ts_areaavg[:]
lowmmout[:] = lower_ts_mm[:]
upmmout[:] = upper_ts_mm[:]
modelout[:] = models_out
modelconstout[:] = modelsconst_out

censout[:] = ts_ens_areaavg_const[:]
cmmout[:] = avg_ts_cmm[:]
lowcmmout[:] = lower_ts_cmm[:]
upcmmout[:] = upper_ts_cmm[:]

# Set global attributes
setattr(fout, "author", "Ruth Lorenz @IAC, ETH Zurich, Switzerland")
setattr(fout, "contact", "ruth.lorenz@env.ethz.ch")
setattr(fout, "creation date", dt.datetime.today().strftime('%Y-%m-%d'))
setattr(fout, "Script", "plot_ts_constrained_ensemble.py")
setattr(fout, "Input files located in:", path)
fout.close()

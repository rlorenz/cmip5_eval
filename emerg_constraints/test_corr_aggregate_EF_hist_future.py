#!/usr/bin/python
'''
File Name : test_corr_aggregate_EF_hist_future.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 12-10-2016
Modified: Wed 12 Oct 2016 09:38:50 AM CEST
Purpose: plot correlation of historical to future change
	to test if emergent constraint

'''

import netCDF4 as nc # to work with NetCDF files
import numpy as np
import os # operating system interface
from os.path import expanduser
home = expanduser("~") # Get users home directory
workdir = os.getcwd()
import glob
import sys
sys.path.insert(0, home + '/scripts/plot_scripts/utils/')
sys.path.insert(0, workdir + '/functions/')
from func_read_data import func_read_netcdf
from clim_seas_TLL import clim_mon_TLL, std_mon_TLL, seas_avg_mon_TLL, seas_std_mon_TLL, clim_seas_mon_TLL, ann_avg_mon_TLL
from calc_trend_TLL import trend_TLL
from scipy import signal, stats
from point_inside_polygon import point_inside_polygon
import math
import matplotlib.pyplot as plt
from draw_utils import draw
from mpl_toolkits.basemap import maskoceans, shiftgrid
from sklearn.decomposition import PCA as sklearnPCA
###
# Define input
###
# target variable and explaining variables
target_var = ['tasmax']
const_var = ['tasmax']
agg_var = ['ef']
agg_threshold = 0.5

# climatology:clim, variability:std, trend:trnd
var_file_target = ['std']
var_file_const = ['clim']
var_file_agg = ['clim']

# kind is cyc: annual cycle, ann: annual mean, seas: seasonal mean,
# or mon: monthly values 
var_kind_target = ['seas']
var_kind_const = ['seas']
var_kind_agg = ['seas']

# choose data for particular month or season (0:DJF, 1:MAM, 2:JJA, 3:SON)?
res = [2]
res_name = 'JJA'

# choose region if required, otherwise longname = 'GLOBAL'
region = 'NAM'
longname_region = 'North America'

archive = '/net/atmos/data/cmip5-ng'
indir = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/'
outdir = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/emerg_const/%s/' %(
    target_var[0])
if (os.access(outdir, os.F_OK) == False):
    os.makedirs(outdir)

syear_hist = 1980
eyear_hist = 2015
syear_fut = 2065
eyear_fut = 2099

experiment = 'rcp85'
grid = 'g025'

# normalize data? if variables on different scale we need to normalize
# for total least squares regression
normalize = True

###
# Read data
###
print "Read model data"
# find all files for target variable
name_target = '%s/%s/%s_mon_*_%s_*_%s.nc' %(archive, target_var[0],
                                            target_var[0], experiment, grid)
model_names_target = []
for filenames in glob.glob(name_target):
    fh = nc.Dataset(filenames, mode = 'r')
    model = fh.source_model
    if model == 'ACCESS1.3':
        model = 'ACCESS1-3'
    elif model == 'FGOALS_g2':
        model = 'FGOALS-g2'
    ens = fh.source_ensemble
    model_names_target.append(model + '_' + ens)
    fh.close()

# find all files for constraining variable
if (const_var[0] != 'ef'):
    name_var2 = '%s/%s/%s_mon_*_%s_*_%s.nc' %(archive, const_var[0],
                                              const_var[0], experiment, grid)
else:
    name_var2 = '%s/%s/mon/%s_mon_*_%s_*_%s.nc' %(indir, const_var[0],
                                                  const_var[0], experiment,
                                                  grid)
model_names_var2 = []
for filenames in glob.glob(name_var2):
    fh = nc.Dataset(filenames, mode = 'r')
    model = fh.source_model
    if model == 'ACCESS1.3':
        model = 'ACCESS1-3'
    elif model == 'FGOALS_g2':
        model = 'FGOALS-g2'
    ens = fh.source_ensemble
    model_names_var2.append(model + '_' + ens)
    fh.close()

# find all files for aggregating variable
if (agg_var[0] != 'ef'):
    name_agg = '%s/%s/%s_mon_*_%s_*_%s.nc' %(archive, agg_var[0],
                                              agg_var[0], experiment, grid)
else:
    name_agg = '%s/%s/mon/%s_mon_*_%s_*_%s.nc' %(indir, agg_var[0],
                                                  agg_var[0], experiment,
                                                  grid)
model_names_agg = []
for filenames in glob.glob(name_agg):
    fh = nc.Dataset(filenames, mode = 'r')
    model = fh.source_model
    if model == 'ACCESS1.3':
        model = 'ACCESS1-3'
    elif model == 'FGOALS_g2':
        model = 'FGOALS-g2'
    ens = fh.source_ensemble
    model_names_agg.append(model + '_' + ens)
    fh.close()

# find overlapping models, ensembles
overlap = model_names_target
overlap = list(set(model_names_var2) & set(overlap))
overlap = list(set(model_names_agg) & set(overlap))

nfiles = len(overlap)
model_names = overlap
d_change_target = dict()
d_target_hist = dict()
d_target_hist_ts = dict()
d_var2_hist = dict()
d_var2_hist_ts = dict()
change_target_areaavg = np.empty((len(model_names)))
var2_hist_areaavg = np.empty((len(model_names)))
print str(nfiles)+ ' matching files found'
for f in xrange(len(model_names)):
    model = model_names[f].split('_', 1)[0]
    ens = model_names[f].split('_', 1)[1]

    # read target variable
    filename_target = '%s/%s/%s_mon_%s_%s_%s_%s.nc' %(archive, target_var[0],
                                                      target_var[0], model,
                                                      experiment, ens, grid)
    target_hist = func_read_netcdf(filename_target, target_var[0],
                                   var_units = True,
                                   syear = syear_hist, eyear = eyear_hist)
    target_hist_data = target_hist['data']
    if isinstance(target_hist_data, np.ma.core.MaskedArray):
        target_hist_data = target_hist_data.filled(np.NaN)

    lat = target_hist['lat']
    lon = target_hist['lon']
    hist_dates = target_hist['dates']
    unit_target = target_hist['unit']
    hist_months = np.asarray([hist_dates[i].month for i in xrange(len(hist_dates))])
    hist_years = np.asarray([hist_dates[i].year for i in xrange(len(hist_dates))])

    target_fut = func_read_netcdf(filename_target, target_var[0],
                                  syear = syear_fut, eyear = eyear_fut)
    target_fut_data = target_fut['data']
    if isinstance(target_fut_data, np.ma.core.MaskedArray):
        target_fut_data = target_fut_data.filled(np.NaN)
    fut_dates = target_fut['dates']
    fut_months = np.asarray([fut_dates[i].month for i in xrange(len(fut_dates))])
    fut_years = np.asarray([fut_dates[i].year for i in xrange(len(fut_dates))])

    # read constraining variable
    if (const_var[0] != 'ef'):
        filename2 = '%s/%s/%s_mon_%s_%s_%s_%s.nc' %(archive, const_var[0],
                                                    const_var[0], model,
                                                    experiment, ens, grid)
    else:
        filename2 = '%s/%s/mon/%s_mon_%s_%s_%s_%s.nc' %(indir, const_var[0],
                                                        const_var[0], model,
                                                        experiment, ens, grid)
    var_hist = func_read_netcdf(filename2, const_var[0],
                                   var_units = True,
                                   syear = syear_hist, eyear = eyear_hist)
    var2_hist_data = var_hist['data']
    if const_var[0] == 'ef':
        var2_hist_data = np.ma.masked_outside(var2_hist_data, -0.1, 1.1)
    if isinstance(var2_hist_data, np.ma.core.MaskedArray):
        var2_hist_data = var2_hist_data.filled(np.NaN)

    if f == 0:
        #hist_dates = var_hist['dates']
        unit_var2 = var_hist['unit']

    # read aggregating variable
    if (agg_var[0] != 'ef'):
        filename3 = '%s/%s/%s_mon_%s_%s_%s_%s.nc' %(archive, agg_var[0],
                                                    agg_var[0], model,
                                                    experiment, ens, grid)
    else:
        filename3 = '%s/%s/mon/%s_mon_%s_%s_%s_%s.nc' %(indir, agg_var[0],
                                                        agg_var[0], model,
                                                        experiment, ens, grid)
    aggvar_hist = func_read_netcdf(filename3, agg_var[0],
                                   var_units = True,
                                   syear = syear_hist, eyear = eyear_hist)
    aggvar_hist_data = aggvar_hist['data']
    if agg_var[0] == 'ef':
        aggvar_hist_data = np.ma.masked_outside(aggvar_hist_data, -0.1, 1.1)
    #if isinstance(var2_hist_data, np.ma.core.MaskedArray):
    #    aggvar_hist_data = aggvar_hist_data.filled(np.NaN)

    ## calculate annual, seasonal, or monthly clim, std, or trnd
    # normalize if necessary (normalize = True)
    if normalize:
        norm = 0
        for field in [target_hist_data, target_fut_data, var2_hist_data]:
            tmp1 = field[:]
            tmp_mean = np.nanmean(tmp1, axis = 0)
            tmp_std = np.nanstd(tmp1, axis = 0)
            tmp_norm = (tmp1 - tmp_mean) / tmp_std
            if (norm == 0):
                target_hist_data = tmp_norm[:]
            elif (norm == 1):
                target_fut_data = tmp_norm[:]
            elif (norm == 2):
                var2_hist_data = tmp_norm[:]
            norm = norm + 1

    # target historical
    # detrend timeseris before calculating variability
    if (var_file_target[0] == 'std'):
        if np.isnan(target_hist_data).any():
            tmp_dtr = np.empty((target_hist_data.shape))
            for ilat in xrange(len(lat)):
                for ilon in xrange(len(lon)):
                    tmp = target_hist_data[:, ilat, ilon]
                    if np.isnan(tmp).any():
                        tmp_dtr[:, ilat, ilon] = np.NaN
                    else:
                        tmp_dtr[:, ilat, ilon] = signal.detrend(tmp, axis = 0,
                                                                type = 'linear',
                                                                bp = 0)
            del tmp
        else:
            tmp_dtr = signal.detrend(target_hist_data, axis = 0,
                                     type = 'linear', bp = 0)

    if (var_kind_target[0] == 'ann'):
        tmp = ann_avg_mon_TLL(target_hist_data, hist_years)
        target_hist_ts = tmp[:]
        if (var_file_target[0] == 'clim'):
            target_hist_tim = tmp
        elif (var_file_target[0] == 'std'):
            target_hist_tim = ann_std_mon_TLL(tmp_dtr, hist_years)
        elif (var_file_target[0] == 'trnd'):
            target_hist_tim = trend_TLL(np.squeeze(tmp),
                                            drop = False)
    if (var_kind_target[0] == 'seas'):
        tmp = clim_seas_mon_TLL(target_hist_data, hist_months,
                                hist_years)
        if res != None:
            target_hist_ts = np.squeeze(tmp[res[0], :, :,:])
        else:
            target_hist_ts = tmp[:]
        if (var_file_target[0] == 'clim'):
            target_hist_tim = seas_avg_mon_TLL(target_hist_data, hist_months)
            if res[0] != None:
                target_hist_tim = target_hist_tim[res, :, :]
        elif (var_file_target[0] == 'std'):
            target_hist_tim = seas_std_mon_TLL(tmp_dtr, hist_months, hist_years)
            if res[0] != None:
                target_hist_tim = target_hist_tim[res, :, :]
        elif (var_file_target[0] == 'trnd'):
            if res == None:
                target_hist_tim = np.empty((4, len(lat), len(lon)))
                for seas in xrange(0, 4):
                    target_hist_tim[seas, :, :] = trend_TLL(np.squeeze(
                        tmp[seas, :, :, :]), drop = False)
            else:
                target_hist_tim = trend_TLL(np.squeeze(tmp[res[0], :, :, :]),
                                            drop = False)
    try:
        del tmp, tmp_dtr
    except (NameError):
        pass

    if (var_kind_target[0] == 'mon'):
        if res != None:
            tmp = target_hist_data[np.where(hist_months == res[0]), :, :]
            target_hist_ts = tmp[:]
        else:
            target_hist_ts = target_hist_data
        if (var_file_target[0] == 'clim'):
            target_hist_tim = clim_mon_TLL(target_hist_data, hist_months)
            if res[0] != None:
                target_hist_tim = target_hist_tim[res, :, :]
        elif (var_file_target[0] == 'std'):
            target_hist_tim = std_mon_TLL(tmp_dtr, hist_months)
            if res[0] != None:
                target_hist_tim = target_hist_tim[res, :, :]
        elif (var_file_target[0] == 'trnd'):
            if res == None:
                target_hist_tim = np.empty((4, len(lat), len(lon)))
                for mon in xrange(0, 11):
                    target_hist_tim[mon, :, :] = trend_TLL(np.squeeze(
                        tmp[mon, :, :, :]), drop = False)
            else:
                target_hist_tim = trend_TLL(np.squeeze(tmp), drop = False)
    try:
        del tmp, tmp_dtr
    except (NameError):
        pass

    # target future
    # detrend timeseris before calculating variability
    if (var_file_target[0] == 'std'):
        if np.isnan(target_fut_data).any():
            tmp_dtr = np.empty((target_fut_data.shape))
            for ilat in xrange(len(lat)):
                for ilon in xrange(len(lon)):
                    tmp = target_fut_data[:, ilat, ilon]
                    if np.isnan(tmp).any():
                        tmp_dtr[:, ilat, ilon] = np.NaN
                    else:
                        tmp_dtr[:, ilat, ilon] = signal.detrend(tmp, axis = 0,
                                                                type = 'linear',
                                                                bp = 0)
            del tmp
        else:
            tmp_dtr = signal.detrend(target_fut_data, axis = 0,
                                     type = 'linear', bp = 0)

    if (var_kind_target[0] == 'ann'):
        tmp = ann_avg_mon_TLL(target_fut_data, fut_years)
        target_fut_tim_ts = tmp[:]
        if (var_file_target[0] == 'clim'):
            target_fut_tim = tmp
        elif (var_file_target[0] == 'std'):
            target_fut_tim = ann_std_mon_TLL(tmp_dtr, fut_years)
        elif (var_file_target[0] == 'trnd'):
            target_fut_tim = trend_TLL(np.squeeze(tmp),
                                            drop = False)
    if (var_kind_target[0] == 'seas'):
        tmp = clim_seas_mon_TLL(target_fut_data, fut_months,
                                fut_years)
        if res != None:
            target_fut_tim_ts = np.squeeze(tmp[res[0], :, :, :])
        else:
            target_fut_tim_ts = tmp[:]
        if (var_file_target[0] == 'clim'):
            target_fut_tim = seas_avg_mon_TLL(target_fut_data, fut_months)
            if res[0] != None:
                target_fut_tim = target_fut_tim[res, :, :]
        elif (var_file_target[0] == 'std'):
            target_fut_tim = seas_std_mon_TLL(tmp_dtr, fut_months, fut_years)
            if res[0] != None:
                target_fut_tim = target_fut_tim[res, :, :]
        elif (var_file_target[0] == 'trnd'):
            if res == None:
                target_fut_tim = np.empty((4, len(lat), len(lon)))
                for seas in xrange(0, 4):
                    target_fut_tim[seas, :, :] = trend_TLL(np.squeeze(
                        tmp[seas, :, :, :]), drop = False)
            else:
                target_fut_tim = trend_TLL(np.squeeze(tmp[res[0], :, :, :]),
                                            drop = False)
    try:
        del tmp, tmp_dtr
    except (NameError):
        pass

    if (var_kind_target[0] == 'mon'):
        if res != None:
            tmp = target_fut_data[np.where(fut_months == res[0]), :, :]
        else:
            tmp = target_fut_data
        target_fut_tim_ts = tmp[:]
        if (var_file_target[0] == 'clim'):
            target_fut_tim = clim_mon_TLL(target_fut_data, fut_months)
            if res[0] != None:
                target_fut_tim = target_fut_tim[res, :, :]
        elif (var_file_target[0] == 'std'):
            target_fut_tim = std_mon_TLL(tmp_dtr, fut_months)
            if res[0] != None:
                target_fut_tim = target_fut_tim[res, :, :]
        elif (var_file_target[0] == 'trnd'):
            if res == None:
                target_fut_tim = np.empty((4, len(lat), len(lon)))
                for mon in xrange(0, 11):
                    target_fut_tim[mon, :, :] = trend_TLL(np.squeeze(
                        tmp[mon, :, :, :]), drop = False)
            else:
                target_fut_tim = trend_TLL(np.squeeze(tmp), drop = False)
    try:
        del tmp, tmp_dtr
    except (NameError):
        pass

    # const_var historical
    # detrend timeseris before calculating variability
    if (var_file_const[0] == 'std'):
        if np.isnan(var2_hist_data).any():
            tmp_dtr = np.empty((var2_hist_data.shape))
            for ilat in xrange(len(lat)):
                for ilon in xrange(len(lon)):
                    tmp = var2_hist_data[:, ilat, ilon]
                    if np.isnan(tmp).any():
                        tmp_dtr[:, ilat, ilon] = np.NaN
                    else:
                        tmp_dtr[:, ilat, ilon] = signal.detrend(tmp, axis = 0,
                                                                type = 'linear',
                                                                bp = 0)
            del tmp
        else:
            tmp_dtr = signal.detrend(var2_hist_data, axis = 0,
                                     type = 'linear', bp = 0)

    if (var_kind_const[0] == 'ann'):
        tmp = ann_avg_mon_TLL(var2_hist_data, hist_years)
        var2_hist_ts = tmp[:]
        if (var_file_const[0] == 'clim'):
            var2_hist_tim = tmp
        elif (var_file_const[0] == 'std'):
            var2_hist_tim = ann_std_mon_TLL(tmp_dtr, hist_years)
        elif (var_file_const[0] == 'trnd'):
            if res == None:
                var2_hist_tim = trend_TLL(np.squeeze(tmp),
                                          drop = False)
    if (var_kind_const[0] == 'seas'):
        tmp = clim_seas_mon_TLL(var2_hist_data, hist_months,
                                hist_years)
        if res == None:
            var2_hist_ts = tmp[:]
        else:
            var2_hist_ts = np.squeeze(tmp[res[0], :, :, :])
        if (var_file_const[0] == 'clim'):
            var2_hist_tim = seas_avg_mon_TLL(var2_hist_data, hist_months)
            if res[0] != None:
                var2_hist_tim = var2_hist_tim[res, :, :]
        elif (var_file_const[0] == 'std'):
            var2_hist_tim = seas_std_mon_TLL(tmp_dtr, hist_months, hist_years)
            if res[0] != None:
                var2_hist_tim = var2_hist_tim[res, :, :]
        elif (var_file_const[0] == 'trnd'):
            if res == None:
                var2_hist_tim = np.empty((4, len(lat), len(lon)))
                for seas in xrange(0, 4):
                    var2_hist_tim[seas, :, :] = trend_TLL(np.squeeze(
                        tmp[seas, :, :, :]), drop = False)
            else:
                var2_hist_tim = trend_TLL(np.squeeze(tmp[res[0], :, :, :]),
                                          drop = False)
    try:
        del tmp, tmp_dtr
    except (NameError):
        pass

    if (var_kind_const[0] == 'mon'):
        if res != None:
            tmp = var2_hist_data[np.where(hist_months == res[0]), :, :]
            var2_hist_ts = tmp[:]
        else:
            var2_hist_ts = var2_hist_data
        if (var_file_const[0] == 'clim'):
            var2_hist_tim = clim_mon_TLL(var2_hist_data, hist_months)
            if res[0] != None:
                var2_hist_tim = var2_hist_tim[res, :, :]
        elif (var_file_const[0] == 'std'):
            var2_hist_tim = std_mon_TLL(tmp_dtr, hist_months)
            if res[0] != None:
                var2_hist_tim = var2_hist_tim[res, :, :]
        elif (var_file_const[0] == 'trnd'):
            if res == None:
                var2_hist_tim = np.empty((4, len(lat), len(lon)))
                for mon in xrange(0, 11):
                    var2_hist_tim[mon, :, :] = trend_TLL(np.squeeze(
                        tmp[mon, :, :, :]), drop = False)
            else:
                var2_hist_tim = trend_TLL(np.squeeze(tmp), drop = False)
    try:
        del tmp, tmp_dtr
    except (NameError):
        pass

    # aggregating variable
    # detrend timeseris before calculating variability
    if (var_file_agg[0] == 'std'):
        if np.isnan(aggvar_hist_data).any():
            tmp_dtr = np.empty((aggvar_hist_data.shape))
            for ilat in xrange(len(lat)):
                for ilon in xrange(len(lon)):
                    tmp = aggvar_hist_data[:, ilat, ilon]
                    if np.isnan(tmp).any():
                        tmp_dtr[:, ilat, ilon] = np.NaN
                    else:
                        tmp_dtr[:, ilat, ilon] = signal.detrend(tmp, axis = 0,
                                                                type = 'linear',
                                                                bp = 0)
            del tmp
        else:
            tmp_dtr = signal.detrend(aggvar_hist_data, axis = 0,
                                     type = 'linear', bp = 0)

    if (var_kind_agg[0] == 'ann'):
        tmp = ann_avg_mon_TLL(aggvar_hist_data, hist_years)
        aggvar_hist_ts = tmp[:]
        if (var_file_agg[0] == 'clim'):
            aggvar_hist_tim = tmp
        elif (var_file_agg[0] == 'std'):
            aggvar_hist_tim = ann_std_mon_TLL(tmp_dtr, hist_years)
        elif (var_file_agg[0] == 'trnd'):
            if res == None:
                aggvar_hist_tim = trend_TLL(np.squeeze(tmp),
                                          drop = False)
    if (var_kind_agg[0] == 'seas'):
        tmp = clim_seas_mon_TLL(aggvar_hist_data, hist_months,
                                hist_years)
        if res == None:
            aggvar_hist_ts = tmp[:]
        else:
            aggvar_hist_ts = np.squeeze(tmp[res[0], :, :, :])
        if (var_file_agg[0] == 'clim'):
            aggvar_hist_tim = seas_avg_mon_TLL(aggvar_hist_data, hist_months)
            if res[0] != None:
                aggvar_hist_tim = aggvar_hist_tim[res, :, :]
        elif (var_file_agg[0] == 'std'):
            aggvar_hist_tim = seas_std_mon_TLL(tmp_dtr, hist_months, hist_years)
            if res[0] != None:
                aggvar_hist_tim = aggvar_hist_tim[res, :, :]
        elif (var_file_agg[0] == 'trnd'):
            if res == None:
                aggvar_hist_tim = np.empty((4, len(lat), len(lon)))
                for seas in xrange(0, 4):
                    aggvar_hist_tim[seas, :, :] = trend_TLL(np.squeeze(
                        tmp[seas, :, :, :]), drop = False)
            else:
                aggvar_hist_tim = trend_TLL(np.squeeze(tmp[res[0], :, :, :]),
                                          drop = False)
    try:
        del tmp, tmp_dtr
    except (NameError):
        pass

    if (var_kind_agg[0] == 'mon'):
        if res != None:
            tmp = aggvar_hist_data[np.where(hist_months == res[0]), :, :]
            aggvar_hist_ts = tmp[:]
        else:
            aggvar_hist_ts = aggvar_hist_data
        if (var_file_agg[0] == 'clim'):
            aggvar_hist_tim = clim_mon_TLL(aggvar_hist_data, hist_months)
            if res[0] != None:
                aggvar_hist_tim = aggvar_hist_tim[res, :, :]
        elif (var_file_agg[0] == 'std'):
            aggvar_hist_tim = std_mon_TLL(tmp_dtr, hist_months)
            if res[0] != None:
                aggvar_hist_tim = aggvar_hist_tim[res, :, :]
        elif (var_file_agg[0] == 'trnd'):
            if res == None:
                aggvar_hist_tim = np.empty((4, len(lat), len(lon)))
                for mon in xrange(0, 11):
                    aggvar_hist_tim[mon, :, :] = trend_TLL(np.squeeze(
                        tmp[mon, :, :, :]), drop = False)
            else:
                aggvar_hist_tim = trend_TLL(np.squeeze(tmp), drop = False)
    try:
        del tmp, tmp_dtr
    except (NameError):
        pass

    # calculate change from hist to fut for target variable
    change_target = target_fut_tim - target_hist_tim

    # create mask based on aggregating variable    
    agg_mask = np.ma.masked_where(aggvar_hist_tim > agg_threshold, aggvar_hist_tim).mask

    ## mask by aggregation mask
    change_target_mask = np.ma.masked_array(change_target, agg_mask)
    change_target = change_target_mask.filled(np.NaN)
    target_hist_mask = np.ma.masked_array(target_hist_tim, agg_mask)
    target_hist = target_hist_mask.filled(np.NaN)
    var2_hist_tim_mask = np.ma.masked_array(var2_hist_tim, agg_mask)
    var2_hist_tim = var2_hist_tim_mask.filled(np.NaN)

    ## if any region defined, cut data into region
    if region != None:
        mask = np.loadtxt('%s/scripts/plot_scripts/areas_txt/%s.txt' %(home,
                                                                     region))
        # shiftgrid since masks defined from -180 to 180
        lons_orig = lon[:]
        change_target, lon =  shiftgrid(180., change_target, lons_orig,
                                        start = False)
        target_hist_tim, lon =  shiftgrid(180., target_hist_tim, lons_orig,
                                          start = False)
        target_hist_ts, lon =  shiftgrid(180., target_hist_ts, lons_orig,
                                         start = False)
        var2_hist_tim, lon =  shiftgrid(180., var2_hist_tim, lons_orig,
                                        start = False)
        var2_hist_ts, lon =  shiftgrid(180., var2_hist_ts, lons_orig,
                                       start = False)
        change_target_reg_nan = np.ndarray((change_target.shape))
        target_hist_tim_reg_nan = np.ndarray((target_hist_tim.shape))
        target_hist_ts_reg_nan = np.ndarray((target_hist_ts.shape))
        var2_hist_tim_reg_nan = np.ndarray((var2_hist_tim.shape))
        var2_hist_ts_reg_nan = np.ndarray((var2_hist_ts.shape))
        for ilat in xrange(len(lat)):
            for ilon in xrange(len(lon)):
                if (point_inside_polygon(lat[ilat], lon[ilon],
                                         np.fliplr(mask))):
                    change_target_reg_nan[:, ilat, ilon] = change_target[:, ilat, ilon]
                    target_hist_tim_reg_nan[:, ilat, ilon] = target_hist_tim[:, ilat, ilon]
                    target_hist_ts_reg_nan[:, ilat, ilon] = target_hist_ts[:, ilat, ilon]
                    var2_hist_tim_reg_nan[:, ilat, ilon] = var2_hist_tim[:, ilat, ilon]
                    var2_hist_ts_reg_nan[:, ilat, ilon] = var2_hist_ts[:, ilat, ilon]
                else:
                    change_target_reg_nan[:, ilat, ilon] = np.NaN
                    target_hist_tim_reg_nan[:, ilat, ilon] = np.NaN
                    target_hist_ts_reg_nan[:, ilat, ilon] = np.NaN
                    var2_hist_tim_reg_nan[:, ilat, ilon] = np.NaN
                    var2_hist_ts_reg_nan[:, ilat, ilon] = np.NaN
        idx_lat = np.where((lat > np.min(mask[:, 1])) & (lat < np.max(mask[:, 1])))
        lat = lat[idx_lat]

        tmp1_reg = np.empty((len(change_target_reg_nan), len(lat), len(lon)))
        tmp11_reg = np.empty((len(target_hist_tim_reg_nan), len(lat), len(lon)))
        tmp1_ts_reg = np.empty((len(target_hist_ts_reg_nan), len(lat), len(lon)))
        tmp2_reg = np.empty((len(var2_hist_tim_reg_nan), len(lat), len(lon)))
        tmp2_ts_reg = np.empty((len(var2_hist_ts_reg_nan), len(lat), len(lon)))

        idx_lon = np.where((lon > np.min(mask[:, 0])) & (lon < np.max(mask[:, 0])))
        lon = lon[idx_lon]

        change_target_reg = np.empty((len(change_target_reg_nan), len(lat),
                                      len(lon)))
        tmp1_reg = np.squeeze(change_target_reg_nan[:, idx_lat, :], 1)
        target_hist_tim_reg = np.empty((len(target_hist_tim_reg_nan), len(lat),
                                      len(lon)))
        tmp11_reg = np.squeeze(target_hist_tim_reg_nan[:, idx_lat, :], 1)
        target_hist_ts_reg = np.empty((len(target_hist_ts_reg_nan), len(lat),
                                      len(lon)))
        tmp1_ts_reg = np.squeeze(target_hist_ts_reg_nan[:, idx_lat, :], 1)
        var2_hist_tim_reg = np.empty((len(var2_hist_tim_reg_nan), len(lat),
                                      len(lon)))
        tmp2_reg = np.squeeze(var2_hist_tim_reg_nan[:, idx_lat, :], 1)
        var2_hist_ts_reg = np.empty((len(var2_hist_ts_reg_nan), len(lat),
                                      len(lon)))
        tmp2_ts_reg = np.squeeze(var2_hist_ts_reg_nan[:, idx_lat, :], 1)

        change_target_reg = np.squeeze(tmp1_reg[:, :, idx_lon], 2)
        target_hist_tim_reg = np.squeeze(tmp11_reg[:, :, idx_lon], 2)
        target_hist_ts_reg = np.squeeze(tmp1_ts_reg[:, :, idx_lon], 2)
        var2_hist_tim_reg = np.squeeze(tmp2_reg[:, :, idx_lon], 2)
        var2_hist_ts_reg = np.squeeze(tmp2_ts_reg[:, :, idx_lon], 2)
        #del change_target
        #del target_hist_tim
        #del target_hist_ts
        #del var2_hist_tim
        #del var2_hist_ts
    else:
        change_target_reg = change_target
        target_hist_tim_reg = target_hist_tim
        target_hist_ts_reg = target_hist_ts
        var2_hist_tim_reg = var2_hist_tim
        var2_hist_ts_reg = var2_hist_ts

    d_change_target[model + '_' + ens] = change_target_reg[:]
    d_target_hist[model + '_' + ens] = target_hist_tim_reg[:]
    d_target_hist_ts[model + '_' + ens] = target_hist_ts_reg[:]
    d_var2_hist[model + '_' + ens] = var2_hist_tim_reg[:]
    d_var2_hist_ts[model + '_' + ens] = var2_hist_ts_reg[:]

    ## calculate area average
    ## weight for latitude differences in area
    w_lat = np.cos(lat * (4.0 * math.atan(1.0) / 180)) 
    ma_change_target_reg = np.ma.masked_array(change_target_reg,
                                              np.isnan(change_target_reg))
    ma_var2_hist_tim_reg = np.ma.masked_array(var2_hist_tim_reg,
                                              np.isnan(var2_hist_tim_reg))
    tmp1_latweight = np.ma.average(np.squeeze(ma_change_target_reg), axis = 0,
                                   weights = w_lat)
    change_target_areaavg[f] = np.nanmean(tmp1_latweight.filled(np.nan))
    tmp2_latweight = np.ma.average(np.squeeze(ma_var2_hist_tim_reg), axis = 0,
                                   weights = w_lat)
    var2_hist_areaavg[f] = np.nanmean(tmp2_latweight.filled(np.nan))

# dict to array
target_hist = np.array([np.squeeze(value) for key, value in sorted(d_target_hist.iteritems())], dtype = float)
model_keys1 = [key for key, value in sorted(d_target_hist.iteritems())]
var2_hist = np.array([np.squeeze(value) for key, value in sorted(d_var2_hist.iteritems())], dtype = float)
model_keys2 = [key for key, value in sorted(d_var2_hist.iteritems())]
change_target = np.array([np.squeeze(value) for key, value in sorted(d_change_target.iteritems())], dtype = float)

## correlation area averages of all models
ma_var2_hist_areaavg = np.ma.masked_array(var2_hist_areaavg, np.isnan(var2_hist_areaavg))
ma_change_target_areaavg = np.ma.masked_array(change_target_areaavg,
                                              np.isnan(change_target_areaavg))
corr = np.ma.corrcoef(ma_var2_hist_areaavg, ma_change_target_areaavg)

print 'Mean correlation between delta %s %s %s and %s %s %s is: %s' %(target_var[0],
                                                                   var_file_target[0],
                                                                   var_kind_target[0],
                                                                      const_var[0],
                                                                    var_file_const[0],
                                                                    var_kind_const[0],
                                                     round(np.nanmean(corr[0, 1]), 3))
## calculate temporal correlation historical period
corr_hist = np.empty((len(lat), len(lon)))
for ilat in xrange(len(lat)):
    for ilon in xrange(len(lon)):
        if ((np.isnan(target_hist[:, ilat, ilon]).all()) or
            (np.sum(np.isfinite(var2_hist[:, ilat, ilon])) < 3)):
            corr_hist[ilat, ilon] = np.NaN
        else:
            spear = stats.spearmanr(target_hist[:, ilat, ilon],
                                    var2_hist[:, ilat, ilon], axis = 0,
                                    nan_policy = 'omit')
            corr_hist[ilat, ilon] = spear[0]
print 'Mean historical correlation between %s and %s is: %s' %(target_var[0],
                                                               const_var[0],
                                                      round(np.nanmean(corr_hist), 3))
## calculate correlation future period
corr_fut = np.empty((len(lat), len(lon)))
for ilat in xrange(len(lat)):
    for ilon in xrange(len(lon)):
        if ((np.isnan(change_target[:, ilat, ilon]).all()) or
            (np.sum(np.isfinite(var2_hist[:, ilat, ilon])) < 3)):
            corr_fut[ilat, ilon] = np.NaN
        else:
            spear = stats.spearmanr(change_target[:, ilat, ilon],
                                    var2_hist[:, ilat, ilon], axis = 0,
                                    nan_policy = 'omit')
            corr_fut[ilat, ilon] = spear[0]
print 'Mean correlation between change in %s and historical %s is: %s' %(target_var[0],
                                                                         const_var[0],
                                                                         round(np.nanmean(corr_fut), 3))

## calculate ordinary least squares and total least squares
slope, intercept, r_val, p_val, std_err = stats.linregress(var2_hist_areaavg,
                                                           change_target_areaavg)

# pca and total least squares only if normalized or same variable
if ((target_var[0] == const_var[0]) and (var_file_target[0] == var_file_const[0])) or normalize:
    pca_data = np.stack((var2_hist_areaavg,change_target_areaavg), axis = 1)
    sklearn_pca = sklearnPCA(n_components = 2)
    sklearn_transf = sklearn_pca.fit(pca_data)
    means = sklearn_transf.mean_
    loadings = sklearn_transf.components_
    slope_pca = loadings[1, 0] / loadings[0, 0]
    intercept_pca = means[1] - slope_pca * means[0]

### Plotting
plotname = '%scorr_%s_%s_%s_change_vs_%s_%s_%s_hist_agg%slt%s_%s_%s.pdf' %(outdir,
                                                                 target_var[0],
                                                                 var_kind_target[0],
                                                                 var_file_target[0],
                                                                 const_var[0],
                                                                 var_kind_const[0],
                                                                 var_file_const[0],
                                                                 agg_var[0],
                                                                 agg_threshold,
                                                                 region, grid)
title = 'Correlation %s %s %s change vs. %s %s %s' %(target_var[0],
                                                     var_kind_target[0],
                                                     var_file_target[0],
                                                     const_var[0],
                                                     var_kind_const[0],
                                                     var_file_const[0])
draw(corr_fut, lat, lon, title = title, levels = np.arange(-0.9, 1.1, 0.2),
     colors = 'RdBu_r', region = region)
plt.savefig(plotname)

if ((target_var[0] != const_var[0]) or (var_file_target[0] != var_file_const[0]) or
    (var_kind_target[0] != var_kind_const[0])):
    plotname = '%scorr_%s_%s_%s_vs_%s_%s_%s_agg%slt%s_%s-%s_%s_%s.pdf' %(outdir,
                                                               target_var[0],
                                                               var_kind_target[0],
                                                               var_file_target[0],
                                                               const_var[0],
                                                               var_kind_const[0],
                                                               var_file_const[0],
                                                               agg_var[0],
                                                               agg_threshold,
                                                               str(syear_hist),
                                                               str(eyear_hist),
                                                               region, grid)
    title = 'Correlation %s %s %s vs. %s %s %s' %(target_var[0],
                                                  var_kind_target[0],
                                                  var_file_target[0],
                                                  const_var[0],
                                                  var_kind_const[0],
                                                  var_file_const[0])
    draw(corr_hist, lat, lon, title = title, levels = np.arange(-0.9, 1.1, 0.2),
         colors = 'RdBu_r', region = region)
    plt.savefig(plotname)

## plot scatter
abline_values = [slope * i + intercept for i in var2_hist_areaavg]
try:
    abline_values_pca = [slope_pca * i + intercept_pca for i in var2_hist_areaavg]
except (NameError):
    pass
plotname = '%sdelta_%s_%s_%s_vs_%s_%s_%s_agg%slt%s_%s_%s.pdf' %(outdir,
                                                             target_var[0],
                                                             var_kind_target[0],
                                                             var_file_target[0],
                                                             const_var[0],
                                                             var_kind_const[0],
                                                             var_file_const[0],
                                                             agg_var[0],
                                                             agg_threshold,
                                                             region, grid)

fig = plt.figure(figsize = (7, 6), dpi = 300)
ax = fig.add_subplot(111)
plt.scatter(var2_hist_areaavg, change_target_areaavg)
if (corr[0, 1] >= 0.3):
    plt.plot(var2_hist_areaavg, abline_values, 'k')
try:
    plt.plot(var2_hist_areaavg, abline_values_pca, color = 'grey')
except (NameError):
    pass

xticks = plt.xticks()[0]
dx = xticks[1] - xticks[0]
yticks = plt.yticks()[0]
dy = yticks[1] - yticks[0]
plt.text(0.8, 0.95,'OLS R$^2$ = %s' %(round(r_val**2, 3)), ha = 'center',
         va = 'center', transform = ax.transAxes)
plt.text(0.8, 0.9,'Corr = %s' %(round(np.nanmean(corr[0, 1]), 3)), ha = 'center', 
         va = 'center', transform = ax.transAxes)
plt.title(longname_region + ' ' + res_name)
if normalize:
    plt.xlabel(const_var[0] + ' ' + var_file_const[0] + ' [-]')
    plt.ylabel('$\Delta$' + target_var[0] + ' ' + var_file_target[0] + ' [-]')
else:
    plt.xlabel(const_var[0] + ' ' + var_file_const[0] + ' [' + unit_var2 + ']')
    plt.ylabel('$\Delta$' + target_var[0] + ' ' + var_file_target[0] + ' [' + unit_target + ']')

plt.savefig(plotname)

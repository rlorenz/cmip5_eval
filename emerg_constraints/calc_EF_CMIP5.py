#!/usr/bin/python
'''
File Name : calc_EF_CMIP5.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 18-10-2016
Modified: Tue 18 Oct 2016 08:59:45 AM CEST
Purpose: calculate evaporative fraction for all available CMIP5 runs


'''
import netCDF4 as nc
import numpy as np
import glob as glob
import os
import datetime as dt
import calendar
from netcdftime import utime
from os.path import expanduser
home = expanduser("~") # Get users home directory
import sys
sys.path.insert(0, home+'/scripts/plot_scripts/utils/')
from func_read_data import func_read_netcdf
###
# Define input
###
variable1 = 'hfls'
variable2 = 'hfss'
freq = 'mon'
experiment = 'rcp85'

update = False #force to update no matter if file alrady exists?

archive = '/net/atmos/data/cmip5-ng'

outdir = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/ef/%s' %(freq)
if (os.access(outdir, os.F_OK) == False):
    os.makedirs(outdir)

grid = 'g025'

###
# Read data
###
print "Read model data"
# find all files for target variable
name_var1 = '%s/%s/%s_%s_*_%s_*_%s.nc' %(archive, variable1, variable1, freq,
                                         experiment, grid)
model_names_var1 = []
for filenames in glob.glob(name_var1):
    fh = nc.Dataset(filenames, mode = 'r')
    model = fh.source_model
    if model == 'ACCESS1.3':
        model = 'ACCESS1-3'
    elif model == 'FGOALS_g2':
        model = 'FGOALS-g2'
    ens = fh.source_ensemble
    model_names_var1.append(model + '_' + ens)
    fh.close()

# find all files for variable 2
name_var2 = '%s/%s/%s_%s_*_%s_*_%s.nc' %(archive, variable2, variable2, freq,
                                         experiment, grid)
model_names_var2 = []
for filenames in glob.glob(name_var2):
    fh = nc.Dataset(filenames, mode = 'r')
    model = fh.source_model
    if model == 'ACCESS1.3':
        model = 'ACCESS1-3'
    elif model == 'FGOALS_g2':
        model = 'FGOALS-g2'
    ens = fh.source_ensemble
    model_names_var2.append(model + '_' + ens)
    fh.close()
# find overlapping models, ensembles
overlap = model_names_var1
overlap = list(set(model_names_var2) & set(overlap))

nfiles = len(overlap)
model_names = overlap

print str(nfiles)+ ' matching files found'
for f in xrange(len(model_names)):
    model = model_names[f].split('_', 1)[0]
    ens = model_names[f].split('_', 1)[1]

    filename_var1 = '%s/%s/%s_%s_%s_%s_%s_%s.nc' %(archive, variable1,
                                                   variable1, freq, model,
                                                   experiment, ens, grid)
    var1_read = func_read_netcdf(filename_var1, variable1,
                                 var_units = True)
    var1_data = var1_read['data']
    if isinstance(var1_data, np.ma.core.MaskedArray):
        var1_data = var1_data.filled(np.NaN)

    lat = var1_read['lat']
    lon = var1_read['lon']
    var1_dates = var1_read['dates']
    unit_var1 = var1_read['unit']

    filename_var2 = '%s/%s/%s_%s_%s_%s_%s_%s.nc' %(archive, variable2,
                                                variable2, freq, model,
                                                experiment, ens, grid)

    var2_read = func_read_netcdf(filename_var2, variable2,
                                 var_units = True)
    var2_data = var2_read['data']
    var2_dates = var2_read['dates']
    if isinstance(var2_data, np.ma.core.MaskedArray):
        var2_data = var2_data.filled(np.NaN)

    if f == 0:
        #hist_dates = var_hist['dates']
        unit_var2 = var2_read['unit']

    # check that both variables span same time period
    if (var2_dates.any() != var1_dates.any()):
        print 'Warning: hfls and hfss do not span the same time period'
        sys.exit
    else:
	dates = var1_dates
    # Calculate evaporative fraction for each model over all time steps
    ef = var1_data / (var1_data + var2_data)
    # mask unphysical values
    ef = np.ma.masked_outside(ef, -0.5, 1.5)

    # save data into netcdfs
    datesout = var1_dates
    time_out = var1_read['time']
    Fill = 1.e20
    fout=nc.Dataset('%s/ef_%s_%s_%s_%s_%s.nc' %(outdir, freq, model, experiment,
                                                ens, grid), mode = 'w')

    fout.createDimension('time', None)
    fout.createDimension('lat', lat.shape[0])
    fout.createDimension('lon', lon.shape[0])

    timeout = fout.createVariable('time', 'f8', ('time'), fill_value = Fill)
    setattr(timeout, 'calendar', str(var1_read['tcal']))
    setattr(timeout, 'units', str(var1_read['tunit']))

    latout = fout.createVariable('lat', 'f8', ('lat'), fill_value = Fill)
    setattr(latout, 'Longname', 'Latitude')
    setattr(latout, 'units', 'degrees_north')

    lonout = fout.createVariable('lon', 'f8', ('lon'), fill_value = Fill)
    setattr(lonout, 'Longname', 'Longitude')
    setattr(lonout, 'units', 'degrees_east')

    efout = fout.createVariable('ef', 'f4', ('time', 'lat', 'lon'),
                              fill_value = Fill)
    setattr(efout, 'missing_value', Fill)
    setattr(efout, 'Longname', 'Evaporative fraction')
    setattr(efout, 'units', '-')
    setattr(efout, 'description', 'Evaporative fraction EF = hfls/(hfls+hfss)')

    timeout[:] = time_out[:]
    latout[:] = lat[:]
    lonout[:] = lon[:]
    efout[:] = ef[:]

    # Set global attributes
    setattr(fout, "source_model", str(model))
    setattr(fout, "source_ensemble", str(ens))
    setattr(fout, "source_experiment", "historical,"+experiment)
    setattr(fout, "freq", "monthly")
    setattr(fout, "author","Ruth Lorenz @IAC, ETH Zurich, Switzerland")
    setattr(fout, "contact","ruth.lorenz@env.ethz.ch")
    setattr(fout, "creation date", dt.datetime.today().strftime('%Y-%m-%d'))

    setattr(fout, "Script", "calc_EF_CMIP5.py")
    setattr(fout, "Input files located in:", archive)
    fout.close()

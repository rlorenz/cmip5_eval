#!/usr/bin/python
'''
File Name : conc_regr_TXx_CMIP5_bio.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 10-11-2016
Modified: Thu 10 Nov 2016 05:22:03 PM CET
Purpose: Concatenate and regrid TXx files located in 
	/net/bio/climphys2/fischeer/CMIP5/EXTREMES/STATS/

'''
import os # operating system interface
import glob

###
# Define input
###
archive = '/net/bio/climphys2/fischeer/CMIP5/EXTREMES/STATS/'

outdir = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/TXx/'
regrid_func = 'remapbil'
grid = '/home/rlorenz/scripts/postproc_eraint/25x25grid' #'r144x72'
GRID = 'g025'
experiment = 'rcp85'

ifile = 'tas_*_%s_*_2006-2100.YEARMAX.nc' %(experiment)

#force to update no matter if file alrady exists?
update = True

if (os.access(outdir,os.F_OK) == False):
        os.makedirs(outdir)

for filename in glob.glob(archive + ifile):
    print "Regrid " + filename + " data"
    model = filename.split('_')[1]
    ensemble = filename.split('_')[3]
    histfile = '%s/tas_%s_historical_%s_1901-2005.YEARMAX.nc' %(archive, model, ensemble)
    outfile = '%s/tas_%s_%s_%s_1901-2100.YEARMAX_%s.nc' %(outdir, model, experiment, ensemble, GRID)
    if (os.path.isfile(outfile)) and not update:
        continue
    else:
        os.system('cdo %s,%s %s tmp_rcp.nc' %(regrid_func, grid, filename))
        os.system('cdo %s,%s %s tmp_hist.nc' %(regrid_func, grid, histfile))
        os.system('ncrcat -O tmp_hist.nc tmp_rcp.nc %s' %(outfile))
        os.system('ncrename -v tas,TXx %s' %(outfile))
        os.system('ncatted -h -a source_model,global,c,c,%s %s' %(model, outfile))
        os.system('ncatted -h -a source_ensemble,global,c,c,%s %s' %(ensemble, outfile))

#!/usr/bin/python
'''
File Name : test_corr_hist_future.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 12-10-2016
Modified: Tue 17 Jan 2017 05:56:23 PM CET
Purpose: plot correlation of historical to future change
	to test if emergent constraint

'''

import netCDF4 as nc # to work with NetCDF files
import numpy as np
import os # operating system interface
from os.path import expanduser
home = expanduser("~") # Get users home directory
workdir = os.getcwd()
import glob
import sys
sys.path.insert(0, home + '/scripts/plot_scripts/utils/')
sys.path.insert(0, workdir + '/functions/')
from func_read_data import func_read_netcdf
from clim_seas_TLL import clim_mon_TLL, std_mon_TLL, seas_avg_mon_TLL, seas_std_mon_TLL, clim_seas_mon_TLL, ann_avg_mon_TLL, ann_std_mon_TLL
from calc_trend_TLL import trend_TLL
from scipy import signal, stats
from point_inside_polygon import point_inside_polygon
import math
import matplotlib.pyplot as plt
from draw_utils import draw
from mpl_toolkits.basemap import maskoceans, shiftgrid
from sklearn.decomposition import PCA as sklearnPCA
###
# Define input
###
# target variable and explaining variables
target_var = 'tasmax'
variable2 = ['hfls']
# climatology:clim, variability:std, trend:trnd
var_file_target = ['clim']
var_file_2 = ['clim']
# kind is cyc: annual cycle, ann: annual mean, seas: seasonal mean,
# or mon: monthly values 
var_kind_target = ['seas']
var_kind_2 = ['seas']
# choose data for particular month or season (0:DJF, 1:MAM, 2:JJA, 3:SON)?
res = [2]
res_name = 'JJA'

# choose region if required, otherwise longname = 'GLOBAL'
region = 'SOEU'
longname_region = 'Southern Europe'
# mask ocean? if True maskocean function returns masked array! -> 
masko = False

archive = '/net/atmos/data/cmip5-ng'
indir = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/'
outdir = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/emerg_const/%s/' %(
    target_var)
if (os.access(outdir, os.F_OK) == False):
    os.makedirs(outdir)

syear_hist = 1980
eyear_hist = 2014
syear_fut = 2065
eyear_fut = 2099

experiment = 'rcp85'
grid = 'g025'

# normalize data? if variables on different scale we need to normalize
# for total least squares regression
normalize = False

###
# Read data
###
print "Read model data"
# find all files for target variable
if (target_var == 'ef'):
    name_target = '%s/%s/mon/%s_mon_*_%s_*_%s.nc' %(indir, target_var,
                                                  target_var, experiment,
                                                  grid)
elif (target_var == 'pi'):
    name_target = '%s/LandCoupPi/mon/%s_mon_*_%s_*_%s.nc' %(indir,
                                                            target_var,
                                                            experiment, grid)
elif (target_var == 'TXx'):
    name_target = '%s/TXx/tas_*_%s_*_1901-2100.YEARMAX_%s.nc' %(indir,
                                                                experiment,
                                                                grid)
else:
    name_target = '%s/%s/%s_mon_*_%s_*_%s.nc' %(archive, target_var,
                                              target_var, experiment, grid)
model_names_target = []
for filenames in glob.glob(name_target):
    fh = nc.Dataset(filenames, mode = 'r')
    model = fh.source_model
    if model == 'ACCESS1.3':
        model = 'ACCESS1-3'
    elif model == 'FGOALS_g2':
        model = 'FGOALS-g2'
    ens = fh.source_ensemble
    model_names_target.append(model + '_' + ens)
    fh.close()

# find all files for variable 2
if (variable2[0] == 'ef'):
    name_var2 = '%s/%s/mon/%s_mon_*_%s_*_%s.nc' %(indir, variable2[0],
                                                  variable2[0], experiment,
                                                  grid)
elif (variable2[0] == 'pi'):
    name_var2 = '%s/LandCoupPi/mon/%s_mon_*_%s_*_%s.nc' %(indir, variable2[0],
                                                          experiment, grid)
else:
    name_var2 = '%s/%s/%s_mon_*_%s_*_%s.nc' %(archive, variable2[0],
                                              variable2[0], experiment, grid)

model_names_var2 = []
for filenames in glob.glob(name_var2):
    fh = nc.Dataset(filenames, mode = 'r')
    model = fh.source_model
    if model == 'ACCESS1.3':
        model = 'ACCESS1-3'
    elif model == 'FGOALS_g2':
        model = 'FGOALS-g2'
    ens = fh.source_ensemble
    model_names_var2.append(model + '_' + ens)
    fh.close()
# find overlapping models, ensembles
overlap = model_names_target
overlap = list(set(model_names_var2) & set(overlap))

nfiles = len(overlap)
model_names = overlap
d_change_target = dict()
d_target_hist = dict()
d_target_hist_ts = dict()
d_var2_hist = dict()
d_var2_hist_ts = dict()
change_target_areaavg = np.empty((len(model_names)))
var2_hist_areaavg = np.empty((len(model_names)))
print str(nfiles)+ ' matching files found'
for f in xrange(len(model_names)):
    model = model_names[f].split('_', 1)[0]
    ens = model_names[f].split('_', 1)[1]
    if (target_var == 'ef'):
        filename_target = '%s/%s/mon/%s_mon_%s_%s_%s_%s.nc' %(indir, target_var,
                                                              target_var, model,
                                                              experiment, ens,
                                                              grid)
    elif (target_var == 'pi'):
        filename_target = '%s/LandCoupPi/mon/%s_mon_%s_%s_%s_%s.nc' %(indir,
                                                                      target_var,
                                                                      model,
                                                                      experiment,
                                                                      ens, grid)
    elif (target_var == 'TXx'):
        filename_target = '%s/TXx/tas_%s_%s_%s_1901-2100.YEARMAX_%s.nc' %(indir,
                                                                          model,
                                                                          experiment,
                                                                          ens,
                                                                          grid)
    else:
        filename_target = '%s/%s/%s_mon_%s_%s_%s_%s.nc' %(archive, target_var,
                                                          target_var, model,
                                                          experiment, ens, grid)

    target_hist = func_read_netcdf(filename_target, target_var,
                                   var_units = True,
                                   syear = syear_hist, eyear = eyear_hist)
    target_hist_data = target_hist['data']
    if isinstance(target_hist_data, np.ma.core.MaskedArray):
        target_hist_data = target_hist_data.filled(np.NaN)

    lat = target_hist['lat']
    lon = target_hist['lon']
    hist_dates = target_hist['dates']
    unit_target = target_hist['unit']
    hist_months = np.asarray([hist_dates[i].month for i in xrange(len(hist_dates))])
    hist_years = np.asarray([hist_dates[i].year for i in xrange(len(hist_dates))])

    target_fut = func_read_netcdf(filename_target, target_var,
                                  syear = syear_fut, eyear = eyear_fut)
    target_fut_data = target_fut['data']
    if isinstance(target_fut_data, np.ma.core.MaskedArray):
        target_fut_data = target_fut_data.filled(np.NaN)
    fut_dates = target_fut['dates']
    fut_months = np.asarray([fut_dates[i].month for i in xrange(len(fut_dates))])
    fut_years = np.asarray([fut_dates[i].year for i in xrange(len(fut_dates))])

    if (variable2[0] == 'ef'):
        filename2 = '%s/%s/mon/%s_mon_%s_%s_%s_%s.nc' %(indir, variable2[0],
                                                        variable2[0], model,
                                                        experiment, ens, grid)
    elif (variable2[0] == 'pi'):
        filename2 = '%s/LandCoupPi/mon/%s_mon_%s_%s_%s_%s.nc' %(indir,
                                                                variable2[0],
                                                                model,
                                                                experiment,
                                                                ens, grid)
    else:
        filename2 = '%s/%s/%s_mon_%s_%s_%s_%s.nc' %(archive, variable2[0],
                                                    variable2[0], model,
                                                    experiment, ens, grid)
    var_hist = func_read_netcdf(filename2, variable2[0],
                                   var_units = True,
                                   syear = syear_hist, eyear = eyear_hist)
    var2_hist_data = var_hist['data']
    if variable2[0] == 'ef':
        var2_hist_data = np.ma.masked_outside(var2_hist_data, -0.1, 1.1)
    if isinstance(var2_hist_data, np.ma.core.MaskedArray):
        var2_hist_data = var2_hist_data.filled(np.NaN)
    hist_dates = var_hist['dates']
    var_hist_months = np.asarray([hist_dates[i].month for i in xrange(len(hist_dates))])
    var_hist_years = np.asarray([hist_dates[i].year for i in xrange(len(hist_dates))])
    if f == 0:
        unit_var2 = var_hist['unit']

    ## calculate annual, seasonal, or monthly clim, std, or trnd
    # normalize if necessary (normalize = True)
    if normalize:
        norm = 0
        for field in [target_hist_data, target_fut_data, var2_hist_data]:
            tmp1 = field[:]
            tmp_mean = np.nanmean(tmp1, axis = 0)
            tmp_std = np.nanstd(tmp1, axis = 0)
            tmp_norm = (tmp1 - tmp_mean) / tmp_std
            if (norm == 0):
                target_hist_data = tmp_norm[:]
            elif (norm == 1):
                target_fut_data = tmp_norm[:]
            elif (norm == 2):
                var2_hist_data = tmp_norm[:]
            norm = norm + 1

    # target historical
    if (var_kind_target[0] == 'ann'):
        if (target_var != 'TXx'):
            tmp = ann_avg_mon_TLL(target_hist_data, hist_years)
        else:
            tmp = target_hist_data
        target_hist_ts = tmp[:]
        del tmp
        if (var_file_target[0] == 'clim'):
            target_hist_tim = np.nanmean(tmp, axis = 0, keepdims = True)
        elif (var_file_target[0] == 'std'):
            if np.isnan(target_hist_ts).any():
                tmp_dtr = np.empty((target_hist_ts.shape))
                for ilat in xrange(len(lat)):
                    for ilon in xrange(len(lon)):
                        tmp = target_hist_ts[:, ilat, ilon]
                        if np.isnan(tmp).any():
                            tmp_dtr[:, ilat, ilon] = np.NaN
                        else:
                            tmp_dtr[:, ilat, ilon] = signal.detrend(tmp,
                                                                    axis = 0,
                                                                type = 'linear',
                                                                bp = 0)
                del tmp
            else:
                tmp_dtr = signal.detrend(target_hist_ts, axis = 0,
                                         type = 'linear', bp = 0)
            if (target_var != 'TXx'):
                target_hist_tim = ann_std_mon_TLL(tmp_dtr, hist_years)
            else:
                target_hist_tim = np.nanstd(tmp_dtr)
        elif (var_file_target[0] == 'trnd'):
            target_hist_tim = trend_TLL(np.squeeze(tmp),
                                            drop = False)
    if (var_kind_target[0] == 'seas'):
        tmp = clim_seas_mon_TLL(target_hist_data, hist_months,
                                hist_years)
        if res[0] != None:
            target_hist_ts = np.squeeze(tmp[res[0], :, :])
        else:
            target_hist_ts = tmp[:]
        del tmp
        if (var_file_target[0] == 'clim'):
            target_hist_tim = seas_avg_mon_TLL(target_hist_data, hist_months)
            if res[0] != None:
                target_hist_tim = target_hist_tim[res, :, :]
        elif (var_file_target[0] == 'std'):
            if np.isnan(target_hist_ts).any():
                tmp_dtr = np.empty((target_hist_ts.shape))
                for ilat in xrange(len(lat)):
                    for ilon in xrange(len(lon)):
                        tmp = target_hist_ts[:, ilat, ilon]
                        if np.isnan(tmp).any():
                            tmp_dtr[:, ilat, ilon] = np.NaN
                        else:
                            tmp_dtr[:, ilat, ilon] = signal.detrend(tmp,
                                                                    axis = 0,
                                                                type = 'linear',
                                                                bp = 0)
                del tmp
            else:
                tmp_dtr = signal.detrend(target_hist_ts, axis = 0,
                                         type = 'linear', bp = 0)
            target_hist_tim = np.nanstd(np.squeeze(tmp_dtr), axis = 0,
                                        keepdims = True)
        elif (var_file_target[0] == 'trnd'):
            if res == None:
                target_hist_tim = np.empty((4, len(lat), len(lon)))
                for seas in xrange(0, 4):
                    target_hist_tim[seas, :, :] = trend_TLL(np.squeeze(
                        tmp[seas, :, :, :]), drop = False)
            else:
                target_hist_tim = trend_TLL(np.squeeze(tmp[res[0], :, :, :]),
                                            drop = False)
    try:
        del tmp, tmp_dtr
    except (NameError):
        pass

    if (var_kind_target[0] == 'mon'):
        if res != None:
            tmp = target_hist_data[np.where(hist_months == res[0]), :, :]
            target_hist_ts = tmp[:]
        else:
            target_hist_ts = target_hist_data
        if (var_file_target[0] == 'clim'):
            target_hist_tim = clim_mon_TLL(target_hist_data, hist_months)
            if res[0] != None:
                target_hist_tim = target_hist_tim[res, :, :]
        elif (var_file_target[0] == 'std'):
            if np.isnan(target_hist_ts).any():
                tmp_dtr = np.empty((target_hist_ts.shape))
                for ilat in xrange(len(lat)):
                    for ilon in xrange(len(lon)):
                        tmp = target_hist_ts[:, ilat, ilon]
                        if np.isnan(tmp).any():
                            tmp_dtr[:, ilat, ilon] = np.NaN
                        else:
                            tmp_dtr[:, ilat, ilon] = signal.detrend(tmp,
                                                                    axis = 0,
                                                                type = 'linear',
                                                                bp = 0)
                del tmp
            else:
                tmp_dtr = signal.detrend(target_hist_ts, axis = 0,
                                         type = 'linear', bp = 0)
            target_hist_tim = np.nanstd(np.squeeze(tmp_dtr), axis = 0,
                                        keepdims = True)
            if res[0] != None:
                target_hist_tim = target_hist_tim[res, :, :]
        elif (var_file_target[0] == 'trnd'):
            if res == None:
                target_hist_tim = np.empty((4, len(lat), len(lon)))
                for mon in xrange(0, 11):
                    target_hist_tim[mon, :, :] = trend_TLL(np.squeeze(
                        tmp[mon, :, :, :]), drop = False)
            else:
                target_hist_tim = trend_TLL(np.squeeze(tmp), drop = False)
    try:
        del tmp, tmp_dtr
    except (NameError):
        pass

    # target future
    if (var_kind_target[0] == 'ann'):
        if (target_var != 'TXx'):
            tmp = ann_avg_mon_TLL(target_fut_data, fut_years)
        else:
            tmp = target_fut_data
        target_fut_tim_ts = tmp[:]
        if (var_file_target[0] == 'clim'):
            target_fut_tim = np.nanmean(tmp, axis = 0)
        elif (var_file_target[0] == 'std'):
            if np.isnan(target_fut_tim_ts).any():
                tmp_dtr = np.empty((target_fut_tim_ts.shape))
                for ilat in xrange(len(lat)):
                    for ilon in xrange(len(lon)):
                        tmp = target_fut_tim_ts[:, ilat, ilon]
                        if np.isnan(tmp).any():
                            tmp_dtr[:, ilat, ilon] = np.NaN
                        else:
                            tmp_dtr[:, ilat, ilon] = signal.detrend(tmp,
                                                                    axis = 0,
                                                                    type = 'linear',
                                                                    bp = 0)
                del tmp
            else:
                tmp_dtr = signal.detrend(target_fut_tim_ts, axis = 0,
                                         type = 'linear', bp = 0)
            target_fut_tim = np.nanstd(np.squeeze(tmp_dtr), axis = 0,
                                    keepdims = True)
        elif (var_file_target[0] == 'trnd'):
            target_fut_tim = trend_TLL(np.squeeze(tmp),
                                            drop = False)

    if (var_kind_target[0] == 'seas'):
        tmp = clim_seas_mon_TLL(target_fut_data, fut_months,
                                fut_years)
        if res != None:
            target_fut_tim_ts = np.squeeze(tmp[res[0], :, :, :])
        else:
            target_fut_tim_ts = tmp[:]
        if (var_file_target[0] == 'clim'):
            target_fut_tim = seas_avg_mon_TLL(target_fut_data, fut_months)
            if res[0] != None:
                target_fut_tim = target_fut_tim[res, :, :]
        elif (var_file_target[0] == 'std'):
            if np.isnan(target_fut_tim_ts).any():
                tmp_dtr = np.empty((target_fut_tim_ts.shape))
                for ilat in xrange(len(lat)):
                    for ilon in xrange(len(lon)):
                        tmp = target_fut_tim_ts[:, ilat, ilon]
                        if np.isnan(tmp).any():
                            tmp_dtr[:, ilat, ilon] = np.NaN
                        else:
                            tmp_dtr[:, ilat, ilon] = signal.detrend(tmp,
                                                                    axis = 0,
                                                                    type = 'linear',
                                                                    bp = 0)
                del tmp
            else:
                tmp_dtr = signal.detrend(target_fut_tim_ts, axis = 0,
                                         type = 'linear', bp = 0)
            target_fut_tim = np.nanstd(tmp_dtr, axis = 0,
                                       keepdims = True)
        elif (var_file_target[0] == 'trnd'):
            if res == None:
                target_fut_tim = np.empty((4, len(lat), len(lon)))
                for seas in xrange(0, 4):
                    target_fut_tim[seas, :, :] = trend_TLL(np.squeeze(
                        tmp[seas, :, :, :]), drop = False)
            else:
                target_fut_tim = trend_TLL(np.squeeze(tmp[res[0], :, :, :]),
                                            drop = False)
    try:
        del tmp, tmp_dtr
    except (NameError):
        pass

    if (var_kind_target[0] == 'mon'):
        if res != None:
            tmp = target_fut_data[np.where(fut_months == res[0]), :, :]
        else:
            tmp = target_fut_data
        target_fut_tim_ts = tmp[:]
        if (var_file_target[0] == 'clim'):
            target_fut_tim = clim_mon_TLL(target_fut_data, fut_months)
            if res[0] != None:
                target_fut_tim = target_fut_tim[res, :, :]
        elif (var_file_target[0] == 'std'):
            if np.isnan(target_fut_tim_ts).any():
                tmp_dtr = np.empty((target_fut_tim_ts.shape))
                for ilat in xrange(len(lat)):
                    for ilon in xrange(len(lon)):
                        tmp = target_fut_tim_ts[:, ilat, ilon]
                        if np.isnan(tmp).any():
                            tmp_dtr[:, ilat, ilon] = np.NaN
                        else:
                            tmp_dtr[:, ilat, ilon] = signal.detrend(tmp,
                                                                    axis = 0,
                                                                    type = 'linear',
                                                                    bp = 0)
                del tmp
            else:
                tmp_dtr = signal.detrend(target_fut_tim_ts, axis = 0,
                                         type = 'linear', bp = 0)
            target_fut_tim = np.nanstd(tmp_dtr, axis = 0,
                                       keepdims = True)
            if res[0] != None:
                target_fut_tim = target_fut_tim[res, :, :]
        elif (var_file_target[0] == 'trnd'):
            if res == None:
                target_fut_tim = np.empty((4, len(lat), len(lon)))
                for mon in xrange(0, 11):
                    target_fut_tim[mon, :, :] = trend_TLL(np.squeeze(
                        tmp[mon, :, :, :]), drop = False)
            else:
                target_fut_tim = trend_TLL(np.squeeze(tmp), drop = False)
    try:
        del tmp, tmp_dtr
    except (NameError):
        pass

    # variable2 historical
    if (var_kind_2[0] == 'ann'):
        tmp = ann_avg_mon_TLL(var2_hist_data, hist_years)
        var2_hist_ts = tmp[:]
        if (var_file_2[0] == 'clim'):
            var2_hist_tim = np.nanmean(tmp, axis = 0)
        elif (var_file_2[0] == 'std'):
            if np.isnan(var2_hist_ts).any():
                tmp_dtr = np.empty((var2_hist_ts.shape))
                for ilat in xrange(len(lat)):
                    for ilon in xrange(len(lon)):
                        tmp = var2_hist_ts[:, ilat, ilon]
                        if np.isnan(tmp).any():
                            tmp_dtr[:, ilat, ilon] = np.NaN
                        else:
                            tmp_dtr[:, ilat, ilon] = signal.detrend(tmp,
                                                                    axis = 0,
                                                                    type = 'linear',
                                                                    bp = 0)
                del tmp
            else:
                tmp_dtr = signal.detrend(var2_hist_ts, axis = 0,
                                         type = 'linear', bp = 0)
            var2_hist_tim = np.nanstd(np.squeeze(tmp_dtr), axis = 0,
                                      keepdims = True)
        elif (var_file_2[0] == 'trnd'):
            if res == None:
                var2_hist_tim = trend_TLL(np.squeeze(tmp),
                                          drop = False)
    if (var_kind_2[0] == 'seas'):
        tmp = clim_seas_mon_TLL(var2_hist_data, var_hist_months,
                                var_hist_years)
        if res == None:
            var2_hist_ts = tmp[:]
        else:
            var2_hist_ts = np.squeeze(tmp[res[0], :, :, :])
        if (var_file_2[0] == 'clim'):
            var2_hist_tim = seas_avg_mon_TLL(var2_hist_data, var_hist_months)
            if res[0] != None:
                var2_hist_tim = var2_hist_tim[res, :, :]
        elif (var_file_2[0] == 'std'):
            if np.isnan(var2_hist_ts).any():
                tmp_dtr = np.empty((var2_hist_ts.shape))
                for ilat in xrange(len(lat)):
                    for ilon in xrange(len(lon)):
                        tmp = var2_hist_ts[:, ilat, ilon]
                        if np.isnan(tmp).any():
                            tmp_dtr[:, ilat, ilon] = np.NaN
                        else:
                            tmp_dtr[:, ilat, ilon] = signal.detrend(tmp,
                                                                    axis = 0,
                                                                    type = 'linear',
                                                                    bp = 0)
                del tmp
            else:
                tmp_dtr = signal.detrend(var2_hist_ts, axis = 0,
                                         type = 'linear', bp = 0)
            var2_hist_tim = np.nanstd(np.squeeze(tmp_dtr), axis = 0,
                                      keepdims = True)
        elif (var_file_2[0] == 'trnd'):
            if res == None:
                var2_hist_tim = np.empty((4, len(lat), len(lon)))
                for seas in xrange(0, 4):
                    var2_hist_tim[seas, :, :] = trend_TLL(np.squeeze(
                        tmp[seas, :, :, :]), drop = False)
            else:
                var2_hist_tim = trend_TLL(np.squeeze(tmp[res[0], :, :, :]),
                                          drop = False)
    try:
        del tmp, tmp_dtr
    except (NameError):
        pass

    if (var_kind_2[0] == 'mon'):
        if res != None:
            tmp = var2_hist_data[np.where(var_hist_months == res[0]), :, :]
            var2_hist_ts = tmp[:]
        else:
            var2_hist_ts = var2_hist_data
        if (var_file_2[0] == 'clim'):
            var2_hist_tim = clim_mon_TLL(var2_hist_data, var_hist_months)
            if res[0] != None:
                var2_hist_tim = var2_hist_tim[res, :, :]
        elif (var_file_2[0] == 'std'):
            if np.isnan(var2_hist_ts).any():
                tmp_dtr = np.empty((var2_hist_ts.shape))
                for ilat in xrange(len(lat)):
                    for ilon in xrange(len(lon)):
                        tmp = var2_hist_ts[:, ilat, ilon]
                        if np.isnan(tmp).any():
                            tmp_dtr[:, ilat, ilon] = np.NaN
                        else:
                            tmp_dtr[:, ilat, ilon] = signal.detrend(tmp,
                                                                    axis = 0,
                                                                    type = 'linear',
                                                                    bp = 0)
                del tmp
            else:
                tmp_dtr = signal.detrend(var2_hist_ts, axis = 0,
                                         type = 'linear', bp = 0)
            var2_hist_tim = np.nanstd(np.squeeze(tmp_dtr), axis = 0,
                                      keepdims = True)
        elif (var_file_2[0] == 'trnd'):
            if res == None:
                var2_hist_tim = np.empty((4, len(lat), len(lon)))
                for mon in xrange(0, 11):
                    var2_hist_tim[mon, :, :] = trend_TLL(np.squeeze(
                        tmp[mon, :, :, :]), drop = False)
            else:
                var2_hist_tim = trend_TLL(np.squeeze(tmp), drop = False)
    try:
        del tmp, tmp_dtr
    except (NameError):
        pass

    change_target = target_fut_tim - target_hist_tim

    ## mask ocean points if masko = True
    if masko:
        lons2d, lats2d = np.meshgrid(lon, lat)
        change_target = maskoceans(lons2d, lats2d, change_target,
                                   resolution = 'h', grid = 1.25,
                                   inlands = True)
        target_hist_tim = maskoceans(lons2d, lats2d, target_hist_tim,
                                   resolution = 'h', grid = 1.25,
                                   inlands = True)
        var2_hist_tim = maskoceans(lons2d, lats2d, var2_hist_tim,
                                   resolution = 'h', grid = 1.25,
                                   inlands = True)
        for t in xrange(0, target_hist_ts.shape[0]):
            target_hist_ts[t, :, :] = maskoceans(lons2d, lats2d,
                                                 target_hist_ts[t, :, :],
                                                 resolution = 'h', grid = 1.25,
                                                 inlands = True)
            var2_hist_ts[t, :, :] = maskoceans(lons2d, lats2d,
                                               var2_hist_ts[t, :, :],
                                               resolution = 'h', grid = 1.25,
                                               inlands = True)

    ## if any region defined, cut data into region
    if region != None:
        mask = np.loadtxt('%s/scripts/plot_scripts/areas_txt/%s.txt' %(home,
                                                                     region))
        # shiftgrid since masks defined from -180 to 180
        lons_orig = lon[:]
        change_target, lon =  shiftgrid(180., change_target, lons_orig,
                                        start = False)
        target_hist_tim, lon =  shiftgrid(180., target_hist_tim, lons_orig,
                                          start = False)
        target_hist_ts, lon =  shiftgrid(180., target_hist_ts, lons_orig,
                                         start = False)
        var2_hist_tim, lon =  shiftgrid(180., var2_hist_tim, lons_orig,
                                        start = False)
        var2_hist_ts, lon =  shiftgrid(180., var2_hist_ts, lons_orig,
                                       start = False)
        change_target_reg_nan = np.ndarray((change_target.shape))
        target_hist_tim_reg_nan = np.ndarray((target_hist_tim.shape))
        target_hist_ts_reg_nan = np.ndarray((target_hist_ts.shape))
        var2_hist_tim_reg_nan = np.ndarray((var2_hist_tim.shape))
        var2_hist_ts_reg_nan = np.ndarray((var2_hist_ts.shape))
        for ilat in xrange(len(lat)):
            for ilon in xrange(len(lon)):
                if (point_inside_polygon(lat[ilat], lon[ilon],
                                         np.fliplr(mask))):
                    if (type(change_target) == 'numpy.ma.core.MaskedArray'):
                        change_target_reg_nan[:, ilat, ilon] = change_target[:, ilat, ilon].filled(np.NaN)
                    else:
                        change_target_reg_nan[:, ilat, ilon] = change_target[:, ilat, ilon]
                    if (type(target_hist_tim) == 'numpy.ma.core.MaskedArray'):
                        target_hist_tim_reg_nan[:, ilat, ilon] = target_hist_tim[:, ilat, ilon].filled(np.NaN)
                    else:
                        target_hist_tim_reg_nan[:, ilat, ilon] = target_hist_tim[:, ilat, ilon]
                    if (type(target_hist_ts) == 'numpy.ma.core.MaskedArray'):
                        target_hist_ts_reg_nan[:, ilat, ilon] = target_hist_ts[:, ilat, ilon].filled(np.NaN)
                    else:
                        target_hist_ts_reg_nan[:, ilat, ilon] = target_hist_ts[:, ilat, ilon]
                    if (type(var2_hist_tim) == 'numpy.ma.core.MaskedArray'):
                        var2_hist_tim_reg_nan[:, ilat, ilon] = var2_hist_tim[:, ilat, ilon].filled(np.NaN)
                    else:
                        var2_hist_tim_reg_nan[:, ilat, ilon] = var2_hist_tim[:, ilat, ilon]
                    if (type(var2_hist_ts) == 'numpy.ma.core.MaskedArray'):
                        var2_hist_ts_reg_nan[:, ilat, ilon] = var2_hist_ts[:, ilat, ilon].filled(np.NaN)
                    else:
                        var2_hist_ts_reg_nan[:, ilat, ilon] = var2_hist_ts[:, ilat, ilon]
                else:
                    change_target_reg_nan[:, ilat, ilon] = np.NaN
                    target_hist_tim_reg_nan[:, ilat, ilon] = np.NaN
                    target_hist_ts_reg_nan[:, ilat, ilon] = np.NaN
                    var2_hist_tim_reg_nan[:, ilat, ilon] = np.NaN
                    var2_hist_ts_reg_nan[:, ilat, ilon] = np.NaN
        idx_lat = np.where((lat > np.min(mask[:, 1])) & (lat < np.max(mask[:, 1])))
        lat = lat[idx_lat]

        tmp1_reg = np.empty((len(change_target_reg_nan), len(lat), len(lon)))
        tmp11_reg = np.empty((len(target_hist_tim_reg_nan), len(lat), len(lon)))
        tmp1_ts_reg = np.empty((len(target_hist_ts_reg_nan), len(lat), len(lon)))
        tmp2_reg = np.empty((len(var2_hist_tim_reg_nan), len(lat), len(lon)))
        tmp2_ts_reg = np.empty((len(var2_hist_ts_reg_nan), len(lat), len(lon)))

        idx_lon = np.where((lon > np.min(mask[:, 0])) & (lon < np.max(mask[:, 0])))
        lon = lon[idx_lon]

        change_target_reg = np.empty((len(change_target_reg_nan), len(lat),
                                      len(lon)))
        tmp1_reg = np.squeeze(change_target_reg_nan[:, idx_lat, :], 1)
        target_hist_tim_reg = np.empty((len(target_hist_tim_reg_nan), len(lat),
                                      len(lon)))
        tmp11_reg = np.squeeze(target_hist_tim_reg_nan[:, idx_lat, :], 1)
        target_hist_ts_reg = np.empty((len(target_hist_ts_reg_nan), len(lat),
                                      len(lon)))
        tmp1_ts_reg = np.squeeze(target_hist_ts_reg_nan[:, idx_lat, :], 1)
        var2_hist_tim_reg = np.empty((len(var2_hist_tim_reg_nan), len(lat),
                                      len(lon)))
        tmp2_reg = np.squeeze(var2_hist_tim_reg_nan[:, idx_lat, :], 1)
        var2_hist_ts_reg = np.empty((len(var2_hist_ts_reg_nan), len(lat),
                                      len(lon)))
        tmp2_ts_reg = np.squeeze(var2_hist_ts_reg_nan[:, idx_lat, :], 1)

        change_target_reg = np.squeeze(tmp1_reg[:, :, idx_lon], 2)
        target_hist_tim_reg = np.squeeze(tmp11_reg[:, :, idx_lon], 2)
        target_hist_ts_reg = np.squeeze(tmp1_ts_reg[:, :, idx_lon], 2)
        var2_hist_tim_reg = np.squeeze(tmp2_reg[:, :, idx_lon], 2)
        var2_hist_ts_reg = np.squeeze(tmp2_ts_reg[:, :, idx_lon], 2)
        #del change_target
        #del target_hist_tim
        #del target_hist_ts
        #del var2_hist_tim
        #del var2_hist_ts
    else:
        if masko:
            change_target_reg = change_target.filled(np.NaN)
            target_hist_tim_reg = target_hist_tim.filled(np.NaN)
            target_hist_ts_reg = target_hist_ts.filled(np.NaN)
            var2_hist_tim_reg = var2_hist_tim.filled(np.NaN)
            var2_hist_ts_reg = var2_hist_ts.filled(np.NaN)
        else:
            change_target_reg = change_target
            target_hist_tim_reg = target_hist_tim
            target_hist_ts_reg = target_hist_ts
            var2_hist_tim_reg = var2_hist_tim
            var2_hist_ts_reg = var2_hist_ts

    d_change_target[model + '_' + ens] = change_target_reg[:]
    d_target_hist[model + '_' + ens] = target_hist_tim_reg[:]
    d_target_hist_ts[model + '_' + ens] = target_hist_ts_reg[:]
    d_var2_hist[model + '_' + ens] = var2_hist_tim_reg[:]
    d_var2_hist_ts[model + '_' + ens] = var2_hist_ts_reg[:]

    ## calculate area average
    ## weight for latitude differences in area
    w_lat = np.cos(lat * (4.0 * math.atan(1.0) / 180)) 
    ma_change_target_reg = np.ma.masked_array(change_target_reg,
                                              np.isnan(change_target_reg))
    ma_var2_hist_tim_reg = np.ma.masked_array(var2_hist_tim_reg,
                                              np.isnan(var2_hist_tim_reg))
    tmp1_latweight = np.ma.average(np.squeeze(ma_change_target_reg), axis = 0,
                                   weights = w_lat)
    change_target_areaavg[f] = np.nanmean(tmp1_latweight.filled(np.nan))
    tmp2_latweight = np.ma.average(np.squeeze(ma_var2_hist_tim_reg), axis = 0,
                                   weights = w_lat)
    var2_hist_areaavg[f] = np.nanmean(tmp2_latweight.filled(np.nan))

# Read obs data
for obsdata in ['MERRA2', 'Obs']:
    print "Read %s data" %(obsdata)
    if (obsdata == 'MERRA2'):
        path = '/net/tropo/climphys/rlorenz/Datasets/MERRA2/monthly'
        if (variable2[0] == 'tas'):
            temp = func_read_netcdf(
                   '%s/%s.tavgmon_2d_slv_Nx.T2MMEAN.1980-2015_remapbil_%s.nc'
                   %(path, obsdata, grid), 'T2MMEAN', var_units = True,
                   syear = syear_hist, eyear = eyear_hist)
            temp_obs = temp['data']
            obs_unit = temp['unit']
        elif (variable2[0] == 'tasmax'):
            temp = func_read_netcdf(
                   '%s/%s.tavgmon_2d_slv_Nx.T2MMAX.1980-2015_remapbil_%s.nc'
                   %(path, obsdata, grid), 'T2MMAX', var_units = True,
                   syear = syear_hist, eyear = eyear_hist)
            temp_obs = temp['data']
            obs_unit = temp['unit']
        elif (variable2[0] == 'pr'):
            temp = func_read_netcdf(
                   '%s/%s.tavgmon_2d_slv_Nx.TPRECMAX.1980-2015_remapcon2_%s.nc'
                   %(path, obsdata, grid), 'TPRECMAX', var_units = True,
                   syear = syear_hist, eyear = eyear_hist)
            temp_obs = temp['data']
            obs_unit = temp['unit']
        elif (variable2[0] == 'hfls'):
            temp = func_read_netcdf(
                   '%s/%s.tavgmon_2d_flx_Nx.EFLUX.1980-2015_remapbil_%s.nc'
                   %(path, obsdata, grid), 'EFLUX', var_units = True,
                   syear = syear_hist, eyear = eyear_hist)
            temp_obs = temp['data']
            obs_unit = temp['unit']
        elif (variable2[0] == 'psl'):
            temp = func_read_netcdf(
                   '%s/%s.tavgmon_2d_slv_Nx.SLP.1980-2015_remapbil_%s.nc'
                   %(path, obsdata, grid), 'SLP', var_units = True,
                   syear = syear_hist, eyear = eyear_hist)
            temp_obs = temp['data']
            obs_unit = temp['unit']
        elif (variable2[0] == 'ef'):
            temp = func_read_netcdf(
                   '%s/%s.tavgmon_2d_flx_Nx.EF.1980-2015_remapbil_%s.nc'
                   %(path, obsdata, grid), 'EFLUX', var_units = True,
                   syear = syear_hist, eyear = eyear_hist)
            temp_obs = temp['data']
            obs_unit = temp['unit']
        elif (variable2[0] == 'rsus'):
            temp = func_read_netcdf(
                    '%s/%s.tavgmon_2d_rad_Nx.SWGEM.1980-2015_remapbil_%s.nc'
                    %(path, obsdata, grid), 'SWGEM', var_units = True,
                    syear = syear_hist, eyear = eyear_hist)
            temp_obs = temp['data']
            obs_unit = temp['unit']
        elif (variable[0] == 'rsds'):
            temp = func_read_netcdf(
                    '%s/%s.tavgmon_2d_rad_Nx.SWGDN.1980-2015_remapbil_%s.nc'
                    %(path, obsdata, grid), 'SWGDN', var_units = True,
                    syear = syear_hist, eyear = eyear_hist)
            temp_obs = temp['data']
            obs_unit = temp['unit']
        elif (variable2[0] == 'rlus'):
            temp = func_read_netcdf(
                    '%s/%s.tavgmon_2d_rad_Nx.LWGEM.1980-2015_remapbil_%s.nc'
                    %(path, obsdata, grid), 'LWGEM', var_units = True,
                    syear = syear_hist, eyear = eyear_hist)
            temp_obs = temp['data']
            obs_unit = temp['unit']
        elif (variable2[0] == 'huss'):
            temp = func_read_netcdf(
                    '%s/%s.tavgmon_2d_slv_Nx.QV2M.1980-2015_remapbil_%s.nc'
                    %(path, obsdata, grid), 'QV2M', var_units = True,
                    syear = syear_hist, eyear = eyear_hist)
            temp_obs = temp['data']
            obs_unit = temp['unit']
        else:
            print "Warning: no MERRA data for this variable, exiting"
            sys.exit
    if (obsdata == 'Obs'):
        print "Read Obs data"
        path = '/net/tropo/climphys/rlorenz/Datasets/'
        if (variable2[0] == 'tasmax'):
            temp = func_read_netcdf(
                   '%s/HadGHCND/monthly/HadGHCND_TXTN_avg_monthly_acts_195001-201412_remapbil_%s.nc'
                   %(path, grid), 'tmax', var_units = True,
                   syear = syear_hist, eyear = eyear_hist)
            temp_obs = temp['data']
            obs_unit = temp['unit']
            if ((obs_unit == 'degC') or (obs_unit == 'C') or (obs_unit == 'degrees C')):
                ## convert to K
                tmp_obs = temp_obs + 273.15
                temp_obs = tmp_obs[:]
                del tmp_obs
                obs_unit = 'K'
        elif (variable2[0] == 'pr'):
            temp = func_read_netcdf(
                   '%s/GPCP/precip.mon.mean.197901-201607.v2.3.nc' %(
                   path), 'precip', var_units = True, syear = syear_hist,
                   eyear = eyear_hist)
            temp_obs = temp['data']
            obs_unit = temp['unit']
        elif (variable2[0] == 'hfls'):
            temp = func_read_netcdf(
                   '%s/GLEAM/monthly/GLEAM_E_monthly_v3.0a_1980-2014_%s.nc' %(
                   path, grid), 'E', var_units = True, syear = syear_hist,
                   eyear = eyear_hist)
            temp_obs = temp['data']
            obs_unit = temp['unit']
            if ((obs_unit == 'mm/d') or (obs_unit == 'mm/day')):
                ## convert to W m^-2
                factor = 2.5 * 10 ** 6 / (24 * 60 * 60)
                tmp_obs = np.multiply(temp_obs, factor)
                temp_obs = tmp_obs[:]
                del tmp_obs
                obs_unit = 'Wm-2'
        elif (variable2[0] == 'psl'):
            print "Warning: no obs data for this variable, exiting"
            sys.exit
        elif (variable2[0] == 'ef'):
            print "Warning: no obs data for this variable, exiting"
            sys.exit
        elif (variable2[0] == 'rsus'):
            temp = func_read_netcdf(
                    '%s/CERES/CERES_EBAF-sfc_sw_up_all_mon_Ed2.8_Subset_200003-201511_%s.nc'
                    %(path, grid), 'sfc_sw_up_all_mon', var_units = True,
                    syear = syear_hist, eyear = eyear_hist)
            temp_obs = temp['data']
            obs_unit = temp['unit']
        elif (variable2[0] == 'rsds'):
            temp = func_read_netcdf(
                    '%s/CERES/CERES_EBAF-sfc_sw_down_all_mon_Ed2.8_Subset_200003-201511_%s.nc'
                    %(path, grid), 'sfc_sw_down_all_mon', var_units = True,
                    syear = syear_hist, eyear = eyear_hist)
            temp_obs = temp['data']
            obs_unit = temp['unit']
        elif (variable2[0] == 'rlus'):
            temp = func_read_netcdf(
                    '%s/CERES/CERES_EBAF-sfc_lw_up_all_mon_Ed2.8_Subset_200003-201511_%s.nc'
                    %(path, grid), 'sfc_lw_up_all_mon', var_units = True,
                    syear = syear_hist, eyear = eyear_hist)
            temp_obs = temp['data']
            obs_unit = temp['unit']
        else:
            print "Warning: no obs data for this variable, exiting"
            sys.exit
    try:
        obslat = temp['lat']
        obslon = temp['lon']
        obsdates = temp['dates']
        obsmonths = np.asarray([obsdates[i].month for i in xrange(len(obsdates))])
        obsyears = np.asarray([obsdates[i].year for i in xrange(len(obsdates))])
    except NameError:
        obsdates = time_str

    if isinstance(temp_obs, np.ma.core.MaskedArray):
        temp_obs = temp_obs.filled(np.NaN)

    ## shiftgrid to -180 to 180 if necessary (to be compatible with area mask)
    if (obslon[0] >= 0.0) and (obslon[-1] > 180):
        obslons_orig = obslon[:]
        temp_obs, obslon = shiftgrid(180., temp_obs, obslon, start = False)

    ## if any region defined, read region extent from file and cut data into region
    if region != None:
        temp_obs_reg_nan = np.ndarray((temp_obs.shape))
        for ilat in xrange(len(obslat)):
            for ilon in xrange(len(obslon)):
                if (point_inside_polygon(obslat[ilat], obslon[ilon],
                    np.fliplr(mask))):
                    temp_obs_reg_nan[:, ilat, ilon] = temp_obs[:, ilat, ilon]
                else:
                    temp_obs_reg_nan[:, ilat, ilon] = np.NaN
        idx_lat = np.where((obslat > np.min(mask[:, 1])) &
                           (obslat < np.max(mask[:, 1])))
        obslat_orig = obslat
        obslat = obslat[idx_lat]
        tmp1_obs_reg = np.empty((len(temp_obs_reg_nan), len(obslat), len(obslon)))
        idx_lon = np.where((obslon > np.min(mask[:, 0])) &
                           (obslon < np.max(mask[:, 0])))
        obslon_orig = obslon
        obslon = obslon[idx_lon]
        temp_obs_reg = np.empty((len(temp_obs_reg_nan), len(obslat), len(obslon)))
        tmp1_obs_reg = np.squeeze(temp_obs_reg_nan[:, idx_lat, :], 1)
        temp_obs_reg = np.squeeze(tmp1_obs_reg[:, :, idx_lon], 2)
    else:
        area = 'GLOBAL'
        obslat_orig = obslat
        obslon_orig = obslon
        temp_obs_reg = temp_obs
    ## detrend ts before calculating variability etc.
    if np.isnan(temp_obs_reg).any():
        temp_obs_reg_dtr = np.empty((temp_obs_reg.shape))
        for ilat in xrange(len(obslat)):
            for ilon in xrange(len(obslon)):
                tmp = temp_obs_reg[:, ilat, ilon]
                if np.isnan(tmp).any():
                    temp_obs_reg_dtr[:, ilat, ilon] = np.NaN
                else:
                    temp_obs_reg_dtr[:, ilat, ilon] = signal.detrend(tmp, axis = 0,
                                                                     type = 'linear'
                                                                     , bp = 0)
    else:
        temp_obs_reg_dtr = signal.detrend(temp_obs_reg, axis = 0, type = 'linear',
                                          bp = 0)
    ## calculate obs climatology, variability, trend
    print "Calculate obs climatology, variability, trend"
    if (res[0] == None) and (var_kind_2[0] == 'cyc'):
        temp_obs_ts = ann_avg_mon_TLL(temp_obs_reg, obsyears)
    elif (res[0] == None) and (var_kind_2[0] != 'cyc'):
        temp_obs_ts = temp_obs_reg
    else:
        if var_kind_2[0] == 'mon':
            ind = np.where(obsmonths == res[0])
            temp_obs_ts = np.squeeze(temp_obs_reg[ind, :, :])
        elif var_kind_2[0] == 'seas':
            temp_obs_seas_ts = clim_seas_mon_TLL(temp_obs_reg, obsmonths, obsyears)
            temp_obs_ts = temp_obs_seas_ts[res[0], :, :, :]
    if (var_file_2[0] == 'clim'):
        if res[0] == None:
            temp_obs_clim = clim_mon_TLL(temp_obs_reg, obsmonths)
            time_out = range(0, 12)
        elif var_kind_2[0] == 'mon':
            ind = np.where(obsmonths == res[0])
            temp_obs_clim = np.nanmean(np.squeeze(temp_obs_reg[ind, :, :]), axis=0,
                                       keepdims = True)
            time_out = [1]
        elif var_kind_2[0] == 'seas':
            temp_obs_clim_seas = seas_avg_mon_TLL(temp_obs_reg, obsmonths)
            temp_obs_clim = temp_obs_clim_seas[res, :, :]
    if (var_file_2[0] == 'std'):
        if res[0] == None:
            temp_obs_std = std_mon_TLL(temp_obs_reg_dtr, obsmonths)
        elif var_kind_2[0] == 'mon':
            ind = np.where(obsmonths == res[0])
            temp_obs_std = np.nanstd(np.squeeze(temp_obs_reg_dtr[ind, :, :]),
                                     axis = 0, keepdims = True)
        elif var_kind_2[0] == 'seas':
            temp_obs_std_seas = seas_std_mon_TLL(temp_obs_reg_dtr, obsmonths, obsyears)
            temp_obs_std = temp_obs_std_seas[res, :, :]
    ## calculate trend over time defined in res
    if (var_file_2[0] == 'trnd'):
        if res[0] == None:
            temp_obs_trnd = trend_TLL(temp_obs_ts, drop = False)
        else:
            if (var_kind_2[0] == 'mon'):
                ind = np.where(obsmonths == res[0])
                temp_obs_trnd = trend_TLL(np.squeeze(temp_obs_reg[ind, :, :]),
                                          drop = False)
            elif (var_kind_2[0] == 'seas'):
                temp_obs_trnd = trend_TLL(temp_obs_seas_ts[res[0], :, :, :],
                                          drop = False)

    if (var_file_2[0] == 'clim'):
        var2_obs = temp_obs_clim
    elif (var_file_2[0] == 'std'):
        var2_obs = temp_obs_std
    elif (var_file_2[0] == 'trnd'):
        var2_obs = temp_obs_trnd
    # calculate area average for obs
    ma_var2_obs = np.ma.masked_array(var2_obs, np.isnan(var2_obs))
    tmp_latweight = np.ma.average(np.squeeze(ma_var2_obs), axis = 0,
                                  weights = w_lat)
    if (obsdata == 'MERRA2'):
        m2_obs_areaavg = np.nanmean(tmp_latweight.filled(np.nan))
    else:
        obs_areaavg = np.nanmean(tmp_latweight.filled(np.nan))

# dict to array for model data
target_hist = np.array([np.squeeze(value) for key, value in sorted(d_target_hist.iteritems())], dtype = float)
model_keys1 = [key for key, value in sorted(d_target_hist.iteritems())]
var2_hist = np.array([np.squeeze(value) for key, value in sorted(d_var2_hist.iteritems())], dtype = float)
model_keys2 = [key for key, value in sorted(d_var2_hist.iteritems())]
change_target = np.array([np.squeeze(value) for key, value in sorted(d_change_target.iteritems())], dtype = float)

## correlation area averages of all models
ma_var2_hist_areaavg = np.ma.masked_array(var2_hist_areaavg, np.isnan(var2_hist_areaavg))
ma_change_target_areaavg = np.ma.masked_array(change_target_areaavg,
                                              np.isnan(change_target_areaavg))
corr = np.ma.corrcoef(ma_var2_hist_areaavg, ma_change_target_areaavg)

print 'Mean correlation between delta %s %s %s and %s %s %s areaaverages is: %s' %(
                                                                  target_var,
                                                             var_file_target[0],
                                                             var_kind_target[0],
                                                                   variable2[0],
                                                                  var_file_2[0],
                                                                  var_kind_2[0],
                                                   round(np.nanmean(corr[0, 1]),
                                                                      3))
## correlation of all models
#calculate correlation coefficient
ind_miss1 = np.isfinite(var2_hist)
corrcoef = np.corrcoef(change_target[ind_miss1].ravel(),
                                         var2_hist[ind_miss1].ravel())[0,1]
print 'Correlation is: %s' %(corrcoef)
## calculate temporal correlation historical period
corr_hist = np.empty((len(lat), len(lon)))
for ilat in xrange(len(lat)):
    for ilon in xrange(len(lon)):
        if ((np.isnan(target_hist[:, ilat, ilon]).all()) or
            (np.sum(np.isfinite(var2_hist[:, ilat, ilon])) < 3)):
            corr_hist[ilat, ilon] = np.NaN
        else:
            spear = stats.spearmanr(target_hist[:, ilat, ilon],
                                    var2_hist[:, ilat, ilon], axis = 0,
                                    nan_policy = 'omit')
            corr_hist[ilat, ilon] = spear[0]
print 'Mean historical correlation between %s and %s is: %s' %(target_var,
                                                               variable2[0],
                                                    round(np.nanmean(corr_hist),
                                                          3))

## calculate ordinary least squares and total least squares
slope, intercept, r_val, p_val, std_err = stats.linregress(var2_hist_areaavg,
                                                           change_target_areaavg)

# pca and total least squares only if normalized or same variable
if ((target_var == variable2[0]) and (var_file_target[0] == var_file_2[0])) or normalize:
    pca_data = np.stack((var2_hist_areaavg[~np.isnan(var2_hist_areaavg)], change_target_areaavg[~np.isnan(change_target_areaavg)]), axis = 1)
    sklearn_pca = sklearnPCA(n_components = 2)
    sklearn_transf = sklearn_pca.fit(pca_data)
    means = sklearn_transf.mean_
    loadings = sklearn_transf.components_
    slope_pca = loadings[1, 0] / loadings[0, 0]
    intercept_pca = means[1] - slope_pca * means[0]

### Plotting
if ((target_var != variable2[0]) or (var_file_target[0] != var_file_2[0]) or
    (var_kind_target[0] != var_kind_2[0])):
    plotname = '%scorr_%s_%s_%s_vs_%s_%s_%s_%s-%s_%s_%s.pdf' %(outdir,
                                                               target_var,
                                                               var_kind_target[0],
                                                               var_file_target[0],
                                                               variable2[0],
                                                               var_kind_2[0],
                                                               var_file_2[0],
                                                               str(syear_hist),
                                                               str(eyear_hist),
                                                               region, grid)
    title = 'Correlation %s %s %s vs. %s %s %s' %(target_var,
                                                  var_kind_target[0],
                                                  var_file_target[0],
                                                  variable2[0],
                                                  var_kind_2[0], var_file_2[0])
    draw(corr_hist, lat, lon, title = title, levels = np.arange(-0.9, 1.1, 0.2),
         colors = 'RdBu_r', region = region)
    plt.savefig(plotname)

## plot scatter
abline_values = [slope * i + intercept for i in var2_hist_areaavg]
try:
    abline_values_pca = [slope_pca * i + intercept_pca for i in var2_hist_areaavg]
except (NameError):
    pass
plotname = '%sdelta_%s_%s_%s_vs_%s_%s_%s_%s_%s.pdf' %(outdir,
                                                      target_var,
                                                      var_kind_target[0],
                                                      var_file_target[0],
                                                      variable2[0],
                                                      var_kind_2[0],
                                                      var_file_2[0],
                                                      region, grid)

fig = plt.figure(figsize = (7, 6), dpi = 300)
ax = fig.add_subplot(111)

plt.axvline(x = obs_areaavg, color = 'darkgrey', linewidth = 2)
plt.axvline(x = m2_obs_areaavg, color = 'darkgrey', linewidth = 2)

plt.scatter(var2_hist_areaavg, change_target_areaavg)
if (abs(corr[0, 1]) >= 0.3):
    plt.plot(var2_hist_areaavg, abline_values, 'k')
    try:
        plt.plot(var2_hist_areaavg, abline_values_pca, color = 'grey')
    except (NameError):
        pass
xticks = plt.xticks()[0]
dx = xticks[1] - xticks[0]
yticks = plt.yticks()[0]
dy = yticks[1] - yticks[0]

plt.text(0.8, 0.95,'OLS R$^2$ = %s' %(round(r_val**2, 3)), ha='center',
         va='center', transform = ax.transAxes)
plt.text(0.8, 0.9,'Corr = %s' %(round(np.nanmean(corr[0, 1]), 3)), ha='center',
         va='center', transform = ax.transAxes)
plt.text(0.8, 0.85, 'Obs', color = 'darkgrey', ha='center',
         va='center', transform = ax.transAxes)
plt.title(longname_region + ' ' + res_name)
if normalize:
    plt.xlabel(variable2[0] + ' ' + var_file_2[0] + ' [-]')
    plt.ylabel('$\Delta$' + target_var + ' ' + var_file_target[0] + ' [-]')
else:
    plt.xlabel(variable2[0] + ' ' + var_file_2[0] + ' [' + unit_var2 + ']')
    plt.ylabel('$\Delta$' + target_var + ' ' + var_file_target[0] + ' [' + unit_target + ']')

plt.savefig(plotname)

#!/usr/bin/python
'''
File Name : plot_test_corr_hist_future.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 03-01-2017
Modified: Wed 01 Feb 2017 02:44:37 PM CET
Purpose: plot correlation between historical data and change in future
         for CMIP5 models, diagnostics calculated in calc_diag_cmip5_cdo.py 

'''
import netCDF4 as nc # to work with NetCDF files
import numpy as np
import os # operating system interface
from os.path import expanduser
home = expanduser("~") # Get users home directory
import sys
sys.path.insert(0, home + '/scripts/plot_scripts/utils/')
import glob
from scipy import stats
import math
import matplotlib.pyplot as plt
from draw_utils import draw
#from mpl_toolkits.basemap import maskoceans, shiftgrid
from sklearn.decomposition import PCA as sklearnPCA

###
# Define input
###
# target variable and explaining variables
target_var = 'hurs'
variable2 = ['hurs']
# climatology:clim, variability:std, trend:trnd
var_file_target = 'CLIM'
var_file_2 = ['CLIM']
# kind is cyc: annual cycle, ann: annual mean, seas: seasonal mean,
# or mon: monthly values 
var_kind_target = 'seas'
var_kind_2 = ['seas']
# choose data for particular month or season (0:DJF, 1:MAM, 2:JJA, 3:SON)?
res = [2]
res_name_target = 'JJA'
res_name = ['JJA']
freq = 'mon'

# choose region if required, otherwise longname = 'GLOBAL'
region = 'CNEU'
longname_region = 'Central Europe'
# mask ocean? if True maskocean function returns masked array! -> 
masko = 'maskF' # target
masko_v = 'maskF' #variable2

archive = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/'
outdir = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/emerg_const/%s/' %(
    target_var)
if (os.access(outdir, os.F_OK) == False):
    os.makedirs(outdir)

syear_hist = 1980
eyear_hist = 2014
syear_fut = 2065
eyear_fut = 2099

experiment = 'rcp85'
grid = 'g025'
# normalize data? if variables on different scale we need to normalize
# for total least squares regression
normalize = False

###
# Read data
###
###read model data
print "Read model data"
##find all matching files in archive, loop over all of them
#first count matching files in folder
models_t = list()
model_names = list()
name = '%s/%s/%s/%s/%s_%s_*_%s_r?i?p?_%s-%s_%sMEAN_%s_%s.nc' %(
    archive, target_var, freq, masko, target_var, freq, experiment,
    syear_fut,eyear_fut, res_name_target, var_file_target, region)
nfiles_targ = len(glob.glob(name))
print str(nfiles_targ) + ' matching files found for target variable'
for filename in glob.glob(name):
        models_t.append(filename.split('_')[4] + ' ' + filename.split('_')[6])
overlap = models_t
for v in xrange(len(variable2)):
        models_v = list()
        name_v = '%s/%s/%s/%s/%s_%s_*_%s_r?i?p?_%s-%s_%sMEAN_%s_%s.nc' %(
            archive, variable2[v], freq, masko_v, variable2[v],freq, experiment,
            syear_hist, eyear_hist, res_name[v], var_file_2[v], region)
        for filename in glob.glob(name_v):
            models_v.append(filename.split('_')[4] + ' ' +
                            filename.split('_')[6])
        #print models_v
        #find overlapping files for all variables
        overlap = list(set(models_v) & set(overlap))
        del models_v
nfiles = len(overlap)
print str(nfiles) + ' matching files found for all variable'
change_target_areaavg = np.empty((nfiles))
target_hist_areaavg = np.empty((nfiles))
d_change_target = dict()
d_target_hist = dict()
f = 0
for filename in glob.glob(name):
    model_name = filename.split('_')[4] + ' ' + filename.split('_')[6]
    if model_name in overlap:
            #print "Read "+filename+" data"
            fh = nc.Dataset(filename, mode = 'r')
            target_fut = fh.variables[target_var][:] # global data, time, lat, lon
            lat = fh.variables['lat'][:]
            lon = fh.variables['lon'][:]
            tmp = fh.variables[target_var]
            unit_target = tmp.units
            try:
                Fill = tmp._FillValue
            except AttributeError:
                Fill = 1e+20
            fh.close()

            filename_hist = '%s/%s/%s/%s/%s_%s_%s_%s_%s_%s-%s_%sMEAN_%s_%s.nc' %(
                archive, target_var, freq, masko, target_var, freq,
                filename.split('_')[4], experiment, filename.split('_')[6],
                syear_hist, eyear_hist, res_name[v], var_file_target, region)

            fh = nc.Dataset(filename_hist, mode = 'r')
            target_hist = fh.variables[target_var][:] # data: time, lat, lon
            fh.close()

            if (target_var == 'sic') and  (model == "EC-EARTH"):
                with np.errstate(invalid = 'ignore'):
                    target_hist[target_hist < 0.0] = np.NaN
                    target_fut[target_fut < 0.0] = np.NaN

            change_target = target_fut - target_hist

            #check that time axis and grid is identical for model0 and modelX
            if f != 0:
                if change_target0.shape != change_target.shape:
                    print 'Warning: Dimension for model0 and modelX is different!'
                    continue
            else:
                change_target0 = change_target[:]

            model = filename.split('_')[4]
            if model == 'ACCESS1.3':
                    model = 'ACCESS1-3'
            elif model == 'FGOALS_g2':
                    model = 'FGOALS-g2'
            ens = filename.split('_')[6]
            model_names.append(model + '_' + ens)

            if isinstance(change_target, np.ma.core.MaskedArray):
                    #print type(change_target), change_target.shape
                    change_target = change_target.filled(np.NaN)
            d_change_target[model + '_' + ens] = change_target[:]
            d_target_hist[model + '_' + ens] = target_hist[:]

            w_lat = np.cos(lat * (4.0 * math.atan(1.0) / 180)) 
            ma_change_target = np.ma.masked_array(change_target,
                                                  np.isnan(change_target))
            tmp1_latweight = np.ma.average(np.squeeze(ma_change_target),
                                           axis = 0,
                                           weights = w_lat)
            change_target_areaavg[f] = np.nanmean(tmp1_latweight.filled(np.nan))
            ma_target_hist = np.ma.masked_array(target_hist,
                                                np.isnan(target_hist))
            tmp2_latweight = np.ma.average(np.squeeze(ma_target_hist),
                                           axis = 0, weights = w_lat)
            target_hist_areaavg[f] = np.nanmean(tmp2_latweight.filled(np.nan))
 
            f = f + 1
    else:
            continue

var2_hist_areaavg = np.empty((nfiles), float, Fill)
data_unit = list()
d_var2_hist = dict()
for v in xrange(len(variable2)):
    name = '%s/%s/%s/%s/%s_%s_*_%s_r?i?p?_%s-%s_%sMEAN_%s_%s.nc' %(
        archive, variable2[0], freq, masko_v, variable2[0], freq, experiment,
        syear_hist, eyear_hist, res_name[v], var_file_2[0], region)
    f = 0
    for filename in glob.glob(name):
        model_name = filename.split('_')[4] + ' ' + filename.split('_')[6]
        if model_name in overlap:
            #print "Read " + filename + " data"
            fh = nc.Dataset(filename, mode = 'r')
            lon = fh.variables['lon'][:]
            lat = fh.variables['lat'][:]
            var2_hist = fh.variables[variable2[v]][:] # data, time, lat, lon
            #check that time axis and grid is identical for model0 and modelX
            if f != 0:
                if var2_hist0.shape != var2_hist.shape:
                    print 'Warning: Dimension for model0 and modelX is different!'
                    continue
            else:
                var2_hist0 = var2_hist[:]
                tmp = fh.variables[variable2[v]]
                unit_var2 = tmp.units
            fh.close()
            model = filename.split('_')[4]
            if model == 'ACCESS1.3':
                model = 'ACCESS1-3'
            elif model == 'FGOALS_g2':
                model = 'FGOALS-g2'
            ens = filename.split('_')[6]
            model_names.append(model+'_'+ens)

            if isinstance(var2_hist, np.ma.core.MaskedArray):
                #print type(var2_hist), var2_hist.shape
                var2_hist = var2_hist.filled(np.NaN)
                #print type(temp_mod), temp_mod.shape

            if (variable2[v] == 'sic') and  (model == "EC-EARTH"):
                with np.errstate(invalid = 'ignore'):
                    var2_hist[var2_hist < 0.0] = np.NaN
            d_var2_hist[model + '_' + ens] = var2_hist[:]

            ma_var2_hist = np.ma.masked_array(var2_hist,
                                              np.isnan(var2_hist))
            tmp2_latweight = np.ma.average(np.squeeze(ma_var2_hist), axis = 0,
                                           weights = w_lat)
            var2_hist_areaavg[f] = np.nanmean(tmp2_latweight.filled(np.nan))

            f = f + 1
        else:
            continue

# Read obs data
for obsdata in ['MERRA2', 'Obs', 'ERAint']:
    print "Read %s data" %(obsdata)
    if ((obsdata == 'Obs') and
        ((variable2[v] == 'rlus') or (variable2[v] == 'rsds') or
         (variable2[v] == 'clt'))):
        name = '%s/%s/%s/%s/%s_%s_%s_2000-%s_%sMEAN_%s_%s.nc' %(archive,
            variable2[v], freq, masko_v, variable2[v], freq, obsdata,
            eyear_hist, res_name[v], var_file_2[v], region)
    elif ((obsdata == 'Obs') and
          ((variable2[v] == 'psl') or (variable2[v] == 'huss'))):
        obs_areaavg = np.nan
        continue
    else:
        name = '%s/%s/%s/%s/%s_%s_%s_%s-%s_%sMEAN_%s_%s.nc' %(archive,
            variable2[v], freq, masko_v, variable2[v], freq, obsdata, syear_hist,
            eyear_hist, res_name[v], var_file_2[v], region)

    fh = nc.Dataset(name, mode = 'r')
    var2_obs = fh.variables[variable2[v]][:] # global data, time, lat, lon
    if isinstance(var2_obs, np.ma.core.MaskedArray):
            var2_obs = var2_obs.filled(np.NaN)

    # calculate area average for obs
    ma_var2_obs = np.ma.masked_array(var2_obs, np.isnan(var2_obs))
    tmp_latweight = np.ma.average(np.squeeze(ma_var2_obs), axis = 0,
                                  weights = w_lat)
    if (obsdata == 'MERRA2'):
        m2_obs_areaavg = np.nanmean(tmp_latweight.filled(np.nan))
    if (obsdata == 'ERAint'):
        era_obs_areaavg = np.nanmean(tmp_latweight.filled(np.nan))
    else:
        obs_areaavg = np.nanmean(tmp_latweight.filled(np.nan))

# dict to array for model data
target_hist = np.array([np.squeeze(value) for key, value in sorted(d_target_hist.iteritems())], dtype = float)
model_keys1 = [key for key, value in sorted(d_target_hist.iteritems())]
var2_hist = np.array([np.squeeze(value) for key, value in sorted(d_var2_hist.iteritems())], dtype = float)
model_keys2 = [key for key, value in sorted(d_var2_hist.iteritems())]
change_target = np.array([np.squeeze(value) for key, value in sorted(d_change_target.iteritems())], dtype = float)

## correlation area averages of all models
ma_var2_hist_areaavg = np.ma.masked_array(var2_hist_areaavg,
                                          np.isnan(var2_hist_areaavg))
ma_target_hist_areaavg = np.ma.masked_array(target_hist_areaavg,
                                            np.isnan(target_hist_areaavg))
ma_change_target_areaavg = np.ma.masked_array(change_target_areaavg,
                                              np.isnan(change_target_areaavg))
corr = np.ma.corrcoef(ma_var2_hist_areaavg, ma_change_target_areaavg)
corr_hist = np.ma.corrcoef(ma_var2_hist_areaavg, ma_target_hist_areaavg)

print 'Correlation between delta %s %s %s and %s %s %s areaaverages is: %s' %(
    target_var, var_file_target, res_name_target, variable2[0], var_file_2[0],
    res_name[0], round(corr[0, 1], 3))
print 'Correlation between historical %s %s %s and %s %s %s areaaverages is: %s' %(
    target_var, var_file_target, res_name_target, variable2[0], var_file_2[0],
    res_name[0], round(corr_hist[0, 1], 3))

## correlation of all models
#calculate correlation coefficient
ind_miss1 = np.isfinite(var2_hist)
ind_miss2 = np.isfinite(target_hist)
#corrcoef = np.corrcoef(change_target[ind_miss1].ravel(),
#                       var2_hist[ind_miss1].ravel())[0,1]
#print 'Correlation is: %s' %(corrcoef)
#corrcoef_hist = np.corrcoef(target_hist[ind_miss2].ravel(),
#                            var2_hist[ind_miss1].ravel())[0,1]
#print 'Historical Correlation is: %s' %(corrcoef_hist)

## correlation at each grid point
corr2d = np.empty((len(lat), len(lon)))
for ilat in xrange(len(lat)):
    for ilon in xrange(len(lon)):
        if ((np.isnan(change_target[:, ilat, ilon]).all()) or
            (np.sum(np.isfinite(var2_hist[:, ilat, ilon])) < 3)):
            corr2d[ilat, ilon] = np.NaN
        else:
            spear = stats.spearmanr(change_target[:, ilat, ilon],
                                    var2_hist[:, ilat, ilon], axis = 0,
                                    nan_policy = 'omit')
            corr2d[ilat, ilon] = spear[0]
print 'Mean spatial correlation between delta %s and %s is: %s' %(
    target_var, variable2[0], round(np.nanmean(corr2d), 3))

## calculate ordinary least squares and total least squares
slope, intercept, r_val, p_val, std_err = stats.linregress(var2_hist_areaavg,
                                                           change_target_areaavg)

# pca and total least squares only if normalized or same variable
if (((target_var == variable2[0]) and (var_file_target == var_file_2[0]))
    or normalize):
    pca_data = np.stack((var2_hist_areaavg[~np.isnan(var2_hist_areaavg)],
                         change_target_areaavg[~np.isnan(change_target_areaavg)]),
                        axis = 1)
    sklearn_pca = sklearnPCA(n_components = 2)
    sklearn_transf = sklearn_pca.fit(pca_data)
    means = sklearn_transf.mean_
    loadings = sklearn_transf.components_
    slope_pca = loadings[1, 0] / loadings[0, 0]
    intercept_pca = means[1] - slope_pca * means[0]

### Plotting
## plot scatter
abline_values = [slope * i + intercept for i in var2_hist_areaavg]
try:
    abline_values_pca = [slope_pca * i + intercept_pca for i in var2_hist_areaavg]
except (NameError):
    pass
plotname = '%sdelta_%s_%s_%s_vs_%s_%s_%s_%s_%s.pdf' %(
    outdir, target_var, res_name_target, var_file_target, variable2[0],
    res_name[v], var_file_2[0], region, grid)

fig = plt.figure(figsize = (7, 6), dpi = 300)
ax = fig.add_subplot(111)

plt.axvline(x = obs_areaavg, color = 'darkgrey', linewidth = 2)
plt.axvline(x = m2_obs_areaavg, color = 'darkgrey', linewidth = 2)
plt.axvline(x = era_obs_areaavg, color = 'darkgrey', linewidth = 2)

plt.scatter(var2_hist_areaavg, change_target_areaavg)
if (abs(corr[0, 1]) >= 0.3):
    plt.plot(var2_hist_areaavg, abline_values, 'k')
    try:
        plt.plot(var2_hist_areaavg, abline_values_pca, color = 'grey')
    except (NameError):
        pass
xticks = plt.xticks()[0]
dx = xticks[1] - xticks[0]
yticks = plt.yticks()[0]
dy = yticks[1] - yticks[0]

plt.text(0.8, 0.95,'OLS R$^2$ = %s' %(round(r_val ** 2, 3)), ha = 'center',
         va = 'center', transform = ax.transAxes)
plt.text(0.8, 0.9,'Corr = %s' %(round(np.nanmean(corr[0, 1]), 3)),
          ha = 'center', va = 'center', transform = ax.transAxes)
plt.text(0.8, 0.85, 'Obs', color = 'darkgrey', ha = 'center',
         va = 'center', transform = ax.transAxes)
plt.title(longname_region + ' ' + res_name[v])
if normalize:
    plt.xlabel(variable2[0] + ' ' + var_file_2[0] + ' [-]')
    plt.ylabel('$\Delta$' + target_var + ' ' + var_file_target + ' [-]')
else:
    plt.xlabel(variable2[0] + ' ' + var_file_2[0] + ' [' + unit_var2 + ']')
    plt.ylabel('$\Delta$' + target_var + ' ' + var_file_target + ' [' +
               unit_target + ']')

plt.savefig(plotname)

# plot map
plotname = '%scorr_change_%s_%s_%s_vs_%s_%s_%s_%s_%s.pdf' %(
    outdir, target_var, res_name_target, var_file_target, variable2[0],
    res_name[v], var_file_2[0], region, grid)
title = 'Correlation change %s %s %s vs. %s %s %s' %(
    target_var, res_name_target, var_file_target, variable2[0],
    res_name[v], var_file_2[0])
draw(corr2d, lat, lon, title = title, levels = np.arange(-0.9, 1.1, 0.2),
     colors = 'RdBu_r', region = region)
plt.savefig(plotname)

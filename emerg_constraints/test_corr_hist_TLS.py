#!/usr/bin/python
'''
File Name : test_corr_hist_future.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 12-10-2016
Modified: Fri 09 Dec 2016 15:55:34 CET
Purpose: plot correlation of historical to future change
	to test if emergent constraint

'''

import netCDF4 as nc # to work with NetCDF files
import numpy as np
import os # operating system interface
from os.path import expanduser
home = expanduser("~") # Get users home directory
workdir = os.getcwd()
import glob
import sys
sys.path.insert(0, home + '/scripts/plot_scripts/utils/')
sys.path.insert(0, workdir + '/functions/')
from func_read_data import func_read_netcdf
from clim_seas_TLL import clim_mon_TLL, std_mon_TLL, seas_avg_mon_TLL, seas_std_mon_TLL, clim_seas_mon_TLL, ann_avg_mon_TLL
from calc_trend_TLL import trend_TLL
from scipy import signal, stats
from point_inside_polygon import point_inside_polygon
import math
import matplotlib.pyplot as plt
from draw_utils import draw
from mpl_toolkits.basemap import maskoceans, shiftgrid
from sklearn.decomposition import PCA as sklearnPCA
###
# Define input
###
experiment = 'rcp85'
archive = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/'
target_var = 'tasmax'
target_file = 'std'
target_kind = 'seas'
diag_var = ['tasmax','tasmax','hfls','hfls', 'hfls','rlus', 'rlus','rlus','rsds','rsds','rsds', 'psl', 'psl','psl','pr','pr','pr', 'huss','huss','huss']
var_file = ['clim','trnd', 'clim','std','trnd','clim', 'std', 'trnd','clim', 'std', 'trnd','clim', 'std', 'trnd','clim', 'std', 'trnd','clim', 'std', 'trnd']
var_kind = ['seas','seas','seas','seas','seas','seas', 'seas', 'seas','seas','seas','seas','seas','seas', 'seas', 'seas','seas','seas', 'seas','seas','seas']
nvar = len(diag_var)

res_name = 'JJA'
region = 'CNEU'           #cut data over region?
longname_region = 'Central Europe'

syear = 1980
eyear = 2014
nyears = eyear - syear + 1
grid = 'g025'

outdir = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/emerg_const/%s/' %(
    target_var)
if (os.access(outdir, os.F_OK) == False):
    os.makedirs(outdir)

# normalize data? if variables on different scale we need to normalize
# for total least squares regression
normalize = False

###
# Read data
###
##find all matching files in archive, loop over all of them
#first count matching files in folder
models_t = list()
model_names = list()
name = '%s/%s/%s_%s_%s_*_r?i?p?_%s_%s_%s-%s.nc' %(archive,target_var,target_var,target_file,target_kind,region,experiment,syear,eyear)
nfiles_targ = len(glob.glob(name))
print str(nfiles_targ)+ ' matching files found for target variable'
for filename in glob.glob(name):
        models_t.append(filename.split('_')[5]+' '+filename.split('_')[6])
overlap = models_t
for v in xrange(len(diag_var)):
        models_v = list()
        name_v = '%s/%s/%s_%s_%s_*_r?i?p?_%s_%s_%s-%s.nc' %(archive, diag_var[v], diag_var[v], var_file[v], var_kind[v], region, experiment, syear, eyear)
        for filename in glob.glob(name_v):
                models_v.append(filename.split('_')[5] + ' ' + filename.split('_')[6])
        #print models_v
        #find overlapping files for all variables
        overlap = list(set(models_v) & set(overlap))
        del models_v
nfiles = len(overlap)
print str(nfiles) + ' matching files found for all variables'
target_areaavg = np.empty((nfiles))
f = 0
for filename in glob.glob(name):
    model_name = filename.split('_')[5] + ' ' + filename.split('_')[6]
    if model_name in overlap:
            #print "Read "+filename+" data"
            fh = nc.Dataset(filename, mode = 'r')
            temp_mod = fh.variables[target_var][:] # global data, time, lat, lon
            lat = fh.variables['lat'][:]
            lon = fh.variables['lon'][:]
            tmp = fh.variables[target_var]
            target_unit = tmp.units
            Fill = tmp._FillValue
            fh.close()
            #check that time axis and grid is identical for model0 and modelX
            if f != 0:
                if temp_mod0.shape != temp_mod.shape:
                    print 'Warning: Dimension for model0 and modelX is different!'
                    continue
            else:
                temp_mod0 = temp_mod[:]

            model = filename.split('_')[5]
            if model == 'ACCESS1.3':
                    model = 'ACCESS1-3'
            elif model == 'FGOALS_g2':
                    model = 'FGOALS-g2'
            ens = filename.split('_')[6]
            model_names.append(model+'_'+ens)

            if isinstance(temp_mod, np.ma.core.MaskedArray):
                    #print type(temp_mod), temp_mod.shape
                    temp_mod = temp_mod.filled(np.NaN)

            if (target_var == 'sic') and  (model=="EC-EARTH"):
                    with np.errstate(invalid='ignore'):
                            temp_mod[temp_mod < 0.0] = np.NaN
            if normalize:
                tmp1 = temp_mod[:]
                tmp_mean = np.nanmean(tmp1, axis = 0)
                tmp_std = np.nanstd(tmp1, axis = 0)
                tmp_norm = (tmp1 - tmp_mean) / tmp_std
                temp_mod = tmp_norm[:]

            if f == 0:
	      ma_target = np.empty((nfiles, len(lat), len(lon)))
            # average over area and save value for each model
            ## weight for latitude differences in area
            w_lat = np.cos(lat * (4.0 * math.atan(1.0) / 180))
            ma_target[f, :, :] = np.ma.masked_array(temp_mod, np.isnan(temp_mod))
            tmp1_latweight = np.ma.average(np.squeeze(ma_target[f, :, :]), axis = 0,
                                                   weights = w_lat)
            target_areaavg[f] = np.ma.mean(tmp1_latweight)
            f = f + 1
    else:
            continue
ma_data = np.empty((nfiles))
data_areaavg = np.empty((nfiles, nvar), float, Fill)
data_unit = list()
for v in xrange(len(diag_var)):
    name = '%s/%s/%s_%s_%s_*_r?i?p?_%s_%s_%s-%s.nc' %(archive,diag_var[v],diag_var[v],var_file[v],var_kind[v],region,experiment,syear,eyear)
    f = 0
    for filename in glob.glob(name):
            model_name = filename.split('_')[5]+' '+filename.split('_')[6]
            if model_name in overlap:
                    #print "Read "+filename+" data"
                    fh = nc.Dataset(filename,mode='r')
                    lon = fh.variables['lon'][:]
                    lat = fh.variables['lat'][:]
                    #tmp = fh.variables[diag_var[v]]
                    #var_unit = tmp.unit
                    temp_mod = fh.variables[diag_var[v]][:] # global data, time, lat, lon
                    #check that time axis and grid is identical for model0 and modelX
                    if f != 0:
                        if temp_mod0.shape != temp_mod.shape:
                            print 'Warning: Dimension for model0 and modelX is different!'
                            continue
                    else:
                        temp_mod0 = temp_mod[:]
                        tmp = fh.variables[diag_var[v]]
                        data_unit.append(tmp.units)
                    fh.close()
                    model = filename.split('_')[5]
                    if model == 'ACCESS1.3':
                            model = 'ACCESS1-3'
                    elif model == 'FGOALS_g2':
                            model = 'FGOALS-g2'
                    ens = filename.split('_')[6]
                    model_names.append(model+'_'+ens)

                    if isinstance(temp_mod, np.ma.core.MaskedArray):
                            print type(temp_mod), temp_mod.shape
                            temp_mod = temp_mod.filled(np.NaN)

                    if (diag_var[v] == 'sic') and  (model=="EC-EARTH"):
                            with np.errstate(invalid='ignore'):
                                    temp_mod[temp_mod < 0.0] = np.NaN
                    if normalize:
                        tmp1 = temp_mod[:]
                        tmp_mean = np.nanmean(tmp1, axis = 0)
                        tmp_std = np.nanstd(tmp1, axis = 0)
                        tmp_norm = (tmp1 - tmp_mean) / tmp_std
                        temp_mod = tmp_norm[:]

                    if f == 0:
	              ma_data = np.empty((nfiles, len(lat), len(lon)))

                    # average over area and save value for each model
		## weight for latitude differences in area
                    ma_data[f, :, :] = np.ma.masked_array(temp_mod, np.isnan(temp_mod))
                    tmp2_latweight = np.ma.average(np.squeeze(ma_data[f, :, :]), axis = 0,
                                                   weights = w_lat)
                    data_areaavg[f, v] = np.ma.mean(tmp2_latweight)
                    f = f + 1
            else:
                    continue

    ## correlation area averages of all models
    corr = np.ma.corrcoef(data_areaavg[:, v], target_areaavg)

    print 'Mean correlation between %s %s %s and %s %s %s areaaverages is: %s' %(
                                                                  target_var,
                                                             target_file,
                                                             target_kind,
                                                                   diag_var[v],
                                                                  var_file[v],
                                                                  var_kind[v],
                                                   round(np.nanmean(corr[0, 1]),
                                                                      3))
    ## correlation of all models
    #calculate correlation coefficient
    ind_miss1 = np.isfinite(ma_data)
    corrcoef = np.corrcoef(ma_target[ind_miss1].ravel(),
                                         ma_data[ind_miss1].ravel())[0, 1]
    print 'Correlation is: %s' %(corrcoef)

    ## calculate temporal correlation historical period
    if isinstance(ma_target, np.ma.core.MaskedArray):
        target_hist = ma_target.filled(np.NaN)
    else:
        target_hist = ma_target
    if isinstance(ma_data, np.ma.core.MaskedArray):
        data_hist = ma_data.filled(np.NaN)
    else:
        data_hist = ma_data
    corr_hist = np.empty((len(lat), len(lon)))
    for ilat in xrange(len(lat)):
        for ilon in xrange(len(lon)):
            if ((np.isnan(target_hist[:, ilat, ilon]).all()) or
                (np.sum(np.isfinite(data_hist[:, ilat, ilon])) < 3)):
                corr_hist[ilat, ilon] = np.NaN
            else:
                spear = stats.spearmanr(target_hist[:, ilat, ilon],
                                        data_hist[:, ilat, ilon], axis = 0,
                                        nan_policy = 'omit')
                corr_hist[ilat, ilon] = spear[0]
    print 'Mean spearmanr correlation between %s and %s is: %s' %(target_var,
                                                               diag_var[v],
                                                    round(np.nanmean(corr_hist),
                                                          3))

    ## calculate ordinary least squares and total least squares
    slope, intercept, r_val, p_val, std_err = stats.linregress(data_areaavg[:, v],
                                                           target_areaavg)

    # pca and total least squares only if normalized or same variable
    if ((target_var == diag_var[v]) and (target_file == var_file[v])) or normalize:
        pca_data = np.stack((sorted(data_areaavg[:, v]), sorted(target_areaavg)), axis = 1)
        sklearn_pca = sklearnPCA(n_components = 2)
        sklearn_transf = sklearn_pca.fit(pca_data)
        means = sklearn_transf.mean_
        loadings = sklearn_transf.components_
        slope_pca = loadings[1, 0] / loadings[0, 0]
        intercept_pca = means[1] - slope_pca * means[0]

    ## plot scatter
    abline_values = [slope * i + intercept for i in data_areaavg[:, v]]
    try:
        abline_values_pca = [slope_pca * i + intercept_pca for i in data_areaavg[:, v]]
    except (NameError):
        pass
    plotname = '%sscatter_%s_%s_%s_vs_%s_%s_%s_%s_%s_TLS.pdf' %(outdir,
                                                      target_var,
                                                      target_kind,
                                                      target_file,
                                                      diag_var[v],
                                                      var_kind[v],
                                                      var_file[v],
                                                      region, grid)

    fig = plt.figure(figsize = (7, 6), dpi = 300)
    ax = fig.add_subplot(111)
    plt.scatter(data_areaavg[:, v], target_areaavg)
    if (abs(corr[0, 1]) >= 0.3):
        plt.plot(data_areaavg[:, v], abline_values, 'k')
        try:
            plt.plot(data_areaavg[:, v], abline_values_pca, color = 'grey')
        except (NameError):
            pass

    xticks = plt.xticks()[0]
    dx = xticks[1] - xticks[0]
    yticks = plt.yticks()[0]
    dy = yticks[1] - yticks[0]
    plt.text(0.8, 0.95,'OLS R$^2$ = %s' %(round(r_val ** 2, 3)), ha = 'center',
             va ='center', transform = ax.transAxes)
    plt.text(0.8, 0.9,'Corr = %s' %(round(np.nanmean(corr[0, 1]), 3)), ha = 'center',
             va ='center', transform = ax.transAxes)
    plt.title(longname_region + ' ' + res_name)
    if normalize:
        plt.xlabel(diag_var[v] + ' ' + var_file[v] + ' [-]')
        plt.ylabel(target_var + ' ' + target_file + ' [-]')
    else:
        plt.xlabel(diag_var[v] + ' ' + var_file[v] + ' [ ]')
        plt.ylabel(target_var + ' ' + target_file + ' [' + target_unit + ']')
    plt.savefig(plotname)

#!/usr/bin/python
'''
File Name : em_const_lin_reg_hist_to_fut_CMIP5_cdo.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 13-11-2017
Modified:
Purpose: find emergent constraints, pick models within observational range
         plot scatter with linear regressions and observations for each
         diagnostic and constrained by all diagnostic, grey -> not included
'''
import numpy as np
import netCDF4 as nc
from netcdftime import utime
import datetime as dt
import glob as glob
from scipy import stats
import statsmodels.api as sm
from sklearn import linear_model
from sklearn.cross_validation import cross_val_score, train_test_split
import math
import os
from os.path import expanduser
home = expanduser("~") # Get users home directory
import sys
sys.path.insert(0, home + '/scripts/plot_scripts/utils/')
from func_read_data import func_read_netcdf
from func_write_netcdf import func_write_netcdf
import matplotlib
import matplotlib.pyplot as plt

###
# Define input & output
###
experiment = 'rcp85'
archive = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/'

target_var = 'TXx'
target_file = 'CLIM'
res_name_target = 'ANN'
res_time_target = 'MAX'
target_mask = 'maskT'
freq = 'ann'

diag_var = ['TXx', 'TXx', 'pr']   #, 'tas']
var_file = ['TREND', 'CLIM', 'TREND'] #, 'CLIM']
res_name = ['ANN', 'ANN', 'JJA']   #, 'JJA']
res_time = ['MAX', 'MAX', 'MEAN']
freq_v = ['ann', 'ann', 'mon']
masko = ['maskT', 'maskT', 'maskT']    #, 'maskT']
nvar = len(diag_var)

region = 'CNEU'          #cut data over region?
longname_region = 'Central Europe' # for title plot
obsdata = ['MERRA2'] #'Obs', 'MERRA2', 
nobs = len(obsdata)

path = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/'
outdir = '%semerg_const/%s/%s/' %(path, target_var, region)

syear_hist = 1980
eyear_hist = 2014
syear_fut = 2065
eyear_fut = 2099
syear = 1951
eyear = 2100

nyears = eyear_hist - syear_hist + 1
ntim_tot = eyear - syear + 1

grid = 'g025'
Fill = 1e+20

ols = linear_model.LinearRegression(normalize = True) 
Labelols = "OLS"
tsr = linear_model.TheilSenRegressor(random_state = 50, copy_X = True)
Labeltsr = "Theil-Sen"
ridgeCV = linear_model.RidgeCV(normalize = True)
Labelridge = "RidgeCV"

if (os.access(outdir, os.F_OK) == False):
        os.makedirs(outdir)

###read model data
print "Read model data"
## find all matching files in archive, loop over all of them
# first count matching files in folder
models_t = list()
model_names = list()

path_target = '%s/%s/%s/%s/' %(archive, target_var, freq, target_mask)
if region:
    name = '%s%s_%s_*_%s_r?i?p?_%s-%s_%s%s_%s_%s.nc' %(
        path_target, target_var, freq, experiment, syear_fut, eyear_fut,
        res_name_target, res_time_target, target_file, region)
else:
    name = '%s/%s_%s_*_%s_r?i?p?_%s-%s_%s%s_%s.nc' %(
        path_target, target_var, freq, experiment, syear_fut, eyear_fut,
        res_name_target, res_time_target, target_file)

nfiles_targ = len(glob.glob(name))
print str(nfiles_targ) + ' matching files found for target variable'

for filename in glob.glob(name):
    models_t.append(filename.split('_')[4] + ' ' + filename.split('_')[6])

overlap = models_t

for v in xrange(len(diag_var)):
    models_v = list()
    path_var = '%s/%s/%s/%s/' %(archive, diag_var[v], freq_v[v], masko[v])
    if region:
        name_v = '%s%s_%s_*_%s_r?i?p?_%s-%s_%s%s_%s_%s.nc' %(
            path_var, diag_var[v], freq_v[v], experiment, syear_hist,
            eyear_hist, res_name[v], res_time[v], var_file[v], region)
    else:
        name_v = '%s%s_%s_*_%s_r?i?p?_%s-%s_%s%s_%s.nc' %(
            path_var, diag_var[v], freq_v[v], experiment, syear_hist, 
            eyear_hist, res_name[v], res_time[v], var_file[v])

    for filename in glob.glob(name_v):
        models_v.append(filename.split('_')[4] + ' ' +
                        filename.split('_')[6])
    #find overlapping files for all variables
    overlap = list(set(models_v) & set(overlap))
    del models_v, path_var
nfiles = len(overlap)

print str(nfiles) + ' matching files found for all variables'

data = np.empty((nfiles, nvar), float, Fill)
m2_obs_areaavg = np.empty((nvar), float, Fill)
m2_obs_areaavg[:] = np.nan
era_obs_areaavg = np.empty((nvar), float, Fill)
era_obs_areaavg[:] = np.nan
obs_areaavg = np.empty((nvar), float, Fill)
obs_areaavg[:] = np.nan
data_unit = list()
for v in xrange(len(diag_var)):
    # Read obs data
    for o in obsdata:
        print "Read %s data" %(o)
        if ((o == 'Obs') and
            ((diag_var[v] == 'rlus') or (diag_var[v] == 'rsds'))):
            name = '%s/%s/%s/%s/%s_%s_%s_2000-%s_%s%s_%s_%s.nc' %(
                archive, diag_var[v], freq_v[v], masko[v], diag_var[v],
                freq_v[v], o, eyear_hist, res_name[v], res_time[v],
                var_file[v], region)
        else:
            name = '%s/%s/%s/%s/%s_%s_%s_%s-%s_%s%s_%s_%s.nc' %(
                archive, diag_var[v], freq_v[v], masko[v], diag_var[v],
                freq_v[v], o, syear_hist, eyear_hist, res_name[v],
                res_time[v], var_file[v], region)

        fh = nc.Dataset(name, mode = 'r')
        var2_obs = fh.variables[diag_var[v]][:] # global data, time, lat, lon
        if isinstance(var2_obs, np.ma.core.MaskedArray):
            if o == 'Obs':
                maskmiss = var2_obs.mask.copy()
                ma_var2_obs = var2_obs
            else:
                try:
                    ma_var2_obs = np.ma.array(var2_obs, mask = maskmiss)
                except (NameError):
                    ma_var2_obs = var2_obs
        else:
            ma_temp_obs = np.ma.masked_array(var2_obs, np.isnan(var2_obs))
            try:
                ma_var2_obs = np.ma.masked_array(ma_temp_obs, mask = maskmiss)
            except (NameError):
                ma_var2_obs = ma_temp_obs
        lat = fh.variables['lat'][:]
        lon = fh.variables['lon'][:]
        fh.close()
        # calculate area average for obs
        w_lat = np.cos(lat * (4.0 * math.atan(1.0) / 180))
        tmp_latweight = np.ma.average(np.squeeze(ma_var2_obs), axis = 0,
                                      weights = w_lat)
        if (o == 'MERRA2'):
            m2_obs_areaavg[v] = np.nanmean(tmp_latweight.filled(np.nan))
        elif (o == 'ERAint'):
            era_obs_areaavg[v] = np.nanmean(tmp_latweight.filled(np.nan))
        else:
            obs_areaavg[v] = np.nanmean(tmp_latweight.filled(np.nan))

    path_var = '%s/%s/%s/%s/' %(archive, diag_var[v], freq_v[v], masko[v])
    if region:
        name = '%s%s_%s_*_%s_r?i?p?_%s-%s_%s%s_%s_%s.nc' %(
            path_var, diag_var[v], freq_v[v], experiment, syear_hist,
            eyear_hist, res_name[v], res_time[v], var_file[v], region)
    else:
        name = '%s%s_%s_*_%s_r?i?p?_%s-%s_%s%s_%s.nc' %(
            path_var, diag_var[v], freq_v[v], experiment, syear_hist,
            eyear_hist, res_name[v], res_time[v], var_file[v]) 
    f = 0
    for filename in glob.glob(name):
        model_name = filename.split('_')[4] + ' ' + filename.split('_')[6]
        if model_name in overlap:
            #print "Read " + filename + " data"
            fh = nc.Dataset(filename, mode = 'r')
            lon = fh.variables['lon'][:]
            lat = fh.variables['lat'][:]
            temp_mod = fh.variables[diag_var[v]][:] # global data
            #check that time axis and grid is identical for models
            if f != 0:
                if temp_mod0.shape != temp_mod.shape:
                    print('Warning: Dimension for model0 and modelX ' +
                          'is different!')
                    continue
            else:
                temp_mod0 = temp_mod[:]
                tmp = fh.variables[diag_var[v]]
                data_unit.append(tmp.units)
            fh.close()
            # mask model data to obs mask if available
            if isinstance(temp_mod, np.ma.core.MaskedArray):
                try:
                    ma_hist = np.ma.array(temp_mod,
                                          mask = np.tile(maskmiss,
                                                         (temp_mod.shape[0],1)))
                except (NameError):
                    ma_hist = temp_mod
            else:
                ma_temp_mod = np.ma.masked_array(temp_mod, np.isnan(temp_mod))
                try:
                    ma_hist = np.ma.array(ma_temp_mod,
                                          mask = np.tile(maskmiss,
                                                         (ma_temp_mod.shape[0],
                                                          1)))
                except (NameError):
                    ma_hist = ma_temp_mod

            # average over area and save value for each model
            w_lat = np.cos(lat * (4.0 * math.atan(1.0) / 180))
            tmp2_latweight = np.ma.average(np.squeeze(ma_hist),
                                           axis = 0,
                                           weights = w_lat)
            data[f, v] = np.nanmean(tmp2_latweight.filled(np.nan))
            f = f + 1
        else:
            continue

    if data_unit[v] == 'degC':
        degree_sign = u'\N{DEGREE SIGN}'
        data_unit[v] = degree_sign + "C"

target = np.empty((nfiles))
target_ts = dict() #np.empty((ntim_tot, nfiles))
f = 0
if region:
    name = '%s%s_%s_*_%s_r?i?p?_%s-%s_%s%s_%s_%s.nc' %(
        path_target, target_var, freq, experiment, syear_fut, eyear_fut,
        res_name_target, res_time_target, target_file, region)
else:
    name = '%s/%s_%s_*_%s_r?i?p?_%s-%s_%s%s_%s.nc' %(
        path_target, target_var, freq, experiment, syear_fut, eyear_fut,
        res_name_target, res_time_target, target_file)
for filename in glob.glob(name):
    model_name = filename.split('_')[4] + ' ' + filename.split('_')[6]
    if model_name in overlap:
        #print "Read "+filename+" data"
        fh = nc.Dataset(filename, mode = 'r')
        temp_fut = fh.variables[target_var][:] # global data, time, lat, lon
        lat = fh.variables['lat'][:]
        lon = fh.variables['lon'][:]
        tmp = fh.variables[target_var]
        target_unit = tmp.units
        fh.close()
        if (target_file != 'SCALE'):
            path_hist = '%s/%s/%s/%s/' %(archive, target_var, freq, target_mask)
            if region:
                filename_hist = '%s%s_%s_%s_%s_%s_%s-%s_%s%s_%s_%s.nc' %(
                    path_hist, target_var, freq, filename.split('_')[4],
                    experiment, filename.split('_')[6], syear_hist, eyear_hist,
                    res_name_target, res_time_target, target_file, region)
            else:
                filename_hist = '%s/%s_%s_%s_%s_%s_%s-%s_%s%s_%s.nc' %(
                    path_hist, target_var, freq, target_mask,
                    target_var, freq, filename.split('_')[4],
                    experiment, filename.split('_')[6], syear_hist, eyear_hist,
                    res_name_target, res_time_target, target_file)
            fh = nc.Dataset(filename_hist, mode = 'r')
            temp_hist = fh.variables[target_var][:] # global data,time,lat,lon
            fh.close()
            temp_mod = temp_fut - temp_hist
        else:
            temp_mod = temp_fut

        #check that time axis and grid is identical for model0 and modelX
        if f != 0:
            if temp_mod0.shape != temp_mod.shape:
                print 'Warning: Dimension for models are different!'
                continue
        else:
            temp_mod0 = temp_mod[:]

        model = filename.split('_')[4]
        if model == 'ACCESS1.3':
            model = 'ACCESS1-3'
        elif model == 'FGOALS_g2':
            model = 'FGOALS-g2'
        ens = filename.split('_')[6]
        model_names.append(model + '_' + ens)
        # mask model data to obs mask if available
        if isinstance(temp_mod, np.ma.core.MaskedArray):
            try:
                ma_target = np.ma.array(temp_mod,
                                  mask = np.tile(maskmiss,
                                                 (temp_mod.shape[0], 1)))
            except (NameError):
                ma_target = temp_mod
        else:
            ma_tmp_target = np.ma.masked_array(temp_mod, np.isnan(temp_mod))
            try:
                ma_target = np.ma.array(ma_tmp_target,
                                        mask = np.tile(maskmiss,
                                                       (ma_tmp_target.shape[0],
                                                        1)))
            except (NameError):
                ma_target = ma_tmp_target

        # average over area and save value for each model
        w_lat = np.cos(lat * (4.0 * math.atan(1.0) / 180))
        tmp1_latweight = np.ma.average(np.squeeze(ma_target),
                                       axis = 0,
                                       weights = w_lat)
        target[f] = np.nanmean(tmp1_latweight.filled(np.nan))

        # read data over whole timeseries for plotting
        path_ts = '%s/%s/%s/%s/' %(archive, target_var, freq, target_mask)
        if region:
            filename_ts = '%s%s_%s_%s_%s_%s_%s-%s_%s%s_%s.nc' %(
                path_ts, target_var, freq, filename.split('_')[4],
                experiment, filename.split('_')[6], syear, eyear,
                res_name_target, res_time_target, region)
        else:
            filename_ts = '%s/%s_%s_%s_%s_%s_%s-%s_%s%s.nc' %(
                path_ts, target_var, freq, target_mask,
                target_var, freq, filename.split('_')[4],
                experiment, filename.split('_')[6], syear, eyear,
                res_name_target, res_time_target)
        fh = nc.Dataset(filename_ts, mode = 'r')
        temp_ts = fh.variables[target_var][:] # global data,time,lat,lon
        time = fh.variables['time']
        cdftime = utime(time.units, calendar = time.calendar)
        dates = cdftime.num2date(time[:])
        years = np.asarray([dates[i].year for i in xrange(len(dates))])
        fh.close()
        if isinstance(temp_ts, np.ma.core.MaskedArray):
            try:
                ma_temp_ts = np.ma.array(temp_ts,
                                         mask = np.tile(maskmiss,
                                                        (temp_ts.shape[0], 1)))
            except (NameError):
                ma_temp_ts = temp_ts
        else:
            ma_tmp = np.ma.masked_array(temp_ts, np.isnan(temp_ts))
            try:
                ma_temp_ts = np.ma.array(ma_tmp, mask = np.tile(maskmiss,
                                                        (temp_ts.shape[0], 1)))
            except (NameError):
                ma_temp_ts = ma_tmp
        tmp_ts_latweight = np.ma.average(np.squeeze(ma_temp_ts), axis = 1,
                                         weights = w_lat)
        target_ts[model + '_' + ens] = np.nanmean(tmp_ts_latweight.filled(np.nan), axis = 1)
        f = f + 1
    else:
        continue
if target_unit == 'degC':
    target_unit = degree_sign + "C"

# Split the dataset in two equal parts
X_train, X_test, y_train, y_test = train_test_split(
    data, np.squeeze(target.reshape(-1, 1)), test_size = 0.5, random_state = 10)

# fit regressions
ols.fit(X_train, y_train)
ols_lin_pred = ols.predict(X_test)

tsr.fit(X_train, y_train)
tsr_lin_pred = tsr.predict(X_test)

ridgeCV.fit(X_train, y_train)
ridgeCV_lin_pred = ridgeCV.predict(X_test)

result = sm.OLS( np.squeeze(target.reshape(-1, 1)), data ).fit()
print result.summary()

print("The model is trained on the full training set (train_test_split).")
print("The scores are computed on the full test set.")
print()
#OLS
print('OLS')
# The coefficients
#print('Coefficients OLS: \n', ols.coef_)
# The mean square error
print("Residual sum of squares OLS: %.2f"
      % np.mean((ols_lin_pred - y_test) ** 2))
# Explained variance score: 1 is perfect prediction
print('Variance score OLS: %.2f' % ols.score(X_test, y_test))
#CrossValidation
cv = 5
scoresOLS = cross_val_score(ols, X_test, y_test, cv = cv)
print("Accuracy OLS: %0.2f (+/- %0.2f)" % (scoresOLS.mean(),
                                           scoresOLS.std() * 2))
print()
print('Theil-Sen:')
print('Variance score: %.2f' % tsr.score(X_test, y_test))
print()
print('Ridge CV:')
print('Variance score: %.2f' % ridgeCV.score(X_test, y_test))
print()
del X_train, X_test, y_train, y_test

# plot scatter with linear regressions for each diag_var
data_mask = np.ma.array(data, mask = False)
corr = np.empty(nvar)
for v in xrange(nvar):
    # Split the dataset in two equal parts
    X_train, X_test, y_train, y_test = train_test_split(data[:, v], target,
        test_size = 0.5, random_state = 10)

    # fit regressions
    #ols.fit(X_train.reshape(-1, 1), y_train.reshape(-1, 1))
    #ols_lin_pred = ols.predict(X_test.reshape(-1, 1))

    #tsr.fit(X_train.reshape(-1, 1), y_train.reshape(-1, 1))
    #tsr_lin_pred = tsr.predict(X_test.reshape(-1, 1))

    #ridgeCV.fit(X_train.reshape(-1, 1), y_train.reshape(-1, 1))
    #ridgeCV_lin_pred = ridgeCV.predict(X_test.reshape(-1, 1))

    # calculate OLS using statsmodel package, and calculate confidence interval
    X_train2 = sm.add_constant(X_train)
    result = sm.OLS(y_train, X_train2).fit()
    X_test2 = sm.add_constant(X_test)
    y_pred = result.predict(X_test2)
    #print result.summary()
    ols_R2 = result.rsquared

    y_hat = result.predict(X_train2)
    y_err = y_train - y_hat
    mean_x = X_train2.T[1].mean()
    n = len(X_train2)
    dof = n - result.df_model - 1
    t = stats.t.ppf(1-0.025, df=dof)
    s_err = np.sum(np.power(y_err, 2))
    conf = t * np.sqrt((s_err / (n - 2)) * (1.0 / n + 
                                            (np.power((X_test - mean_x), 2) / 
                                             ((np.sum(np.power(X_test, 2))) - 
                                              n * (np.power(mean_x, 2))))))
    upper = y_pred + abs(conf)
    lower = y_pred - abs(conf)

    # calculate correlation 
    corr[v] = np.corrcoef(target, data[:, v])[0, 1]
    print ('Correlation: %.2f' %corr[v])

    ## constrain models
    ## if data not within obs range -> mask

    # range of obs values
    tmp_obs = [m2_obs_areaavg[v], era_obs_areaavg[v], obs_areaavg[v]]
    obs_mean = np.nanmedian(tmp_obs)
    obs_min = np.nanmin(tmp_obs)
    obs_max = np.nanmax(tmp_obs)
    obs_std = np.nanstd(tmp_obs)
    min_diff = obs_mean - obs_min
    max_diff = obs_mean - obs_max
    err = abs(max([min_diff, max_diff], key = abs))
    obs_range1 = [obs_min - err, obs_max + err]
    #print obs_range1
    obs_range2 = [obs_mean - 0.2 * obs_mean, obs_mean + 0.2 * obs_mean]
    #print obs_range2
    # +- 3sigma
    obs_range = [obs_mean - 3 * obs_std, obs_mean + 3 * obs_std]
    #print obs_range

    obs_range = [min([obs_range1[0], obs_range2[0]]), max([obs_range1[1],
                                                           obs_range2[1]])]
    print obs_range

    # find models outside obs range
    data_mask[:, v] = np.ma.masked_outside(data[:, v], obs_range[0],
                                           obs_range[1])

    plt_data = data_mask[:, v].filled(np.nan)

    fig = plt.figure(figsize = (10, 7), dpi = 300)
    ax = fig.add_subplot(111)

    ax.scatter(np.squeeze(data[:, v]), target, marker = 'o', color = 'grey',
                edgecolors = 'none', s = 40)
    ax.scatter(plt_data, target, marker = 'o', color = 'black',
                edgecolors = 'none', s = 40)

    indsort = np.argsort(X_test)
    ax.fill_between(sorted(X_test), lower[indsort], upper[indsort],
                    color = 'darkorchid',
                    facecolor = 'darkorchid', alpha = 0.4)

    ax.axvline(x = obs_areaavg[v], color = 'darkgrey', linewidth = 2,
                label = 'Obs')
    ax.axvline(x = m2_obs_areaavg[v], color = 'darkgrey', linewidth = 2)
    ax.axvline(x = era_obs_areaavg[v], color = 'darkgrey', linewidth = 2)

    ax2 = ax.twinx()
    rv = stats.norm(loc = obs_mean, scale = obs_std)
    fit = rv.pdf(sorted(X_test))
    ax2.plot(sorted(X_test), fit, color = 'mediumblue', linewidth = 1.5)
    ax2.set_ylabel('pdf', color = 'mediumblue')
    ax2.tick_params('y', colors = 'mediumblue')
    #ax2.set_ylim([0, 100])

    #plt.plot(X_test.reshape(-1, 1), ols_lin_pred, color = 'blue', linewidth = 3,
    #    label = '%s, R$^2$: %0.2f' %(
    #        Labelols, ols.score(X_test.reshape(-1, 1), y_test.reshape(-1, 1))))
    #plt.plot(X_test.reshape(-1, 1), tsr_lin_pred, color = 'purple',
    #         linewidth = 3,
    #         label = '%s, R$^2$: %0.2f' %(Labeltsr,
    #                                      tsr.score(X_test.reshape(-1, 1),
    #                                                y_test.reshape(-1, 1))))
    #plt.plot(X_test.reshape(-1, 1), ridgeCV_lin_pred, color = 'seagreen',
    #         linewidth = 3,
    #         label = '%s, R$^2$: %0.2f' %(Labelridge,
    #                                      ridgeCV.score(X_test.reshape(-1, 1),
    #                                                    y_test.reshape(-1, 1))))

    ax.plot(X_test, y_pred, '-', color = 'darkorchid', linewidth = 2,
            label = '%s, R$^2$: %0.2f' %('OLS', ols_R2))

    ax.set_xlabel('%s %s %s [%s]' %(diag_var[v], var_file[v], res_name[v],
                                    data_unit[v]))
    ax.set_ylabel('$\Delta$%s %s %s [%s]' %(target_var, target_file,
                                            res_name_target, target_unit))
    ax.legend(loc = 'upper left', frameon = False)
    #plt.text(0.1, 0.73, 'Corr = %s' %(round(corr, 2)), fontsize = 15,
    #         ha = 'center', va = 'center', transform = ax.transAxes)
    plt.text(0.1, 0.83, 'Corr = %s' %(round(corr[v], 2)), fontsize = 15,
             ha = 'center', va = 'center', transform = ax.transAxes)
    plt.title(longname_region + ' ' + res_name_target)

    plt.savefig('%sem_constrained_hist_%s_%s_%s_%s_%s_%s_%s_%sobs.pdf' %(
        outdir, target_var, target_file, res_name_target, target_mask,
            diag_var[v], var_file[v], res_name[v], nobs))
    del X_train, X_test, y_train, y_test

# masked data over all diag_vars
constrained_models = np.ma.masked_where(np.ma.getmask(data_mask[:, 0]),
                                        model_names)
if nvar > 1:
    constrained_models = np.ma.masked_where(np.ma.getmask(data_mask[:, 1]),
                                            constrained_models)
if nvar > 2:
    constrained_models = np.ma.masked_where(np.ma.getmask(data_mask[:, 2]),
                                            constrained_models)

## plot constrained data versus diag with largest correlation
ind_const = np.argmax(corr)
data_constrained = np.ma.array(data[:, ind_const], mask = False)
data_constrained = np.ma.masked_where(np.ma.getmask(data_mask[:, 0]),
                                      data_constrained)
if nvar > 1:
    data_constrained = np.ma.masked_where(np.ma.getmask(data_mask[:, 1]),
                                          data_constrained)
if nvar > 2:
    data_constrained = np.ma.masked_where(np.ma.getmask(data_mask[:, 2]),
                                          data_constrained)

data_constrained_plot = np.array(data_constrained[~data_constrained.mask])
target_masked = target[~data_constrained.mask]

corr_constr = np.ma.corrcoef(target, data_constrained)[0, 1]

fig = plt.figure(figsize = (10, 7), dpi = 300)
ax = fig.add_subplot(111)

plt.scatter(np.squeeze(data[:, ind_const]), target, marker = 'o',
            color = 'grey',
            edgecolors = 'none', s = 40)
plt.scatter(data_constrained_plot, target_masked, marker = 'o', color = 'black',
            edgecolors = 'none', s = 40)

plt.axvline(x = obs_areaavg[ind_const], color = 'darkgrey', linewidth = 2,
            label = 'Obs')
plt.axvline(x = m2_obs_areaavg[ind_const], color = 'darkgrey', linewidth = 2)
plt.axvline(x = era_obs_areaavg[ind_const], color = 'darkgrey', linewidth = 2)

plt.xlabel('%s %s %s [%s]' %(diag_var[ind_const], var_file[ind_const],
                             res_name[ind_const], data_unit[ind_const]))
plt.ylabel('$\Delta$%s %s %s [%s]' %(target_var, target_file,
                                     res_name_target, target_unit))
plt.legend(loc = 'upper left', frameon = False)
plt.text(0.1, 0.8, 'Corr = %s' %(round(corr_constr, 2)), fontsize = 15,
         ha = 'center', va = 'center', transform = ax.transAxes)

plt.title(longname_region + ' ' + res_name_target)
plt.savefig('%sem_constrained_%sdiags_hist_%s_%s_%s_%s_%s_%s_%s_%sobs.pdf' %(
        outdir, nvar, target_var, target_file, res_name_target, target_mask,
        diag_var[ind_const], var_file[ind_const], res_name[ind_const], nobs))

## save set of constrained models into separate text file
with open('%sconstrained_models_%s_%s_%s_%s_%sdiags_%sobs.txt' %(
    outdir, target_var, target_file, res_name_target, target_mask, nvar, nobs),
        "w") as text_file:
    for m in range(len(constrained_models)):
        try:
            text_file.write(constrained_models[m] + "\n")
        except TypeError:
            continue

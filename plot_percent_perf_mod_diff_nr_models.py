#!/usr/bin/python
'''
File Name : plot_percent_perf_mod_test.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 07-03-2018
Modified: Wed 07 Mar 2018 05:43:01 PM CET
Purpose: plot percent models within 90% at certain sigma_s value for all sigma_d


'''
import numpy as np
import netCDF4 as nc
import matplotlib.pyplot as plt
###
# Define input & output
###
target_var = 'tasmax'
target_file = 'CLIM'
target_seas = 'JJA'
region = 'NAM'
obsdata = 'MERRA2'
diags = '4'
###
# Read data
###
infile = 'percent_%s_%s_%s_%s_%s_RMSE_%s.nc' %(target_var, target_file,
                                               target_seas, diags, 
                                               region, obsdata)
ifile = nc.Dataset(infile, mode = 'r')
d2 = ifile.variables['D2'][:]
test = ifile.variables['test'][11, :]
ifile.close()

infile = 'percent_oneperInst_%s_%s_%s_%s_%s_RMSE_%s.nc' %(target_var,
                                                          target_file,
                                                          target_seas, diags, 
                                                          region, obsdata)
ifile = nc.Dataset(infile, mode = 'r')
d2_onepI = ifile.variables['D2'][:]
test_onepI = ifile.variables['test'][11, :]
ifile.close()

infile = 'percent_indepModels_%s_%s_%s_%s_%s_RMSE_%s.nc' %(target_var,
                                                          target_file,
                                                          target_seas, diags, 
                                                          region, obsdata)
ifile = nc.Dataset(infile, mode = 'r')
d2_indepM = ifile.variables['D2'][:]
test_indepM = ifile.variables['test'][11, :]
ifile.close()

###
# Plot data
###
fig = plt.figure(figsize = (10, 8), dpi = 300)
plt.tick_params(axis = 'both', which = 'major', labelsize = 15)
plt.axhline(y = 0.8, color = 'k', linewidth = 1)

plt.plot(d2, test, color = 'purple', linewidth = 3, label = 'all Models')
plt.plot(d2_onepI, test_onepI, color = 'seagreen', linewidth = 3, label = 'one per Institution')
plt.plot(d2_indepM, test_indepM, color = 'firebrick', linewidth = 3, label = '"independent" Models')

plt.axvline(x = 0.5, color = 'k', linewidth = 2)

plt.title('Perfect model test in %s for %s at $\sigma_S$ $\simeq$ 0.6' %(region, obsdata), fontsize = 18)
plt.xlabel('Parameter for weighting model performance $\sigma_D$', fontsize = 18)
plt.ylabel('Fraction of perfect model tests within 10-90%', fontsize = 18)

leg = plt.legend(loc = 'lower right', fontsize = 18)  # leg defines legend -> can be modified
leg.draw_frame(False)

plt.savefig('percent_perf_mod_test_diff_nr_models_%s_%s_%s_%s_%s.pdf' %(
    target_var, target_file, target_seas, region, obsdata))

#!/usr/bin/python
'''
File Name : eval_weight_mm_mult_vars_mult_obs_cmip5-ng.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 08-09-2016
Modified: Wed 07 Dec 2016 01:47:05 CET
Purpose: calculate weighted multi model means based on
	Knutti et al. 2016, use delta matrixes calculated
	beforehand, use mutliple variables and obs datasets (2)


'''
import netCDF4 as nc # to work with NetCDF files
import numpy as np
from scipy import signal
import math
import copy
import os # operating system interface
from os.path import expanduser
home = expanduser("~") # Get users home directory
workdir = os.getcwd()
import glob
import sys
sys.path.insert(0, home + '/scripts/plot_scripts/utils/')
sys.path.insert(0, workdir + '/functions/')
from func_read_data import func_read_netcdf
from clim_seas_TLL import clim_mon_TLL, std_mon_TLL, seas_avg_mon_TLL, seas_std_mon_TLL, clim_seas_mon_TLL, ann_avg_mon_TLL
from calc_trend_TLL import trend_TLL
from point_inside_polygon import point_inside_polygon
from func_calc_wu_wq import calc_wu, calc_wq, calc_weights_approx
from func_eval_wmm_nonwmm_error_indexI import error_indexI
import datetime as dt
from netcdftime import utime
from func_write_netcdf import func_write_netcdf
import matplotlib.pyplot as plt
import operator
from mpl_toolkits.basemap import shiftgrid
###
# Define input
###
## multiple variables possible but deltas need to be available
# and first one determines plotting and titles in plots
variable = ['tasmax', 'rlus', 'hfls']
# climatology:clim, variability:std, trend:trnd
var_file = ['std', 'std', 'std']
# kind is cyc: annual cycle, seas: seasonal or mon: monthly values
var_kind = ['seas', 'seas', 'seas']
# choose data for particular month or season?
res = 2
# weight of individual fields, all equal weight 1 at the moment
fields_weight = [1, 1, 1]
## observational datasets
obs = ["MERRA2", "Obs"]
# choose region if required, otherwise 'GLOBAL'
region = 'NAM'

#method to calculate optimal sigmas, at the moment only fixed is supported
## (estimate sigams beforehand in calc_opt_sigmas.py)
method= 'fix'

archive = '/net/atmos/data/cmip5-ng'
indir = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/'
outdir = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/Eval_Weight/%s/' %(
    variable[0])
if (os.access(outdir, os.F_OK) == False):
    os.makedirs(outdir)

syear_eval = 1980
eyear_eval = 2014
syear_eval0 = 1980
eyear_eval0 = 2014
syear_eval1 = 1950
eyear_eval1 = 2014
s_year = 1951
e_year = 2100

experiment = 'rcp85'
grid = 'g025'

## Error estimate, RMSE, perkins_SS etc.
err = 'RMSE' #'perkins_SS'
err_var = 'rmse' #'SS'

# free parameter "radius of similarity" , minimum: internal variability ~0.04
# the larger the more distant models are considered similar
sigma_S2 = 0.6 #1.0963
# free parameter "radius of model quality"
# minimum is smallest obs. bias seen in ensemble ~+-1.8**2
# wide: mean intermodel distance in CMIP5
sigma_D2 = 0.7 #1.8353

###
# Read data
###
## read rmse for var_file
## first read model names for all var_file from text file
## select all models that occur for all var_files,
## or if testing select number of simulations defined in test
rmse_models = dict()
overlap = list()

for v in xrange(len(var_file)):
    rmsefile = '%s%s/%s_%s_%s_all_%s_%s-%s' %(
                indir, variable[v], variable[v], var_file[v], var_kind[v],
                experiment, syear_eval, eyear_eval)
    if (os.access(rmsefile + '.txt', os.F_OK) == True):
        rmse_models[var_file[v]] = np.genfromtxt(rmsefile + '.txt',
                                                 delimiter = '',
                                                 dtype = None).tolist()[: - 1]
        if v > 0:
            overlap = list(set(rmse_models[var_file[v]]) & set(overlap))
        else:
            overlap = rmse_models[var_file[v]]
        try:
            overlap = overlap[0:test]
        except (NameError):
            pass

indices = dict()
rmse_d = np.ndarray((len(var_file), len(overlap), len(overlap)))
rmse_q = np.ndarray((len(var_file), len(obs), len(overlap)))
for v in xrange(len(var_file)):
    rmsefile = '%s%s/%s_%s_%s_all_%s_%s-%s' %(
                indir, variable[v], variable[v], var_file[v], var_kind[v],
                experiment, syear_eval, eyear_eval)
    ## find indices of model_names in rmse_file
    rmse_models[var_file[v]] = np.genfromtxt(rmsefile + '.txt',
                                             delimiter = '',
                                             dtype = None).tolist()[: - 1]
    for m in xrange(len(overlap)):
        indices[overlap[m]] = rmse_models[var_file[v]].index(overlap[m])
    ind = sorted(indices.values())
    model_names = sorted(indices.keys())
    for o in xrange(len(obs)):
        if (os.access('%s_%s_%s_%s.nc' %(rmsefile, region, obs[o], err),
                      os.F_OK) == True):
            print err + ' already exist, read from netcdf'
            fh = nc.Dataset('%s_%s_%s_%s.nc' %(
                            rmsefile, region, obs[o], err), mode = 'r')
            rmse_all = fh.variables[err_var]
            rmse_d[v, :, :] = rmse_all[ind, ind]
            rmse_q[v, o, :] = rmse_all[ - 1, ind]
            fh.close()
        else:
            print "RMSE delta matrix %s_%s_%s_%s.nc does not exist, exiting" %(
                            rmsefile, region, obs[o], err)
            sys.exit
delta_u = np.ndarray((len(var_file), len(model_names), len(model_names)))
delta_q = np.ndarray((len(var_file), len(obs), len(model_names)))
for v in xrange(len(var_file)):
    ## normalize rmse by median
    med = np.median(rmse_d[v, :, :])
    delta_u[v, :, :] = rmse_d[v, :, :] / med
    for o in xrange(len(obs)):
        delta_q[v, o, :] = rmse_q[v, o, :] / med

## average deltas over fields, taking field weight into account
## (all 1 at the moment)
field_w_extend_u = np.reshape(np.repeat(fields_weight,
                                        len(model_names) * len(model_names)),
                              (len(fields_weight), len(model_names), len(model_names)))
delta_u = np.sqrt(np.sum(field_w_extend_u * delta_u,
                         axis = 0) / np.sum(fields_weight))

field_w_extend_q = np.reshape(np.repeat(fields_weight,
                                        len(obs) * len(model_names)),
                              (len(fields_weight), len(obs), len(model_names)))
delta_q = np.sqrt(np.sum(field_w_extend_q * delta_q,
                         axis = 0) / np.sum(fields_weight))
## average delta_q over all obs datasets if more than one
if len(obs) > 1:
    obs_weight = 1.0/len(obs)
    delta_q = np.sum(obs_weight * delta_q, axis = 0)

## read obs data
print "Read %s data" %(obs[0])
obsfile_ts = '%s%s/%s_%s_%s_%s_%s_%s_%s-%s.nc' %(
            indir, variable[0], variable[0], 'ts', var_kind[0],
            obs[0], region, experiment, syear_eval0, eyear_eval0)
fh = nc.Dataset(obsfile_ts, mode = 'r')
temp_erai_ts = fh.variables[variable[0]][:]
time = fh.variables['time']
cdftime = utime(time.units, calendar = time.calendar)
obsdates = cdftime.num2date(time[:])
obs1years = np.asarray([obsdates[i].year for i in xrange(len(obsdates))])
fh.close()

obsfile_clim = '%s%s/%s_%s_%s_%s_%s_%s_%s-%s.nc' %(
            indir, variable[0], variable[0], 'clim', var_kind[0],
            obs[0], region, experiment, syear_eval0, eyear_eval0)
fh = nc.Dataset(obsfile_clim, mode = 'r')
temp_erai_clim = fh.variables[variable[0]][:]
fh.close()

obsfile_std = '%s%s/%s_%s_%s_%s_%s_%s_%s-%s.nc' %(
            indir, variable[0], variable[0], 'std', var_kind[0],
            obs[0], region, experiment, syear_eval0, eyear_eval0)
fh = nc.Dataset(obsfile_std, mode = 'r')
temp_erai_std = fh.variables[variable[0]][:]
fh.close()

obsfile = '%s%s/%s_%s_%s_%s_%s_%s_%s-%s.nc' %(
            indir, variable[0], variable[0], var_file[0], var_kind[0],
            obs[0], region, experiment, syear_eval0, eyear_eval0)
fh = nc.Dataset(obsfile, mode = 'r')
temp_erai_var = fh.variables[variable[0]][:]
lat = fh.variables['lat'][:]
lon = fh.variables['lon'][:]
fh.close()

## calculate ERAint area means
rad = 4.0 * math.atan(1.0) / 180
w_lat = np.cos(lat * rad) #weight for latitude differences in area

tmp_latweight = np.ma.empty((len(obs1years), len(lon)))
ma_temp_erai_ts = np.ma.masked_array(temp_erai_ts, np.isnan(temp_erai_ts))
for ilon in xrange(len(lon)):
    tmp_latweight[:, ilon] = np.ma.average(ma_temp_erai_ts[:, :, ilon], axis = 1, weights = w_lat)
erai_ts_areaavg = np.nanmean(tmp_latweight.filled(np.nan), axis = 1)

ma_temp_erai_var = np.ma.masked_array(temp_erai_var, np.isnan(temp_erai_var))
tmp_latweight = np.ma.average(ma_temp_erai_var, axis = 0, weights = w_lat)
erai_var_areaavg = np.nanmean(tmp_latweight.filled(np.nan), axis = 0)

print "Read %s data" %(obs[1])
obsfile_ts = '%s%s/%s_%s_%s_%s_%s_%s_%s-%s.nc' %(
            indir, variable[0], variable[0], 'ts', var_kind[0],
            obs[1], region, experiment, syear_eval1, eyear_eval1)
fh = nc.Dataset(obsfile_ts, mode = 'r')
temp_merra_ts = fh.variables[variable[0]][:]
time = fh.variables['time']
cdftime = utime(time.units, calendar = time.calendar)
obsdates = cdftime.num2date(time[:])
obs2years = np.asarray([obsdates[i].year for i in xrange(len(obsdates))])
fh.close()

obsfile_clim = '%s%s/%s_%s_%s_%s_%s_%s_%s-%s.nc' %(
            indir, variable[0], variable[0], 'clim', var_kind[0],
            obs[1], region, experiment, syear_eval1, eyear_eval1)
fh = nc.Dataset(obsfile_clim, mode = 'r')
temp_merra_clim = fh.variables[variable[0]][:]
fh.close()

obsfile_std = '%s%s/%s_%s_%s_%s_%s_%s_%s-%s.nc' %(
            indir, variable[0], variable[0], 'std', var_kind[0],
            obs[1], region, experiment, syear_eval1, eyear_eval1)
fh = nc.Dataset(obsfile_std, mode = 'r')
temp_merra_std = fh.variables[variable[0]][:]
fh.close()

obsfile = '%s%s/%s_%s_%s_%s_%s_%s_%s-%s.nc' %(
            indir, variable[0], variable[0], var_file[0], var_kind[0],
            obs[1], region, experiment, syear_eval1, eyear_eval1)
fh = nc.Dataset(obsfile, mode = 'r')
temp_merra_var = fh.variables[variable[0]][:]
lat = fh.variables['lat'][:]
lon = fh.variables['lon'][:]
fh.close()

## calculate MERRA2 area means
w_lat = np.cos(lat * rad) #weight for latitude differences in area

tmp_latweight = np.ma.empty((len(obs2years), len(lon)))
ma_temp_merra_ts = np.ma.masked_array(temp_merra_ts, np.isnan(temp_merra_ts))
for ilon in xrange(len(lon)):
    tmp_latweight[:, ilon] = np.ma.average(ma_temp_merra_ts[:, :, ilon], axis = 1, weights = w_lat)
merra_ts_areaavg = np.nanmean(tmp_latweight.filled(np.nan), axis = 1)

ma_temp_merra_var = np.ma.masked_array(temp_merra_var, np.isnan(temp_merra_var))
tmp_latweight = np.ma.average(ma_temp_merra_var, axis = 0, weights = w_lat)
merra_var_areaavg = np.nanmean(tmp_latweight.filled(np.nan), axis = 0)

### read model data
## same models as in rmse file
print "Read model data"
d_temp_mod_ts = dict()
d_temp_mod = dict()
nfiles = len(model_names)
print '%s matching files' %(str(nfiles))
for f in xrange(len(model_names)):
    model = model_names[f].split('_', 1)[0]
    ens = model_names[f].split('_', 1)[1]

    ## need to re-read data over full time period to be able to
    ## plot whole time period
    ## and recalculate clim, std, trnd
    filename = '%s/%s/%s_mon_%s_%s_%s_%s.nc' %(archive, variable[0],
                                               variable[0], model,
                                               experiment, ens, grid)
    print "Read %s data" %(filename)
    model_data = func_read_netcdf(filename, variable[0], var_units = True,
                                  syear = s_year, eyear = e_year)
    temp_mod = model_data['data']
    lat = model_data['lat']
    lon = model_data['lon']
    dates = model_data['dates']
    unit = model_data['unit']
    months = np.asarray([dates[i].month for i in xrange(len(dates))])
    years = np.asarray([dates[i].year for i in xrange(len(dates))])

    # shiftgrid to -180 to 180 if necessary (to be compatible with area mask)
    if (lon[0] >= 0.0) and (lon[-1] > 180):
        lons_orig = lon[:]
        temp_mod, lon = shiftgrid(180., temp_mod, lon, start = False)

    if isinstance(temp_mod, np.ma.core.MaskedArray):
        temp_mod = temp_mod.filled(np.NaN)
    if (variable[0] == 'sic') and  (model == "EC-EARTH"):
        with np.errstate(invalid = 'ignore'):
            temp_mod[temp_mod < 0.0] = np.NaN

    ## if any region defined, cut data into region
    if region != None:
        mask=np.loadtxt('/home/rlorenz/scripts/plot_scripts/areas_txt/%s.txt' %(
            region))
        temp_mod_reg_nan = np.ndarray((temp_mod.shape))
        for ilat in xrange(len(lat)):
            for ilon in xrange(len(lon)):
                if (point_inside_polygon(lat[ilat], lon[ilon],
                                         np.fliplr(mask))):
                    temp_mod_reg_nan[:, ilat, ilon] = temp_mod[:, ilat, ilon]
                else:
                    temp_mod_reg_nan[:, ilat, ilon] = np.NaN
        idx_lat = np.where((lat > np.min(mask[:, 1])) &
                           (lat < np.max(mask[:, 1])))
        lat = lat[idx_lat]
        tmp1_mod_reg = np.empty((len(temp_mod_reg_nan), len(lat), len(lon)))
        tmp1_mod_reg.fill(np.NaN)
        idx_lon = np.where((lon > np.min(mask[:, 0])) &
                           (lon < np.max(mask[:, 0])))
        lon = lon[idx_lon]
        temp_mod_reg = np.empty((len(temp_mod_reg_nan), len(lat), len(lon)))
        temp_mod_reg.fill(np.NaN)
        tmp1_mod_reg = np.squeeze(temp_mod_reg_nan[:, idx_lat, :], 1)
        temp_mod_reg = np.squeeze(tmp1_mod_reg[:, :, idx_lon], 2)
        del temp_mod
    else:
        temp_mod_reg = temp_mod

    ## calculate timeseries and save for each model
    if (res == None) and (var_kind[0] == 'cyc'):
        temp_mod_ts = ann_avg_mon_TLL(temp_mod_reg, years)
        d_temp_mod_ts[model + '_' + ens] = temp_mod_ts
        ts_years = years[0::12]
        time_out = model_data['time'][0::12]
    elif (res == None) and (var_kind[0] != 'cyc'):
        d_temp_mod_ts[model + '_' + ens] = temp_mod_reg
    else:
        if var_kind[0] == 'mon':
            ind=np.where(months == res)
            d_temp_mod_ts[model + '_' + ens] = np.squeeze(temp_mod_reg[ind, :, :])
            ts_years = years[ind]
        elif var_kind[0] == 'seas':
            temp_mod_clim = np.empty((1, len(lat), len(lon)))
            temp_mod_clim.fill(np.NaN)
            temp_mod_clim_seas = seas_avg_mon_TLL(temp_mod_reg, months)
            d_temp_mod_ts[model + '_' + ens] = clim_seas_mon_TLL(temp_mod_reg,
                                                                 months,
                                                                 years)[res, :, :, :]
            ts_years = years[0::12]    
    ## calculate climatology
    if var_file[0] == 'clim':
        if res == None:
            d_temp_mod[model + '_' + ens] = clim_mon_TLL(temp_mod_reg, months)
        else:
            if var_kind[0] == 'mon':
                ind=np.where(months == res)
                d_temp_mod[model + '_' + ens] = np.nanmean(np.squeeze(temp_mod_reg[ind, :, :]),
                                                           axis = 0, keepdims = True)
            elif var_kind[0] == 'seas':
                temp_mod_clim = np.empty((1, len(lat), len(lon)))
                temp_mod_clim.fill(np.NaN)
                temp_mod_clim_seas = seas_avg_mon_TLL(temp_mod_reg, months)
                temp_mod_clim[0, :, :] = temp_mod_clim_seas[res, :, :]
                d_temp_mod[model + '_' + ens] = temp_mod_clim
    if var_file[0] == 'std':
        ## detrend before calculating variability
        if np.isnan(temp_mod_reg).any():
            temp_mod_reg_dtr = np.empty((temp_mod_reg.shape))
            temp_mod_reg_dtr.fill(np.NaN)
            for ilat in xrange(len(lat)):
                for ilon in xrange(len(lon)):
                    tmp = temp_mod_reg[:, ilat, ilon]
                    if np.isnan(tmp).any():
                        temp_mod_reg_dtr[:, ilat, ilon] = np.NaN
                    else:
                        temp_mod_reg_dtr[:, ilat, ilon] = signal.detrend(tmp, axis = 0, type = 'linear', bp = 0)
        else:
            temp_mod_reg_dtr = signal.detrend(temp_mod_reg, axis = 0,
                                              type = 'linear', bp = 0)
        if res == None:
            d_temp_mod[model + '_' + ens] = std_mon_TLL(temp_mod_reg_dtr,
                                                        months)
        else:
            if var_kind[0] == 'mon':
                ind=np.where(months == res)
                d_temp_mod[model+'_'+ens] = np.nanstd(np.squeeze(temp_mod_reg_dtr[ind, :, :]),
                                                      axis = 0, keepdims = True)
            elif var_kind[0] == 'seas':
                temp_mod_std = np.empty((1, len(lat), len(lon)))
                temp_mod_std.fill(np.NaN)
                temp_mod_std_seas = seas_std_mon_TLL(temp_mod_reg, months, years)
                temp_mod_std[0, :, :] = temp_mod_std_seas[res, :, :]
                d_temp_mod[model + '_' + ens] = temp_mod_std
    if var_file[0] == 'trnd':
        if res == None:
            d_temp_mod[model + '_' + ens] = trend_TLL(np.squeeze(d_temp_mod_ts[model + '_' + ens]), drop = False)
        else:
            if var_kind[0] == 'mon':
                ind = np.where(months == res)
                d_temp_mod[model + '_' + ens] = trend_TLL(np.squeeze(temp_mod_reg[ind, :, :]), drop = False)
            elif var_kind[0] == 'seas':
                d_temp_mod[model + '_' + ens] = trend_TLL(np.squeeze(d_temp_mod_ts[model + '_' + ens]), drop = False)

###
# Calculate weights
###
print "Calculate weights for model dependence (u:uniqueness) and quality (q)"
wu_end = calc_wu(delta_u, model_names, sigma_S2)
wq_end = calc_wq(delta_q, model_names, sigma_D2)

###
# Calculate weighted multi-model climatologies
###
print "Calculate weighted and non-weighted model means"
approx_wmm = calc_weights_approx(wu_end, wq_end, model_names, d_temp_mod)
## area average of weighted model mean
w_lat = np.cos(lat * rad) #weight for latitude differences in area
ma_approx_wmm = np.ma.masked_array(approx_wmm['approx'],
                                   np.isnan(approx_wmm['approx']))
tmp_latweight = np.ma.average(np.squeeze(ma_approx_wmm), axis = 0,
                              weights = w_lat)
approx_wmm_areaavg = np.nanmean(tmp_latweight.filled(np.nan))

## area average for all models
d_temp_mod_areaavg = dict()
for key, value in d_temp_mod.iteritems():
    ma_temp_mod = np.ma.masked_array(value, np.isnan(value))
    tmp_latweight = np.ma.average(np.squeeze(ma_temp_mod), axis = 0,
                                  weights = w_lat)
    d_temp_mod_areaavg[key] = np.nanmean(tmp_latweight.filled(np.nan))
    del tmp_latweight

## for time series calculate average mean first
d_temp_mod_ts_areaavg = dict()
for key, value in d_temp_mod_ts.iteritems():
    ma_temp_mod_ts = np.ma.masked_array(value, np.isnan(value))
    tmp_latweight = np.ma.empty((len(ts_years), len(lon)))
    for ilon in xrange(len(lon)):
        tmp_latweight[:, ilon] = np.ma.average(ma_temp_mod_ts[:, :, ilon],
                                               axis = 1, weights = w_lat)
    d_temp_mod_ts_areaavg[key] = np.nanmean(tmp_latweight.filled(np.nan),
                                            axis = 1)

approx_wmm_ts_areaavg = calc_weights_approx(wu_end, wq_end, model_names,
                                    d_temp_mod_ts_areaavg)

## test if sum of all weights equals one
weights = approx_wmm['weights']
if (type(weights) is dict):
    dims_w = len(weights.values())
    if (dims_w == len(model_names)):
        if (np.sum(weights.values()) != 1.0):
            print "Warning: Sum of all weights does not equal 1 but " + str(np.sum(weights.values()))
else:
    dims_w = weights.shape
    if (dims_w[0] == len(model_names)):
        if (np.sum(weights) != 1.0):
            print "Warning: Sum of all weights does not equal 1 but " + str(np.sum(weights))
    else:
        if (np.sum(weights[0, 0, :]) != 1.0):
            print "Warning: Sum of all weights does not equal 1 but " + str(np.sum(weights[0, 0, :]))

print approx_wmm_areaavg.shape, np.nanmax(approx_wmm_areaavg), np.nanmin(approx_wmm_areaavg)
print approx_wmm_ts_areaavg['approx'].shape, np.nanmax(approx_wmm_ts_areaavg['approx']), np.nanmin(approx_wmm_ts_areaavg['approx'])

# calculate unweighted mean
tmp2 = 0
if (var_file[0] == 'std'):
    for key, value in d_temp_mod.iteritems():
        tmp_pow = np.power(value, 2)
        tmp2 = tmp2 + tmp_pow
    mm = np.sqrt(tmp2 / len(model_names))
else: 
    for key, value in d_temp_mod.iteritems():
        tmp2 = tmp2 + value
    mm = tmp2 / len(model_names)
print mm.shape, np.nanmax(mm), np.nanmin(mm)

tmp3 = 0
for key, value in d_temp_mod_ts_areaavg.iteritems():
    tmp3 = tmp3 + value

mm_ts_areaavg = tmp3 / len(model_names)
print mm_ts_areaavg.shape, np.nanmax(mm_ts_areaavg), np.nanmin(mm_ts_areaavg)

# area mean non-weighted
ma_mm = np.ma.masked_array(mm, np.isnan(mm))
if len(mm.shape) == 3:
    tmp_latweight = np.ma.empty((len(mm), len(lon)))
    for ilon in xrange(len(lon)):
        tmp_latweight[:, ilon] = np.ma.average(ma_mm[:, :, ilon],
                                               axis = 1, weights = w_lat)
        mm_areaavg = np.nanmean(tmp_latweight.filled(np.nan), axis = 1)
else:
   tmp_latweight = np.ma.average(ma_mm, axis = 0, weights = w_lat)
   mm_areaavg = np.nanmean(tmp_latweight.filled(np.nan))

## Calculate Error index I_2
I2_1 = error_indexI(np.squeeze(approx_wmm['approx']), np.squeeze(mm), temp_erai_clim, temp_erai_std, lat, lon, var_kind[0])
print I2_1

I2_2 = error_indexI(np.squeeze(approx_wmm['approx']), np.squeeze(mm), temp_merra_clim, temp_merra_std, lat, lon, var_kind[0])
print I2_2

## calculate spread for timeseries
ts_areaavg = np.array([value for key, value in sorted(d_temp_mod_ts_areaavg.iteritems())], dtype = float)
round_weights = np.empty((len(ts_areaavg)))
round_weights.fill(np.NaN)
m = 0
for key, value in sorted(d_temp_mod_ts_areaavg.iteritems()):
    round_weights[m] = int(round(weights[key] * len(model_names) * 1000))
    m = m + 1
ind_end = np.cumsum(round_weights).astype(int)
ind_start = copy.deepcopy(ind_end)
ind_start[0] = 0
ind_start[1:] = copy.deepcopy(ind_end[:len(model_names) - 1] + 1)
tmp = np.empty((len(model_names) * 1000 + 10, ts_areaavg.shape[1]))
tmp.fill(np.NaN)
for m in xrange(len(model_names)):
    if ind_end[m] > 100010:
        ind_end[m] = 100009
    for kk in xrange(ind_start[m], ind_end[m] + 1):
        tmp[kk, :] = ts_areaavg[m, :]
dim = np.count_nonzero(tmp[:, 0])

tmp2 = tmp[:dim, :]
tmp1 = np.empty(tmp2.shape, dtype = float)
tmp1.fill(np.NaN)
for t in xrange(tmp2.shape[1]):
    tmp1[:, t] = sorted(tmp2[:, t], reverse = True)
del tmp2
dim = np.count_nonzero(tmp1[:, 0])
print dim
ind_5 = int(math.ceil(0.05 * dim) - 1)
print ind_5
ind_95 = int(math.floor(0.95 * dim) - 1)
print ind_95
lower_ts_wmm = tmp1[ind_5, :]
upper_ts_wmm = tmp1[ind_95, :]
avg_ts_wmm = np.nanmean(tmp1, axis = 0)
del tmp
del tmp1

tmp21 = np.empty(ts_areaavg.shape, dtype=float)
tmp21.fill(np.NaN)
for t in xrange(ts_areaavg.shape[1]):
    tmp21[:, t] = sorted(ts_areaavg[:, t], reverse = True)
dim = np.count_nonzero(tmp21[:, 0])
print dim
ind_5 = int(math.ceil(0.05 * dim) - 1)
ind_95 = int(math.floor(0.95 * dim) - 1)
lower_ts_mm = tmp21[ind_5, :]
print ind_5
upper_ts_mm = tmp21[ind_95, :]
print ind_95
avg_ts_mm = np.nanmean(tmp21, axis = 0)
del tmp21

###
# Plotting
###
print "Plot data"
swu_txt = str(np.round(sigma_S2, 3))
swq_txt = str(np.round(sigma_D2, 3))
## plot area average of annual timeseries
fig = plt.figure(figsize = (10, 5), dpi = 300)
colors = plt.matplotlib.cm.YlOrRd(np.linspace(0, 1, nfiles + 20))
sorted_weights = sorted(weights.items(), key=operator.itemgetter(1))
for key, value in d_temp_mod_ts_areaavg.iteritems():
    indc = sorted_weights.index((key, weights[key]))
    plt.plot(ts_years, value, color = colors[indc], linewidth = 0.5)

plt.plot(obs1years, erai_ts_areaavg, color = "mediumblue",
          linestyle = '-', label = obs[0], linewidth = 3.0)
plt.plot(obs2years, merra_ts_areaavg, color = "purple",
          linestyle = '-', label = obs[1], linewidth = 3.0)
plt.plot(ts_years, mm_ts_areaavg, 'k-',
         label = "non-weighted MMM", linewidth = 3.0)
plt.plot(ts_years, approx_wmm_ts_areaavg['approx'], color = "red", 
         linestyle = '-', label = "weighted MMM", linewidth = 3.0)

plt.xlabel('Year')
plt.ylabel(region + ' ' + variable[0] + ' ' + var_file[0] + ' ' + var_kind[0] + ' [' + unit + '] timeseries')  # can use LaTeX for subscripts etc
plt.grid(True)
leg = plt.legend(loc = 'upper left')  ## leg defines legend -> can be modified
leg.draw_frame(False)

plt.savefig('%s%s_%s_%s_%s_%s_%s_%s_swu%s_swq%s_%s_mean_wmm_ts.pdf' %(
        outdir, variable[0], var_file[0], var_kind[0], obs[0], obs[1], region,
        err, swu_txt, swq_txt, method))

## plot area average of annual timeseries for obs, mm and wmm
## with uncertainty spread (5-95%)
fig = plt.figure(figsize = (10, 5), dpi = 300)
plt.plot(obs1years, erai_ts_areaavg, "mediumblue", label = obs[0], linewidth = 3.0)
plt.plot(obs2years, merra_ts_areaavg, "purple", label = obs[1], linewidth = 3.0)

plt.plot(ts_years, mm_ts_areaavg, 'k-',
         label = "non-weighted MMM", linewidth = 3.0)
plt.plot(ts_years, lower_ts_mm, color = "grey")
plt.plot(ts_years, upper_ts_mm, color = "grey")
plt.fill_between(ts_years, lower_ts_mm, upper_ts_mm, facecolor='grey',
                 alpha = 0.4)

#plt.plot(ts_years, approx_wmm_ts_areaavg['approx'], color = "green",
#         linestyle = '-', label = "weighted MMM", linewidth = 3.0)
plt.plot(ts_years, avg_ts_wmm, color = "red", linestyle = '-',
         label = "weighted MMM", linewidth = 3.0)
plt.plot(ts_years, lower_ts_wmm, color = "red")
plt.plot(ts_years, upper_ts_wmm, color = "red")
plt.fill_between(ts_years, lower_ts_wmm, upper_ts_wmm, facecolor='red',
                 alpha = 0.4)

plt.xlabel('Year')
plt.ylabel('%s  %s %s %s [%s] timeseries' %(region, variable[0], var_file[0],
                                            var_kind[0], unit))  # can use LaTeX
                                                            # for subscripts etc
plt.grid(True)
leg = plt.legend(loc = 'upper left')  # leg defines legend -> can be modified
leg.draw_frame(False)

plt.savefig('%s%s_%s_%s_%s_%s_%s_%s_swu%s_swq%s_%s_mean_wmm_ts_spread.pdf' %(
        outdir, variable[0], var_file[0], var_kind[0], obs[0], obs[1], region,
        err, swu_txt, swq_txt, method))

#!/usr/bin/python
'''
File Name : calc_rmse_tasmax_pi.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 11-05-2016
Modified: Mon 22 Aug 2016 02:18:48 PM CEST
Purpose: calculate rmse for pi for weighted mm


'''
import netCDF4 as nc # to work with NetCDF files
import numpy as np
import os # operating system interface
from os.path import expanduser
home = expanduser("~") # Get users home directory
import glob
import sys
sys.path.insert(0,home+'/scripts/plot_scripts/utils/')
from func_read_data import func_read_netcdf
from func_calc_rmse import func_calc_rmse
from point_inside_polygon import point_inside_polygon
import datetime as dt

###
# Define input
###
variable = ['pi_seas']
var_file = ['clim']
var_kind = ['seas']
fields_weight = [1]
res = 3
region = 'CNA_flip'           #cut data over region?

archive = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/LandCoupPi/TX90_HW/'
outdir = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/%s/' %(variable[0])
infile = 'LandCoupPi_HW_day_*_rcp85_*_19790101-20151231_remapcon2'
syear = 1980
eyear = 2015
nyears = eyear - syear + 1

experiment = 'rcp85'
grid = 'g025'
obsdata = 'ERAint' #ERAint or MERRA2

if (os.access(outdir,os.F_OK)==False):
        os.makedirs(outdir)

#read obs data
if (obsdata == 'ERAint'):
    print "Read ERAint data"
    path = '/net/tropo/climphys/rlorenz/ERA-INT_HW/LandCoupPi/'
    if variable[0] == 'pi_seas':
        ifile = 'LandCoupPi_all_ERAint_19790101-20151231_remapcon2_g025.nc'
    elif (variable[0] == 'pi_HW'):
        ifile = 'LandCoupPi_HW_ERAint_19790101-20151231_remapcon2_g025.nc'
    else:
        print 'Error: variable does not exist, exiting'
        sys.exit
elif (obsdata == 'MERRA2'):
    print "Read MERRA2 data"
    path = '/net/tropo/climphys/rlorenz/MERRA2_HW/LandCoupPi/'
    if variable[0] == 'pi_seas':
        ifile = 'LandCoupPi_all_MERRA2_19800101-20151231_remapcon2_g025.nc'
    elif (variable[0] == 'pi_HW'):
        ifile = 'LandCoupPi_HW_MERRA2_19800101-20151231_remapcon2_g025.nc'
    else:
        print 'Error: variable does not exist, exiting'
        sys.exit

obsfile = nc.Dataset(path+ifile,mode='r')
temp = obsfile.variables[variable[0]]
lat = obsfile.variables['lat'][:]
lon = obsfile.variables['lon'][:]    

if isinstance(temp, np.ma.core.MaskedArray):
        temp = temp.filled(np.NaN)
if res!=None:
    temp_obs = temp[res,:,:]
else:
    temp_obs = temp[:]

###read model data
print "Read model data"
##find all matching files in archive, loop over all of them
#first count matching files in folder to initialze variable rmse per model:
name = '%s/%s_%s.nc' %(archive,infile,grid)
nfiles = len(glob.glob(name))
model_names = []
d_temp_mod = {}
print str(nfiles)+ ' matching files found'
f = 0
for filename in glob.glob(name):
    print "Read "+filename+" data"
    ncfile = nc.Dataset(filename,mode='r')
    temp_mod = ncfile.variables[variable[0]]
    lat = ncfile.variables['lat'][:]
    lon = ncfile.variables['lon'][:]
    #check that time axis and grid is identical for model0 and modelX and obs
    if temp.shape != temp_mod.shape:
        print 'Warning: Dimension for model and '+obsdata+' is different!'
        continue
    if f != 0:
        if temp_mod0.shape != temp_mod.shape:
            print 'Warning: Dimension for model0 and modelX is different!'
            continue
    else:
        temp_mod0 = temp_mod[:]
    if isinstance(temp_mod, np.ma.core.MaskedArray):
            temp_mod = temp_mod.filled(np.NaN)
    model = filename.split('_')[6]
    ens = filename.split('_')[8]
    print model, ens
    if res!=None:
        d_temp_mod[model+'_'+ens] = temp_mod[res,:,:]
    else:
        d_temp_mod[model+'_'+ens] = temp_mod[:]
    model_names.append(model+'_'+ens)
    ncfile.close()
    f = f+1

obsfile.close()
model_names.append("Obs")

#calculate rmse
if region!=None:
    print 'Region is '+ region
    area = region
    mask=np.loadtxt('/home/rlorenz/scripts/plot_scripts/areas_txt/'+region+'.txt')
    #if any region defined, cut data into region before calculating rmse
    temp_obs_reg = np.ndarray((temp_obs.shape))
    d_temp_mod_reg = {}
    for ilat in xrange(len(lat)):
        for ilon in xrange(len(lon)):
            if (point_inside_polygon(lat[ilat],lon[ilon],np.fliplr(mask))):
                temp_obs_reg[ilat,ilon] = temp_obs[ilat,ilon]
            else:
                temp_obs_reg[ilat,ilon] = np.NaN
    for key, value in d_temp_mod.iteritems():
        tmp = np.ndarray((value.shape))
        for ilat in xrange(len(lat)):
            for ilon in xrange(len(lon)):
                if (point_inside_polygon(lat[ilat],lon[ilon],np.fliplr(mask))):
                    tmp[ilat,ilon] = value[ilat,ilon]
                else:
                    tmp[ilat,ilon] = np.NaN
        d_temp_mod_reg[key] = tmp
    rmse_all = func_calc_rmse(d_temp_mod_reg,temp_obs_reg,lat,lon,model_names[:-1],var_kind[0])
else:
    area = 'GLOBAL'
    rmse_all = func_calc_rmse(d_temp_mod,temp_obs,lat,lon,model_names[:-1],var_kind[0])

print "Save deltas to netcdf"
fout=nc.Dataset('%s%s_%s_%s_%s_%s_RMSE_all_%s_%s-%s.nc' 
                %(outdir,variable[0],var_file[0],var_kind[0],area,obsdata,experiment,syear,eyear),mode='w')
fout.createDimension('x',len(model_names))
fout.createDimension('y',len(model_names))

xout = fout.createVariable('x','f8',('x'),fill_value = 1e20)
setattr(xout,'Longname','ModelNames')
yout = fout.createVariable('y','f8',('y'),fill_value = 1e20)
setattr(yout,'Longname','ModelNames')

rmseout = fout.createVariable('rmse','f8',('x','y'),fill_value = 1e20)
setattr(rmseout,'Longname','Root Mean Squared Error')
setattr(rmseout,'units','-')
setattr(rmseout,'description','Root mean squared error in between models and models and reference') 

xout[:] = range(len(model_names))[:]
yout[:] = range(len(model_names))[:]
rmseout[:] = rmse_all[:]

# Set global attributes
setattr(fout,"author","Ruth Lorenz @IAC, ETH Zurich, Switzerland")
setattr(fout,"contact","ruth.lorenz@env.ethz.ch")
setattr(fout,"creation date",dt.datetime.today().strftime('%Y-%m-%d'))
setattr(fout,"comment","")

setattr(fout,"Script","calc_rmse_for_weight_beyond_democracy.py")
setattr(fout,"Input files located in:", archive)
fout.close()

#save model names into separate text file
with open('%s%s_%s_%s_%s_%s_RMSE_all_%s_%s-%s.txt' %(outdir,variable[0],var_file[0],var_kind[0],area,obsdata,experiment,syear,eyear), "w") as text_file:
        for m in range(len(model_names)):
                text_file.write(model_names[m]+"\n")

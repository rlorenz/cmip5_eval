#!/usr/bin/python
'''
File Name : tas_cmip5-ng.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 25-02-2016
Modified: Mon 29 Feb 2016 02:53:19 PM CET
Purpose: plot tas from cmip5-ng archive


'''
import netCDF4 as nc # to work with NetCDF files
import numpy as np
import datetime as dt
from math import atan
from netcdftime import utime
import calendar
import matplotlib.pyplot as plt
from os.path import expanduser
home = expanduser("~") # Get users home directory
import os # operating system interface
import glob
import sys
sys.path.insert(0,home+'/scripts/plot_scripts/utils/TaylorDiag/')
from taylorDiagram import TaylorDiagram
sys.path.insert(0,home+'/scripts/plot_scripts/utils/')
from func_read_data import func_read_netcdf
from point_inside_polygon import point_inside_polygon
from calc_RMSE_obs_mod_3D import rmse_3D
import itertools
from mpl_toolkits.basemap import maskoceans, shiftgrid
###
# Define input
###
variable = 'tasmax'
obsdata = 'ERAint'
archive = '/net/atmos/data/cmip5-ng'
workdir = '/net/tropo/climphys/rlorenz/cmip5-ng/work/'
outdir = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/%s/' %(variable)
infile = variable+'_mon'
syear = 1980
eyear = 2014
nyears = eyear - syear + 1

exp = 'rcp85'
grid = 'g025'
#for testing
#model = 'CESM1-CAM5'
ens = 'r1i1p1'

region = 'NAM'

if (os.access(outdir,os.F_OK)==False):
        os.makedirs(outdir)
#if (os.access(workdir,os.F_OK)==False):
#        os.makedirs(workdir)

#os.system('cd '+workdir)
#read obs data
if (obsdata == 'ERAint'):
        print "Read ERAint data"
        path = '/net/tropo/climphys/rlorenz/Datasets/ERAint/ERAint_mon'
        if (variable=='tas'):
            temp = func_read_netcdf('%s/T2M_monthly_ERAint_197901-201601_%s.nc'
                                    %(path,grid),'T2M',var_units=True,
                                    syear=syear, eyear=eyear)
            temp_obs = temp['data']
            obs_unit = temp['unit']
        elif (variable=='pr'):
            temp = func_read_netcdf('%s/RTOT_monthly_ERAint_197901-201412_%s.nc'
                                    %(path,grid),'RTOT',var_units=True,
                                    syear=syear, eyear=eyear)
            temp_obs = temp['data']
            obs_unit = temp['unit']
        elif (variable=='sic'):
            temp = func_read_netcdf('%s/ci_monthly_ERAint_197901-201601_%s.nc'
                                    %(path,grid),'ci',syear=syear, eyear=eyear)
            #ERAint data is in fraction -> convert to percent as in CMIP5
            temp_obs = temp['data']*100
            obs_unit = 'percent'
        elif (variable=='hfls'):
            temp = func_read_netcdf('%s/slhf_monthly_ERAint_197901-201512_%s.nc'
                                    %(path,grid),'slhf',var_units=True,
                                    syear=syear, eyear=eyear)
            temp_obs = -temp['data']
            obs_unit = temp['unit']
        elif (variable=='tasmax'):
            temp = func_read_netcdf('%s/T2MAX_monthly_ERAint_197901-201412_%s.nc'
                                    %(path,grid),'T2MAX',
                                    syear=syear, eyear=eyear)
            temp_obs = temp['data']+273.15
            obs_unit = 'K'
elif (obsdata == 'MERRA2'):
        print "Read MERRA2 data"
        path = '/net/tropo/climphys/rlorenz/Datasets/MERRA2/monthly'

        if (variable=='tas'):
            temp = func_read_netcdf('%s/MERRA2.tavgmon_2d_slv_Nx.T2MMEAN.1980-2015_remapbil_%s.nc'
                                    %(path,grid),'T2MMEAN',var_units=True,
                                    syear=syear, eyear=eyear)
        elif (variable=='tasmax'):
            temp = func_read_netcdf('%s/MERRA2.tavgmon_2d_slv_Nx.T2MMAX.1980-2015_remapbil_%s.nc'
                                    %(path,grid),'T2MMAX',var_units=True,
                                    syear=syear, eyear=eyear)
        elif (variable=='tasmin'):
            temp = func_read_netcdf('%s/MERRA2.tavgmon_2d_slv_Nx.T2MMIN.1980-2015_remapbil_%s.nc'
                                    %(path,grid),'T2MMIN',var_units=True,
                                    syear=syear, eyear=eyear)
        elif (variable=='hfls'):
            temp = func_read_netcdf('%s/MERRA2.tavgmon_2d_flx_Nx.EFLUX.1980-2015_remapbil_%s.nc'
                                    %(path,grid),'EFLUX',var_units=True,
                                    syear=syear, eyear=eyear)
        elif (variable=='hfss'):
            temp = func_read_netcdf('%s/MERRA2.tavgmon_2d_flx_Nx.HFLUX.1980-2015_remapbil_%s.nc'
                                    %(path,grid),'HFLUX',var_units=True,
                                    syear=syear, eyear=eyear)
        elif (variable=='slp'):
            temp = func_read_netcdf('%s/MERRA2.tavgmon_2d_slv_Nx.SLP.1980-2015_remapbil_%s.nc'
                                    %(path,grid),'SLP',var_units=True,
                                    syear=syear, eyear=eyear)
        temp_obs = temp['data']
        obs_unit = temp['unit']
elif (obsdata == 'Obs'):
        print "Read Obs data"
        path = '/net/tropo/climphys/rlorenz/Datasets/'

        if (variable == 'tas'):
            temp = func_read_netcdf(
                   '%s/BerkeleyEarth/Complete_TAVG_LatLong_%s.nc'
                   %(path,grid), 'TAVG', var_units = True,
                   syear = syear, eyear = eyear)
            temp_obs = temp['data']
            obs_unit = temp['unit']
        elif (variable == 'tasmax'):
            temp = func_read_netcdf(
                   '%s/HadGHCND/monthly/HadGHCND_TXTN_avg_monthly_acts_195001-201412_remapbil_%s.nc'
                   %(path, grid), 'tmax', var_units = True,
                   syear = syear, eyear = eyear)
            temp_obs = temp['data']
            obs_unit = temp['unit']
            if ((obs_unit == 'decC') or (obs_unit == 'C') or (obs_unit == 'degrees C')):
                ## convert to K
                tmp_obs = temp_obs + 273.15
                temp_obs = tmp_obs[:]
                del tmp_obs
                obs_unit = 'K'
        elif (variable == 'hfls'):
            temp = func_read_netcdf(
                   '%s/GLEAM/monthly/GLEAM_E_monthly_v3.0a_1980-2014_%s.nc' %(
                   path, grid), 'E', var_units = True, syear = syear,
                   eyear = eyear)
            temp_obs = temp['data']
            obs_unit = temp['unit']
            #tmp_file = '%s/LandFluxEVAL/LandFluxEVAL.merged.89-05.monthly.diagnostic.%s.nc' %(path, grid)
            #ifile = nc.Dataset(tmp_file, mode = 'r')
            #obs_unit = ifile.variables['ET_mean'].units
            #time = ifile.variables['time'][:]
            #time_str = [str(i) for i in time]
            #orig_years = np.asarray([int(y[0:4]) for y in time_str])
            #months = np.asarray([int(y[4:6]) for y in time_str])
            #temp_obs = ifile.variables['E'][(orig_years >= syear) &
            #                                (orig_years <= eyear) , :, :]
            #obsmonths = months[(orig_years >= syear) & (orig_years <= eyear)]
            #obsyears = orig_years[(orig_years >= syear) & (orig_years <= eyear)]
            #obslat = ifile.variables['lat'][:]
            #obslon = ifile.variables['lon'][:]
            #ifile.close()
            #del orig_years
            if ((obs_unit == 'mm/d') or (obs_unit == 'mm/day')):
                ## convert to W m^-2
                factor = 2.5 * 10 ** 6 / (24 * 60 * 60)
                tmp_obs = np.multiply(temp_obs, factor)
                temp_obs = tmp_obs[:]
                del tmp_obs
                obs_unit = 'Wm-2'
        elif (variable == 'psl'):
            print "Warning: no obs data for this variable, exiting"
            sys.exit
        elif (variable == 'ef'):
            print "Warning: no obs data for this variable, exiting"
            sys.exit
        elif (variable == 'rsus'):
            temp = func_read_netcdf(
                    '%s/CERES/CERES_EBAF-sfc_net_sw_all_mon_Ed2.8_Subset_200003-201511_%s.nc'
                    %(path, grid), 'sfc_net_sw_all_mon', var_units = True,
                    syear = syear, eyear = eyear)
            temp_obs = temp['data']
            obs_unit = temp['unit']
        elif (variable == 'rlus'):
            temp = func_read_netcdf(
                    '%s/CERES/CERES_EBAF-sfc_net_lw_all_mon_Ed2.8_Subset_200003-201511_%s.nc'
                    %(path, grid), 'sfc_net_lw_all_mon', var_units = True,
                    syear = syear, eyear = eyear)
            temp_obs = temp['data']
            obs_unit = temp['unit']
        else:
            print "Warning: no obs data for this variable, exiting"
            sys.exit
else:
        print'Wrong obsdata specified, not available, exit'
        sys.exit

obslat = temp['lat']
obslon = temp['lon']
obsdates = temp['dates']
obsmonths = np.asarray([obsdates[i].month for i in xrange(len(obsdates))])
obsyears = np.asarray([obsdates[i].year for i in xrange(len(obsdates))])

if isinstance(temp_obs, np.ma.core.MaskedArray):
        temp_obs = temp_obs.filled(np.NaN)

if (obslon[0] >= 0.0) and (obslon[-1] > 180):
    #flip lon for point_inside_polygon
    obslon_orig = obslon[:]
    temp_obs, obslon =  shiftgrid(180., temp_obs, obslon, start = False)

#if any region defined, read region extent from file and cut data into region
if region!=None:
        print 'Region is %s' %(region)
        area = region
        mask=np.loadtxt('/home/rlorenz/scripts/plot_scripts/areas_txt/%s.txt' %(region))
        temp_obs_reg_nan = np.ndarray((temp_obs.shape))
        for ilat in xrange(len(obslat)):
                for ilon in xrange(len(obslon)):
                        if (point_inside_polygon(obslat[ilat],obslon[ilon],np.fliplr(mask))):
                                temp_obs_reg_nan[:,ilat,ilon] = temp_obs[:,ilat,ilon]
                        else:
                                temp_obs_reg_nan[:,ilat,ilon] = np.NaN
        idx_lat = np.where((obslat>mask[2,1]) & (obslat<mask[0,1]))
        obslat = obslat[idx_lat]
        tmp1_obs_reg = np.empty((len(temp_obs_reg_nan),len(obslat),len(obslon)))
        tmp1_obs_reg.fill(np.NaN)
        idx_lon = np.where((obslon>mask[0,0]) & (obslon<mask[1,0]))
        obslon = obslon[idx_lon]
        temp_obs_reg = np.empty((len(temp_obs_reg_nan),len(obslat),len(obslon)))
        temp_obs_reg.fill(np.NaN)
        tmp1_obs_reg = np.squeeze(temp_obs_reg_nan[:,idx_lat,:],1)
        temp_obs_reg = np.squeeze(tmp1_obs_reg[:,:,idx_lon],2)

        #del temp_obs
else:
        area = 'GLOBAL'
        temp_obs_reg = temp_obs

###read model data
##find all matching files in archive, loop over all of them
#first count matching files in folder to initialze varible rmse per model:
name = '%s/%s/%s_*_%s_%s_%s.nc' %(archive,variable,infile,exp,ens,grid)
nfiles = len(glob.glob(name))
model_names = []
d_bias = {}
d_bias_glob = {}
d_rmse = {}
d_temp_std_glob = {}
d_temp_corrcoef = {}
print str(nfiles)+ 'matching files found'
for filename in glob.glob(name):
    fh = nc.Dataset(filename,mode='r')
    lon = fh.variables['lon'][:]
    lat = fh.variables['lat'][:]
    time = fh.variables['time']
    print "Create dates from netcdf time for model"
    nccalendar = time.calendar
    cdftime = utime(time.units,calendar=time.calendar)
    dates=cdftime.num2date(time[:])
    years=np.asarray([dates[i].year for i in xrange(len(dates))])
    dates=dates[(years>=syear) & (years<=eyear)]
    months = np.asarray([dates[i].month for i in xrange(len(dates))])
    print "Read "+filename+" data"
    temp_mod= fh.variables[variable][(years>=syear) & (years<=eyear),:,:]

    # shiftgrid to -180 to 180 if necessary (to be compatible with area mask)
    if (lon[0] >= 0.0) and (lon[-1] > 180):
        lons_orig = lon[:]
        temp_mod, lon = shiftgrid(180., temp_mod, lon, start = False)

    #check that time axis and grid is identical for obs and model
    if temp_obs.shape != temp_mod.shape:
        print 'Warning: Dimension for model and ERAint is different!'
        continue
    if (dates[0].year != obsdates[0].year) or (dates[0].month != obsdates[0].month):
        print 'time axis for model and ERAint is not identical, exiting!'
        print 'model starts '+str(dates[0].month)+' '+str(dates[0].year)+', while ERAint starts '+str(obsdates[0].month)+' '+str(obsdates[0].year)
        sys.exit
    
    model = fh.source_model
    model_names.append(model)

     #if any region defined, cut data into region
    if region != None:
        temp_mod_reg_nan = np.ndarray((temp_mod.shape))
        for ilat in xrange(len(lat)):
                for ilon in xrange(len(lon)):
                        if (point_inside_polygon(lat[ilat],lon[ilon],np.fliplr(mask))):
                                temp_mod_reg_nan[:,ilat,ilon] = temp_mod[:,ilat,ilon]
                        else:
                                temp_mod_reg_nan[:,ilat,ilon] = np.NaN
        idx_lat = np.where((lat>mask[2,1]) & (lat<mask[0,1]))
        lat = lat[idx_lat]
        tmp1_mod_reg = np.empty((len(temp_mod_reg_nan),len(lat),len(lon)))
        tmp1_mod_reg.fill(np.NaN)
        idx_lon = np.where((lon>mask[0,0]) & (lon<mask[1,0]))
        lon = lon[idx_lon]
        temp_mod_reg = np.empty((len(temp_mod_reg_nan),len(lat),len(lon)))
        temp_mod_reg.fill(np.NaN)
        tmp1_mod_reg = np.squeeze(temp_mod_reg_nan[:,idx_lat,:],1)
        temp_mod_reg = np.squeeze(tmp1_mod_reg[:,:,idx_lon],2)
        del temp_mod
    else:
        temp_mod_reg = temp_mod
   
    temp_mod_clim = np.ndarray((12,len(lat),len(lon)))
    temp_obs_clim = np.ndarray((12,len(lat),len(lon)))
    #calculate climatology
    for m in range(1,13):
        M = m-1
        temp_mod_clim[M,:,:] = np.mean(temp_mod_reg[months==m,:,:], axis = 0)
        temp_obs_clim[M,:,:] = np.mean(temp_obs_reg[months==m,:,:], axis = 0)

    d_bias[model] = np.mean(temp_mod_clim, axis = 0) - np.mean(temp_obs_clim,
                                                               axis = 0)
    d_bias_glob[model] = np.mean(d_bias[model])

    #calculate rms
    print "Calculate RMSE"
    d_rmse[model] = rmse_3D(temp_obs_clim,temp_mod_clim,lat,lon,tp = "cyc")
    print d_rmse[model]

    #calculate standard deviation
    temp_mod_std = np.nanstd(temp_mod_clim,axis = 0)
    temp_obs_std = np.nanstd(temp_obs_clim,axis = 0)

    #calculate area means, so far global
    d_temp_std_glob[model] = np.nanmean(temp_mod_std)
    d_temp_std_glob[obsdata] = np.nanmean(temp_obs_std)

    #calculate correlation coefficient
    ind_miss1 = np.isfinite(temp_obs_clim)
    d_temp_corrcoef[model] = np.corrcoef(temp_mod_clim[ind_miss1].ravel(),
                                         temp_obs_clim[ind_miss1].ravel())[0,1]
    print d_temp_corrcoef[model]

    fh.close()

#save RMSE into netcdf for further use
#fout=nc.Dataset('%srmse_%s_%s_%s' %(outdir,variable,syear,eyear),mode='w')

#print data into TaylorDiagram for visualization
fig = plt.figure(figsize=(10.5,8))
#fig.suptitle("Title", size='x-large')

# Taylor diagram
dia = TaylorDiagram(d_temp_std_glob[obsdata], fig=fig, rect=111, label="Reference")

colors = plt.matplotlib.cm.viridis(np.linspace(0,1,nfiles))
shape_list = ['D', '8', 'p', 'o', '*','s','H','h','v','^','<','>']
markers = itertools.cycle(shape_list)
print markers
# Add samples to Taylor diagram
for n in range(len(model_names)):
    dia.add_sample(d_temp_std_glob[model_names[n]], d_temp_corrcoef[model_names[n]], marker=markers.next(), markersize = 8, ls='', c=colors[n], label=model_names[n])

# Add RMS contours, and label them
contours = dia.add_contours(colors='0.5')
plt.clabel(contours, inline=1, fontsize=10)

# Add a figure legend
fig.legend(dia.samplePoints,[ p.get_label() for p in dia.samplePoints ],numpoints=1, prop=dict(size='small'), loc='upper right',bbox_to_anchor=(1.01, 1.01))

#plt.show()
plt.savefig(outdir+variable+'_'+exp+'_taylorDiag_'+obsdata+'_'+area+'_'+str(syear)+'-'+str(eyear)+'.pdf')

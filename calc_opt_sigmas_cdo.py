#!/usr/bin/python
'''
File Name : calc_opt_sigmas_cdo.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 08-02-2017
Modified: Wed 08 Feb 2017 11:59:09 AM CET
Purpose: calculate optimal sigmas for weighting
	 multi model mean, diagnostics precalculated
         using cdo in calc_diag_cmip5_cdo.py  

'''
import netCDF4 as nc # to work with NetCDF files
import numpy as np
from netcdftime import utime
import datetime as dt
#from scipy import signal
import math
import os # operating system interface
from os.path import expanduser
home = expanduser("~") # Get users home directory
import glob
import sys
sys.path.insert(0, '%s/scripts/plot_scripts/utils/' %(home))
from func_read_data import func_read_netcdf
from func_calc_wu_wq import calc_wu, calc_wq, calc_weights_approx 
import matplotlib.pyplot as plt
from OrderedSet import OrderedSet
import json

# for py3 forward compatibility
from builtins import range
from io import open

import logging
from info_utils import set_logger
logger = logging.getLogger(__name__)
set_logger(level = logging.INFO)
logger.info('Start')
###
# Define input
###
target_var = 'tas'
# climatology: CLIM, variability: STD, trend: TREND
target_file = 'CLIM'
res_name_target = 'JJA'
target_mask = 'maskT'
res_time_target = 'MEAN'
freq = 'mon'

# multiple variables possible but deltas need to be available
diag_var = ['tas', 'pr']
# climatology:clim, variability:std, trend:trnd
var_file = ['CLIM', 'CLIM']
# use ocean masked RMSE?
masko = ['maskT', 'maskT', 'maskT', 'maskT', 'maskT', 'maskT', 'maskT']
# kind is cyc: annual cycle, mon: monthly values, seas: seasonal
res_name = ['JJA', 'JJA', 'JJA', 'JJA', 'JJA', 'JJA', 'JJA']
res_time = ['MEAN', 'MEAN', 'MEAN', 'MEAN', 'MEAN', 'MEAN', 'MEAN']
freq_v = ['mon', 'mon', 'mon', 'mon', 'mon', 'mon', 'mon']

# weight of individual fields, all equal weight 1 at the moment
fields_weight = [1, 1] #[1, 1, 1, 1, 1, 1, 1]

# choose region if required
region = 'EUR_3SREX'
if region != None:
    area = region
else:
    area = 'GLOBAL'

syear_eval = 1951
eyear_eval = 2005
nyears = eyear_eval - syear_eval + 1

experiment = 'rcp85'

obsdata = 'Obs' #ERAint or MERRA2
err = 'RMSE' #'perkins_SS', 'RMSE'
err_var = 'rmse' #'SS', 'rmse'

## method to calculate optimal sigmas, correlation or IError or inpercentile
# IError needs obs data!
method = 'inpercentile'  

indir = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/'
outdir = '%sEval_Weight/%s/sigmas/' %(indir, target_var)
if (os.access(outdir, os.F_OK) == False):
    os.makedirs(outdir)

sigma_size = 41
ndiag = len(diag_var)

###
# Read data
###
## read rmse for var_file
## first read model names for all var_file from text file
## select all models that occur for all var_files,
## or if testing select number of simulations defined in test
rmse_models = dict()
overlap = list()
rmse_file_target = '%s%s/%s/%s/%s_%s_all_%s_%s-%s' %(
    indir, target_var, freq, target_mask, target_var, res_name_target,
    experiment, syear_eval, eyear_eval)
overlap = np.genfromtxt(rmse_file_target + '.txt', delimiter = '',
                        dtype = None).tolist()

for v in range(len(var_file)):
    rmsefile = '%s%s/%s/%s/%s_%s_%s_all_%s_%s-%s' %(
        indir, diag_var[v], freq_v[v], masko[v], diag_var[v], var_file[v],
        res_name[v], experiment, syear_eval, eyear_eval)
    if (os.access(rmsefile + '.txt', os.F_OK) == True):
        rmse_models[var_file[v]] = np.genfromtxt(rmsefile + '.txt',
                                                 delimiter = '',
                                                 dtype = None).tolist()[: - 1]
        overlap = list(OrderedSet(rmse_models[var_file[v]]) & OrderedSet(overlap))
        try:
            overlap = overlap[0:test]
        except (NameError):
            pass

indices = dict()
rmse_d = np.ndarray((ndiag, len(overlap), len(overlap)))
rmse_q = np.ndarray((ndiag, len(overlap)))
for v in range(ndiag):
    rmsefile = '%s%s/%s/%s/%s_%s_%s_all_%s_%s-%s' %(
        indir, diag_var[v], freq_v[v], masko[v], diag_var[v], var_file[v],
        res_name[v], experiment, syear_eval, eyear_eval)
    ## find indices of model_names in rmse_file
    rmse_models[var_file[v]] = np.genfromtxt(rmsefile + '.txt', delimiter = '',
                                             dtype = None).tolist()[: - 1]
    for m in range(len(overlap)):
        indices[overlap[m]] = rmse_models[var_file[v]].index(overlap[m])
    ind = sorted(indices.values())
    model_names = sorted(indices.keys())

    if (os.access('%s%s/%s/%s/%s_%s_%s_all_%s_%s-%s_%s_%s_%s.nc' %(
        indir, diag_var[v], freq_v[v], masko[v], diag_var[v], var_file[v], 
        res_name[v], experiment, syear_eval, eyear_eval, area, obsdata, err),
            os.F_OK) == True):
        logger.info('%s already exist, read from netcdf' %err)
        fh = nc.Dataset('%s%s/%s/%s/%s_%s_%s_all_%s_%s-%s_%s_%s_%s.nc' %(
            indir, diag_var[v], freq_v[v], masko[v], diag_var[v], var_file[v],
            res_name[v], experiment, syear_eval, eyear_eval, area, obsdata,
            err), mode = 'r')
        rmse_all = fh.variables[err_var]
        rmse_d[v, :, :] = rmse_all[ind, ind]
        rmse_q[v, :] = rmse_all[ - 1, ind]
        fh.close()
    else:
        logger.error('RMSE delta matrix does not exist yet, exiting')
        sys.exit
delta_u = np.ndarray((ndiag, len(model_names), len(model_names)))
delta_q = np.ndarray((ndiag, len(model_names)))
for v in range(ndiag):
    ## normalize rmse by median
    med = np.nanmedian(rmse_d[v, :, :])
    delta_u[v, :, :] = rmse_d[v, :, :] / med
    delta_q[v, :] = rmse_q[v, :] / med
    
## average deltas over fields,
## taking field weight into account (all 1 at the moment)
field_w_extend_u = np.reshape(np.repeat(fields_weight,
                                        len(model_names) * len(model_names)),
                              (len(fields_weight), len(model_names),
                               len(model_names)))
delta_u = np.sqrt(np.nansum(field_w_extend_u * delta_u, axis = 0)
                  / np.nansum(fields_weight))
    
field_w_extend_q = np.reshape(np.repeat(fields_weight, len(model_names)),
                              (len(fields_weight), len(model_names)))
delta_q = np.sqrt(np.nansum(field_w_extend_q * delta_q, axis = 0)
                  / np.nansum(fields_weight))

if (method == 'IError'):
    if region:
        obsfile = '%s%s/%s/%s/%s_%s_%s_%s-%s_%s%s_%s_%s.nc' %(
            indir, target_var, freq, target_mask, target_var, freq,
            obsdata, syear_eval, eyear_eval, res_name_target, res_time_target, 
            'CLIM', region)
    else:
        obsfile = '%s%s/%s/%s/%s_%s_%s_%s-%s_%s%s_%s.nc' %(
            indir, target_var, freq, target_mask, target_var, freq, obsdata,
            syear_eval, eyear_eval, res_name_target, res_time_target, 'CLIM')
    fh = nc.Dataset(obsfile, mode = 'r')
    temp_obs_clim = fh.variables[diag_var[0]][:]
    lat = fh.variables['lat'][:]
    fh.close()
    rad = 4.0 * math.atan(1.0) / 180
    w_lat = np.cos(lat * rad) # weight for latitude differences in area
    if region:
        obsfile = '%s%s/%s/%s/%s_%s_%s_%s-%s_%s%s_%s_%s.nc' %(
            indir, target_var, freq, target_mask, target_var, freq,
            obsdata, syear_eval, eyear_eval, res_name_target, res_time_target,
            'STD', region)
    else:
        obsfile = '%s%s/%s/%s/%s_%s_%s_%s-%s_%s%s_%s.nc' %(
            indir, target_var, freq, target_mask, target_var, freq, obsdata,
            syear_eval, eyear_eval, res_name_target, res_time_target, 'STD')
    fh = nc.Dataset(obsfile, mode = 'r')
    temp_obs_std = fh.variables[diag_var[0]][:]
    fh.close()

    ma_temp_obs_clim = np.ma.masked_array(np.squeeze(temp_obs_clim),
                                          np.isnan(temp_obs_clim))
    tmp_latweight = np.ma.average(ma_temp_obs_clim, axis = 0,
                                  weights = w_lat)
    temp_obs_clim_areaavg = np.nanmean(tmp_latweight.filled(np.nan))

    ma_temp_obs_std = np.ma.masked_array(np.squeeze(temp_obs_std),
                                         np.isnan(temp_obs_std))
    tmp_latweight = np.ma.average(ma_temp_obs_std, axis = 0, weights = w_lat)
    temp_obs_std_areaavg = np.nanmean(tmp_latweight.filled(np.nan))

### read model data
## same models as in rmse file
logger.info('Read model data')
d_temp_mod_ts_areaavg = dict()
d_temp_mod_areaavg = dict()
nfiles = len(model_names)
logger.info('%s matching files' %(str(nfiles)))
for f in range(len(model_names)):
    model = model_names[f].split('_', 1)[0]
    ens = model_names[f].split('_', 1)[1]
    if region:
        modfile_ts = '%s%s/%s/%s/%s_%s_%s_%s_%s_%s-%s_%s%s_%s.nc' %(
            indir, target_var, freq, target_mask, target_var, freq, model,
            experiment, ens, syear_eval, eyear_eval, res_name_target,
            res_time_target, region)
    else:
        modfile_ts = '%s%s/%s/%s/%s_%s_%s_%s_%s_%s-%s_%s%s.nc' %(
            indir, target_var, freq, target_mask, target_var, freq, model,
            experiment, ens, syear_eval, eyear_eval, res_name_target,
            res_time_target)

    fh = nc.Dataset(modfile_ts, mode = 'r')
    temp_mod_ts = fh.variables[target_var][:]
    lat = fh.variables['lat'][:]
    lon = fh.variables['lon'][:]
    fh.close()
    if region:
        modfile = '%s%s/%s/%s/%s_%s_%s_%s_%s_%s-%s_%s%s_%s_%s.nc' %(
            indir, target_var, freq, target_mask, target_var, freq, model,
            experiment, ens, syear_eval, eyear_eval, res_name_target,
            res_time_target, target_file, region)
    else:
        modfile = '%s%s/%s/%s/%s_%s_%s_%s_%s_%s-%s_%s%s_%s.nc' %(
            indir, target_var, freq, target_mask, target_var, freq, model,
            experiment, ens, syear_eval, eyear_eval, res_name_target,
            res_time_target, target_file)
    fh = nc.Dataset(modfile, mode = 'r')
    temp_mod = fh.variables[target_var][:]
    fh.close()

    rad = 4.0 * math.atan(1.0) / 180
    w_lat = np.cos(lat * rad) # weight for latitude differences in area

    ## calculate weighted area average
    ma_temp_mod_ts = np.ma.masked_array(temp_mod_ts, np.isnan(temp_mod_ts))
    tmp_latweight = np.ma.empty((nyears, len(lon)))
    for ilon in xrange(len(lon)):
        tmp_latweight[:, ilon] = np.ma.average(ma_temp_mod_ts[:, :, ilon],
                                               axis = 1, weights = w_lat)
    d_temp_mod_ts_areaavg[model + '_' + ens] = np.nanmean(tmp_latweight.filled(np.nan), axis = 1)

    ma_temp_mod = np.ma.masked_array(temp_mod, np.isnan(temp_mod))
    tmp_latweight = np.ma.average(np.squeeze(ma_temp_mod), axis = 0,
                                  weights = w_lat)
    d_temp_mod_areaavg[model + '_' + ens] = np.nanmean(tmp_latweight.filled(np.nan))
    del tmp_latweight

logger.info('Find optimal sigmas')
## get sigmas, test over range of sigmas and find either:
## ideal correlation of weighted mean with original values
## (method = 'correlation')
## largest IError metric between weighted mean and unweighted mean
## (method = 'IError')
## calculate which sigmas result in values in between 90% percentile
## (method = 'inpercentile')
tmp = np.mean(delta_u)
sigma_S2 = np.linspace(tmp - 0.9 * tmp, tmp + 0.9 * tmp, sigma_size)  # wu
sigma_D2 = np.linspace(tmp - 0.9 * tmp, tmp + 0.9 * tmp, sigma_size)  # wq

## for perfect model approach only use one ensemble per model
ind_ens1 = [i for i in range(len(model_names)) if not model_names[i].endswith('r1i1p1')]
delta_u_ens1 = np.delete(delta_u, ind_ens1, axis = 0)
model_names_ens1 = np.delete(model_names, ind_ens1, axis = 0)
w_u = calc_wu(delta_u_ens1, model_names_ens1, sigma_S2)
w_q = calc_wq(delta_u_ens1, model_names_ens1, sigma_D2)
logger.info('wu and wq calculated for all sigmas')

temp_mod_ts_ens1 = [value for (key, value) in sorted(iteritems(d_temp_mod_ts_areaavg)) if 'r1i1p1' in key]
model_keys_ts = [key for (key, value) in sorted(iteritems(d_temp_mod_ts_areaavg)) if 'r1i1p1' in key]

ntim = temp_mod_ts_ens1[0].shape[0]

temp_mod_avg_ens1 = [value for (key, value) in sorted(iteritems(d_temp_mod_areaavg)) if 'r1i1p1' in key]
model_keys = [key for (key, value) in sorted(iteritems(d_temp_mod_areaavg)) if 'r1i1p1' in key]

logger.debug('Number of models: %s' %(len(temp_mod_avg_ens1)))
logger.debug('Model keys: %s' %model_keys)
logger.info('Calculate approximations and weights')
tmp_wmm_ts = calc_weights_approx(w_u, w_q, model_keys_ts, temp_mod_ts_ens1)
tmp_wmm_avg = calc_weights_approx(w_u, w_q, model_keys, temp_mod_avg_ens1)

if (method == 'correlation'):
    from func_calc_corr import calc_corr
    logger.info('Calculate correlations to determine optimal sigmas')
    approx = tmp_wmm_ts['approx']
    corr = calc_corr(sigma_S2, sigma_D2, approx, np.array(temp_mod_ts_ens1,
                                                          dtype = float),
                     model_names_ens1)
    ## calculate indices of optimal sigmas
    ind1, ind2 = np.unravel_index(np.argmax(corr), corr.shape)
    sigma_S2_end = sigma_S2[ind1]
    sigma_D2_end = sigma_D2[ind2]
    logger.info('Optimal sigmas are %s and %s' %(str(sigma_S2_end), str(sigma_D2_end)))

    ## plot optimal sigmas in correlation sigma space
    levels = np.arange(np.min(corr), np.max(corr), 0.01)
    fig = plt.figure(figsize=(10, 8), dpi=300)
    cax = plt.contourf(sigma_D2, sigma_S2, corr, levels, cmap = plt.cm.YlOrRd,
                       extend = 'both')
    cbar = plt.colorbar(cax, orientation = 'vertical')
    lx = plt.xlabel('$\sigma_D$', fontsize = 18)
    ly = plt.ylabel('$\sigma_S$', fontsize = 18)
    sax = plt.scatter(sigma_D2_end, sigma_S2_end, marker = 'o', c = 'k',s = 5)
    plt.savefig('%s/corr_sigmas_%s_%s_%s.pdf' %(outdir, target_var,
                                                diag_var[-1], area))
elif (method == 'IError'):
    from func_eval_wmm_nonwmm_error_indexI import error_indexI
    logger.info('Calculate error Index I to determine optimal sigmas')
    tmp_mm = np.nanmean(np.array(temp_mod_ts_ens1, dtype = float), axis = 0)
    I2_sigmas = np.empty((len(sigma_S2),len(sigma_D2)))
    I2_sigmas.fill(np.NaN)
    for s2 in range(len(sigma_S2)):
        for d2 in range(len(sigma_D2)):
            ## calculate average weight from perfect model approach
            weights = np.nanmean(tmp_wmm_ts['weights'][s2, d2, :, :], axis = 0)
            ## calculate wmm for this sigma pair
            tmp_mod = 0
            for m in range(len(model_names_ens1)):
                tmp_mod= tmp_mod + tmp_wmm_ts['approx'][s2, d2, m, :] * weights[m]
            tmp_wmm_clim = tmp_mod / np.nansum(weights)
            I2_sigmas[s2, d2] = error_indexI(tmp_wmm_clim, tmp_mm, 
                                             temp_obs_clim_areaavg,
                                             temp_obs_std_areaavg)
    ## calcuate indices of smallest I2 error to determine optimal sigmas
    ind1, ind2 = np.unravel_index(np.argmin(I2_sigmas), I2_sigmas.shape)
    sigma_S2_end = sigma_S2[ind1]
    sigma_D2_end = sigma_D2[ind2]
    logger.info('Optimal sigmas are %s and %s' %(str(sigma_S2_end), str(sigma_D2_end)))
elif (method == 'inpercentile'):
    from func_calc_inpercentile import calc_inpercentile
    logger.info('Use percentile method to determine optimal sigmas')
    test_perc = calc_inpercentile(tmp_wmm_avg['weights'],
                                  np.array(temp_mod_avg_ens1, dtype = float))
    # find smallest sigma values within 90% 80% of time, test_perc[s2, d2]
    ind8 = np.empty((len(sigma_S2)))
    ind8.fill(np.NaN)
    for s2 in range(len(sigma_S2)):
        ind8[s2] = np.where(test_perc[s2, :] >= 0.8)[0][0]
    indmin = np.min(ind8)
    s2 = int(next(x[0] for x in enumerate(ind8) if x[1] == indmin))
    d2 = int(np.min(ind8) - 1)
    sigma_S2_end = sigma_S2[s2]
    sigma_D2_end = sigma_D2[d2]
    logger.info('Optimal sigmas are %s and %s' %(
        str(sigma_S2_end), str(sigma_D2_end)))
    
    # save optimal sigmas to json file
    sigma_S2_out = "%ssigma_S2_inperc_%s_%s_%s_%s%s_%s_%s_%s_%s-%s.txt" %(
        outdir, target_var, target_file, target_mask, ndiag, diag_var[0],
        var_file[0], res_name[0], region, syear_eval, eyear_eval)
    with open(sigma_S2_out, "w") as text_file_S2:
        json.dump(sigma_S2_end, text_file_S2)

    sigma_D2_out = "%ssigma_D2_inperc_%s_%s_%s_%s%s_%s_%s_%s_%s-%s.txt" %(
        outdir, target_var, target_file, target_mask, ndiag, diag_var[0],
        var_file[0], res_name[0], region, syear_eval, eyear_eval)
    with open(sigma_D2_out, "w") as text_file_D2:
        json.dump(sigma_D2_end, text_file_D2)

    # save data to netcdf
    fout = nc.Dataset('%spercent_%s_%s_%s_%s_%s_%s_%s_%s-%s.nc' %(
        outdir, diag_var[0], var_file[0], res_name[0], ndiag, area,
        err, obsdata, syear_eval, eyear_eval), mode = 'w')
    fout.createDimension('S2', len(sigma_S2))
    fout.createDimension('D2', len(sigma_D2))
    s2out = fout.createVariable('S2', 'f8', ('S2'), fill_value = 1e20)
    setattr(s2out, 'Longname', 'sigma quality')
    d2out = fout.createVariable('D2', 'f8', ('D2'), fill_value = 1e20)
    setattr(d2out, 'Longname', 'sigma dependency')
    testout = fout.createVariable('test', 'f8', ('S2', 'D2'), fill_value = 1e20)
    setattr(testout, 'Longname', ' ')
    setattr(testout, 'units', '-')
    setattr(testout, 'description', ' ')

    s2out[:] = sigma_S2[:]
    d2out[:] = sigma_D2[:]
    testout[:] = test_perc[:]

    # Set global attributes
    setattr(fout,"author","Ruth Lorenz @IAC, ETH Zurich, Switzerland")
    setattr(fout,"contact","ruth.lorenz@env.ethz.ch")
    setattr(fout,"creation date", dt.datetime.today().strftime('%Y-%m-%d'))
    setattr(fout, "comment", "Optimal sigmas: S2 = %s and D2 = %s" %(
        round(sigma_S2_end, 4), round(sigma_D2_end, 4)))
    setattr(fout, "Script", "calc_opt_sigmas_cdo.py")
    fout.close()

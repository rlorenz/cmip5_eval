#!/usr/bin/python
'''
File Name : pred_ar_cmip.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 21-03-2018
Modified: Wed 21 Mar 2018 02:26:01 PM CET
Purpose: use autoregresive model to forecast data from cmip5
         use auto_arima to estimate degree of model
         ARIMA can take care of non-stationary time series -> use d >= 1

'''
import numpy as np
import netCDF4 as nc
from netcdftime import utime
import datetime as dt
import glob
import math
import sys
import os # operating system interface
from os.path import expanduser
home = expanduser("~") # Get users home directory
sys.path.insert(0, home + '/scripts/plot_scripts/utils/')
from func_read_data import func_read_netcdf
from pandas import Series
from pandas import DataFrame
from pandas import concat
from statsmodels.tsa.arima_model import ARIMA
import statsmodels.api as sm
from statsmodels.graphics.tsaplots import plot_acf, plot_pacf
import matplotlib.pyplot as plt
from sklearn.metrics import mean_squared_error
import pickle
###
# Define input & output
###
experiment = 'rcp85'
archive = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/'
freq = 'mon'
grid = 'g025'

target_var = 'tas'
# climatology: CLIM, variability: STD, trend: TREND
target_file = 'CLIM'
res_name_target = 'JJA'
target_mask = 'maskT'
res_time_target = 'MEAN'

# cut data over region?
region = 'CNEU'
if region != None:
    area = region
else:
    area = 'GLOBAL'

# include initial conditions ensemble members or not?
ensembles = False

# time range
syear = 1950
eyear = 2100
#ratio how to split train and test data
split_ratio = 0.33

## parameters of ARIMA model
p = 0 # lag order
d = 1 # degree of differencing
q = 1 # order of moving average
d_par = None
# or parameters per model:
#par_in = open("%s_%s_%s_%s_d_opt_aic.pkl" %(target_var, target_file,
#                                              res_name_target, region), "rb")
#d_par = pickle.load(par_in)

# define directories
path = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/%s/%s/%s/' %(
    target_var, freq, target_mask)
outdir = '%sstat_mod_cmip/%s/ARIMA/' %(path, area)
if (os.access(outdir, os.F_OK) == False):
    os.makedirs(outdir)
infile = '%s%s_%s_*_%s_*_%s-%s_%s%s' %(path, target_var, freq, experiment,
                                       syear, eyear, res_name_target,
                                       res_time_target)
###
# Read data
###
### read model data
print "Read model data"
if region:
    name = '%s_%s.nc' %(infile, region)
else:
    name = '%s.nc' %infile
nfiles = len(glob.glob(name))
models_1ens = list()
model_noens = list()
d_ts_pred = dict()
d_ts_test = dict()
d_ts_train = dict()
d_ts_trainpred = dict()
d_aic = dict()
for filename in glob.glob(name):
    print 'Reading data from %s' %filename
    model = filename.split('_')[4]
    ens = filename.split('_')[6]
    if ensembles:
        models_1ens.append(model + '_' + ens)
    else:
        if model not in model_noens:
            if ens == 'r10i1p1' or ens == 'r11i1p1' or ens == 'r12i1p1' or ens == 'r13i1p1':
                print 'We do not want ensemble r1xi1p1 but r1i1p1 from %s' %model
                continue
            else:
                model_noens.append(model)
                models_1ens.append(model + '_' + ens)
        else:
            print '%s already used, not using multiple ensemble members' %model
            continue
    fh = nc.Dataset(filename, mode = 'r')
    tmp = fh.variables[target_var]
    unit = tmp.units
    temp_mod = fh.variables[target_var][:]
    lat = fh.variables['lat'][:]
    lon = fh.variables['lon'][:]
    time = fh.variables['time']
    cdftime = utime(time.units, calendar = time.calendar)
    dates = cdftime.num2date(time[:])
    years = np.asarray([dates[i].year for i in xrange(len(dates))])
    fh.close()
    # area average for time series
    if isinstance(temp_mod, np.ma.core.MaskedArray):
        ma_mod = temp_mod
    else:
        ma_mod = np.ma.masked_array(temp_mod, np.isnan(temp_mod))
    w_lat = np.cos(lat * (4.0 * math.atan(1.0) / 180))
    tmp_latweight = np.ma.average(np.squeeze(ma_mod), axis = 1,
                                  weights = w_lat)
    ts_areaavg = np.nanmean(tmp_latweight.filled(np.nan), axis = 1)
    #break

    series = Series(ts_areaavg, index = years)
    
    #fig = plt.figure(figsize = (10, 5), dpi = 300)
    #plot_acf(series, lags = 50)
    #plt.title('%s %s %s' %(target_var, res_name_target, region))
    #plt.savefig('%sacf_%s_%s_%s_%s_%s-%s_%s.pdf' %(
    #    outdir, target_var, res_name_target, model, ens, syear,
    #    eyear, region))
    #plt.close(fig)
    
    #fig = plt.figure(figsize = (10, 5), dpi = 300)
    #plot_pacf(series, lags = 50)
    #plt.savefig('%spacf_%s_%s_%s_%s_%s-%s_%s.pdf' %(
    #    outdir, target_var, res_name_target, model, ens, syear,
    #    eyear, region))
    #plt.close(fig)
    
    X = series.values
    # split full data into train and test set (only values, no index)
    size = int(len(X) * split_ratio)
    train, test = X[0:size], X[size:len(X)]

    ts_train = Series(ts_areaavg[0 : size], index = years[0 : size])
    ts_test = Series(ts_areaavg[size : len(X)], index = years[size : len(X)])

    #fig = plt.figure(figsize = (10, 5), dpi = 300)
    #plot_acf(ts_train)
    #plt.title('%s %s %s' %(target_var, res_name_target, region))
    #plt.savefig('%sacf_%s_%s_%s_%s_%s-%s_%s.pdf' %(
    #    outdir, target_var, res_name_target, model, ens, years[0],
    #    years[size -1], region))
    #plt.close(fig)
    
    #fig = plt.figure(figsize = (10, 5), dpi = 300)
    #plot_pacf(ts_train)
    #plt.savefig('%spacf_%s_%s_%s_%s_%s-%s_%s.pdf' %(
    #    outdir, target_var, res_name_target, model, ens, years[0],
    #    years[size -1], region))
    #plt.close(fig)
    
    #fig = plt.figure(figsize = (10, 5), dpi = 300)
    #plt.plot(ts_train)
    #plt.xlabel('Year')
    #plt.ylabel('%s %s %s [%s]' %(region, target_var, res_name_target, unit))
    #plt.savefig('%sts_%s_%s_%s_%s_%s-%s_%s.pdf' %(
    #    outdir, target_var, res_name_target, model, ens, years[0],
    #    years[size -1], region))
    #plt.close(fig)
    
    # fit model
    if d_par:
        p = d_par[model + '_' + ens][0]
        q = d_par[model + '_' + ens][1]
    arima_model = ARIMA(train, order = (p, d, q))
    model_fit = arima_model.fit(disp = 0)
    #print(model_fit.summary())
    d_aic[model + '_' + 'ens'] = model_fit.aic
    
    # plot residual errors
    #fig = plt.figure(figsize = (10, 5), dpi = 300)
    #residuals = DataFrame(model_fit.resid)
    #plt.plot(residuals)
    #plt.savefig('%sresiduals_ts_%s_%s_%s_%s_%s.pdf' %(
    #    outdir, target_var, res_name_target, model, ens, region))
    #plt.close(fig)
    
    #print(residuals.describe())

    predictions = model_fit.predict(start = len(train),
                                    end = len(train) + len(test) - 1,
                                    dynamic = False, typ = "levels")
    #for i in range(len(predictions)):
    #    print('predicted = %f, expected = %f' % (predictions[i], test[i]))
    error = mean_squared_error(test, predictions)
    print('Test MSE: %.3f' % error)

    ts_pred = Series(predictions,
                     index = years[len(train) : len(train) + len(test)])

    # plot results
    #fig = plt.figure(figsize = (10, 5), dpi = 300)
    #plt.plot(ts_train, color = 'black')
    #plt.plot(ts_test)
    #plt.plot(ts_pred, color = 'red')
    #plt.title('Train, test, and prediction by ARIMA model (%s, %s, %s)' %(
    #    str(p), str(d), str(q)))
    #plt.xlabel('Year')
    #plt.ylabel('%s %s %s [%s]' %(region, target_var, res_name_target, unit))
    #plt.savefig('%spred_ARIMA_model_%s_%s_%s_%s_%s.pdf' %(
    #    outdir, target_var, res_name_target, model, ens, region))
    #plt.close(fig)
    
    # put data into dict for plotting of all models
    d_ts_train[model + '_' + ens] = ts_train
    d_ts_test[model + '_' + ens] = ts_test
    d_ts_pred[model + '_' + ens] = ts_pred

    # concat training period and prediction together
    # for saving of data
    #ts_trainnan = Series(np.NaN, index = years[0 : size])
    frames = [ts_train, ts_pred]
    ts_trainpred = concat(frames)
    d_ts_trainpred[model + '_' + ens] = ts_trainpred

# plot results
if d_par:
    p = 'x'
    q = 'y'
fig = plt.figure(figsize = (10, 5), dpi = 300)
for key in d_ts_train.keys():
    d_ts_train[key].index[0]
    plt.plot(d_ts_train[key], color = 'black')
    plt.plot(d_ts_test[key], color = 'blue')
    plt.plot(d_ts_pred[key], color = 'red')
plt.title('Train, test, and prediction by ARIMA model (%s, %s, %s)' %(
        str(p), str(d), str(q)))
plt.xlabel('Year')
plt.ylabel('%s %s %s [%s]' %(region, target_var, res_name_target, unit))
plt.savefig('%spred_ARIMA%s%s%s_split%s_model_%s_%s_%s.pdf' %(
    outdir, str(p), str(d), str(q), str(split_ratio), target_var, res_name_target, region))
plt.close()

print np.nanmean(d_aic.values())

# save data to output file
ts_pred_out = open("%spred_ARIMA%s%s%s_model_split%s_%s_%s_%s.pkl" %(
    outdir, str(p), str(d), str(q), str(split_ratio), target_var, res_name_target, region), "wb")
pickle.dump(d_ts_trainpred, ts_pred_out)
ts_pred_out.close()

#!/usr/bin/python
'''
File Name : pred_ar_cmip.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 21-03-2018
Modified: Wed 21 Mar 2018 02:26:01 PM CET
Purpose: use autoregresive model to forecast data from cmip5
         use auto_arima to estimate degree of model
         ARIMA can take care of non-stationary time series -> use d >= 1

'''
import numpy as np
import netCDF4 as nc
from netcdftime import utime
import datetime as dt
import glob
import math
import sys
import os # operating system interface
from os.path import expanduser
home = expanduser("~") # Get users home directory
sys.path.insert(0, home + '/scripts/plot_scripts/utils/')
from func_read_data import func_read_netcdf
from pandas import Series
from pandas import DataFrame
from pandas import concat
from statsmodels.tsa.arima_model import ARIMA
import statsmodels.api as sm
from statsmodels.graphics.tsaplots import plot_acf, plot_pacf
import matplotlib.pyplot as plt
from sklearn.metrics import mean_squared_error
#import pickle
import json
###
# Define input & output
###
experiment = 'rcp85'
archive = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/'
freq = 'ann'
grid = 'g025'

target_var = 'TXx'
# climatology: CLIM, variability: STD, trend: TREND
target_file = 'CLIM'
res_name_target = 'ANN'
target_mask = 'maskT'
res_time_target = 'MAX'

# cut data over region?
region = 'CNEU'
if region != None:
    area = region
else:
    area = 'GLOBAL'

# include initial conditions ensemble members or not?
ensembles = False

# time range
syear = 1950
eyear = 2100
#historical time period, 50 or 30 years
syear_hist = 1950
eyear_hist = 1999

## parameters of ARIMA model
p = 0 # lag order
d = 1 # degree of differencing
q = 1 # order of moving average
d_par = None
# or parameters per model:
#par_in = open("%s_%s_%s_%s_d_opt_aic.pkl" %(target_var, target_file,
#                                              res_name_target, region), "rb")
#d_par = pickle.load(par_in)

# define directories
path = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/%s/%s/%s/' %(
    target_var, freq, target_mask)
outdir = '%sstat_mod_cmip/%s/ARIMA/' %(path, area)
if (os.access(outdir, os.F_OK) == False):
    os.makedirs(outdir)
infile = '%s%s_%s_*_%s_*_%s-%s_%s%s' %(path, target_var, freq, experiment,
                                       syear, eyear, res_name_target,
                                       res_time_target)
###
# Read data
###
### read model data
print "Read model data"
if region:
    name = '%s_%s.nc' %(infile, region)
else:
    name = '%s.nc' %infile
nfiles = len(glob.glob(name))
models_1ens = list()
model_noens = list()
d_ts_pred = dict()
d_ts_test = dict()
d_ts_train = dict()
#d_ts_trainpred = dict()
df_ts_trainpred = DataFrame()
d_aic = dict()
for filename in glob.glob(name):
    print 'Reading data from %s' %filename
    model = filename.split('_')[4]
    ens = filename.split('_')[6]
    if ensembles:
        models_1ens.append(model + '_' + ens)
    else:
        if model not in model_noens:
            if ens == 'r10i1p1' or ens == 'r11i1p1' or ens == 'r12i1p1' or ens == 'r13i1p1':
                print 'We do not want ensemble r1xi1p1 but r1i1p1 from %s' %model
                continue
            else:
                model_noens.append(model)
                models_1ens.append(model + '_' + ens)
        else:
            print '%s already used, not using multiple ensemble members' %model
            continue
    fh = nc.Dataset(filename, mode = 'r')
    tmp = fh.variables[target_var]
    unit = tmp.units
    temp_mod = fh.variables[target_var][:]
    lat = fh.variables['lat'][:]
    lon = fh.variables['lon'][:]
    time = fh.variables['time']
    cdftime = utime(time.units, calendar = time.calendar)
    dates = cdftime.num2date(time[:])
    years = np.asarray([dates[i].year for i in xrange(len(dates))])
    fh.close()
    # area average for time series
    if isinstance(temp_mod, np.ma.core.MaskedArray):
        ma_mod = temp_mod
    else:
        ma_mod = np.ma.masked_array(temp_mod, np.isnan(temp_mod))
    w_lat = np.cos(lat * (4.0 * math.atan(1.0) / 180))
    tmp_latweight = np.ma.average(np.squeeze(ma_mod), axis = 1,
                                  weights = w_lat)
    ts_areaavg = np.nanmean(tmp_latweight.filled(np.nan), axis = 1)

    series = Series(ts_areaavg, index = years)
    
    # predict one year at a time, then slide window forward
    # historical time: 1950 - eyear_hist
    eind = np.where(series.index == eyear_hist)[0][0] + 1
    years_fut = years[eind : ]
    eind_1 = eind.copy()
    for yr in years_fut:
        # split full data into train and test set
        ts_train = series[0 : eind]
        ts_test = series[eind : eind + 1]

        # fit model
        if d_par:
            p = d_par[model + '_' + ens][0]
            q = d_par[model + '_' + ens][1]
        arima_model = ARIMA(ts_train.values, order = (p, d, q))
        model_fit = arima_model.fit(disp = 0)
        #print(model_fit.summary())

        predictions = model_fit.predict(start = len(ts_train),
                                        end = len(ts_train) + len(ts_test) - 1,
                                        dynamic = False, typ = "levels")
        #for i in range(len(predictions)):
        #    print('predicted = %f, expected = %f' % (predictions[i], test[i]))
        #error = mean_squared_error(ts_test.values, predictions)
        #print('Test MSE: %.3f' % error)

        if (yr == eyear_hist + 1):
            l_pred = list(predictions)
            AIC = [model_fit.aic]
        else:
            l_pred.append(predictions[0])
            AIC.append(model_fit.aic)
        eind = eind + 1
    d_aic[model + '_' + 'ens'] = AIC
    ts_pred = Series(l_pred, index = years_fut)
    
    # put data into dict for plotting of all models
    d_ts_train[model + '_' + ens] = ts_train
    d_ts_test[model + '_' + ens] = ts_test
    d_ts_pred[model + '_' + ens] = ts_pred

    # concat training period and prediction together
    # for saving of data
    #ts_trainnan = Series(np.NaN, index = years[0 : size])
    frames = [ts_train[0 : eind_1], ts_pred]
    ts_trainpred = concat(frames)
    df_ts_trainpred[model + '_' + ens] = ts_trainpred

    error = mean_squared_error(series[eind_1 : ].values, ts_pred.values)
    print('Test MSE: %.3f' % error)

# plot results
if d_par:
    p = 'x'
    q = 'y'
fig = plt.figure(figsize = (10, 5), dpi = 300)
for key in d_ts_train.keys():
    d_ts_train[key].index[0]
    plt.plot(d_ts_train[key], color = 'black')
    plt.plot(d_ts_test[key], color = 'blue')
    plt.plot(d_ts_pred[key], color = 'red')
plt.title('Train, test, and prediction by ARIMA model (%s, %s, %s)' %(
        str(p), str(d), str(q)))
plt.xlabel('Year')
plt.ylabel('%s %s %s [%s]' %(region, target_var, res_name_target, unit))
plt.savefig('%spred_ARIMA%s%s%s_model_%s-%s_shift_window_%s_%s_%s.pdf' %(
    outdir, str(p), str(d), str(q), syear_hist, eyear_hist, target_var, res_name_target, region))
plt.close()

print np.nanmean(d_aic.values())

# save data to output file
#ts_pred_out = open("%spred_ARIMA%s%s%s_model_%s-%s_shift_window_%s_%s_%s.pkl" %(
#    outdir, str(p), str(d), str(q), syear_hist, eyear_hist, target_var, res_name_target, region), "wb")
#pickle.dump(df_ts_trainpred, ts_pred_out)
#ts_pred_out.close()

ts_file_out = "%spred_ARIMA%s%s%s_model_%s-%s_shift_window_%s_%s_%s.txt" %(
    outdir, str(p), str(d), str(q), syear_hist, eyear_hist, target_var, res_name_target, region)
with open(ts_file_out, 'w') as fp:
    df_ts_trainpred.to_json(fp)

#!/usr/bin/python
'''
File Name : pred_ar_cmip.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 21-03-2018
Modified: Wed 21 Mar 2018 02:26:01 PM CET
Purpose: use autoregresive model to forecast data from cmip5
         time series should be iid, seasonal averages removes seasonality,
         trend????

'''
import numpy as np
import netCDF4 as nc
from netcdftime import utime
import datetime as dt
import glob
import math
import sys
import os # operating system interface
from os.path import expanduser
home = expanduser("~") # Get users home directory
sys.path.insert(0, home + '/scripts/plot_scripts/utils/')
from func_read_data import func_read_netcdf
from pandas import Series
from pandas import concat
from pandas import DataFrame
from statsmodels.tsa.ar_model import AR
from statsmodels.graphics.tsaplots import plot_acf, plot_pacf
import matplotlib.pyplot as plt
from pandas.plotting import lag_plot, autocorrelation_plot
from sklearn.metrics import mean_squared_error

###
# Define input & output
###
experiment = 'rcp85'
archive = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/'
freq = 'mon'
grid = 'g025'

target_var = 'tasmax'
# climatology: CLIM, variability: STD, trend: TREND
target_file = 'CLIM'
res_name_target = 'JJA'
target_mask = 'maskT'
res_time_target = 'MEAN'

# cut data over region?
region = 'CNEU'
if region != None:
    area = region
else:
    area = 'GLOBAL'

# include initial conditions ensemble members or not?
ensembles = False

# time ranges
syear_eval = 1951 # start of ts used to fit model
eyear_eval = 1999 # end of ts used to fit model
syear_hist = 1951
eyear_hist = 1999
syear_fut = 1975
eyear_fut = 2024
syear = 1951
eyear = 2100

nyears = eyear_hist - syear_hist + 1
ntim_tot = eyear - syear + 1

# define directories
path = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/%s/%s/%s/' %(
    target_var, freq, target_mask)
outdir = '%sstat_mod_cmip/%s/AR/' %(path, area)
if (os.access(outdir, os.F_OK) == False):
    os.makedirs(outdir)
infile = '%s%s_%s_*_%s_*_%s-%s_%s%s' %(path, target_var, freq, experiment,
                                       syear, eyear, res_name_target,
                                       res_time_target)
###
# Read data
###
### read model data
print "Read model data"
if region:
    name = '%s_%s.nc' %(infile, region)
else:
    name = '%s.nc' %infile
nfiles = len(glob.glob(name))
models_1ens = list()
model_noens = list()
for filename in glob.glob(name):
    print 'Reading data from %s' %filename
    model = filename.split('_')[4]
    ens = filename.split('_')[6]
    if ensembles:
        models_1ens.append(model + '_' + ens)
    else:
        if model not in model_noens:
            model_noens.append(model)
            models_1ens.append(model + '_' + ens)
        else:
            print '%s already used, not using multiple ensemble members' %model
            continue
    fh = nc.Dataset(filename, mode = 'r')
    tmp = fh.variables[target_var]
    unit = tmp.units
    temp_mod = fh.variables[target_var][:]
    lat = fh.variables['lat'][:]
    lon = fh.variables['lon'][:]
    time = fh.variables['time']
    cdftime = utime(time.units, calendar = time.calendar)
    dates = cdftime.num2date(time[:])
    years = np.asarray([dates[i].year for i in xrange(len(dates))])
    fh.close()
    # area average for time series
    if isinstance(temp_mod, np.ma.core.MaskedArray):
        ma_mod = temp_mod
    else:
        ma_mod = np.ma.masked_array(temp_mod, np.isnan(temp_mod))
    w_lat = np.cos(lat * (4.0 * math.atan(1.0) / 180))
    tmp_latweight = np.ma.average(np.squeeze(ma_mod), axis = 1,
                                  weights = w_lat)
    ts_areaavg = np.nanmean(tmp_latweight.filled(np.nan), axis = 1)
    break
ind_eval1 = np.where(years == syear_eval)[0][0]
ind_eval2 = np.where(years == eyear_eval)[0][0] + 1
ts_eval = Series(ts_areaavg[ind_eval1 : ind_eval2], index = dates[ind_eval1 : ind_eval2])

fig = plt.figure(figsize = (10, 5), dpi = 300)
plt.plot(ts_eval)
plt.xlabel('Year')
plt.ylabel('%s %s %s [%s]' %(region, target_var, res_name_target, unit))
plt.savefig('ts_%s_%s_%s_%s_%s-%s_%s.pdf' %(target_var, res_name_target, model, ens, syear_eval, eyear_eval, region))

#fig = plt.figure(figsize = (8, 8), dpi = 300)
#lag_plot(ts_eval)
#plt.savefig('lag_plot.pdf')

fig = plt.figure(figsize = (10, 5), dpi = 300)
autocorrelation_plot(ts_eval)
plt.title('%s %s %s' %(target_var, res_name_target, region))
plt.savefig('acf_%s_%s_%s_%s_%s.pdf' %(target_var, res_name_target, model, ens, region))

#plot_acf(ts_eval, lags = 10)
#plt.savefig('acf_plot.pdf')

plot_pacf(ts_eval, lags = 10)
plt.savefig('pacf_plot.pdf')

# create lagged dataset
series = Series(ts_areaavg, index = dates)
values = DataFrame(series.values)
dataframe = concat([values.shift(1), values], axis = 1)
dataframe.columns = ['t-1', 't+1']
# split into train and test sets
X = dataframe.values
train, test = X[1:len(X) - 50], X[len(X) - 50:]
train_X, train_y = train[:, 0], train[:, 1]
test_X, test_y = test[:, 0], test[:, 1]
 
# persistence model
def model_persistence(x):
    return x
 
# walk-forward validation
predictions = list()
for x in test_X:
    yhat = model_persistence(x)
    predictions.append(yhat)
test_score = mean_squared_error(test_y, predictions)
print('Test MSE: %.3f' % test_score)
# plot predictions vs expected
fig = plt.figure(figsize = (10, 5), dpi = 300)
plt.plot(test_y)
plt.plot(predictions, color = 'red')
plt.xlabel('Year')
plt.ylabel('%s %s %s [%s]' %(region, target_var, res_name_target, unit))
plt.savefig('pred_persistence_model_%s_%s_%s_%s_%s.pdf' %(target_var, res_name_target, model, ens, region))

# train autoregression
X = series.values
train, test = X[1:len(X) - 50], X[len(X) - 50:]
ar_model = AR(train)
model_fit = ar_model.fit()
print('Lag: %s' % model_fit.k_ar)
print('Coefficients: %s' % model_fit.params)
# make predictions
predictions = model_fit.predict(start = len(train), end = len(train) + len(test) - 1, dynamic = False)
for i in range(len(predictions)):
    print('predicted=%f, expected=%f' % (predictions[i], test[i]))
error = mean_squared_error(test, predictions)
print('Test MSE: %.3f' % error)
# plot results
fig = plt.figure(figsize = (10, 5), dpi = 300)
plt.plot(test)
plt.plot(predictions, color = 'red')
plt.xlabel('Year')
plt.ylabel('%s %s %s [%s]' %(region, target_var, res_name_target, unit))
plt.savefig('pred_AR_model_%s_%s_%s_%s_%s.pdf' %(target_var, res_name_target, model, ens, region))

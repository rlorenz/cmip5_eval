#!/usr/bin/python
'''
File Name : pred_arima_3D_cmip.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 21-03-2018
Modified: Wed 21 Mar 2018 02:26:01 PM CET
Purpose: use autoregresive model to forecast data from cmip5
         calculate model for every grid point without area averaging
'''
import numpy as np
import netCDF4 as nc
from netcdftime import utime
import datetime as dt
import glob
import math
import sys
import os # operating system interface
from os.path import expanduser
home = expanduser("~") # Get users home directory
sys.path.insert(0, home + '/scripts/plot_scripts/utils/')
from func_read_data import func_read_netcdf
from pandas import Series
from pandas import DataFrame
from statsmodels.tsa.arima_model import ARIMA
import statsmodels.api as sm
from statsmodels.graphics.tsaplots import plot_acf, plot_pacf
import matplotlib.pyplot as plt
from sklearn.metrics import mean_squared_error
import xarray as xr
###
# Define input & output
###
experiment = 'rcp85'
archive = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/'
freq = 'ann'
grid = 'g025'

target_var = 'TXx'
# climatology: CLIM, variability: STD, trend: TREND
target_file = 'CLIM'
res_name_target = 'ANN'
target_mask = 'maskT'
res_time_target = 'MAX'

# cut data over region?
region = 'CNEU'
if region != None:
    area = region
else:
    area = 'GLOBAL'

# include initial conditions ensemble members or not?
ensembles = True

# time range
syear = 1950
eyear = 2100
#ration how to split train and test data
split_ratio = 0.33

## parameters of ARIMA model
p = 0 # lag order
d = 1 # degree of differencing
q = 1 # order of moving average
d_par = None
# or parameters per model:
#par_in = open("%s_%s_%s_%s_d_opt_aic.pkl" %(target_var, target_file,
#                                              res_name_target, region), "rb")
#import pickle
#d_par = pickle.load(par_in)

# define directories
path = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/%s/%s/%s/' %(
    target_var, freq, target_mask)
outdir = '%sstat_mod_cmip/%s/ARIMA/' %(path, area)
if (os.access(outdir, os.F_OK) == False):
    os.makedirs(outdir)
infile = '%s%s_%s_*_%s_*_%s-%s_%s%s' %(path, target_var, freq, experiment,
                                       syear, eyear, res_name_target,
                                       res_time_target)
###
# Read data
###
### read model data
print "Read model data"
if region:
    name = '%s_%s.nc' %(infile, region)
else:
    name = '%s.nc' %infile
nfiles = len(glob.glob(name))
models_1ens = list()
model_noens = list()
d_ts_pred = dict()
d_ts_test = dict()
d_ts_train = dict()
print '%s files found' %(str(len(glob.glob(name))))
for filename in glob.glob(name):
    error_model = 0
    print 'Reading data from %s' %filename
    model = filename.split('_')[4]
    ens = filename.split('_')[6]
    if ensembles:
        models_1ens.append(model + '_' + ens)
    else:
        if model not in model_noens:
            if ens == 'r10i1p1' or ens == 'r11i1p1' or ens == 'r12i1p1' or ens == 'r13i1p1':
                print 'We do not want ensemble r1xi1p1 but r1i1p1 from %s' %model
                continue
            model_noens.append(model)
            models_1ens.append(model + '_' + ens)
        else:
            print '%s already used, not using multiple ensemble members' %model
            continue
    fh = nc.Dataset(filename, mode = 'r')
    tmp = fh.variables[target_var]
    unit = tmp.units
    temp_mod = fh.variables[target_var][:]
    lat = fh.variables['lat'][:]
    lon = fh.variables['lon'][:]
    time = fh.variables['time']
    cdftime = utime(time.units, calendar = time.calendar)
    dates = cdftime.num2date(time[:])
    years = np.asarray([dates[i].year for i in xrange(len(dates))])
    fh.close()
    if isinstance(temp_mod, np.ma.core.MaskedArray):
        temp_mod3D = temp_mod.filled(np.nan)
    else:
        temp_mod3D = temp_mod
    ts_pred3D = np.ndarray((len(temp_mod3D), len(lat), len(lon)))
    ts_test3D = np.ndarray((len(temp_mod3D), len(lat), len(lon)))
    for y in xrange(len(lat)):
        for x in xrange(len(lon)):
            if np.isnan(temp_mod3D[:, y, x]).any():
                ts_pred3D[:, y, x] = np.NaN
                ts_test3D[:, y, x] = np.NaN
            else:
                series = Series(temp_mod3D[:, y, x], index = years)
        
                X = series.values
                # split full data into train and test set
                size = int(len(X) * split_ratio)
                train, test = X[0:size], X[size:len(X)]

                ts_train = Series(temp_mod3D[0 : size, y, x],
                                  index = years[0 : size])
                ts_test = Series(temp_mod3D[size : len(X), y, x],
                                 index = years[size : len(X)])
    
                # fit model
                if d_par:
                    p = d_par[model + '_' + ens][0]
                    q = d_par[model + '_' + ens][1]
                arima_model = ARIMA(train, order = (p, d, q))
                model_fit = arima_model.fit(disp = 0)
                #print(model_fit.summary())
    
                predictions = model_fit.predict(
                    start = len(train), end = len(train) + len(test) - 1,
                    dynamic = False, typ = "levels")
                error = mean_squared_error(test, predictions)
                error_model = error_model + error

                ts_pred3D[0 : size, y, x] = ts_train
                ts_pred3D[size : len(X), y, x] = Series(
                    predictions,
                    index = years[len(train) : len(train) + len(test)])
                ts_test3D[0 : size, y, x] = ts_train
                ts_test3D[size : len(X), y, x] = ts_test
    avg_error = error_model / (len(lat) * len(lon))
    print('Test MSE: %.3f' % avg_error)

    # put data into dataframe/xarray??? for saving data of all models
    data_ar_pred = xr.DataArray(ts_pred3D, coords = {'lon': (lon), 'lat': (lat), 'time': years}, dims = ['time', 'lat', 'lon'])
    data_ts_test = xr.DataArray(ts_test3D, coords = {'lon': (lon), 'lat': (lat), 'time': years}, dims = ['time', 'lat', 'lon'])
    try:
        ds_towrite1
    except NameError:
        ds_towrite1 = xr.Dataset(data_vars = {model + '_' + ens:
                                             (['time', 'lat', 'lon'],
                                              data_ar_pred)},
                                coords = {'lon': (lon), 'lat': (lat),
                                          'time': years})
        ds_towrite_test1 = xr.Dataset(data_vars = {model + '_' + ens:
                                             (['time', 'lat', 'lon'],
                                              data_ts_test)},
                                coords = {'lon': (lon), 'lat': (lat),
                                          'time': years})
    else:
        #d_opt = {model + '_' + ens: (['time', 'lat', 'lon'], data_ar_pred)}
        #ds_towrite = ds_towrite.assign(**d_opt)
        d_ts_test[model + '_' + ens] = data_ts_test
        d_ts_pred[model + '_' + ens] = data_ar_pred

ds_towrite = ds_towrite1.assign(**d_ts_pred)
ds_towrite_test = ds_towrite1.assign(**d_ts_test)

# p, q names
if d_par != None:
    p = 'x'
    q = 'y'

# save data to output file
outfile = "%spred_3D_ARIMA%s%s%s_model_split%s_%s_%s_%s.nc" %(
    outdir, str(p), str(d), str(q), str(split_ratio), target_var, res_name_target, region)
if os.path.exists(outfile):
    os.remove(outfile)
print 'Save data to netcdf  %s' %outfile
ds_towrite.to_netcdf(outfile)

outfile = "%straintest_3D_split%s_%s_%s_%s.nc" %(
    outdir, str(split_ratio), target_var, res_name_target, region)
if os.path.exists(outfile):
    os.remove(outfile)
print 'Save data to netcdf  %s' %outfile
ds_towrite_test.to_netcdf(outfile)

#!/usr/bin/python
'''
File Name : pred_ar_cmip.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 21-03-2018
Modified: Wed 21 Mar 2018 02:26:01 PM CET
Purpose: find ideal parameters for ARIMA model
         first difference series to remove trend
         then find best model based on AIC and BIC
'''
import numpy as np
import netCDF4 as nc
from netcdftime import utime
import datetime as dt
import glob
import math
import sys
import os # operating system interface
from os.path import expanduser
home = expanduser("~") # Get users home directory
sys.path.insert(0, home + '/scripts/plot_scripts/utils/')
from func_read_data import func_read_netcdf
from pandas import Series
from pandas import DataFrame
from statsmodels.tsa.arima_process import arma_generate_sample
from statsmodels.tsa.arima_model import ARIMA
import statsmodels.api as sm
from statsmodels.graphics.tsaplots import plot_acf, plot_pacf
import matplotlib.pyplot as plt
import pickle
###
# Define input & output
###
experiment = 'rcp85'
archive = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/'
freq = 'mon'
grid = 'g025'

target_var = 'tas'
# climatology: CLIM, variability: STD, trend: TREND
target_file = 'CLIM'
res_name_target = 'JJA'
target_mask = 'maskT'
res_time_target = 'MEAN'

# cut data over region?
region = 'CNEU'
if region != None:
    area = region
else:
    area = 'GLOBAL'

# include initial conditions ensemble members or not?
ensembles = False

# time range
syear = 1950
eyear = 2100
#ration how to split train and test data
split_ratio = 0.33

# define directories
path = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/%s/%s/%s/' %(
    target_var, freq, target_mask)
outdir = '%sstat_mod_cmip/%s/ARIMA/' %(path, area)
if (os.access(outdir, os.F_OK) == False):
    os.makedirs(outdir)
infile = '%s%s_%s_*_%s_*_%s-%s_%s%s' %(path, target_var, freq, experiment,
                                       syear, eyear, res_name_target,
                                       res_time_target)

# define function to create a differenced series
def difference(dataset, interval = 1):
	diff = list()
	for i in range(interval, len(dataset)):
		value = dataset[i] - dataset[i - interval]
		diff.append(value)
	return Series(diff)

###
# Read data
###
### read model data
print "Read model data"
if region:
    name = '%s_%s.nc' %(infile, region)
else:
    name = '%s.nc' %infile
nfiles = len(glob.glob(name))
models_1ens = list()
model_noens = list()
d_opt_aic = dict()
d_opt_bic = dict()
for filename in glob.glob(name):
    print 'Reading data from %s' %filename
    model = filename.split('_')[4]
    ens = filename.split('_')[6]
    if ensembles:
        models_1ens.append(model + '_' + ens)
    else:
        if model not in model_noens:
            model_noens.append(model)
            models_1ens.append(model + '_' + ens)
        else:
            print '%s already used, not using multiple ensemble members' %model
            continue
    fh = nc.Dataset(filename, mode = 'r')
    tmp = fh.variables[target_var]
    unit = tmp.units
    temp_mod = fh.variables[target_var][:]
    lat = fh.variables['lat'][:]
    lon = fh.variables['lon'][:]
    time = fh.variables['time']
    cdftime = utime(time.units, calendar = time.calendar)
    dates = cdftime.num2date(time[:])
    years = np.asarray([dates[i].year for i in xrange(len(dates))])
    fh.close()
    # area average for time series
    if isinstance(temp_mod, np.ma.core.MaskedArray):
        ma_mod = temp_mod
    else:
        ma_mod = np.ma.masked_array(temp_mod, np.isnan(temp_mod))
    w_lat = np.cos(lat * (4.0 * math.atan(1.0) / 180))
    tmp_latweight = np.ma.average(np.squeeze(ma_mod), axis = 1,
                                  weights = w_lat)
    ts_areaavg = np.nanmean(tmp_latweight.filled(np.nan), axis = 1)
    #break

    series = Series(ts_areaavg, index = dates)

    # difference time series to make stationary
    diff_series = difference(series)

    fig = plt.figure(figsize = (10, 5), dpi = 300)
    plot_acf(diff_series, lags = 50)
    plt.title('ACF 1st order Difference %s %s %s' %(target_var, res_name_target,
                                                    region))
    plt.savefig('%sacf_diff1_%s_%s_%s_%s_%s-%s_%s.pdf' %(
        outdir, target_var, res_name_target, model, ens, syear,
        eyear, region))

    fig = plt.figure(figsize = (10, 5), dpi = 300)
    plot_pacf(diff_series, lags = 50)
    plt.title('PACF 1st order Difference %s %s %s' %(target_var,
                                                     res_name_target, region))
    plt.savefig('%spacf_diff1_%s_%s_%s_%s_%s-%s_%s.pdf' %(
        outdir, target_var, res_name_target, model, ens, syear,
        eyear, region))

    # find parameters
    res = sm.tsa.arma_order_select_ic(diff_series.values, max_ar = 10,
                                      max_ma = 10, ic = ['aic', 'bic'],
                                      trend = 'c')
    res.aic_min_order
    res.bic_min_order

    d_opt_aic[model + '_' + ens] = res.aic_min_order
    d_opt_bic[model + '_' + ens] = res.bic_min_order

d_opt_aic_out = open("%s_%s_%s_%s_d_opt_aic.pkl" %(target_var, target_file,
                                                   res_name_target, region),
                     "wb")
pickle.dump(d_opt_aic, d_opt_aic_out)
d_opt_aic_out.close()
    
d_opt_bic_out = open("%s_%s_%s_%s_d_opt_bic.pkl" %(target_var, target_file,
                                                   res_name_target, region),
                     "wb")
pickle.dump(d_opt_bic, d_opt_bic_out)
d_opt_bic_out.close()

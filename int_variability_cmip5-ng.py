#!/usr/bin/python
'''
File Name : int_variability_cmip5-ng.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 01-03-2016
Modified: Tue 01 Mar 2016 11:04:02 AM CET
Purpose: Calculate internal variability from cmip5-ng ensemble data
	the internal variability is the minimum for similarity parameter
	in Sanderson et al. 2015, J.Clim
	parameter has to be choosen when looking beyond model democracy


'''
import netCDF4 as nc # to work with NetCDF files
import numpy as np
#import datetime as dt
from netcdftime import utime
import glob
import sys
import os # operating system interface

###
# Define input
###
variable = 'tas'

archive = '/net/atmos/data/cmip5-ng'
#workdir = '/net/tropo/climphys/rlorenz/cmip5-ng/work/'
outdir = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/%s/' %(variable)
infile = variable+'_mon'
syear = 1979
eyear = 2005
nyears = eyear - syear + 1
#model = 'IPSL-CM5A-LR'
experiment = 'historicalGHG'
grid = 'g025'

if (os.access(outdir,os.F_OK)==False):
        os.makedirs(outdir)

###read model data
##find all matching files in archive, loop over all of them
#find all models
name = '%s/%s/%s_*_%s_r1i1p1_%s.nc' %(archive,variable,infile,experiment,grid)
nmodels = len(glob.glob(name))
print "Total number of model found: "+str(nmodels)
model_names = []
d_std = {}
d_var = {}
d_std_glob = {}
for filename in glob.glob(name):
    fh = nc.Dataset(filename,mode='r')
    model = fh.source_model
    model_names.append(model)
    fh.close()

#for each model find all ensembles
for m in range(len(model_names)):
    name_ens = '%s/%s/%s_%s_%s_*_%s.nc' %(archive,variable,infile,model_names[m],experiment,grid)
    #read data from all ensembles
    nens = len(glob.glob(name_ens))
    print str(nens)+" ensemble members found for "+model_names[m]
    if (nens > 1):
        e = 0
        for ensemble in glob.glob(name_ens):
            fh = nc.Dataset(ensemble,mode='r')
            lon = fh.variables['lon'][:]
            lat = fh.variables['lat'][:]
            time = fh.variables['time']
            print "Create dates from netcdf time for model"
            nccalendar = time.calendar
            cdftime = utime(time.units,calendar=time.calendar)
            dates=cdftime.num2date(time[:])
            years=np.asarray([dates[i].year for i in xrange(len(dates))])
            dates=dates[(years>=syear) & (years<=eyear)]
            months = np.asarray([dates[i].month for i in xrange(len(dates))])
            print "Read "+ensemble
            if (e == 0):
                temp_mod = np.empty((nens,len(dates),len(lat),len(lon)),dtype=float)
                temp_mod.fill(np.NaN)
    
            if (years[-1] < syear):
                print "Analysis time period and data availability do not overlapp"
                temp_mod[e,:,:,:] = float('nan')
                e = e + 1
            else:
                temp_mod[e,:,:,:]= fh.variables[variable][(years>=syear) & (years<=eyear),:,:]
                e = e + 1
        print "Max: "+str(np.amax(temp_mod))
        print "Min: "+str(np.amin(temp_mod))
    
        #calculate internal climate variability
        temp_mod_avgt = np.nanmean(temp_mod,axis = 1)
        d_std[model_names[m]] = np.nanstd(temp_mod_avgt,axis = 0)
        d_var[model_names[m]] = np.nanvar(temp_mod_avgt,axis = 0)
        #global average
        d_std_glob[model_names[m]] = np.nanmean(d_std[model_names[m]])
    else:
        print "only one ensemble member, cannot calculate internal variability"

std_glob_mm = np.nanmean(d_std_glob.values())
var_glob_mm = np.nanmean(np.nanmean(d_var.values()))

#!/usr/bin/python
'''
File Name : calc_DTR_obs_cdo.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 24-08-2017
Modified: Thu 24 Aug 2017 05:39:33 PM CEST
Purpose: calculate DTR from observations


'''
import netCDF4 as nc # to work with NetCDF files
import numpy as np
from cdo import *   # load cdo functionality
cdo = Cdo()
from os.path import expanduser
home = expanduser("~") # Get users home directory
import glob

###
# Define input
###
variable1 = 'tasmax'
variable2 = 'tasmin'
outvar = 'dtr'
var_kind = 'seas'    #kind is ann: annual, mon: monthly, seas: seasonal
seasons = ['DJF', 'MAM', 'SON', 'JJA'] #choose data for particular season?
region = ['EUR', 'CNEU']           #cut data over region?
obs = ['Obs', 'MERRA2', 'ERAint'] # 'ERAint', 'MERRA2', 'Obs'

workdir = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/%s/work/' %(
    outvar)

syear = 1980
eyear = 2014
nyears = eyear - syear + 1
grid = 'g025'
masko = True

pathtrop = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/'
if masko:
    outdir = '%s/%s/mon/maskT/' %(pathtrop, outvar)
    oceanmask = "%s/scripts/plot_scripts/areas_txt/seamask_%s.nc" %(home, grid)
else:
    outdir = '%s/%s/mon/maskF/' %(pathtrop, outvar)

if (os.access(workdir,os.F_OK) == False):
    os.makedirs(workdir)
if (os.access(outdir, os.F_OK) == False):
        os.makedirs(outdir)

#read obs data
for dataset in obs:
    if dataset == 'ERAint':
        print "Read ERAint data"
        path = '/net/tropo/climphys/rlorenz/Datasets/ERAint/ERAint_mon'

        varname1 = 'mx2t'
        filename1 = '%s/%s_monmean_ERAint_197901-201512_%s.nc' %(
            path, varname1, grid)
        varname2 = 'mn2t'
        filename2 = '%s/%s_monmean_ERAint_197901-201512_%s.nc' %(
            path, varname2, grid)
    elif dataset == 'MERRA2':
        print "Read MERRA2 data"
        path = '/net/tropo/climphys/rlorenz/Datasets/MERRA2/monthly'

        varname1 = 'T2MMAX'
        varname2 = 'T2MMIN'
        lev = 'slv'
        filename1 = '%s/%s.tavgmon_2d_%s_Nx.%s.1980-2015_remapbil_%s.nc' %(
                path, dataset, lev, varname1, grid)
        filename2 = '%s/%s.tavgmon_2d_%s_Nx.%s.1980-2015_remapbil_%s.nc' %(
                path, dataset, lev, varname2, grid)
    elif (dataset == 'Obs'):
        print "Read Obs data"
        path = '/net/tropo/climphys/rlorenz/Datasets/'
        if ((region == 'EUR') or (region == 'CNEU')):
            varname1 = 'tx'
            filename1 = '%s/E-OBS/tx_mon_1950-2017_reg_v16.0_%s' %(path, grid)
            varname2 = 'tn'
            filename2 = '%s/E-OBS/tn_mon_1950-2017_reg_v16.0_%s' %(path, grid)
        else:
            varname1 = 'tmax'
            filename1 = '%s/HadGHCND/monthly/HadGHCND_TXTN_avg_monthly_acts_195001-201412_remapbil_%s.nc' %(path, grid)
            varname2 = 'tmin'
            filename2 = '%s/HadGHCND/monthly/HadGHCND_TXTN_avg_monthly_acts_195001-201412_remapbil_%s.nc' %(path, grid)

    fh = nc.Dataset(filename1, mode = 'r')
    try:
        unit = fh.variables[varname1].units
    except AttributeError:
        unit = "unknown"
    fh.close()

    infile1 = '%s_mon' %(variable1)
    infile2 = '%s_mon' %(variable2)
    for res in seasons:
        tmpfile1 = '%s/%s_%s_%s-%s_%s' %(workdir, infile1, dataset, syear,
                                         eyear, res)

        tmpfile2 = '%s/%s_%s_%s-%s_%s' %(workdir, infile2, dataset, syear,
                                          eyear, res)

        outfile = '%s/%s_mon_%s_%s-%s_%s' %(outdir, outvar, dataset, syear, eyear,
                                        res)

        datefile = cdo.seldate('%s-01-01,%s-12-31' %(syear, eyear),
                               input = '%s' %(filename1))
        boxfile = cdo.sellonlatbox(-180,180,-90,90, input = datefile)
        varfile = cdo.selname(varname1, input = boxfile)
        namefile = cdo.chname(varname1,variable1, input = varfile)
        cdo.selseas(res, input = namefile, output = '%s.nc' %(tmpfile1))

        os.system("ncatted -h -O -a valid_min,,d,, %s.nc" %(tmpfile1))
        os.system("ncatted -h -O -a valid_max,,d,, %s.nc" %(tmpfile1))

        datefile2 = cdo.seldate('%s-01-01,%s-12-31' %(syear, eyear),
                                input = '%s' %(filename2))
        boxfile2 = cdo.sellonlatbox(-180,180,-90,90, input = datefile2)
        varfile2 = cdo.selname(varname2, input = boxfile2)
        namefile2 = cdo.chname(varname2,variable2, input = varfile2)
        cdo.selseas(res, input = namefile2, output = '%s.nc' %(tmpfile2))

        os.system("ncatted -h -O -a valid_min,,d,, %s.nc" %(tmpfile2))
        os.system("ncatted -h -O -a valid_max,,d,, %s.nc" %(tmpfile2))

        if ((variable1 == 'tasmax') or (variable1 == 'tas') or
            (variable1 == 'tasmin')) and (unit == 'K'):
            unitfile = cdo.subc(273.15, options = '-b 64',
                                input = '%s.nc' %(tmpfile1))
            cdo.chunit('"K","degC"', input = unitfile,
                       output = '%s.nc' %(tmpfile1))
        if ((variable2 == 'tasmax') or (variable2 == 'tas') or
            (variable2 == 'tasmin')) and (unit == 'K'):
            unitfile = cdo.subc(273.15, options = '-b 64',
                                input = '%s.nc' %(tmpfile2))
            cdo.chunit('"K","degC"', input = unitfile,
                       output = '%s.nc' %(tmpfile2))
        if masko:
            maskfile = '%s_masko.nc' %(tmpfile1)
            cdo.setmissval(0, input = "-mul -eqc,1 %s %s.nc" %(oceanmask,
                                                               tmpfile1),
                           output = '%s' %(maskfile))
            os.system("mv %s %s.nc" %(maskfile, tmpfile1))
            maskfile2 = '%s_masko.nc' %(tmpfile2)
            cdo.setmissval(0, input = "-mul -eqc,1 %s %s.nc" %(oceanmask,
                                                               tmpfile2),
                           output = '%s' %(maskfile2))
            os.system("mv %s %s.nc" %(maskfile2, tmpfile2))

        subfile = cdo.sub(input = '%s.nc %s.nc' %(tmpfile1, tmpfile2))
        dtrfile = cdo.chname('%s,%s' %(variable1, outvar), input = subfile,
                             output = '%s.nc' %(outfile))
        if (var_kind == 'seas'):
            cdo.seasmean(input = '%s.nc' %(outfile),
                         output = '%sMEAN.nc' %(outfile))
            cdo.yseasmean(input = '%s.nc' %(outfile),
                          output = '%sMEAN_CLIM.nc' %(outfile))
        elif (var_kind == 'ann'):
            cdo.yearmean(input = '%s.nc' %(outfile),
                         output = '%sMEAN.nc' %(outfile))
            cdo.timmean(input = '%s.nc' %(outfile),
                           output = '%sMEAN_CLIM.nc' %(outfile))
        else:
            cdo.ymonmean(input = '%s.nc' %(outfile),
                          output = '%sMEAN_CLIM.nc' %(outfile))

        cdo.detrend(input = '%sMEAN.nc' %(outfile),
                    output = '%sMEAN_DETREND.nc' %(tmpfile1))
        cdo.regres(input = '%sMEAN.nc' %(outfile),
                   output = '%sMEAN_TREND.nc' %(outfile))
        cdo.timstd(input = '%sMEAN_DETREND.nc' %(tmpfile1),
                   output = '%sMEAN_STD.nc' %(outfile))

        if region != None:
            # loop over regions
            for reg in region:
                print 'Region is %s' %(reg)
                area = reg
                mask = np.loadtxt(
                    '/home/rlorenz/scripts/plot_scripts/areas_txt/%s.txt' %(reg))

                lonmax = np.max(mask[:, 0])
                lonmin = np.min(mask[:, 0])
                latmax = np.max(mask[:, 1]) 
                latmin = np.min(mask[:, 1])
                cdo.sellonlatbox(lonmin,lonmax,latmin,latmax,
                                 input = '%sMEAN_CLIM.nc' %(outfile),
                                 output = '%sMEAN_CLIM_%s.nc' %(outfile, reg))
                cdo.sellonlatbox(lonmin,lonmax,latmin,latmax,
                                 input = '%sMEAN_STD.nc' %(outfile),
                                 output = '%sMEAN_STD_%s.nc' %(outfile, reg))
                cdo.sellonlatbox(lonmin,lonmax,latmin,latmax,
                                 input = '%sMEAN_TREND.nc' %(outfile),
                                 output = '%sMEAN_TREND_%s.nc' %(outfile, reg))
                cdo.sellonlatbox(lonmin,lonmax,latmin,latmax,
                                 input = '%sMEAN.nc' %(outfile),
                                 output = '%sMEAN_%s.nc' %(outfile, reg))

# clean up workdir
filelist = glob.glob(workdir + '*')
for f in filelist:
    os.remove(f)

#!/usr/bin/python
'''
File Name : calc_CORR_cmip5_cdo.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 28-07-2017
Modified: Thu 29 Jun 2017 04:08:36 PM CEST
Purpose: Script calculating Evaporative Fractio from cmip5 data for further use
         e.g. in Mult_Diag_Lin_Reg

'''
import netCDF4 as nc # to work with NetCDF files
import numpy as np
from cdo import *   # load cdo functionality
cdo = Cdo()
import os
from os.path import expanduser
home = expanduser("~") # Get users home directory
import glob

###
# Define input
###
variable = 'tas'
variable2 = 'clt'
var_kind = 'seas'    #kind is ann: annual, mon: monthly, seas: seasonal
seasons = ['DJF', 'MAM', 'SON']         #choose data for particular month or season?
region = ['EUR', 'CNEU']       #cut data over region?
obs = ['ERAint', 'MERRA2', 'Obs'] # 'ERAint' or 'MERRA2'

workdir = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/%s%s/work/' %(variable, variable2)

syear = 1980
eyear = 2014
nyears = eyear - syear + 1
experiment = 'rcp85'
grid = 'g025'
masko = True

pathtrop = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/'
if masko:
    oceanmask = "%s/scripts/plot_scripts/areas_txt/seamask_%s.nc" %(home, grid)
    outdir = '%s/%s%s/mon/maskT/' %(pathtrop, variable, variable2)
else:
    outdir = '%s/%s%s/mon/maskF/' %(pathtrop, variable, variable2)

if (os.access(workdir,os.F_OK) == False):
    os.makedirs(workdir)
if (os.access(outdir, os.F_OK) == False):
    os.makedirs(outdir)

#read obs data
for dataset in obs:
    if dataset == 'ERAint':
        print "Read ERAint data"
        path = '/net/tropo/climphys/rlorenz/Datasets/ERAint/ERAint_mon'
        if (variable == 'tas'):
            filename = '%s/T2M_monthly_ERAint_197901-201601_%s.nc' %(path, grid)
            varname = 'T2M'
        elif (variable == 'tasmax'):
            filename = '%s/mx2t_monmean_ERAint_197901-201512_%s.nc' %(path,
                                                                      grid)
            varname = 'mx2t'
        if (variable2 == 'clt'):
            filename2 = '%s/cld_monthly_ERAint_197901-201612_%s.nc' %(path,
                                                                      grid)
            varname2 = 'tcc'
        elif (variable2 == 'huss'):
            continue
    elif dataset == 'MERRA2':
        print "Read MERRA2 data"
        path = '/net/tropo/climphys/rlorenz/Datasets/MERRA2/monthly'
        if (variable == 'tas'):
            varname = 'T2MMEAN'
            filename = '%s/%s.tavgmon_2d_slv_Nx.%s.1980-2015_remapbil_%s.nc' %(path, dataset, varname, grid)
        elif (variable == 'tasmax'):
            filename = '%s/MERRA2.tavgmon_2d_slv_Nx.T2MMAX.1980-2015_remapbil_%s.nc' %(path,grid)
            varname = 'T2MMAX'
        if (variable2 == 'clt'):
            varname2 = 'CLDTOT'
            filename2 = '%s/%s.tavgmon_2d_rad_Nx.%s.1980-2015_remapbil_%s.nc' %(
                path, dataset, varname2, grid)
        elif (variable2 == 'huss'):
            varname2 = 'QV2M'
            filename2 = '%s/%s.tavgmon_2d_slv_Nx.%s.1980-2015_remapbil_%s.nc' %(
                path, dataset, varname2, grid)
    elif (dataset == 'Obs'):
        print "Read Obs data"
        path = '/net/tropo/climphys/rlorenz/Datasets/'
        if (variable == 'tas'):
            if ((region == 'EUR') or (region == 'CNEU')):
                filename = '%s/E-OBS/tg_mon_1950-2017_reg_v16.0_%s.nc' %(path,
                                                                      grid)
                varname = 'tg'
            else:
                filename = '%s/BerkeleyEarth/Complete_TAVG_abs_LatLong_011750-122015_%s.nc' %(path, grid)
                varname = 'temperature'
        elif (variable == 'tasmax'):
            if ((region == 'EUR') or (region == 'CNEU')):
                filename = '%s/E-OBS/tx_mon_1950-2017_reg_v16.0_%s.nc' %(path,
                                                                      grid)
                varname = 'tx'
            else:
                filename = '%s/HadGHCND/monthly/HadGHCND_TXTN_avg_monthly_acts_195001-201412_remapbil_%s.nc' %(path, grid)
                varname = 'tmax'
        if (variable2 == 'huss'):
            varname2 = 'q_abs'
            filename2 = '%s/HadISDH/HadISDH.landq.3.0.0.2016p_FLATgridIDPHA5by5_anoms7605_JAN2017_cf_remapcon2_%s.nc' %(
                path, grid)
        if (variable2 == 'clt'):
            varname2 = 'clt'
            filename2 = '%s/MODIS/%s_MODIS_L3_C5_200003-201109_%s.nc' %(
                path, varname2, grid)
    for res in seasons:
        if (variable2 == 'clt') and (dataset == 'Obs'):
            if (res == 'JJA') or (res == 'MAM'):
                syear = 2000
                eyear = 2011
            elif (res == 'DJF'):
                syear = 2001
                eyear = 2010
            else:
                syear = 2000
                eyear = 2010

        tmpfile = '%s/%s_mon_%s_%s-%s_%s' %(workdir, variable, dataset, syear,
                                            eyear, res)
        tmpfile2 = '%s/%s_mon_%s_%s-%s_%s' %(workdir, variable2, dataset, syear,
                                             eyear, res)

        outfile = '%s/%s%s_mon_%s_%s-%s_%s' %(outdir, variable, variable2,
                                              dataset, syear, eyear, res)

        if (var_kind == 'seas'):
            cdo.selseas(res, options = "-f nc", input = 
                        "-seldate,%s-01-01,%s-12-31 -sellonlatbox,-180,180,-90,90 %s"
                        %(syear, eyear, filename), output = '%s.nc' %(tmpfile))
            cdo.selseas(res, options = "-f nc", input = 
                        "-seldate,%s-01-01,%s-12-31 -sellonlatbox,-180,180,-90,90  %s"
                        %(syear, eyear, filename2),
                        output = '%s.nc' %(tmpfile2))
        else:
            cdo.seldate('%s-01-01,%s-12-31' %(syear, eyear), options = "-f nc",
                        input = "-sellonlatbox,-180,180,-90,90 %s"
                        %(filename), output = '%s.nc' %(tmpfile))
            cdo.seldate('%s-01-01,%s-12-31' %(syear, eyear), options = "-f nc",
                        input = "-sellonlatbox,-180,180,-90,90 %s"
                        %(filename2), output = '%s.nc' %(tmpfile2))
        if masko:
            maskfile = '%s_masko.nc' %(tmpfile)
            cdo.setmissval(0, input = "-mul -eqc,1 %s %s.nc" %(oceanmask,
                                                               tmpfile),
                           output = '%s' %(maskfile))
            os.system("mv %s %s.nc" %(maskfile, tmpfile))
            maskfile2 = '%s_masko.nc' %(tmpfile2)
            cdo.setmissval(0, input = "-mul -eqc,1 %s %s.nc" %(oceanmask,
                                                               tmpfile2),
                           output = '%s' %(maskfile))
            os.system("mv %s %s.nc" %(maskfile, tmpfile2))

        varfile = cdo.selname(varname, input = '%s.nc' %(tmpfile))
        varfile2 = cdo.selname(varname2, input = '%s.nc' %(tmpfile2))

        if (var_kind == 'seas'):
            cdo.seasmean(input = varfile,
                         output = '%sMEAN.nc' %(tmpfile))
            cdo.seasmean(input = varfile2,
                          output = '%sMEAN.nc' %(tmpfile2))
        elif (var_kind == 'ann'):
            cdo.yearmean(input = varfile,
                         output = '%sMEAN.nc' %(tmpfile))
            cdo.yearmean(input = varfile2,
                         output = '%sMEAN.nc' %(tmpfile2))
        else:
            cdo.selname(varname, input = '%s.nc' %(tmpfile),
                        output = '%sMEAN.nc' %(tmpfile))
            cdo.selname(varname2, input = '%s.nc' %(tmpfile2),
                        output = '%sMEAN.nc' %(tmpfile2))

        corfile = cdo.timcor(options = "-b 64",
                             input = varfile + ' ' +  varfile2)
        varnamefile = cdo.chname('%s,%s%s' %(varname, variable, variable2),
                             input = corfile)
        unitname = cdo.setunit('-', input = varnamefile)
        cdo.setvrange('-1,1', input = unitname,
                      output = '%sMEAN_CORR.nc' %(outfile))

        if region:
            # loop over regions
            for reg in region:
                print 'Region is %s' %(reg)
                area = region
                mask = np.loadtxt(
                    '/home/rlorenz/scripts/plot_scripts/areas_txt/%s.txt' %(
                        reg))

                lonmax = np.max(mask[:, 0])
                lonmin = np.min(mask[:, 0])
                latmax = np.max(mask[:, 1]) 
                latmin = np.min(mask[:, 1])
                cdo.sellonlatbox(lonmin,lonmax,latmin,latmax,
                                 input = '%sMEAN_CORR.nc' %(outfile),
                                 output = '%sMEAN_CORR_%s.nc' %(outfile, reg))

# clean up workdir
filelist = glob.glob(workdir + '*')
for f in filelist:
    os.remove(f)

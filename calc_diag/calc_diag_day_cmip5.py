#!/usr/bin/python
'''
File Name : calc_diag_day_cmip5.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 05-01-2017
Modified: Thu 05 Jan 2017 11:36:18 AM CET
Purpose: calculate diagnostics based on daily data from cmip5 data
         for further use, e.g. emergent_constraints plotting, Mult_Diag_lin_Reg

'''
import netCDF4 as nc # to work with NetCDF files
import numpy as np
from cdo import *   # load cdo functionality
cdo = Cdo()
from os.path import expanduser
home = expanduser("~") # Get users home directory
import glob
import sys
sys.path.insert(0,home+'/scripts/plot_scripts/utils/')
from func_read_data import func_read_netcdf
from point_inside_polygon import point_inside_polygon
import datetime as dt
from netcdftime import utime
from func_write_netcdf import func_write_netcdf

###
# Define input
###
variable = 'tasmax'
var_kind = 'seas'    #kind is ann: annual, mon: monthly, seas: seasonal
res = 'JJA'                       #choose data for particular month or season?
region = 'EUR'           #cut data over region?

archive = '/net/atmos/data/cmip5-ng'
workdir = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/%s/work/' %(
    variable)
outdir = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/%s/day/' %(variable)
syear = 2065
eyear = 2099
nyears = eyear - syear + 1

experiment = 'rcp45'
grid = 'g025'

if (os.access(workdir,os.F_OK) == False):
    os.makedirs(workdir)
if (os.access(outdir,os.F_OK) == False):
    os.makedirs(outdir)

if region != None:
    print 'Region is %s' %(region)
    area = region
    mask = np.loadtxt('/home/rlorenz/scripts/plot_scripts/areas_txt/%s.txt' %(
        region))

###read model data
print "Read model data"
print 'Processing variable %s %s' %(variable,
                                    var_kind)
##find all matching files in archive, loop over all of them
#first count matching files in folder:
infile = variable + '_day'
name = '%s/%s/%s_*_%s_*_%s.nc' %(archive, variable, infile, experiment,
                                 grid)
nfiles = len(glob.glob(name))
model_names = []
d_temp_mod_ts = dict()
d_temp_mod_clim = dict()
d_temp_mod_std = dict()
d_temp_mod_trnd = dict()

print str(nfiles)+ ' matching files found'
f = 0
for filename in glob.glob(name):
    print "Processing " + filename
    fh = nc.Dataset(filename, mode = 'r')
    unit = fh.variables[variable].units
    model = fh.source_model
    if model == 'ACCESS1.3':
        model = 'ACCESS1-3'
    elif model == 'FGOALS_g2':
        model = 'FGOALS-g2'
    ens = fh.source_ensemble
    model_names.append(model+'_'+ens)
    fh.close()

    tmpfile = '%s/%s_%s_%s_%s_%s-%s_%s' %(workdir, infile, model,
                                          experiment, ens, syear, eyear,
                                          res)
    outfile = '%s/%s_%s_%s_%s_%s-%s_%s' %(outdir, infile, model,
                                          experiment, ens, syear, eyear,
                                          res)
    cdo.selseas(res, input = "-seldate,%s-01-01,%s-12-31 -sellonlatbox,-180,180,-90,90 %s"
                %(syear, eyear, filename), output = '%s.nc' %(tmpfile))
    if (variable == 'pr') and  (unit == 'kg m-2 s-1'):
        unitfile = cdo.mulc(24 * 60 * 60, input = '%s.nc' %(tmpfile))
        cdo.chunit('kg m-2 s-1','mm/day', input = unitfile, output = '%s.nc' %(tmpfile))
    cdo.seasmean(input = '%s.nc' %(tmpfile),
                 output = '%sMEAN.nc' %(outfile)) 
    cdo.detrend(input = '%sMEAN.nc' %(outfile),
                output = '%sMEAN_DETREND.nc' %(tmpfile))
    cdo.sub(input = '%sMEAN_DETREND.nc %sMEAN.nc' %(tmpfile, outfile),
            output = '%sMEAN_RESID.nc' %(tmpfile))
    cdo.sub(input = '%sMEAN_RESID.nc %sMEAN_DETREND.nc' %(tmpfile,
                                                          tmpfile),
            output = '%sMEAN_TREND.nc' %(outfile))
    cdo.splityear(input = '%sMEAN_TREND.nc' %(outfile),
                  output = '%s/year' %(workdir))
    cdo.splityear(input = '%s.nc' %(tmpfile),
                  output = '%s/daily' %(workdir))
    cdo.sub(input = '%s/daily%s.nc %s/year%s.nc' %(workdir, eyear, 
                                                   workdir, eyear),
            output = '%s_DAILYDETREND.nc' %(tmpfile))
    cdo.timstd(input = '%s_DAILYDETREND.nc' %(tmpfile), output = '%s_DSV.nc' %(outfile))

    if region:
        lonmax = np.max(mask[:, 0])
        lonmin = np.min(mask[:, 0])
        latmax = np.max(mask[:, 1]) 
        latmin = np.min(mask[:, 1])
        cdo.sellonlatbox(lonmin,lonmax,latmin,latmax,
                         input = '%s_DSV.nc' %(outfile),
                         output = '%s_DSV_%s.nc' %(outfile, region))

#save model names into separate text file
with open('%s%s_%s_all_%s_%s-%s.txt' %(outdir, variable, 
                                       var_kind, experiment,
                                       syear, eyear), "w") as text_file:
for m in range(len(model_names)):
    text_file.write(model_names[m] + "\n")

# clean up workdir
os.remove(workdir+'*')

#!/usr/bin/python
'''
File Name : calc_diag_cmip5.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 01-07-2016
Modified: Fri 16 Dec 2016 18:52:35 CET
Purpose: Script calculating diagnostics from cmip5 data for further use
         e.g. in Mult_Diag_Lin_Reg


'''
import netCDF4 as nc # to work with NetCDF files
import numpy as np
from scipy import signal
import os # operating system interface
from os.path import expanduser
home = expanduser("~") # Get users home directory
import glob
import sys
sys.path.insert(0,home+'/scripts/plot_scripts/utils/')
from func_read_data import func_read_netcdf
from clim_seas_TLL import clim_mon_TLL, std_mon_TLL, seas_avg_mon_TLL, seas_std_mon_TLL, clim_seas_mon_TLL, ann_avg_mon_TLL
from calc_trend_TLL import trend_TLL
from point_inside_polygon import point_inside_polygon
import datetime as dt
from netcdftime import utime
from func_write_netcdf import func_write_netcdf
from mpl_toolkits.basemap import maskoceans, shiftgrid
###
# Define input
###
variable = ['tasmax', 'tasmax', 'tasmax']
var_file = ['clim', 'std', 'trnd']     #climatology:clim, variability:std, trend:trnd
var_kind = ['seas', 'seas', 'seas']    #kind is ann: annual means, mon: monthly means, seas: seasonal means
res = [2, 2, 2]                       #choose data for particular month or season?
region = 'EUR'           #cut data over region?

archive = '/net/atmos/data/cmip5-ng'
outdir = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/%s/' %(variable[0])
syear = 2065
eyear = 2099
nyears = eyear - syear + 1

experiment = 'rcp85'
grid = 'g025'

if (os.access(outdir, os.F_OK) == False):
    os.makedirs(outdir)

if region != None:
    print 'Region is %s' %(region)
    area = region
    mask = np.loadtxt('/home/rlorenz/scripts/plot_scripts/areas_txt/%s.txt' %(
        region))

###read model data
print "Read model data"
for v in xrange(len(variable)):
    print 'Processing variable %s %s %s' %(variable[v], var_file[v],
                                           var_kind[v])
    ##find all matching files in archive, loop over all of them
    #first count matching files in folder:
    infile = variable[v] + '_mon'
    name = '%s/%s/%s_*_%s_*_%s.nc' %(archive, variable[v], infile, experiment,
                                     grid)
    nfiles = len(glob.glob(name))
    model_names = []
    d_temp_mod_ts = dict()
    d_temp_mod_clim = dict()
    d_temp_mod_std = dict()
    d_temp_mod_trnd = dict()

    print str(nfiles)+ ' matching files found'
    f = 0
    for filename in glob.glob(name):
        print "Read " + filename + " data"
        model_data = func_read_netcdf(filename, variable[v], var_units = True,
                                      syear = syear, eyear = eyear)
        temp_mod = model_data['data']
        mod_unit = model_data['unit']
        lat = model_data['lat']
        lon = model_data['lon']
        dates = model_data['dates']
        months = np.asarray([dates[i].month for i in xrange(len(dates))])
        years = np.asarray([dates[i].year for i in xrange(len(dates))])

        fh = nc.Dataset(filename, mode = 'r')
        model = fh.source_model
        if model == 'ACCESS1.3':
            model = 'ACCESS1-3'
        elif model == 'FGOALS_g2':
            model = 'FGOALS-g2'
        ens = fh.source_ensemble
        model_names.append(model+'_'+ens)
        fh.close()

        ## shiftgrid to -180 to 180 if necessary
        ## (to be compatible with area mask)
        if (lon[0] >= 0.0) and (lon[-1] > 180):
            lons_orig = lon[:]
            temp_mod, lon = shiftgrid(180., temp_mod, lon, start = False)

        #check that time axis and grid is identical for model0, modelX and obs
        if f != 0:
            if temp_mod0.shape != temp_mod.shape:
                print 'Warning: Dimension for model0 and modelX is different!'
                continue
            if (dates[0].year != dates0[0].year) or (dates[0].month != dates0[0].month):
                print 'time axis for model0 and modelX is not identical, exiting!'
                print 'model0 starts '+dates0[0]+ ', while modelX starts '+dates[0]
                sys.exit
        else:
            dates0 = dates[:]
            temp_mod0 = temp_mod[:]

        if isinstance(temp_mod, np.ma.core.MaskedArray):
            temp_mod = temp_mod.filled(np.NaN)
            #print type(temp_mod), temp_mod.shape
        if (variable[v] == 'pr') and  (mod_unit == 'kg m-2 s-1'):
            print 'convert pr to mm/day'
            tmp_mod = temp_mod * (24 * 60 * 60)
            temp_mod = tmp_mod[:]
            del tmp_mod
            mod_unit = 'mm/day'            
        if (variable[v] == 'sic') and  (model == "EC-EARTH"):
            with np.errstate(invalid = 'ignore'):
                temp_mod[temp_mod < 0.0] = np.NaN

        #if any region defined, cut data into region
        if region != None:   
            temp_mod_reg_nan = np.ndarray((temp_mod.shape))
            for ilat in xrange(len(lat)):
                for ilon in xrange(len(lon)):
                    if (point_inside_polygon(lat[ilat], lon[ilon],
                                             np.fliplr(mask))):
                        temp_mod_reg_nan[:, ilat, ilon] = temp_mod[:, ilat, ilon]
                    else:
                        temp_mod_reg_nan[:, ilat, ilon] = np.NaN
            idx_lat = np.where((lat > np.min(mask[:, 1])) & (lat < np.max(mask[:, 1])))
            lat = lat[idx_lat]
            tmp1_mod_reg = np.empty((len(temp_mod_reg_nan), len(lat), len(lon)))
            idx_lon = np.where((lon > np.min(mask[:, 0])) & (lon < np.max(mask[:, 0])))
            lon = lon[idx_lon]
            temp_mod_reg = np.empty((len(temp_mod_reg_nan), len(lat), len(lon)))
            tmp1_mod_reg = np.squeeze(temp_mod_reg_nan[:, idx_lat, :], 1)
            temp_mod_reg = np.squeeze(tmp1_mod_reg[:, :, idx_lon], 2)
            del temp_mod
        else:
            temp_mod_reg = temp_mod

        #calculate timeseries and save for each model
        if var_kind[v] == 'mon':
            ind=np.where(months == res[v])
            temp_mod_ts = np.squeeze(temp_mod_reg[ind, :, :])
            d_temp_mod_ts[model + '_' + ens] = temp_mod_ts
            time_out = model_data['time'][ind]
        elif var_kind[v] == 'seas':
            temp_mod_clim = np.empty((1, len(lat), len(lon)))
            temp_mod_clim_seas = seas_avg_mon_TLL(temp_mod_reg, months)
            temp_mod_ts = clim_seas_mon_TLL(temp_mod_reg, months, years)[res[0], :, :, :]
            time_out = model_data['time'][0::12]
            d_temp_mod_ts[model + '_' + ens] = temp_mod_ts

        #save ts data
        outfile = '%s%s_%s_%s_%s_%s_%s_%s_%s-%s.nc' %(outdir, variable[v], 'ts',
                                                      var_kind[v], model, ens,
                                                      area, experiment, syear,
                                                      eyear)
        if len(time_out) > 1:
            func_write_netcdf(outfile, temp_mod_ts, variable[v], lon, lat,
                              timevar = time_out,
                              time_unit = model_data['tunit'],
                              time_cal = model_data['tcal'],
                              var_units = mod_unit,
                              Description = '%s timeseries over %s %s' %(
                                  variable[v], var_kind[v], res))
        else:
            func_write_netcdf(outfile, temp_mod_ts, variable[v], lon, lat,
                              var_units = mod_unit,
                              Description = '%s timeseries over %s %s' %(
                                  variable[v], var_kind[v], res))

        #calculate climatology
        if (var_file[v] == 'clim'):
            if res[v] == None:
                time_out = range(0,12)
                temp_mod_clim = clim_mon_TLL(temp_mod_reg,months)
            elif var_kind[v] == 'mon':
                ind=np.where(months==res[v])
                time_out = [1]
                temp_mod_clim = np.nanmean(np.squeeze(temp_mod_reg[ind, :, :]),
                                           axis = 0, keepdims = True)
            elif var_kind[v] == 'seas':
                temp_mod_clim = np.empty((1, len(lat), len(lon)))
                time_out = [1]
                temp_mod_clim_seas = seas_avg_mon_TLL(temp_mod_reg, months)
                temp_mod_clim[0, :, :] = temp_mod_clim_seas[res[v], :, :]
                 
                d_temp_mod_clim[model + '_' + ens] = temp_mod_clim
            #save climatology data
            outfile = '%s%s_%s_%s_%s_%s_%s_%s_%s-%s.nc' %(outdir,
                                                          variable[v],
                                                          var_file[v],
                                                          var_kind[v],
                                                          model, ens, area,
                                                          experiment,syear,
                                                          eyear)
            if len(time_out) > 1:
                func_write_netcdf(outfile, temp_mod_clim, variable[v],
                                  lon, lat, timevar = time_out,
                                  time_unit = 'month',
                                  var_units = mod_unit,
                                  Description = '%s climatology over %s %s' %(
                                      variable[v], var_kind[v], res))
            else:
                func_write_netcdf(outfile, temp_mod_clim, variable[v],
                                  lon, lat, var_units = mod_unit,
                                  Description = '%s climatology over %s %s' %(
                                      variable[v], var_kind[v], res))

        #detrend ts before calculating variability
        if (var_file[v] == 'std'):
            if np.isnan(temp_mod_ts).any():
                temp_mod_reg_dtr = np.empty((temp_mod_ts.shape))
                for ilat in xrange(len(lat)):
                    for ilon in xrange(len(lon)):
                        tmp = temp_mod_ts[:,ilat,ilon]
                        if np.isnan(tmp).any():
                            temp_mod_reg_dtr[:,ilat,ilon] = np.NaN
                        else:
                            temp_mod_reg_dtr[:,ilat,ilon] = signal.detrend(tmp,
                                            axis = 0, type = 'linear', bp = 0)
            else:
                temp_mod_reg_dtr = signal.detrend(temp_mod_ts, axis = 0)
            if (var_kind[v] == 'ann'):
                temp_mod_std = np.nanstd(temp_mod_reg_dtr, axis = 0,
                                         keepdims = True)
            elif (var_kind[v] == 'mon') or (var_kind[v] == 'seas'):
                temp_mod_std = np.nanstd(np.squeeze(temp_mod_reg_dtr), axis = 0,
                                         keepdims = True)
            time_out = [1]
            d_temp_mod_std[model + '_' + ens] = temp_mod_std
            #save variability data
            outfile = '%s%s_%s_%s_%s_%s_%s_%s_%s-%s.nc' %(outdir, variable[v],
                                                          var_file[v],
                                                          var_kind[v],
                                                          model, ens, area,
                                                          experiment, syear,
                                                          eyear)
            if len(time_out) > 1:
                func_write_netcdf(outfile, temp_mod_std, variable[v], lon, lat,
                                  timevar = time_out, time_unit = 'month',
                                  var_units = mod_unit,
                                  Description = '%s variability over %s %s' %(variable[v], var_kind[v], res))
            else:
                func_write_netcdf(outfile, temp_mod_std, variable[v], lon, lat,
                                  var_units = mod_unit,
                                  Description = '%s variability over %s %s' %(variable[v], var_kind[v], res))

        #calculate trend over time defined in res
        if (var_file[v] == 'trnd'):
            if res[v] != None:
                if (var_kind[v] == 'mon'):    
                    ind=np.where(months == res[v])
                    time_out = [1]
                    temp_mod_trnd = trend_TLL(np.squeeze(temp_mod_reg[ind,:,:]),
                                              drop = False)
                elif (var_kind[v] == 'seas'):
                    time_out = [1]
                    temp_mod_trnd = trend_TLL(np.squeeze(d_temp_mod_ts[model +
                                                                       '_' +
                                                                       ens]),
                                              drop = False)
            else:
                temp_mod_trnd = trend_TLL(np.squeeze(temp_mod_ts), drop = False)
                time_out = [1]
            d_temp_mod_trnd[model + '_' + ens] = temp_mod_trnd
            #save trend data
            outfile = '%s%s_%s_%s_%s_%s_%s_%s_%s-%s.nc' %(outdir, variable[v],
                                                          var_file[v],
                                                          var_kind[v], model,
                                                          ens, area, experiment,
                                                          syear, eyear)
            if len(time_out) > 1:
                func_write_netcdf(outfile, temp_mod_trnd, variable[v], lon, lat,
                                  timevar = time_out, time_unit = 'month',
                                  var_units = mod_unit,
                                  Description = '%s trend over %s %s' %(variable[v], var_kind[v], res))
            else:
                func_write_netcdf(outfile, temp_mod_trnd, variable[v], lon, lat,
                                  var_units = mod_unit,
                                  Description = '%s trend over %s %s' %(variable[v], var_kind[v], res))

        f = f + 1

        #save model names into separate text file
        with open('%s%s_%s_%s_%s_all_%s_%s-%s.txt' %(outdir, variable[v],
                                                     var_file[v], var_kind[v],
                                                     area, experiment, syear,
                                                     eyear), "w") as text_file:
            for m in range(len(model_names)):
                text_file.write(model_names[m] + "\n")

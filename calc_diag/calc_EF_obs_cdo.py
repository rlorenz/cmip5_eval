#!/usr/bin/python
'''
File Name : calc_EF_obs_cdo.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 28-07-2017
Modified: Thu 29 Jun 2017 04:08:36 PM CEST
Purpose: Script calculating Evaporative Fraction from obs data for further use
         e.g. in Mult_Diag_Lin_Reg

'''
import netCDF4 as nc # to work with NetCDF files
import numpy as np
from cdo import *   # load cdo functionality
cdo = Cdo()
import os
from os.path import expanduser
home = expanduser("~") # Get users home directory
import glob

###
# Define input
###
variable = 'hfls'
variable2 = 'hfss'
var_kind = 'seas'    # kind is ann: annual, mon: monthly, seas: seasonal
seasons = ['DJF', 'MAM', 'JJA', 'SON'] # choose data for particular season?
region = ['EUR', 'CNEU']               # cut data over region?
obs = ['ERAint', 'MERRA2']             # 'ERAint' or 'MERRA2'

workdir = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/ef/work/'
outdir = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/ef/mon/'
syear = 1980
eyear = 2014
nyears = eyear - syear + 1

experiment = 'rcp85'
grid = 'g025'
masko = True

pathtrop = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/'
if masko:
    oceanmask = "%s/scripts/plot_scripts/areas_txt/seamask_%s.nc" %(home, grid)
    outdir = '%s/ef/mon/maskT/' %(pathtrop)
else:
    outdir = '%s/ef/mon/maskF/' %(pathtrop)

if (os.access(workdir,os.F_OK) == False):
    os.makedirs(workdir)
if (os.access(outdir, os.F_OK) == False):
    os.makedirs(outdir)

#read obs data
for dataset in obs:
    if dataset == 'ERAint':
        print "Read ERAint data"
        path = '/net/tropo/climphys/rlorenz/Datasets/ERAint/ERAint_mon'
        if (variable == 'hfls'):
            filename = '%s/slhf_monthly_ERAint_197901-201512_%s.nc' %(path,grid)
            varname = 'slhf'
        if (variable2 == 'hfss'):
            filename2 = '%s/sshf_monthly_ERAint_197901-201512_%s.nc' %(path,
                                                                      grid)
            varname2 = 'sshf'
    elif dataset == 'MERRA2':
        print "Read MERRA2 data"
        path = '/net/tropo/climphys/rlorenz/Datasets/MERRA2/monthly'
        if (variable == 'hfls'):
            filename = '%s/MERRA2.tavgmon_2d_flx_Nx.EFLUX.1980-2015_remapbil_%s.nc' %(path,grid)
            varname = 'EFLUX'
        if (variable2 == 'hfss'):
            varname2 = 'HFLUX'
            filename2 = '%s/MERRA2.tavgmon_2d_flx_Nx.HFLUX.1980-2015_remapbil_%s.nc' %(path, grid)
    elif (dataset == 'Obs'):
        print "Do not have sensible heat flux from Obs"
        sys.exit

    for res in seasons:
        tmpfile = '%s/%s_mon_%s_%s-%s_%s' %(workdir, variable, dataset, syear,
                                            eyear, res)
        tmpfile2 = '%s/%s_mon_%s_%s-%s_%s' %(workdir, variable2, dataset, syear,
                                             eyear, res)

        outfile = '%s/ef_mon_%s_%s-%s_%s' %(outdir, dataset, syear, eyear, res)

        if (var_kind == 'seas'):
            cdo.selseas(res, options = "-f nc", input = 
                        "-seldate,%s-01-01,%s-12-31 -sellonlatbox,-180,180,-90,90 %s"
                        %(syear, eyear, filename), output = '%s.nc' %(tmpfile))
            cdo.selseas(res, options = "-f nc", input = 
                        "-seldate,%s-01-01,%s-12-31 -sellonlatbox,-180,180,-90,90  %s"
                        %(syear, eyear, filename2),
                        output = '%s.nc' %(tmpfile2))
        else:
            cdo.seldate('%s-01-01,%s-12-31' %(syear, eyear), options = "-f nc",
                        input = "-sellonlatbox,-180,180,-90,90 %s"
                        %(filename), output = '%s.nc' %(tmpfile))
            cdo.seldate('%s-01-01,%s-12-31' %(syear, eyear), options = "-f nc",
                        input = "-sellonlatbox,-180,180,-90,90 %s"
                        %(filename2), output = '%s.nc' %(tmpfile2))
        if masko:
            maskfile = '%s_masko.nc' %(tmpfile)
            cdo.setmissval(0, input = "-mul -eqc,1 %s %s.nc" %(oceanmask,
                                                               tmpfile),
                           output = '%s' %(maskfile))
            os.system("mv %s %s.nc" %(maskfile, tmpfile))
            maskfile2 = '%s_masko.nc' %(tmpfile2)
            cdo.setmissval(0, input = "-mul -eqc,1 %s %s.nc" %(oceanmask,
                                                               tmpfile2),
                           output = '%s' %(maskfile))
            os.system("mv %s %s.nc" %(maskfile, tmpfile2))

        varfile = cdo.selname(varname, input = '%s.nc' %(tmpfile))
        varfile2 = cdo.selname(varname2, input = '%s.nc' %(tmpfile2))

        if (var_kind == 'seas'):
            cdo.seasmean(input = varfile,
                         output = '%sMEAN.nc' %(tmpfile))
            cdo.seasmean(input = varfile2,
                          output = '%sMEAN.nc' %(tmpfile2))
        elif (var_kind == 'ann'):
            cdo.yearmean(input = varfile,
                         output = '%sMEAN.nc' %(tmpfile))
            cdo.yearmean(input = varfile2,
                         output = '%sMEAN.nc' %(tmpfile2))
        else:
            cdo.selname(varname, input = '%s.nc' %(tmpfile),
                        output = '%sMEAN.nc' %(tmpfile))
            cdo.selname(varname2, input = '%s.nc' %(tmpfile2),
                        output = '%sMEAN.nc' %(tmpfile2))

        sumfile = cdo.add(input = varfile + ' ' +  varfile2)
        effile = cdo.div(input = varfile + ' ' + sumfile)
        efvarname = cdo.chname('%s,ef' %(varname), input = effile)
        unitname = cdo.setunit('-', input = efvarname)
        cdo.setvrange('0,1', input = unitname, output = '%s.nc' %(outfile))

        if (var_kind == 'seas'):
            cdo.seasmean(input = '%s.nc' %(outfile),
                         output = '%sMEAN.nc' %(outfile))
            cdo.yseasmean(input = '%s.nc' %(outfile),
                          output = '%sMEAN_CLIM.nc' %(outfile))
        elif (var_kind == 'ann'):
            cdo.yearmean(input = '%s.nc' %(outfile),
                         output = '%sMEAN.nc' %(outfile))
            cdo.timmean(input = '%s.nc' %(outfile),
                           output = '%sMEAN_CLIM.nc' %(outfile))
        else:
            cdo.ymonmean(input = '%s.nc' %(outfile),
                          output = '%sMEAN_CLIM.nc' %(outfile))
        cdo.detrend(input = '%sMEAN.nc' %(outfile),
                    output = '%s/ef_mon_%s_%s-%s_%sMEAN_DETREND.nc' %(
                        workdir, dataset, syear, eyear, res))
        cdo.regres(input = '%sMEAN.nc' %(outfile),
                   output = '%sMEAN_TREND.nc' %(outfile))
        cdo.timstd(input = '%s/ef_mon_%s_%s-%s_%sMEAN_DETREND.nc' %(
            workdir, dataset, syear, eyear, res),
                   output = '%sMEAN_STD.nc' %(outfile))

        if region:
            # loop over regions
            for reg in region:
                print 'Region is %s' %(reg)
                area = region
                mask = np.loadtxt(
                    '/home/rlorenz/scripts/plot_scripts/areas_txt/%s.txt' %(
                        reg))

                lonmax = np.max(mask[:, 0])
                lonmin = np.min(mask[:, 0])
                latmax = np.max(mask[:, 1]) 
                latmin = np.min(mask[:, 1])
                cdo.sellonlatbox(lonmin,lonmax,latmin,latmax,
                                 input = '%sMEAN_CLIM.nc' %(outfile),
                                 output = '%sMEAN_CLIM_%s.nc' %(outfile, reg))
                cdo.sellonlatbox(lonmin,lonmax,latmin,latmax,
                                 input = '%sMEAN_STD.nc' %(outfile),
                                 output = '%sMEAN_STD_%s.nc' %(outfile, reg))
                cdo.sellonlatbox(lonmin,lonmax,latmin,latmax,
                                 input = '%sMEAN_TREND.nc' %(outfile),
                                 output = '%sMEAN_TREND_%s.nc' %(outfile, reg))
                cdo.sellonlatbox(lonmin,lonmax,latmin,latmax,
                                 input = '%sMEAN.nc' %(outfile),
                                 output = '%sMEAN_%s.nc' %(outfile, reg))

# clean up workdir
filelist = glob.glob(workdir + '*')
for f in filelist:
    os.remove(f)

#!/usr/bin/python
'''
File Name : calc_diag_cmip5_cdo.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 06-01-2017
Modified: Tue 15 Aug 2017 21:38:23 CEST
Purpose: Script calculating diagnostics from cmip5 data for further use
         e.g. in Mult_Diag_Lin_Reg

'''
import netCDF4 as nc # to work with NetCDF files
import numpy as np
from cdo import *   # load cdo functionality
cdo = Cdo()
import os
from os.path import expanduser
home = expanduser("~") # Get users home directory
import sys
sys.path.insert(0, '%s/scripts/plot_scripts/utils/' %(home))
sys.path.insert(0, '%s/scripts/plot_scripts/utils/utils_lukbrunn' %(home))
import glob
from get_filenames import Filenames
import logging
from info_utils import set_logger
logger = logging.getLogger(__name__)
set_logger(level = logging.INFO)
logger.info('Start')
###
# Define input
###
varnames = ['tas', 'pr', 'psl']
#varnames = ['hfls']
#varnames = ['pr', 'huss', 'psl', 'rlus', 'rsds', 'hfls', 'tasmax', 'tasmin']
var_kind = 'seas'    #kind is ann: annual, mon: monthly, seas: seasonal
seasons = ['DJF', 'MAM', 'JJA', 'SON']      #choose data for particular month or season?
region = ['EUR_3SREX']      #cut data over region?
syear = 2031
eyear = 2060
nyears = eyear - syear + 1

experiment = 'rcp26'
grid = 'g025'
masko = True

archive = '/net/atmos/data/cmip5-ng'
pathtrop = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/'

for variable in varnames:
    workdir = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/%s/work/' %(
        variable)

    if masko:
        oceanmask = "%s/scripts/plot_scripts/areas_txt/seamask_%s.nc" %(home,
                                                                        grid)
        outdir = '%s/%s/mon/maskT/' %(pathtrop, variable)
    else:
        outdir = '%s/%s/mon/maskF/' %(pathtrop, variable)

    if (os.access(workdir,os.F_OK) == False):
        os.makedirs(workdir)
    if (os.access(outdir, os.F_OK) == False):
        os.makedirs(outdir)

    ### read model data
    logger.info("Read model data")
    logger.info('Processing variable %s %s' %(variable, var_kind))
    ## find all matching files in archive, loop over all of them
    # first count matching files in folder:
    model_names = []
    fn = Filenames(
        file_pattern = '{varn}/{varn}_mon_{model}_{scenario}_{ensemble}_g025.nc'
        , base_path = archive
    )
    fn.apply_filter(varn = variable, scenario = [experiment])
    #models = fn.get_variable_values('model')
    files = fn.get_filenames()

    logger.info(str(len(files)) + ' matching files found')

    for filename in files:
        logger.info("Processing " + filename)
        fh = nc.Dataset(filename, mode = 'r')
        unit = fh.variables[variable].units
        model = fh.source_model
        if model == 'ACCESS1.3':
            model = 'ACCESS1-3'
        elif model == 'FGOALS_g2':
            model = 'FGOALS-g2'
        ens = fh.source_ensemble
        model_names.append(model + '_' + ens)
        fh.close()

        for res in seasons:
            tmpfile = '%s/%s_mon_%s_%s_%s_%s-%s_%s' %(workdir, variable, model,
                                                  experiment, ens, syear, eyear,
                                                  res)
            outfile = '%s/%s_mon_%s_%s_%s_%s-%s_%s' %(outdir, variable, model,
                                                  experiment, ens, syear, eyear,
                                                  res)
            if (var_kind == 'seas'):
                cdo.selseas(res, input = 
                            "-seldate,%s-01-01,%s-12-31 -sellonlatbox,-180,180,-90,90 %s"
                            %(syear, eyear, filename),
                            output = '%s.nc' %(tmpfile))
            else:
                cdo.seldate('%s-01-01,%s-12-31' %(syear, eyear),
                            input = "-sellonlatbox,-180,180,-90,90 %s"
                            %(filename), output = '%s.nc' %(tmpfile))
            if (variable == 'pr') and  (unit == 'kg m-2 s-1'):
                unitfile = cdo.mulc(24 * 60 * 60, input = '%s.nc' %(tmpfile))
                cdo.mulc(24 * 60 * 60, input = '%s.nc' %(tmpfile),
                         output = unitfile)
                newunit = "mm/day"
                cdo.chunit('"kg m-2 s-1",%s' %(newunit), input = unitfile,
                           output = '%s.nc' %(tmpfile))
            if (variable == 'huss') and  ((unit == 'kg/kg') or 
                                          (unit == 'kg kg-1') or (unit == '1')):
                unitfile = cdo.mulc(1000, options = '-b 64',
                                    input = '%s.nc' %(tmpfile))
                newunit = "g/kg"
                if (unit == 'kg/kg'):
                    cdo.chunit('"kg/kg",%s' %(newunit), input = unitfile,
                               output = '%s.nc' %(tmpfile))
                if (unit == 'kg kg-1'):
                    cdo.chunit('"kg kg-1",%s' %(newunit), input = unitfile,
                               output = '%s.nc' %(tmpfile))
                if (unit == '1'):
                    cdo.chunit('"1",%s' %(newunit), input = unitfile,
                               output = '%s.nc' %(tmpfile))
            if (((variable == 'tas') or (variable == 'tasmax') or
                (variable == 'tasmin') or (variable == 'tos')) and 
                (unit == 'K')):
                unitfile = cdo.subc(273.15, input = '%s.nc' %(tmpfile))
                newunit = "degC"
                cdo.chunit('"K",%s' %(newunit), input = unitfile,
                           output = '%s.nc' %(tmpfile))

            if masko:
                maskfile = '%s_masko.nc' %(tmpfile)
                cdo.setmissval(0, input = "-mul -eqc,1 %s %s.nc" %(oceanmask,
                                                                   tmpfile),
                               output = '%s' %(maskfile))
                cdo.setmissval(1e20, input = '%s' %(maskfile),
                               output = '%s_newmiss.nc' %(tmpfile))
                os.system("mv '%s_newmiss.nc' %s.nc" %(tmpfile, tmpfile))
            if (variable == 'tos'):
                rangefile = '%s_vrange.nc' %(tmpfile)
                cdo.setvrange('0,40', input = '%s.nc' %tmpfile,
                              output = '%s' %rangefile)
                os.system("mv %s %s.nc" %(rangefile, tmpfile))
            if (var_kind == 'seas'):
                cdo.seasmean(input = '%s.nc' %(tmpfile),
                             output = '%sMEAN.nc' %(outfile))
                cdo.yseasmean(input = '%s.nc' %(tmpfile),
                              output = '%sMEAN_CLIM.nc' %(outfile))
            elif (var_kind == 'ann'):
                cdo.yearmean(input = '%s.nc' %(tmpfile),
                             output = '%sMEAN.nc' %(outfile))
                cdo.timmean(input = '%s.nc' %(tmpfile),
                               output = '%sMEAN_CLIM.nc' %(outfile))
            else:
                cdo.ymonmean(input = '%s.nc' %(tmpfile),
                              output = '%sMEAN_CLIM.nc' %(outfile))
            cdo.detrend(input = '%sMEAN.nc' %(outfile),
                        output = '%sMEAN_DETREND.nc' %(tmpfile))
            cdo.regres(input = '%sMEAN.nc' %(outfile),
                       output = '%sMEAN_TREND.nc' %(outfile))
            cdo.timstd(input = '%sMEAN_DETREND.nc' %(tmpfile),
                       output = '%sMEAN_STD.nc' %(outfile))

            if region:
                # loop over regions
                for reg in region:
                    logger.info('Region is %s' %(reg))
                    area = reg
                    mask = np.loadtxt(
                        '%s/scripts/plot_scripts/areas_txt/%s.txt' %(
                            home, reg))

                    lonmax = np.max(mask[:, 0])
                    lonmin = np.min(mask[:, 0])
                    latmax = np.max(mask[:, 1]) 
                    latmin = np.min(mask[:, 1])
                    cdo.sellonlatbox(lonmin,lonmax,latmin,latmax,
                                     input = '%sMEAN_CLIM.nc' %(outfile),
                                     output = '%sMEAN_CLIM_%s.nc' %(outfile,
                                                                    reg))
                    cdo.sellonlatbox(lonmin,lonmax,latmin,latmax,
                                     input = '%sMEAN_STD.nc' %(outfile),
                                     output = '%sMEAN_STD_%s.nc' %(outfile,
                                                                   reg))
                    cdo.sellonlatbox(lonmin,lonmax,latmin,latmax,
                                     input = '%sMEAN_TREND.nc' %(outfile),
                                     output = '%sMEAN_TREND_%s.nc' %(outfile,
                                                                     reg))
                    cdo.sellonlatbox(lonmin,lonmax,latmin,latmax,
                                     input = '%sMEAN.nc' %(outfile),
                                     output = '%sMEAN_%s.nc' %(outfile, reg))

    #save model names into separate text file
    with open('%s%s_%s_all_%s_%s-%s.txt' %(outdir, variable, res, experiment,
                                           syear, eyear), "w") as text_file:
        for m in range(len(model_names)):
            text_file.write(model_names[m] + "\n")

    # clean up workdir
    filelist = glob.glob(workdir + '*')
    for f in filelist:
        os.remove(f)

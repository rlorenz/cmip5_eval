#!/usr/bin/python
'''
File Name : calc_Rnet_cmip5_cdo.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 15-06-2017
Modified: Tue 15 Aug 2017 21:41:19 CEST
Purpose: Script calculating net radiation from cmip5 data for further use
         e.g. in Mult_Diag_Lin_Reg

'''
import netCDF4 as nc # to work with NetCDF files
import numpy as np
from cdo import *   # load cdo functionality
cdo = Cdo()
import os
from os.path import expanduser
home = expanduser("~") # Get users home directory
import glob

###
# Define input
###
variable = 'rlds'
variable2 = 'rlus'
variable3 = 'rsds'
variable4 = 'rsus'
var_kind = 'seas'    #kind is ann: annual, mon: monthly, seas: seasonal
seasons = ['JJA']          #choose data for particular month or season?
region = ['EUR', 'CNEU']       #cut data over region?

archive = '/net/atmos/data/cmip5-ng'
workdir = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/rnet/work/'
outdir = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/rnet/mon/'
syear = 2070
eyear = 2099
nyears = eyear - syear + 1

experiment = 'rcp85'
grid = 'g025'
masko = True

pathtrop = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/'
if masko:
    oceanmask = "%s/scripts/plot_scripts/areas_txt/seamask_%s.nc" %(home, grid)
    outdir = '%s/rnet/mon/maskT/' %(pathtrop)
else:
    outdir = '%s/rnet/mon/maskF/' %(pathtrop)

if (os.access(workdir,os.F_OK) == False):
    os.makedirs(workdir)
if (os.access(outdir, os.F_OK) == False):
    os.makedirs(outdir)


### read model data
print "Read model data"
print 'Processing variable %s %s' %(variable,
                                    var_kind)
## find all matching files in archive, loop over all of them
# first count matching files in folder:
infile = variable + '_mon'
name = '%s/%s/%s_*_%s_*_%s.nc' %(archive, variable, infile, experiment,
                                 grid)
nfiles = len(glob.glob(name))

model_names = []

print str(nfiles) + ' matching files found'
for filename in glob.glob(name):
    print "Processing " + filename
    fh = nc.Dataset(filename, mode = 'r')
    unit = fh.variables[variable].units
    model = fh.source_model
    if model == 'ACCESS1.3':
        model = 'ACCESS1-3'
    elif model == 'FGOALS_g2':
        model = 'FGOALS-g2'
    ens = fh.source_ensemble
    model_names.append(model + '_' + ens)
    fh.close()

    # check if file exists for other variables, otherwise continue
    filename2 = '%s/%s/%s_mon_%s_%s_%s_%s.nc' %(archive, variable2, variable2,
                                                model, experiment, ens, grid)
    if (os.path.isfile(filename2) == False):
        continue
    filename3 = '%s/%s/%s_mon_%s_%s_%s_%s.nc' %(archive, variable3, variable3,
                                                model, experiment, ens, grid)
    if (os.path.isfile(filename3) == False):
        continue
    filename4 = '%s/%s/%s_mon_%s_%s_%s_%s.nc' %(archive, variable4, variable4,
                                                model, experiment, ens, grid)
    if (os.path.isfile(filename4) == False):
        continue
        
    for res in seasons:
        tmpfile = '%s/%s_%s_%s_%s_%s-%s_%s' %(workdir, infile, model,
                                              experiment, ens, syear, eyear,
                                              res)
        tmpfile2 = '%s/%s_mon_%s_%s_%s_%s-%s_%s' %(workdir, variable2, model,
                                                   experiment, ens, syear,
                                                   eyear, res)
        tmpfile3 = '%s/%s_mon_%s_%s_%s_%s-%s_%s' %(workdir, variable3, model,
                                                   experiment, ens, syear, 
                                                   eyear, res)
        tmpfile4 = '%s/%s_mon_%s_%s_%s_%s-%s_%s' %(workdir, variable4, model,
                                                   experiment, ens, syear, 
                                                   eyear, res)

        outfile = '%s/rnet_mon_%s_%s_%s_%s-%s_%s' %(outdir, model,
                                                    experiment, ens, syear, 
                                                    eyear, res)

        if (var_kind == 'seas'):
            cdo.selseas(res, input = 
                        "-seldate,%s-01-01,%s-12-31 -sellonlatbox,-180,180,-90,90 %s"
                        %(syear, eyear, filename), output = '%s.nc' %(tmpfile))
            cdo.selseas(res, input = 
                        "-seldate,%s-01-01,%s-12-31 -sellonlatbox,-180,180,-90,90 %s"
                        %(syear, eyear, filename2), output = '%s.nc' %(tmpfile2))
            cdo.selseas(res, input = 
                        "-seldate,%s-01-01,%s-12-31 -sellonlatbox,-180,180,-90,90 %s"
                        %(syear, eyear, filename3), output = '%s.nc' %(tmpfile3))
            cdo.selseas(res, input = 
                        "-seldate,%s-01-01,%s-12-31 -sellonlatbox,-180,180,-90,90 %s"
                        %(syear, eyear, filename4), output = '%s.nc' %(tmpfile4))

        else:
            cdo.seldate('%s-01-01,%s-12-31' %(syear, eyear),
                        input = "-sellonlatbox,-180,180,-90,90 %s"
                        %(filename), output = '%s.nc' %(tmpfile))
            cdo.seldate('%s-01-01,%s-12-31' %(syear, eyear),
                        input = "-sellonlatbox,-180,180,-90,90 %s"
                        %(filename2), output = '%s.nc' %(tmpfile2))
            cdo.seldate('%s-01-01,%s-12-31' %(syear, eyear),
                        input = "-sellonlatbox,-180,180,-90,90 %s"
                        %(filename3), output = '%s.nc' %(tmpfile3))
            cdo.seldate('%s-01-01,%s-12-31' %(syear, eyear),
                        input = "-sellonlatbox,-180,180,-90,90 %s"
                        %(filename4), output = '%s.nc' %(tmpfile4))
        if masko:
            maskfile = '%s_masko.nc' %(tmpfile)
            cdo.setmissval(0, input = "-mul -eqc,1 %s %s.nc" %(oceanmask,
                                                               tmpfile), 
                           output = '%s' %(maskfile))
            os.system("mv %s %s.nc" %(maskfile, tmpfile))
            maskfile2 = '%s_masko.nc' %(tmpfile2)
            cdo.setmissval(0, input = "-mul -eqc,1 %s %s.nc" %(oceanmask,
                                                               tmpfile2),
                           output = '%s' %(maskfile))
            os.system("mv %s %s.nc" %(maskfile, tmpfile2))
            maskfile3 = '%s_masko.nc' %(tmpfile3)
            cdo.setmissval(0, input = "-mul -eqc,1 %s %s.nc" %(oceanmask,
                                                               tmpfile3),
                           output = '%s' %(maskfile))
            os.system("mv %s %s.nc" %(maskfile, tmpfile3))
            maskfile4 = '%s_masko.nc' %(tmpfile3)
            cdo.setmissval(0, input = "-mul -eqc,1 %s %s.nc" %(oceanmask,
                                                               tmpfile4),
                           output = '%s' %(maskfile))
            os.system("mv %s %s.nc" %(maskfile, tmpfile4))

        lwnet = cdo.sub(input = '%s.nc %s.nc' %(tmpfile, tmpfile2))
        swnet = cdo.sub(input = '%s.nc %s.nc' %(tmpfile3, tmpfile4))
        rnetfile = cdo.add(input = '%s %s' %(lwnet, swnet))
        cdo.chname('%s,rnet' %(variable), input = rnetfile,
                   output = '%s.nc' %(outfile))

        if (var_kind == 'seas'):
            cdo.seasmean(input = '%s.nc' %(outfile),
                         output = '%sMEAN.nc' %(outfile))
            cdo.yseasmean(input = '%s.nc' %(outfile),
                          output = '%sMEAN_CLIM.nc' %(outfile))
        elif (var_kind == 'ann'):
            cdo.yearmean(input = '%s.nc' %(outfile),
                         output = '%sMEAN.nc' %(outfile))
            cdo.timmean(input = '%s.nc' %(outfile),
                           output = '%sMEAN_CLIM.nc' %(outfile))
        else:
            cdo.ymonmean(input = '%s.nc' %(outfile),
                          output = '%sMEAN_CLIM.nc' %(outfile))
        cdo.detrend(input = '%sMEAN.nc' %(outfile),
                    output = '%s/ef_mon_%s_%s_%s_%s-%s_%sMEAN_DETREND.nc' %(
                        workdir, model, experiment, ens, syear, eyear, res))
        cdo.regres(input = '%sMEAN.nc' %(outfile),
                   output = '%sMEAN_TREND.nc' %(outfile))
        cdo.timstd(input = '%s/ef_mon_%s_%s_%s_%s-%s_%sMEAN_DETREND.nc' %(
                   workdir, model, experiment, ens, syear, eyear, res),
                   output = '%sMEAN_STD.nc' %(outfile))

        if region:
            # loop over regions
            for reg in region:
                print 'Region is %s' %(reg)
                area = reg
                mask = np.loadtxt(
                    '/home/rlorenz/scripts/plot_scripts/areas_txt/%s.txt' %(
                        reg))

                lonmax = np.max(mask[:, 0])
                lonmin = np.min(mask[:, 0])
                latmax = np.max(mask[:, 1]) 
                latmin = np.min(mask[:, 1])
                cdo.sellonlatbox(lonmin,lonmax,latmin,latmax,
                                 input = '%sMEAN_CLIM.nc' %(outfile),
                                 output = '%sMEAN_CLIM_%s.nc' %(outfile, reg))
                cdo.sellonlatbox(lonmin,lonmax,latmin,latmax,
                                 input = '%sMEAN_STD.nc' %(outfile),
                                 output = '%sMEAN_STD_%s.nc' %(outfile, reg))
                cdo.sellonlatbox(lonmin,lonmax,latmin,latmax,
                                 input = '%sMEAN_TREND.nc' %(outfile),
                                 output = '%sMEAN_TREND_%s.nc' %(outfile, reg))
                cdo.sellonlatbox(lonmin,lonmax,latmin,latmax,
                                 input = '%sMEAN.nc' %(outfile),
                                 output = '%sMEAN_%s.nc' %(outfile, reg))

    #save model names into separate text file
    with open('%srnet_%s_all_%s_%s-%s.txt' %(
        outdir, res, experiment, syear, eyear), "w") as text_file:
        for m in range(len(model_names)):
            text_file.write(model_names[m] + "\n")

# clean up workdir
filelist = glob.glob(workdir + '*')
for f in filelist:
    os.remove(f)

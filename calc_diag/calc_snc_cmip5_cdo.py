#!/usr/bin/python
'''
File Name : calc_diag_cmip5_cdo.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 06-01-2017
Modified: Tue 15 Aug 2017 21:38:23 CEST
Purpose: Script calculating diagnostics from cmip5 data for further use
         e.g. in Mult_Diag_Lin_Reg

'''
import netCDF4 as nc # to work with NetCDF files
import datetime as dt
from netcdftime import utime

import numpy as np
import glob
from mpl_toolkits.basemap import shiftgrid
from scipy import stats

#from cdo import *   # load cdo functionality
#cdo = Cdo()
import os
from os.path import expanduser
home = expanduser("~") # Get users home directory
import sys
sys.path.insert(0, home + '/scripts/plot_scripts/utils/')
from point_inside_polygon import point_inside_polygon
from get_area import get_atm_area
from clim_seas_TLL import seas_mon_TLL
from func_write_netcdf import func_write_ts_netcdf
###
# Define input
###
varnames = ['snc']
var_kind = 'seas'    #kind is ann: annual, mon: monthly, seas: seasonal
seasons = 'DJF'      #choose data for particular month or season?
seas = 1 # DJF: 0, MAM: 1, JJA: 2, SON: 3
region = 'CNEU'      #cut data over region?
#syear = 2006
#eyear = 2100
#nyears = eyear - syear + 1

experiment = 'historical'
realm = 'LImon'
grid = 'g025'
masko = True

archive = '/net/atmos/data/cmip5'
pathtrop = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/'
if region != None:
    mask = np.loadtxt('%s/scripts/plot_scripts/areas_txt/%s.txt' %(
        home, region))
area_sum_snc = dict()
trend = dict()
for variable in varnames:
    workdir = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/%s/work/' %(
        variable)

    if masko:
        oceanmask = "%s/scripts/plot_scripts/areas_txt/seamask_%s.nc" %(home,
                                                                        grid)
        outdir = '%s/%s/mon/maskT/' %(pathtrop, variable)
    else:
        outdir = '%s/%s/mon/maskF/' %(pathtrop, variable)

    if (os.access(workdir,os.F_OK) == False):
        os.makedirs(workdir)
    if (os.access(outdir, os.F_OK) == False):
        os.makedirs(outdir)

    ### read model data
    print "Read model data"
    print 'Processing variable %s %s' %(variable, var_kind)
    ## find all matching files in archive, loop over all of them
    # first count matching files in folder:
    try:
        infile =  '%s_%s_*_%s_*_%s01-%s12' %(variable, realm, experiment, syear, eyear)
    except NameError:
        infile =  '%s_%s_*_%s_*' %(variable, realm, experiment)
    name = '%s/%s/%s/%s/*/*/%s.nc' %(archive, experiment, realm, variable, infile)
    nfiles = len(glob.glob(name))
    model_names = []

    print str(nfiles) + ' matching files found'
    for filename in glob.glob(name):
        print "Processing " + filename
        fh = nc.Dataset(filename, mode = 'r')
        model = filename.split('_')[2]
        ens = filename.split('_')[4]
        if model == 'ACCESS1.3':
            model = 'ACCESS1-3'
        elif model == 'FGOALS_g2':
            model = 'FGOALS-g2'
            ens = filename.split('_')[5]
        if model == 'CMCC-CESM':
            print 'Model is %s, wrong grid, continue' %model
            continue
        if not '%s_%s' %(model, ens) in model_names:
            model_names.append(model + '_' + ens)
        else:
            print 'multiple files in folder, model_ensemble already processed'
            continue

        unit = fh.variables[variable].units
        if unit != '%':
            print 'Warning: unit is not percent'
        allnames = '%s/%s/%s/%s/%s/%s/%s_%s_%s_%s_%s_*.nc' %(
            archive, experiment, realm, variable, model, ens, variable, realm,
            model, experiment, ens) 
        files = glob.glob(allnames)
        ifile = nc.MFDataset(files)

        lat = ifile.variables['lat'][:]
        lon = ifile.variables['lon'][:]

        nctime = ifile.variables['time']
        nctime_units = nctime.units
        nctime_cal = nctime.calendar
        cdftime = utime(nctime_units, calendar = nctime_cal)
        dates = cdftime.num2date(nctime[:])
        years = np.asarray([dates[i].year for i in xrange(len(dates))])
        months = np.asarray([dates[i].month for i in xrange(len(dates))])
        if not (np.all(np.diff(years) > 0)):
            print 'Warning: years not monotonically increasing, trying to fix'
            ntim = len(nctime)
            nyears = ntim / 12
            tim_eyear = years[0] + nyears
            new_years = range(years[0], tim_eyear)
            years = np.ndarray((nyears * 12), dtype = int)
            indy = 0
            for y in new_years:
                repeats = [y] * 12
                years[indy : indy + 12] = repeats
                indy = indy + 12
        var = ifile.variables[variable][:]
        fh.close()
        if len(years) != len(var):
            print 'Warning: fixing of time coordinate failed, continue'
            continue

        # need grid to be -180 to 180 for masking, shifgrid if not the case
        if (np.min(lon) >= 0) & (np.max(lon) > 180):
            lons_orig = lon[:]
            var, lon =  shiftgrid(180., var, lon, start = False)
            
        # calculate seasonal averages for each year from monthly values
        var_years = seas_mon_TLL(var, months, years, seasons)
        # extract season
        #var_years = var_seas_years[seas, :, :, :]
        years_ts = years[0::12]

        # sum snow area over region
        tot_snc = np.zeros((len(years_ts)))
        area = get_atm_area(lat, lon)
        for y in xrange(len(years_ts)):
            for ilat in xrange(len(lat)):
                for ilon in xrange(len(lon)):
                    if region:
                        if (point_inside_polygon(lat[ilat], lon[ilon], mask)):
                            #print 'inside area, ilat %s, ilon %s' %(ilat, ilon)
                            area_y = var_years[y, ilat, ilon] * area[ilat, ilon]
                            if not np.isnan(area_y):
                                tot_snc[y] = tot_snc[y] + area_y
                    else:
                        area_y = var_years[y, ilat, ilon] * area[ilat, ilon]
                        if not np.isnan(area_y):
                            tot_snc[y] = tot_snc[y] + area_y
        if (np.all(np.isnan(tot_snc))):
            break
        area_sum_snc[model + '_' + ens] = tot_snc

        outfile = '%s/snc_mon_%s_%s_%s_%s-%s_%s_SUM_%s.nc' %(
            outdir, model, experiment, ens, years_ts[0], years_ts[-1], seasons,
            region)
        func_write_ts_netcdf(outfile, area_sum_snc[model + '_' + ens], 'snc',
                             timevar = years_ts, time_unit = 'year',
                             var_units = 'm^2',
                             Description = 'Total snow cover extent')

        # calculate trend
        trend[model + '_' + ens] = stats.linregress((years_ts),
            area_sum_snc[model + '_' + ens]).slope

    #save model names into separate text file
    try:
        with open('%s%s_%s_all_%s_%s-%s.txt' %(outdir, variable, seasons,
                                               experiment, syear,
                                               eyear), "w") as text_file:
            for m in range(len(model_names)):
                text_file.write(model_names[m] + "\n")
    except NameError:
        with open('%s%s_%s_all_%s.txt' %(outdir, variable, seasons,
                                               experiment), "w") as text_file:
            for m in range(len(model_names)):
                text_file.write(model_names[m] + "\n")

    # clean up workdir
    filelist = glob.glob(workdir + '*')
    for f in filelist:
        os.remove(f)

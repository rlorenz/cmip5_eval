#!/usr/bin/python
'''
File Name : calc_CORR_cmip5_cdo.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 28-07-2017
Modified: Thu 13 Jul 2017 09:56:12 AM CEST
Purpose: Script calculating Evaporative Fractio from cmip5 data for further use
         e.g. in Mult_Diag_Lin_Reg

'''
import netCDF4 as nc # to work with NetCDF files
import numpy as np
from cdo import *   # load cdo functionality
newcdo = "/usr/local/cdo-1.8.2/bin/cdo"
cdo = Cdo()
cdo.setCdo(newcdo)
import os
from os.path import expanduser
home = expanduser("~") # Get users home directory
import sys
sys.path.insert(0,home+'/scripts/plot_scripts/utils/')
from func_read_data import func_read_netcdf
from func_write_netcdf import func_write_netcdf
import glob
from scipy import stats
import math
import copy
###
# Define input
###
variable = 'tas'
variable2 = 'huss'
var_kind = 'seas'    #kind is ann: annual, mon: monthly, seas: seasonal
res = 'JJA'          #choose data for particular month or season?
region = 'EUR'       #cut data over region?
obs = ['Obs', 'MERRA2'] # 'Obs', 'ERAint' or 'MERRA2', or all of them

workdir = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/%s%s/work/' %(
    variable, variable2)
syear = 1980
eyear = 2014
nyears = eyear - syear + 1

experiment = 'rcp85'
grid = 'g025'
masko = False

pathtrop = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/'
if masko:
    oceanmask = "%s/scripts/plot_scripts/areas_txt/seamask_%s.nc" %(home, grid)
    outdir = '%s/%s%s/mon/maskT/' %(pathtrop, variable, variable2)
else:
    outdir = '%s/%s%s/mon/maskF/' %(pathtrop, variable, variable2)

if (os.access(workdir,os.F_OK) == False):
    os.makedirs(workdir)
if (os.access(outdir, os.F_OK) == False):
    os.makedirs(outdir)

if region != None:
    print 'Region is %s' %(region)
    area = region
    mask = np.loadtxt('/home/rlorenz/scripts/plot_scripts/areas_txt/%s.txt' %(
        region))

#read obs data
for dataset in obs:
    if dataset == 'ERAint':
        print "Read ERAint data"
        path = '/net/tropo/climphys/rlorenz/Datasets/ERAint/ERAint_mon'
        if (variable == 'tas'):
            filename = '%s/T2M_monthly_ERAint_197901-201601_%s.nc' %(path, grid)
            varname = 'T2M'
        if (variable2 == 'clt'):
            filename2 = '%s/cld_monthly_ERAint_197901-201612_%s.nc' %(path,
                                                                      grid)
            varname2 = 'tcc'
    elif dataset == 'MERRA2':
        print "Read MERRA2 data"
        path = '/net/tropo/climphys/rlorenz/Datasets/MERRA2/monthly'
        if (variable == 'tas'):
            varname = 'T2MMEAN'
            filename = '%s/%s.tavgmon_2d_slv_Nx.%s.1980-2015_remapbil_%s.nc' %(path, dataset, varname, grid)
        if (variable2 == 'clt'):
            varname2 = 'CLDTOT'
            filename2 = '%s/%s.tavgmon_2d_rad_Nx.%s.1980-2015_remapbil_%s.nc' %(
                path, dataset, varname2, grid)
        elif (variable2 == 'huss'):
            varname2 = 'QV2M'
            filename2 = '%s/%s.tavgmon_2d_slv_Nx.%s.1980-2015_remapbil_%s.nc' %(
                path, dataset, varname2, grid)
    elif (dataset == 'Obs'):
        print "Read Obs data"
        path = '/net/tropo/climphys/rlorenz/Datasets/'
        if (variable == 'tas'):
            if ((region == 'EUR') or (region == 'CNEU')):
                filename = '%s/E-OBS/tg_mon_1950-2014_reg_v11.0_%s.nc' %(path,
                                                                      grid)
                varname = 'tg'
            else:
                filename = '%s/BerkeleyEarth/Complete_TAVG_abs_LatLong_011750-122015_%s.nc' %(path, grid)
                varname = 'temperature'
        if (variable2 == 'clt'):
            varname2 = 'clt'
            filename2 = '%s/MODIS/%s_MODIS_L3_C5_200003-201109_%s.nc' %(
                path, varname2, grid)
            syear = 2000
            eyear = 2011
        if (variable2 == 'huss'):
            varname2 = 'q_abs'
            filename2 = '%s/HadISDH/HadISDH.landq.3.0.0.2016p_FLATgridIDPHA5by5_anoms7605_JAN2017_cf_remapcon2_%s.nc' %(
                path, grid)

    tmpfile = '%s/%s_mon_%s_%s-%s_%s' %(workdir, variable, dataset, syear,
                                        eyear, res)
    tmpfile2 = '%s/%s_mon_%s_%s-%s_%s' %(workdir, variable2, dataset, syear,
                                         eyear, res)

    outfile = '%s/%s%s_mon_%s_%s-%s_%s' %(outdir, variable, variable2, dataset,
                                          syear, eyear, res)

    if (var_kind == 'seas'):
        cdo.selseas(res, input = 
                    "-seldate,%s-01-01,%s-12-31 -sellonlatbox,-180,180,-90,90 %s"
                    %(syear, eyear, filename), output = '%s.nc' %(tmpfile))
        cdo.selseas(res, input = 
                    "-seldate,%s-01-01,%s-12-31 -sellonlatbox,-180,180,-90,90 %s"
                    %(syear, eyear, filename2), output = '%s.nc' %(tmpfile2))

    else:
        cdo.seldate('%s-01-01,%s-12-31' %(syear, eyear),
                    input = "-sellonlatbox,-180,180,-90,90 %s"
                    %(filename), output = '%s.nc' %(tmpfile))
        cdo.seldate('%s-01-01,%s-12-31' %(syear, eyear),
                    input = "-sellonlatbox,-180,180,-90,90 %s"
                    %(filename2), output = '%s.nc' %(tmpfile2))
    if masko:
        maskfile = '%s_masko.nc' %(tmpfile)
        cdo.setmissval(0, input = "-mul -eqc,1 %s %s.nc" %(oceanmask, tmpfile),
                       output = '%s' %(maskfile))
        os.system("mv %s %s.nc" %(maskfile, tmpfile))
        maskfile2 = '%s_masko.nc' %(tmpfile2)
        cdo.setmissval(0, input = "-mul -eqc,1 %s %s.nc" %(oceanmask, tmpfile2),
                       output = '%s' %(maskfile))
        os.system("mv %s %s.nc" %(maskfile, tmpfile2))

    if (var_kind == 'seas'):
        cdo.seasmean(input = '%s.nc' %(tmpfile),
                     output = '%sMEAN.nc' %(tmpfile))
        cdo.seasmean(input = '%s.nc' %(tmpfile2),
                      output = '%sMEAN.nc' %(tmpfile2))
    elif (var_kind == 'ann'):
        cdo.yearmean(input = '%s.nc' %(tmpfile),
                     output = '%sMEAN.nc' %(tmpfile))
        cdo.yearmean(input = '%s.nc' %(tmpfile2),
                       output = '%sMEAN.nc' %(tmpfile2))
    else:
        os.system('mv %s.nc %sMEAN.nc' %(tmpfile, tmpfile))
        os.system('mv %s.nc %sMEAN.nc' %(tmpfile2, tmpfile2))

    if region:
        lonmax = np.max(mask[:, 0])
        lonmin = np.min(mask[:, 0])
        latmax = np.max(mask[:, 1]) 
        latmin = np.min(mask[:, 1])
        cdo.sellonlatbox(lonmin,lonmax,latmin,latmax,
                         input = '%sMEAN.nc' %(tmpfile),
                         output = '%sMEAN_%s.nc' %(tmpfile, region))
        cdo.sellonlatbox(lonmin,lonmax,latmin,latmax,
                         input = '%sMEAN.nc' %(tmpfile2),
                         output = '%sMEAN_%s.nc' %(tmpfile2, region))
        readfile = '%sMEAN_%s.nc' %(tmpfile, region)
        readfile2 = '%sMEAN_%s.nc' %(tmpfile2, region)
    else:
        readfile = '%sMEAN.nc' %(tmpfile)
        readfile2 = '%sMEAN.nc' %(tmpfile2)

    obs_data1 = func_read_netcdf(readfile, varname, var_units = True,
                                 syear = syear, eyear = eyear)
    data1 = obs_data1['data']
    lat = obs_data1['lat']
    lon = obs_data1['lon']
    dates = obs_data1['dates']
    years = np.asarray([dates[i].year for i in xrange(len(dates))])

    obs_data2 = func_read_netcdf(readfile2, varname2, var_units = True,
                                 syear = syear, eyear = eyear)
    data2 = obs_data2['data']
    lat2 = obs_data2['lat']
    lon2 = obs_data2['lon']
    dates2 = obs_data2['dates']
    years2 = np.asarray([dates2[i].year for i in xrange(len(dates2))])

    # check if files have identical dimensions
    if (data1.shape != data2.shape):
        if (years1 != years2):
            print('time dimension in two datasets are not identical, exiting')
        if (lat != lat2):
            print('latitudes in two datasets are not identical, exiting')
        if (lon != lon2):
            print('latitudes in two datasets are not identical, exiting')
        sys.exit
    del lat2, lon2

    # average over space
    w_lat = np.cos(lat * (4.0 * math.atan(1.0) / 180))
    if not isinstance(data1, np.ma.core.MaskedArray):
        data1 = np.ma.masked_array(data1, np.isnan(data1))
    if not isinstance(data2, np.ma.core.MaskedArray):
        data2 = np.ma.masked_array(data2, np.isnan(data2))

    tmp1_latweight = np.ma.average(np.squeeze(data1), axis = 1,
                                   weights = w_lat)
    data1_areaavg = np.nanmean(tmp1_latweight.filled(np.nan), axis = 1)
    tmp2_latweight = np.ma.average(np.squeeze(data2), axis = 1,
                                   weights = w_lat)
    data2_areaavg = np.nanmean(tmp2_latweight.filled(np.nan), axis = 1)

    ## calculate ordinary least squares regression
    slope, intercept, r_val, p_val, std_err = stats.linregress(data1_areaavg,
                                                               data2_areaavg)
    slope_value = copy.copy(slope)
    slope2D = np.ndarray((len(lat), len(lon)))
    for ilat in xrange(len(lat)):
        for ilon in xrange(len(lon)):
            slope, intercept, r_val, p_val, std_err = stats.linregress(data1[:, ilat, ilon],
                                                data2[:, ilat, ilon])
            slope2D[ilat, ilon] = copy.copy(slope)

    if region:
        out_name = '%sMEAN_REGR_%s' %(outfile, region)
    else:
        out_name = '%sMEAN_REGR' %(outfile)
    func_write_netcdf('%s.nc' %(out_name), slope2D,
                      "%s%s" %(variable, variable2), lon, lat,
                      var_units = '-', Description = '',
                      comment = '%s' %(slope_value))

# clean up workdir
filelist = glob.glob(workdir + '*')
for f in filelist:
    os.remove(f)

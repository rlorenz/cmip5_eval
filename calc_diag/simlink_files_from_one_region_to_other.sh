#!/bin/bash
# File Name: simlink_files_from_one_region_to_other.sh
# Author: Ruth Lorenz 
# Created: Mon 06 Nov 2017
# Modified: Mon 06 Nov 2017 11:54:08 AM CET
# Purpose : simlink files from one region to other,
#           e.g. for tos if larger region for both
###-------------------------------------------------------
##---------------------##
## user specifications ##
##-------------------- ##
variable=tos
masko=maskF
RCP=rcp85
region=EUR
newregion=CNEU

archive=/net/tropo/climphys1/rlorenz/processed_CMIP5_data/$variable/mon/$masko/
pattern=${variable}_mon_*_${RCP}_*_????-????_*_${region}.nc

for FILES in $archive$pattern
    do
        cutpath=$(echo $FILES| cut -d'/' -f 10)
        cutfile=$(echo $cutpath| cut -d'_' -f 1-8)
	newfile=${cutfile}_${newregion}.nc
	ln -s $FILES $archive$newfile
done # FILES

# Obs
patternobs=${variable}_mon_Obs_????-????_*_${region}.nc
for FILES in $archive$patternobs
    do
        cutpath=$(echo $FILES| cut -d'/' -f 10)
        cutfile=$(echo $cutpath| cut -d'_' -f 1-6)
	newfile=${cutfile}_${newregion}.nc
	ln -s $FILES $archive$newfile
done # FILES

patternera=${variable}_mon_ERAint_????-????_*_${region}.nc
for FILES in $archive$patternera
    do
        cutpath=$(echo $FILES| cut -d'/' -f 10)
        cutfile=$(echo $cutpath| cut -d'_' -f 1-6)
	newfile=${cutfile}_${newregion}.nc
	ln -s $FILES $archive$newfile
done # FILES

patternmerra=${variable}_mon_MERRA2_????-????_*_${region}.nc
for FILES in $archive$patternmerra
    do
        cutpath=$(echo $FILES| cut -d'/' -f 10)
        cutfile=$(echo $cutpath| cut -d'_' -f 1-6)
	newfile=${cutfile}_${newregion}.nc
	ln -s $FILES $archive$newfile
done # FILES

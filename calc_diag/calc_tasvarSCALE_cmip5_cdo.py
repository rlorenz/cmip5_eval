#!/usr/bin/python
'''
File Name : calc_tasSCALE_cmip5_cdo.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 01-06-2017
Modified: Tue 15 Aug 2017 21:39:32 CEST
Purpose: calculate tas scaled by global mean tas change


'''
import netCDF4 as nc # to work with NetCDF files
import numpy as np
from cdo import *   # load cdo functionality
cdo = Cdo()
import os
from os.path import expanduser
home = expanduser("~") # Get users home directory
import glob

###
# Define input
###
variable = 'tasmax'
variable2 = 'tas'
var_kind = 'seas'    #kind is ann: annual, mon: monthly, seas: seasonal
res = 'JJA'          #choose data for particular month or season?
region = 'CNEU'       #cut data over region?

archive = '/net/atmos/data/cmip5-ng'
workdir = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/%s/work/' %(
    variable)
syear_eval = 1980
eyear_eval = 2014
syear = 2065
eyear = 2099

nyears_eval = eyear_eval - syear_eval + 1
nyears = eyear - syear + 1

experiment = 'rcp85'
grid = 'g025'
masko = True

pathtrop = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/'
if masko:
    oceanmask = "%s/scripts/plot_scripts/areas_txt/seamask_%s.nc" %(home, grid)
    outdir = '%s/%s/mon/maskT/' %(pathtrop, variable)
else:
    outdir = '%s/%s/mon/maskF/' %(pathtrop, variable)

if (os.access(workdir,os.F_OK) == False):
    os.makedirs(workdir)
if (os.access(outdir, os.F_OK) == False):
    os.makedirs(outdir)

if region != None:
    print 'Region is %s' %(region)
    area = region
    mask = np.loadtxt('/home/rlorenz/scripts/plot_scripts/areas_txt/%s.txt' %(
        region))

## find all matching files in archive, loop over all of them
# first count matching files in folder:
infile = variable + '_mon'
name = '%s/%s/%s_*_%s_*_%s.nc' %(archive, variable, infile, experiment,
                                 grid)
nfiles = len(glob.glob(name))
print str(nfiles) + ' matching files found'
for filename in glob.glob(name):
    print "Processing " + filename
    fh = nc.Dataset(filename, mode = 'r')
    model = fh.source_model
    if model == 'ACCESS1.3':
        model = 'ACCESS1-3'
    elif model == 'FGOALS_g2':
        model = 'FGOALS-g2'
    ens = fh.source_ensemble

    unit = fh.variables[variable].units
    fh.close()

    # check if file exists for variable2, otherwise continue
    filename2 = '%s/%s/%s_mon_%s_%s_%s_%s.nc' %(archive, variable2, variable2,
                                                model, experiment, ens, grid)
    if (os.path.isfile(filename2) == False):
        continue

    tmpfile = '%s/%s_%s_%s_%s_%s-%s_' %(workdir, infile, model, experiment, ens,
                                        syear_eval, eyear_eval)
    tmpfile2 = '%s/%s_mon_%s_%s_%s_%s-%s_' %(workdir, variable2, model,
                                               experiment, ens, syear_eval,
                                               eyear_eval)

    outfile = '%s/%s_%s_%s_%s_%s-%s_%s' %(outdir, infile, model, experiment,
                                          ens, syear_eval, eyear_eval, res)
    if (var_kind == 'seas'):
        cdo.selseas(res, input = 
                    "-seldate,%s-01-01,%s-12-31 -sellonlatbox,-180,180,-90,90 %s"
                    %(syear_eval, eyear_eval, filename),
                    output = '%s%s.nc' %(tmpfile, res))
    cdo.seldate('%s-01-01,%s-12-31' %(syear_eval, eyear_eval),
                input = "-sellonlatbox,-180,180,-90,90 %s" %(filename2),
                output = '%sANN.nc' %(tmpfile2))
    if ((variable == 'tas') or (variable == 'tasmax') or
        (variable == 'tasmin')) and (unit == 'K'):
        unitfile = cdo.subc(273.15, input = '%s%s.nc' %(tmpfile, res))
        newunit = "degC"
        cdo.chunit('"K",%s' %(newunit), input = unitfile,
                   output = '%s%s.nc' %(tmpfile, res))
        unitfile2 = cdo.subc(273.15, input = '%sANN.nc' %(tmpfile2))
        cdo.chunit('"K",%s' %(newunit), input = unitfile2,
                   output = '%sANN.nc' %(tmpfile2))

    if masko:
        maskfile = '%smasko.nc' %(tmpfile)
        cdo.setmissval(0, input = "-mul -eqc,1 %s %s%s.nc" %(oceanmask,
                                                             tmpfile, res),
                       output = '%s' %(maskfile))
        os.system("mv %s %s%s.nc" %(maskfile, tmpfile, res))

    if (var_kind == 'seas'):
        #cdo.seasmean(input = '%s%s.nc' %(tmpfile, res),
        #             output = '%sMEAN.nc' %(outfile))
        cdo.yseasmean(input = '%s%s.nc' %(tmpfile, res),
                      output = '%sMEAN_CLIM.nc' %(outfile))
    else:
        cdo.ymonmean(input = '%s%s.nc' %(tmpfile, res),
                      output = '%sMEAN_CLIM.nc' %(outfile))

    #cdo.yearmean(input = '%sANN.nc' %(tmpfile),
    #             output = '%sANN_MEAN.nc' %(tmpfile))
    cdo.timmean(input = '%sANN.nc' %(tmpfile2),
                output = '%sANN_MEAN_CLIM.nc' %(tmpfile2))

    cdo.fldmean(input = '%sANN_MEAN_CLIM.nc' %(tmpfile2),
                output = '%sANN_CLIM_GLOBALMEAN.nc' %(tmpfile2))

    tmpfile = '%s/%s_%s_%s_%s_%s-%s_' %(workdir, infile, model,
                                        experiment, ens, syear, eyear)
    tmpfile2 = '%s/%s_mon_%s_%s_%s_%s-%s_%s' %(workdir, variable2, model,
                                               experiment, ens, syear, eyear,
                                               res)

    outfile = '%s/%s_%s_%s_%s_%s-%s_%s' %(outdir, infile, model,
                                          experiment, ens, syear, eyear, res)
    if (var_kind == 'seas'):
        cdo.selseas(res, input = 
                    "-seldate,%s-01-01,%s-12-31 -sellonlatbox,-180,180,-90,90 %s"
                    %(syear, eyear, filename),
                    output = '%s%s.nc' %(tmpfile, res))
    cdo.seldate('%s-01-01,%s-12-31' %(syear, eyear),
                input = "-sellonlatbox,-180,180,-90,90 %s"
                %(filename), output = '%sANN.nc' %(tmpfile2))
    if (variable == 'pr') and  (unit == 'kg m-2 s-1'):
        unitfile = cdo.mulc(24 * 60 * 60, input = '%s%s.nc' %(tmpfile, res))
        #unitfile = '%s_chunit.nc' %(tmpfile)
        cdo.mulc(24 * 60 * 60, input = '%s%s.nc' %(tmpfile, res),
                 output = unitfile)
        newunit = "mm/day"
        cdo.chunit('"kg m-2 s-1",%s' %(newunit), input = unitfile,
                   output = '%s%s.nc' %(tmpfile, res))
    if ((variable == 'tas') or (variable == 'tasmax') or
        (variable == 'tasmin')) and (unit == 'K'):
        unitfile = cdo.subc(273.15, input = '%s%s.nc' %(tmpfile, res))
        newunit = "degC"
        cdo.chunit('"K",%s' %(newunit), input = unitfile,
                   output = '%s%s.nc' %(tmpfile, res))
        unitfile2 = cdo.subc(273.15, input = '%sANN.nc' %(tmpfile2))
        cdo.chunit('"K",%s' %(newunit), input = unitfile2,
                   output = '%sANN.nc' %(tmpfile2))
    if masko:
        maskfile = '%smasko.nc' %(tmpfile)
        cdo.setmissval(0, input = "-mul -eqc,1 %s %s%s.nc" %(oceanmask,
                                                             tmpfile, res),
                       output = '%s' %(maskfile))
        os.system("mv %s %s%s.nc" %(maskfile, tmpfile, res))

    if (var_kind == 'seas'):
        #cdo.seasmean(input = '%s.nc' %(tmpfile),
        #             output = '%sMEAN.nc' %(outfile))
        cdo.yseasmean(input = '%s%s.nc' %(tmpfile, res),
                      output = '%sMEAN_CLIM.nc' %(outfile))
    else:
        cdo.ymonmean(input = '%s%s.nc' %(tmpfile, res),
                      output = '%sMEAN_CLIM.nc' %(outfile))

    cdo.timmean(input = '%sANN.nc' %(tmpfile2),
                output = '%sANN_MEAN_CLIM.nc' %(tmpfile2))
    cdo.fldmean(input = '%sANN_MEAN_CLIM.nc' %(tmpfile2),
                output = '%sANN_CLIM_GLOBALMEAN.nc' %(tmpfile2))

    cdo.detrend(input = '%sMEAN.nc' %(outfile),
                output = '%sMEAN_DETREND.nc' %(tmpfile))
    cdo.timstd(input = '%sMEAN_DETREND.nc' %(tmpfile),
               output = '%sMEAN_STD.nc' %(outfile))

    nom = cdo.sub(input =
        '%sMEAN_STD.nc %s/%s_%s_%s_%s_%s-%s_%sMEAN_STD.nc' %(
            outfile, outdir, infile, model, experiment, ens, syear_eval,
            eyear_eval, res))
    cdo.sub(input =
            '%sANN_CLIM_GLOBALMEAN.nc %s/%s_mon_%s_%s_%s_%s-%s_ANN_CLIM_GLOBALMEAN.nc' %(tmpfile2, workdir, variable2, model, experiment, ens, syear_eval, eyear_eval),
            output = '%s/tmp_denom.nc' %(workdir))

    fh = nc.Dataset('%s/tmp_denom.nc' %(workdir), mode = 'r')
    glob_mean_ch = fh.variables[variable][:]
    fh.close()
    
    cdo.divc(np.squeeze(glob_mean_ch), input = '%s' %(nom),
             output = '%sMEAN_VARSCALE.nc' %(outfile))

    if region:
        lonmax = np.max(mask[:, 0])
        lonmin = np.min(mask[:, 0])
        latmax = np.max(mask[:, 1]) 
        latmin = np.min(mask[:, 1])
        cdo.sellonlatbox(lonmin,lonmax,latmin,latmax,
                         input = '%sMEAN_STD.nc' %(outfile),
                         output = '%sMEAN_STD_%s.nc' %(outfile, region))
        cdo.sellonlatbox(lonmin,lonmax,latmin,latmax,
                         input = '%sMEAN_VARSCALE.nc' %(outfile),
                         output = '%sMEAN_VARSCALE_%s.nc' %(outfile, region))

# clean up workdir
filelist = glob.glob(workdir + '*')
for f in filelist:
    os.remove(f)

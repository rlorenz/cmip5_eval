#!/usr/bin/python
'''
File Name : calc_CORR_cmip5_cdo.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 28-07-2017
Modified: 
Purpose: Script calculating Evaporative Fractio from cmip5 data for further use
         e.g. in Mult_Diag_Lin_Reg

'''
import netCDF4 as nc # to work with NetCDF files
import numpy as np
from cdo import *   # load cdo functionality
cdo = Cdo()
import os
from os.path import expanduser
home = expanduser("~") # Get users home directory
import sys
sys.path.insert(0,home+'/scripts/plot_scripts/utils/')
from func_read_data import func_read_netcdf
from func_write_netcdf import func_write_netcdf
import glob
from scipy import stats
import math
import copy
###
# Define input
###
variable = 'tas'
variable2 = 'huss'
var_kind = 'seas'    #kind is ann: annual, mon: monthly, seas: seasonal
res = 'JJA'          #choose data for particular month or season?
region = 'CNEU'       #cut data over region?

archive = '/net/atmos/data/cmip5-ng'
workdir = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/%s%s/work/' %(
    variable, variable2)
syear = 1980
eyear = 2014
nyears = eyear - syear + 1

experiment = 'rcp85'
grid = 'g025'
masko = False

pathtrop = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/'
if masko:
    oceanmask = "%s/scripts/plot_scripts/areas_txt/seamask_%s.nc" %(home, grid)
    outdir = '%s/%s%s/mon/maskT/' %(pathtrop, variable, variable2)
else:
    outdir = '%s/%s%s/mon/maskF/' %(pathtrop, variable, variable2)

if (os.access(workdir,os.F_OK) == False):
    os.makedirs(workdir)
if (os.access(outdir, os.F_OK) == False):
    os.makedirs(outdir)

if region != None:
    print 'Region is %s' %(region)
    area = region
    mask = np.loadtxt('/home/rlorenz/scripts/plot_scripts/areas_txt/%s.txt' %(
        region))

### read model data
print "Read model data"
print 'Processing variable %s %s' %(variable,
                                    var_kind)
## find all matching files in archive, loop over all of them
# first count matching files in folder:
infile = variable + '_mon'
name = '%s/%s/%s_*_%s_*_%s.nc' %(archive, variable, infile, experiment,
                                 grid)
nfiles = len(glob.glob(name))

model_names = []

print str(nfiles) + ' matching files found'
for filename in glob.glob(name):
    print "Processing " + filename
    fh = nc.Dataset(filename, mode = 'r')
    model = fh.source_model
    if model == 'ACCESS1.3':
        model = 'ACCESS1-3'
    elif model == 'FGOALS_g2':
        model = 'FGOALS-g2'
    ens = fh.source_ensemble
    model_names.append(model + '_' + ens)
    fh.close()

    # check if file exists for variable2, otherwise continue
    filename2 = '%s/%s/%s_mon_%s_%s_%s_%s.nc' %(archive, variable2, variable2,
                                                model, experiment, ens, grid)
    if (os.path.isfile(filename2) == False):
        continue
        
    tmpfile = '%s/%s_%s_%s_%s_%s-%s_%s' %(workdir, infile, model,
                                          experiment, ens, syear, eyear,
                                          res)
    tmpfile2 = '%s/%s_mon_%s_%s_%s_%s-%s_%s' %(workdir, variable2, model,
                                               experiment, ens, syear, eyear,
                                               res)

    outfile = '%s/%s%s_mon_%s_%s_%s_%s-%s_%s' %(outdir, variable, variable2,
                                                model, experiment, ens, syear,
                                                eyear, res)
    
    if (var_kind == 'seas'):
        cdo.selseas(res, input = 
                    "-seldate,%s-01-01,%s-12-31 -sellonlatbox,-180,180,-90,90 %s"
                    %(syear, eyear, filename), output = '%s.nc' %(tmpfile))
        cdo.selseas(res, input = 
                    "-seldate,%s-01-01,%s-12-31 -sellonlatbox,-180,180,-90,90 %s"
                    %(syear, eyear, filename2), output = '%s.nc' %(tmpfile2))

    else:
        cdo.seldate('%s-01-01,%s-12-31' %(syear, eyear),
                    input = "-sellonlatbox,-180,180,-90,90 %s"
                    %(filename), output = '%s.nc' %(tmpfile))
        cdo.seldate('%s-01-01,%s-12-31' %(syear, eyear),
                    input = "-sellonlatbox,-180,180,-90,90 %s"
                    %(filename2), output = '%s.nc' %(tmpfile2))
    if masko:
        maskfile = '%s_masko.nc' %(tmpfile)
        cdo.setmissval(0, input = "-mul -eqc,1 %s %s.nc" %(oceanmask, tmpfile),
                       output = '%s' %(maskfile))
        os.system("mv %s %s.nc" %(maskfile, tmpfile))
        maskfile2 = '%s_masko.nc' %(tmpfile2)
        cdo.setmissval(0, input = "-mul -eqc,1 %s %s.nc" %(oceanmask, tmpfile2),
                       output = '%s' %(maskfile))
        os.system("mv %s %s.nc" %(maskfile, tmpfile2))

    if (var_kind == 'seas'):
        cdo.seasmean(input = '%s.nc' %(tmpfile),
                     output = '%sMEAN.nc' %(tmpfile))
        cdo.seasmean(input = '%s.nc' %(tmpfile2),
                      output = '%sMEAN.nc' %(tmpfile2))
    elif (var_kind == 'ann'):
        cdo.yearmean(input = '%s.nc' %(tmpfile),
                     output = '%sMEAN.nc' %(tmpfile))
        cdo.yearmean(input = '%s.nc' %(tmpfile2),
                       output = '%sMEAN.nc' %(tmpfile2))
    else:
        os.system('mv %s.nc %sMEAN.nc' %(tmpfile, tmpfile))
        os.system('mv %s.nc %sMEAN.nc' %(tmpfile2, tmpfile2))

    if region:
        lonmax = np.max(mask[:, 0])
        lonmin = np.min(mask[:, 0])
        latmax = np.max(mask[:, 1]) 
        latmin = np.min(mask[:, 1])
        cdo.sellonlatbox(lonmin,lonmax,latmin,latmax,
                         input = '%sMEAN.nc' %(tmpfile),
                         output = '%sMEAN_%s.nc' %(tmpfile, region))
        cdo.sellonlatbox(lonmin,lonmax,latmin,latmax,
                         input = '%sMEAN.nc' %(tmpfile2),
                         output = '%sMEAN_%s.nc' %(tmpfile2, region))
        readfile = '%sMEAN_%s.nc' %(tmpfile, region)
        readfile2 = '%sMEAN_%s.nc' %(tmpfile2, region)
    else:
        readfile = '%sMEAN.nc' %(tmpfile)
        readfile2 = '%sMEAN.nc' %(tmpfile2)

    model_data1 = func_read_netcdf(readfile, variable, var_units = True,
                                   syear = syear, eyear = eyear)
    data1_mod = model_data1['data']
    lat = model_data1['lat']
    lon = model_data1['lon']
    dates = model_data1['dates']
    years = np.asarray([dates[i].year for i in xrange(len(dates))])

    model_data2 = func_read_netcdf(readfile2, variable2, var_units = True,
                                   syear = syear, eyear = eyear)
    data2_mod = model_data2['data']
    lat2 = model_data2['lat']
    lon2 = model_data2['lon']
    dates2 = model_data2['dates']
    years2 = np.asarray([dates2[i].year for i in xrange(len(dates2))])

    # check if files have identical dimensions
    if (data1_mod.shape != data2_mod.shape):
        if (years1 != years2):
            print('time dimension in two datasets are not identical, exiting')
        if (lat != lat2):
            print('latitudes in two datasets are not identical, exiting')
        if (lon != lon2):
            print('latitudes in two datasets are not identical, exiting')
        sys.exit
    del lat2, lon2

    # average over space
    w_lat = np.cos(lat * (4.0 * math.atan(1.0) / 180))
    if not isinstance(data1_mod, np.ma.core.MaskedArray):
        data1_mod = np.ma.masked_array(data1_mod, np.isnan(data1_mod))
    if not isinstance(data2_mod, np.ma.core.MaskedArray):
        data2_mod = np.ma.masked_array(data2_mod, np.isnan(data2_mod))

    tmp1_latweight = np.ma.average(np.squeeze(data1_mod), axis = 1,
                                   weights = w_lat)
    data1_areaavg = np.nanmean(tmp1_latweight.filled(np.nan), axis = 1)
    tmp2_latweight = np.ma.average(np.squeeze(data2_mod), axis = 1,
                                   weights = w_lat)
    data2_areaavg = np.nanmean(tmp2_latweight.filled(np.nan), axis = 1)

    ## calculate ordinary least squares regression
    slope, intercept, r_val, p_val, std_err = stats.linregress(data1_areaavg,
                                                               data2_areaavg)
    slope_value = copy.copy(slope)
    slope2D = np.ndarray((len(lat), len(lon)))
    for ilat in xrange(len(lat)):
        for ilon in xrange(len(lon)):
            slope, intercept, r_val, p_val, std_err = stats.linregress(data1_mod[:, ilat, ilon],
                                     data2_mod[:, ilat, ilon])
            slope2D[ilat, ilon] = copy.copy(slope)

    if region:
        out_name = '%sMEAN_REGR_%s' %(outfile, region)
    else:
        out_name = '%sMEAN_REGR' %(outfile)
    func_write_netcdf('%s.nc' %(out_name), slope2D,
                      "%s%s" %(variable, variable2), lon, lat,
                      var_units = '-', Description = '',
                      comment = '%s' %(slope_value))

#save model names into separate text file
with open('%s%s_%s_all_%s_%s-%s.txt' %(outdir, variable, 
                                       res, experiment,
                                       syear, eyear), "w") as text_file:
    for m in range(len(model_names)):
        text_file.write(model_names[m] + "\n")

# clean up workdir
filelist = glob.glob(workdir + '*')
for f in filelist:
    os.remove(f)

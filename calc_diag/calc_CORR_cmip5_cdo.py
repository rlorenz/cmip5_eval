#!/usr/bin/python
'''
File Name : calc_CORR_cmip5_cdo.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 28-07-2017
Modified: Tue 15 Aug 2017 21:40:35 CEST
Purpose: Script calculating Evaporative Fractio from cmip5 data for further use
         e.g. in Mult_Diag_Lin_Reg

'''
import netCDF4 as nc # to work with NetCDF files
import numpy as np
from cdo import *   # load cdo functionality
cdo = Cdo()
import os
from os.path import expanduser
home = expanduser("~") # Get users home directory
import glob

###
# Define input
###
variable = 'tas'
variable2 = 'clt'
var_kind = 'seas'    #kind is ann: annual, mon: monthly, seas: seasonal
seasons = ['JJA']          #choose data for particular month or season?
region = ['EUR', 'CNEU']       #cut data over region?

archive = '/net/atmos/data/cmip5-ng'
workdir = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/%s%s/work/' %(
    variable, variable2)

syear = 2070
eyear = 2099
nyears = eyear - syear + 1
experiment = 'rcp85'
grid = 'g025'
masko = True

pathtrop = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/'
if masko:
    oceanmask = "%s/scripts/plot_scripts/areas_txt/seamask_%s.nc" %(home, grid)
    outdir = '%s/%s%s/mon/maskT/' %(pathtrop, variable, variable2)
else:
    outdir = '%s/%s%s/mon/maskF/' %(pathtrop, variable, variable2)

if (os.access(workdir,os.F_OK) == False):
    os.makedirs(workdir)
if (os.access(outdir, os.F_OK) == False):
    os.makedirs(outdir)

### read model data
print "Read model data"
print 'Processing variable %s %s' %(variable,
                                    var_kind)
## find all matching files in archive, loop over all of them
# first count matching files in folder:
infile = variable + '_mon'
name = '%s/%s/%s_*_%s_*_%s.nc' %(archive, variable, infile, experiment,
                                 grid)
nfiles = len(glob.glob(name))

model_names = []

print str(nfiles) + ' matching files found'
for filename in glob.glob(name):
    print "Processing " + filename
    fh = nc.Dataset(filename, mode = 'r')
    unit = fh.variables[variable].units
    model = fh.source_model
    if model == 'ACCESS1.3':
        model = 'ACCESS1-3'
    elif model == 'FGOALS_g2':
        model = 'FGOALS-g2'
    ens = fh.source_ensemble
    model_names.append(model + '_' + ens)
    fh.close()

    # check if file exists for variable2, otherwise continue
    filename2 = '%s/%s/%s_mon_%s_%s_%s_%s.nc' %(archive, variable2, variable2,
                                                model, experiment, ens, grid)
    if (os.path.isfile(filename2) == False):
        continue
        
    for res in seasons:
        tmpfile = '%s/%s_%s_%s_%s_%s-%s_%s' %(workdir, infile, model,
                                              experiment, ens, syear, eyear,
                                              res)
        tmpfile2 = '%s/%s_mon_%s_%s_%s_%s-%s_%s' %(workdir, variable2, model,
                                                   experiment, ens, syear,
                                                   eyear, res)

        outfile = '%s/%s%s_mon_%s_%s_%s_%s-%s_%s' %(outdir, variable,
                                                    variable2, model,
                                                    experiment, ens, syear,
                                                    eyear, res)

        if (var_kind == 'seas'):
            cdo.selseas(res, input = 
                        "-seldate,%s-01-01,%s-12-31 -sellonlatbox,-180,180,-90,90 %s"
                        %(syear, eyear, filename), output = '%s.nc' %(tmpfile))
            cdo.selseas(res, input = 
                        "-seldate,%s-01-01,%s-12-31 -sellonlatbox,-180,180,-90,90 %s"
                        %(syear, eyear, filename2),
                        output = '%s.nc' %(tmpfile2))

        else:
            cdo.seldate('%s-01-01,%s-12-31' %(syear, eyear),
                        input = "-sellonlatbox,-180,180,-90,90 %s"
                        %(filename), output = '%s.nc' %(tmpfile))
            cdo.seldate('%s-01-01,%s-12-31' %(syear, eyear),
                        input = "-sellonlatbox,-180,180,-90,90 %s"
                        %(filename2), output = '%s.nc' %(tmpfile2))
        if masko:
            maskfile = '%s_masko.nc' %(tmpfile)
            cdo.setmissval(0, input = "-mul -eqc,1 %s %s.nc" %(oceanmask,
                                                               tmpfile),
                           output = '%s' %(maskfile))
            os.system("mv %s %s.nc" %(maskfile, tmpfile))
            maskfile2 = '%s_masko.nc' %(tmpfile2)
            cdo.setmissval(0, input = "-mul -eqc,1 %s %s.nc" %(oceanmask,
                                                               tmpfile2),
                           output = '%s' %(maskfile))
            os.system("mv %s %s.nc" %(maskfile, tmpfile2))

        if (var_kind == 'seas'):
            cdo.seasmean(input = '%s.nc' %(tmpfile),
                         output = '%sMEAN.nc' %(tmpfile))
            cdo.seasmean(input = '%s.nc' %(tmpfile2),
                          output = '%sMEAN.nc' %(tmpfile2))
        elif (var_kind == 'ann'):
            cdo.yearmean(input = '%s.nc' %(tmpfile),
                         output = '%sMEAN.nc' %(tmpfile))
            cdo.yearmean(input = '%s.nc' %(tmpfile2),
                           output = '%sMEAN.nc' %(tmpfile2))
        else:
            os.system('mv %s.nc %sMEAN.nc' %(tmpfile, tmpfile))
            os.system('mv %s.nc %sMEAN.nc' %(tmpfile2, tmpfile2))

        corfile = cdo.timcor(input = '%s.nc %s.nc' %(tmpfile, tmpfile2))
        varname = cdo.chname('%s,%s%s' %(variable, variable, variable2),
                             input = corfile)
        unitname = cdo.setattribute('%s%s@units=-' %(variable, variable2),
                                    input = varname)
        cdo.setvrange('-1,1', input = unitname, output = '%sMEAN_CORR.nc' %(
            outfile))

        if region:
            # loop over regions
            for reg in region:
                print 'Region is %s' %(reg)
                area = reg
                mask = np.loadtxt(
                    '/home/rlorenz/scripts/plot_scripts/areas_txt/%s.txt' %(
                        reg))

                lonmax = np.max(mask[:, 0])
                lonmin = np.min(mask[:, 0])
                latmax = np.max(mask[:, 1]) 
                latmin = np.min(mask[:, 1])
                cdo.sellonlatbox(lonmin,lonmax,latmin,latmax,
                                 input = '%sMEAN_CORR.nc' %(outfile),
                                 output = '%sMEAN_CORR_%s.nc' %(outfile, reg))

#save model names into separate text file
with open('%s%s%s_%s_all_%s_%s-%s.txt' %(outdir, variable, variable2, 
                                         res, experiment,
                                         syear, eyear), "w") as text_file:
    for m in range(len(model_names)):
        text_file.write(model_names[m] + "\n")

# clean up workdir
filelist = glob.glob(workdir + '*')
for f in filelist:
    os.remove(f)

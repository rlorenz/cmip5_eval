#!/usr/bin/python
'''
File Name : calc_diag_cmip5_cdo.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 06-01-2017
Modified: Mon 27 Nov 2017 09:39:04 AM CET
Purpose: Script calculating diagnostics from cmip5 data for further use
         e.g. in Mult_Diag_Lin_Reg

'''
import netCDF4 as nc # to work with NetCDF files
import numpy as np
from cdo import *   # load cdo functionality
cdo = Cdo()
import os
from os.path import expanduser
home = expanduser("~") # Get users home directory
import glob

###
# Define input
###
varnames = ['TXx']
var_kind = 'ann'    #kind is ann: annual, mon: monthly, seas: seasonal
seasons = ['ANN']      #choose data for particular month or season?
region = ['CNEU']      #cut data over region?
syear = 2070
eyear = 2099
nyears = eyear - syear + 1

experiment = 'rcp85'
grid = 'g025'
masko = True

archive = '/net/atmos/data/cmip5-ng' 
pathtrop = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/'

for variable in varnames:
    workdir = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/%s/work/' %(
        variable)

    if masko:
        oceanmask = "%s/scripts/plot_scripts/areas_txt/seamask_%s.nc" %(home,
                                                                        grid)
        outdir = '%s/%s/%s/maskT/' %(pathtrop, variable, var_kind)
    else:
        outdir = '%s/%s/%s/maskF/' %(pathtrop, variable, var_kind)

    if (os.access(workdir,os.F_OK) == False):
        os.makedirs(workdir)
    if (os.access(outdir, os.F_OK) == False):
        os.makedirs(outdir)

    ### read model data
    print "Read model data"
    print 'Processing variable %s %s' %(variable, var_kind)
    ## find all matching files in archive, loop over all of them
    # first count matching files in folder:
    infile = 'tasmax_day'
    name = '%s/tasmax/%s_*_%s_*_%s.nc' %(archive, infile, experiment, grid)
    nfiles = len(glob.glob(name))
    model_names = []

    print str(nfiles) + ' matching files found'
    for filename in glob.glob(name):
        print "Processing " + filename
        fh = nc.Dataset(filename, mode = 'r')
        unit = fh.variables['tasmax'].units
        model = fh.source_model
        if model == 'ACCESS1.3':
            model = 'ACCESS1-3'
        elif model == 'FGOALS_g2':
            model = 'FGOALS-g2'
        ens = fh.source_ensemble
        model_names.append(model + '_' + ens)
        fh.close()

        for res in seasons:
            tmpfile = '%s/%s_%s_%s_%s_%s-%s_%s' %(workdir, variable, model,
                                                  experiment, ens, syear, eyear,
                                                  res)
            outfile = '%s/%s_ann_%s_%s_%s_%s-%s_%s' %(outdir, variable, model,
                                                      experiment, ens, syear,
                                                      eyear, res)
            if (var_kind == 'seas'):
                cdo.selseas(res, input = 
                            "-seldate,%s-01-01,%s-12-31 -sellonlatbox,-180,180,-90,90 %s"
                            %(syear, eyear, filename),
                            output = '%s.nc' %(tmpfile))
            else:
                cdo.seldate('%s-01-01,%s-12-31' %(syear, eyear),
                            input = "-sellonlatbox,-180,180,-90,90 %s"
                            %(filename), output = '%s.nc' %(tmpfile))

            if ((variable == 'TXx') and (unit == 'K')):
                cdo.subc(273.15, input = '%s.nc' %(tmpfile),
                         output = '%s_unit.nc' %(tmpfile))
                newunit = "degC"
                cdo.chunit('"K",%s' %(newunit),
                           input = '%s_unit.nc' %(tmpfile),
                           output = '%s.nc' %(tmpfile))

            if masko:
                maskfile = '%s_masko.nc' %(tmpfile)
                cdo.setmissval(0, input = "-mul -eqc,1 %s %s.nc" %(oceanmask,
                                                                   tmpfile),
                               output = '%s' %(maskfile))
                os.system("mv %s %s.nc" %(maskfile, tmpfile))

            cdo.yearmax(input = '%s.nc' %(tmpfile),
                        output = '%sMAX.nc' %(tmpfile))
            cdo.chname('tasmax,TXx', input = '%sMAX.nc' %(tmpfile),
                       output = '%s_TXx.nc' %(tmpfile))
            os.system("mv %s_TXx.nc %sMAX.nc" %(tmpfile, outfile))
            cdo.timmean(input = '%sMAX.nc' %(outfile),
                        output = '%sMAX_CLIM.nc' %(outfile))

            cdo.detrend(input = '%sMAX.nc' %(outfile),
                        output = '%sMAX_DETREND.nc' %(tmpfile))
            cdo.regres(input = '%sMAX.nc' %(outfile),
                       output = '%sMAX_TREND.nc' %(outfile))
            cdo.timstd(input = '%sMAX_DETREND.nc' %(tmpfile),
                       output = '%sMAX_STD.nc' %(outfile))

            if region:
                # loop over regions
                for reg in region:
                    print 'Region is %s' %(reg)
                    area = reg
                    mask = np.loadtxt(
                        '%s/scripts/plot_scripts/areas_txt/%s.txt' %(
                            home, reg))

                    lonmax = np.max(mask[:, 0])
                    lonmin = np.min(mask[:, 0])
                    latmax = np.max(mask[:, 1]) 
                    latmin = np.min(mask[:, 1])
                    cdo.sellonlatbox(lonmin,lonmax,latmin,latmax,
                                     input = '%sMAX_CLIM.nc' %(outfile),
                                     output = '%sMAX_CLIM_%s.nc' %(outfile,
                                                                    reg))
                    cdo.sellonlatbox(lonmin,lonmax,latmin,latmax,
                                     input = '%sMAX_STD.nc' %(outfile),
                                     output = '%sMAX_STD_%s.nc' %(outfile,
                                                                   reg))
                    cdo.sellonlatbox(lonmin,lonmax,latmin,latmax,
                                     input = '%sMAX_TREND.nc' %(outfile),
                                     output = '%sMAX_TREND_%s.nc' %(outfile,
                                                                     reg))
                    cdo.sellonlatbox(lonmin,lonmax,latmin,latmax,
                                     input = '%sMAX.nc' %(outfile),
                                     output = '%sMAX_%s.nc' %(outfile, reg))
        # clean up workdir
        filelist = glob.glob(workdir + '*')
        for f in filelist:
            os.remove(f)

    #save model names into separate text file
    with open('%s%s_ANN_all_%s_%s-%s.txt' %(outdir, variable, 
                                           experiment,
                                           syear, eyear), "w") as text_file:
        for m in range(len(model_names)):
            text_file.write(model_names[m] + "\n")


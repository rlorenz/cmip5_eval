#!/usr/bin/python
'''
File Name : calc_HURS_obs_cdo.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 02-02-2018
Modified: 
Purpose: Script calculating relative humidity from reanalysis data
         for further use e.g. in Mult_Diag_Lin_Reg
         based on temperature (T) and dew point temperature (Td)
         es(T) = a1 exp (a3 ((T - T0) / (T - a4)))
         RH = 100 es(Td) / es(T)
         (https://www.ecmwf.int/en/does-era-40-dataset-contain-near-surface-humidity-data, https://www.ecmwf.int/sites/default/files/elibrary/2015/9211-part-iv-physical-processes.pdf)
'''
import netCDF4 as nc # to work with NetCDF files
import numpy as np
from cdo import *   # load cdo functionality
cdo = Cdo()
import os
from os.path import expanduser
home = expanduser("~") # Get users home directory
import glob

###
# Define input
###
variable = 'tas'
variable2 = 'd2m'
outvar = 'hurs'
var_kind = 'seas'    # kind is ann: annual, mon: monthly, seas: seasonal
seasons = ['DJF', 'MAM', 'JJA', 'SON'] # choose data for particular season?
region = ['EUR', 'CNEU']               # cut data over region?
obs = ['ERAint', 'MERRA2']             # 'ERAint' or 'MERRA2'

workdir = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/%s/work/' %outvar
outdir = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/%s/mon/' %outvar
syear = 1980
eyear = 2014
nyears = eyear - syear + 1

experiment = 'rcp85'
grid = 'g025'
masko = False

pathtrop = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/'
if masko:
    oceanmask = "%s/scripts/plot_scripts/areas_txt/seamask_%s.nc" %(home, grid)
    outdir = '%s/%s/mon/maskT/' %(pathtrop, outvar)
else:
    outdir = '%s/%s/mon/maskF/' %(pathtrop, outvar)

if (os.access(workdir,os.F_OK) == False):
    os.makedirs(workdir)
if (os.access(outdir, os.F_OK) == False):
    os.makedirs(outdir)

# define constants used to calculate relative humidity
a1 = 611.21
a3 = 17.502
a4 = 32.19
T0 = 273.16

#read obs data
for dataset in obs:
    if dataset == 'ERAint':
        print "Read ERAint data"
        path = '/net/tropo/climphys/rlorenz/Datasets/ERAint/ERAint_mon'
        if (variable == 'tas'):
            filename = '%s/T2M_monthly_ERAint_197901-201612_%s.nc' %(path, grid)
            varname = 'T2M'
        if (variable2 == 'd2m'):
            filename2 = '%s/d2m_monthly_ERAint_19790101-20161231_%s.nc' %(path,
                                                                      grid)
            varname2 = 'd2m'
    elif dataset == 'MERRA2':
        print "Read MERRA2 data"
        path = '/net/tropo/climphys/rlorenz/Datasets/MERRA2/monthly'
        if (variable == 'tas'):
            filename = '%s/MERRA2.tavgmon_2d_slv_Nx.T2MMEAN.1980-2015_remapbil_%s.nc' %(path,grid)
            varname = 'T2MMEAN'
        if (variable2 == 'd2m'):
            varname2 = 'T2MDEW'
            filename2 = '%s/MERRA2.tavgmon_2d_slv_Nx.T2MDEW.1980-2015_remapbil_%s.nc' %(path, grid)
    elif (dataset == 'Obs'):
        print "Do not have variables for relative humidity from Obs"
        sys.exit

    for res in seasons:
        tmpfile = '%s/%s_mon_%s_%s-%s_%s' %(workdir, variable, dataset, syear,
                                            eyear, res)
        tmpfile2 = '%s/%s_mon_%s_%s-%s_%s' %(workdir, variable2, dataset, syear,
                                             eyear, res)

        outfile = '%s/%s_mon_%s_%s-%s_%s' %(outdir, outvar, dataset, syear,
                                            eyear, res)

        if (var_kind == 'seas'):
            cdo.selseas(res, options = "-f nc", input = 
                        "-seldate,%s-01-01,%s-12-31 -sellonlatbox,-180,180,-90,90 %s"
                        %(syear, eyear, filename), output = '%s.nc' %(tmpfile))
            cdo.selseas(res, options = "-f nc", input = 
                        "-seldate,%s-01-01,%s-12-31 -sellonlatbox,-180,180,-90,90  %s"
                        %(syear, eyear, filename2),
                        output = '%s.nc' %(tmpfile2))
        else:
            cdo.seldate('%s-01-01,%s-12-31' %(syear, eyear), options = "-f nc",
                        input = "-sellonlatbox,-180,180,-90,90 %s"
                        %(filename), output = '%s.nc' %(tmpfile))
            cdo.seldate('%s-01-01,%s-12-31' %(syear, eyear), options = "-f nc",
                        input = "-sellonlatbox,-180,180,-90,90 %s"
                        %(filename2), output = '%s.nc' %(tmpfile2))
        if masko:
            maskfile = '%s_masko.nc' %(tmpfile)
            cdo.setmissval(0, input = "-mul -eqc,1 %s %s.nc" %(oceanmask,
                                                               tmpfile),
                           output = '%s' %(maskfile))
            os.system("mv %s %s.nc" %(maskfile, tmpfile))
            maskfile2 = '%s_masko.nc' %(tmpfile2)
            cdo.setmissval(0, input = "-mul -eqc,1 %s %s.nc" %(oceanmask,
                                                               tmpfile2),
                           output = '%s' %(maskfile))
            os.system("mv %s %s.nc" %(maskfile, tmpfile2))

        varfile = cdo.selname(varname, input = '%s.nc' %(tmpfile))
        varfile2 = cdo.selname(varname2, input = '%s.nc' %(tmpfile2))

        # calculate relative humidity using
        # a1 = 611.21 Pa, a3 = 17.502, a4 = 32.19 K, T0 = 273.16 K over water 
        subfileTT0 = cdo.subc(T0, options = '-b 64',
                              input = '%s.nc' %(tmpfile))
        subfileTa4 = cdo.subc(a4, options = '-b 64',
                              input = '%s.nc' %(tmpfile))
        divfileT = cdo.div(options = '-b 64',
                           input = subfileTT0 + ' ' + subfileTa4)
        multa3T = cdo.mulc(a3, options = '-b 64', input = divfileT)
        expfileT = cdo.exp(options = '-b 64', input = multa3T)
        esTfile = cdo.mulc(a1, options = '-b 64', input = expfileT)

        subfileTdT0 = cdo.subc(T0, options = '-b 64',
                               input = '%s.nc' %(tmpfile2))
        subfileTda4 = cdo.subc(a4, options = '-b 64',
                               input = '%s.nc' %(tmpfile2))
        divfileTd = cdo.div(options = '-b 64',
                            input = subfileTdT0 + ' ' +  subfileTda4)
        multa3Td = cdo.mulc(a3, options = '-b 64', input = divfileTd)
        expfileTd = cdo.exp(options = '-b 64', input = multa3Td)
        esTdfile = cdo.mulc(a1, options = '-b 64', input = expfileTd)

        divesfile = cdo.div(options = '-b 64', input = esTdfile+ ' ' + esTfile)
        hursfile = cdo.mulc(100, options = '-b 64', input = divesfile)

        hursvarname = cdo.chname('%s,hurs' %(varname2), input = hursfile)
        unitname = cdo.setunit('%', input = hursvarname)
        cdo.setvrange('0,100', input = unitname, output = '%s.nc' %(outfile))

        if (var_kind == 'seas'):
            cdo.seasmean(input = '%s.nc' %(outfile),
                         output = '%sMEAN.nc' %(outfile))
            cdo.yseasmean(input = '%s.nc' %(outfile),
                          output = '%sMEAN_CLIM.nc' %(outfile))
        elif (var_kind == 'ann'):
            cdo.yearmean(input = '%s.nc' %(outfile),
                         output = '%sMEAN.nc' %(outfile))
            cdo.timmean(input = '%s.nc' %(outfile),
                           output = '%sMEAN_CLIM.nc' %(outfile))
        else:
            cdo.ymonmean(input = '%s.nc' %(outfile),
                          output = '%sMEAN_CLIM.nc' %(outfile))
        cdo.detrend(input = '%sMEAN.nc' %(outfile),
                    output = '%s/%s_mon_%s_%s-%s_%sMEAN_DETREND.nc' %(
                        workdir, outvar, dataset, syear, eyear, res))
        cdo.regres(input = '%sMEAN.nc' %(outfile),
                   output = '%sMEAN_TREND.nc' %(outfile))
        cdo.timstd(input = '%s/%s_mon_%s_%s-%s_%sMEAN_DETREND.nc' %(
            workdir, outvar, dataset, syear, eyear, res),
                   output = '%sMEAN_STD.nc' %(outfile))

        if region:
            # loop over regions
            for reg in region:
                print 'Region is %s' %(reg)
                area = region
                mask = np.loadtxt(
                    '/home/rlorenz/scripts/plot_scripts/areas_txt/%s.txt' %(
                        reg))

                lonmax = np.max(mask[:, 0])
                lonmin = np.min(mask[:, 0])
                latmax = np.max(mask[:, 1]) 
                latmin = np.min(mask[:, 1])
                cdo.sellonlatbox(lonmin,lonmax,latmin,latmax,
                                 input = '%sMEAN_CLIM.nc' %(outfile),
                                 output = '%sMEAN_CLIM_%s.nc' %(outfile, reg))
                cdo.sellonlatbox(lonmin,lonmax,latmin,latmax,
                                 input = '%sMEAN_STD.nc' %(outfile),
                                 output = '%sMEAN_STD_%s.nc' %(outfile, reg))
                cdo.sellonlatbox(lonmin,lonmax,latmin,latmax,
                                 input = '%sMEAN_TREND.nc' %(outfile),
                                 output = '%sMEAN_TREND_%s.nc' %(outfile, reg))
                cdo.sellonlatbox(lonmin,lonmax,latmin,latmax,
                                 input = '%sMEAN.nc' %(outfile),
                                 output = '%sMEAN_%s.nc' %(outfile, reg))

# clean up workdir
filelist = glob.glob(workdir + '*')
for f in filelist:
    os.remove(f)

#!/usr/bin/python
'''
File Name : calc_diag_obs.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 04-07-2016
Modified: Tue 20 Dec 2016 12:10:38 PM CET
Purpose: calculate diagnostic from obs and save for further use


'''
import netCDF4 as nc # to work with NetCDF files
import numpy as np
from scipy import signal
import os # operating system interface
from os.path import expanduser
home = expanduser("~") # Get users home directory
import glob
import sys
sys.path.insert(0,home+'/scripts/plot_scripts/utils/')
#from calc_RMSE_obs_mod_3D import rmse_3D
from func_read_data import func_read_netcdf
from clim_seas_TLL import clim_mon_TLL, std_mon_TLL, seas_avg_mon_TLL, seas_std_mon_TLL, clim_seas_mon_TLL, ann_avg_mon_TLL
from calc_trend_TLL import trend_TLL
from point_inside_polygon import point_inside_polygon
import datetime as dt
from netcdftime import utime
from func_write_netcdf import func_write_netcdf
from mpl_toolkits.basemap import shiftgrid
###
# Define input
###
variable = ['huss', 'huss', 'huss']    #calculate variables separately
var_file = ['clim', 'std', 'trnd']     #climatology:clim, variability:std, trend:trnd
var_kind = ['seas', 'seas', 'seas']    #kind is cyc: annual cycle, mon: monthly means, seas: seasonal means
res = [2, 2, 2]                       #choose data for particular month or season?
region = 'EUR'           #cut data over region?
dataset = 'MERRA2' # 'ERAint' or 'MERRA2'

outdir = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/%s/' %(variable[0])
syear = 1980
eyear = 2014
nyears = eyear - syear + 1
grid = 'g025'

if (os.access(outdir,os.F_OK)==False):
        os.makedirs(outdir)
#read obs data
print "Read data"
if dataset == 'ERAint':
        path = '/net/tropo/climphys/rlorenz/Datasets/ERAint/ERAint_mon'

        if (variable[0]=='tas'):
            temp = func_read_netcdf('%s/T2M_monthly_ERAint_197901-201601_%s.nc' %(path,grid),'T2M',var_units=True,syear=syear, eyear=eyear)
            temp_obs = temp['data']
            obs_unit = temp['unit']
        elif (variable[0]=='pr'):
            temp = func_read_netcdf('%s/RTOT_monthly_ERAint_197901-201412_%s.nc' %(path,grid),'RTOT',var_units=True,syear=syear, eyear=eyear)
            temp_obs = temp['data']
            obs_unit = temp['unit']
            if (obs_unit == 'kg m-2 s-1'):
                ## convert to mm/day
                tmp_obs = temp_obs * (24 * 60 * 60)
                temp_obs = tmp_obs[:]
                del tmp_obs
                obs_unit = 'mm/day'
        elif (variable[0]=='sic'):
            temp = func_read_netcdf('%s/ci_monthly_ERAint_197901-201601_%s.nc' %(path,grid),'ci',syear=syear, eyear=eyear)
            #ERAint data is in fraction -> convert to percent as in CMIP5
            temp_obs = temp['data']*100
            obs_unit = 'percent'
        elif (variable[0]=='hfls'):
            temp = func_read_netcdf('%s/slhf_monthly_ERAint_197901-201512_%s.nc' %(path,grid),'slhf',var_units=True,syear=syear, eyear=eyear)
            temp_obs = -temp['data']
            obs_unit = temp['unit']
        elif (variable[0]=='tasmax'):
            temp = func_read_netcdf('%s/T2MAX_monthly_ERAint_197901-201412_%s.nc' %(path,grid),'T2MAX',syear=syear, eyear=eyear)
            temp_obs = temp['data']+273.15
            obs_unit = 'K'
elif dataset == 'MERRA2':
        path = '/net/tropo/climphys/rlorenz/Datasets/MERRA2/monthly'
        if (variable[0]=='tas'):
            temp = func_read_netcdf('%s/MERRA2.tavgmon_2d_slv_Nx.T2MMEAN.1980-2015_remapbil_%s.nc' %(path,grid),'T2MMEAN',var_units=True,syear=syear, eyear=eyear)
        elif (variable[0]=='tasmax'):
            temp = func_read_netcdf('%s/MERRA2.tavgmon_2d_slv_Nx.T2MMAX.1980-2015_remapbil_%s.nc' %(path,grid),'T2MMAX',var_units=True,syear=syear, eyear=eyear)
        elif (variable[0]=='pr'):
            temp = func_read_netcdf('%s/MERRA2.tavgmon_2d_slv_Nx.PRECTOTCORR.1980-2015_remapcon2_%s.nc' %(path,grid),'PRECTOTCORR',var_units=True,syear=syear, eyear=eyear)
        elif (variable[0]=='hfls'):
            temp = func_read_netcdf('%s/MERRA2.tavgmon_2d_flx_Nx.EFLUX.1980-2015_remapbil_%s.nc' %(path,grid),'EFLUX',var_units=True,syear=syear, eyear=eyear)
        elif (variable[0]=='psl'):
            temp = func_read_netcdf('%s/MERRA2.tavgmon_2d_slv_Nx.SLP.1980-2015_remapbil_%s.nc' %(path,grid),'SLP',var_units=True,syear=syear, eyear=eyear)
        elif (variable[0] == 'rsds'):
            temp = func_read_netcdf(
                    '%s/MERRA2.tavgmon_2d_rad_Nx.SWGDN.1980-2015_remapbil_%s.nc'
                    %(path, grid), 'SWGDN', var_units = True,
                    syear = syear, eyear = eyear)
        elif (variable[0] == 'rlus'):
            temp = func_read_netcdf(
                    '%s/MERRA2.tavgmon_2d_rad_Nx.LWGEM.1980-2015_remapbil_%s.nc'
                    %(path, grid), 'LWGEM', var_units = True,
                    syear = syear, eyear = eyear)
        elif (variable[0] == 'huss'):
            temp = func_read_netcdf(
                    '%s/MERRA2.tavgmon_2d_slv_Nx.QV2M.1980-2015_remapbil_%s.nc'
                    %(path, grid), 'QV2M', var_units = True,
                    syear = syear, eyear = eyear)
        temp_obs = temp['data']
        obs_unit = temp['unit']
        if (variable[0] == 'pr') and (obs_unit == 'kg m-2 s-1'):
            ## convert to mm/day
            tmp_obs = temp_obs * (24 * 60 * 60)
            temp_obs = tmp_obs[:]
            del tmp_obs
            obs_unit = 'mm/day'
else:
        print'Wrong obsdata specified, not available, exit'
        sys.exit
lat = temp['lat']
lon = temp['lon']    
obsdates = temp['dates']
obsmonths = np.asarray([obsdates[i].month for i in xrange(len(obsdates))])
obsyears = np.asarray([obsdates[i].year for i in xrange(len(obsdates))])
        
if isinstance(temp_obs, np.ma.core.MaskedArray):
        temp_obs = temp_obs.filled(np.NaN)

## shiftgrid to -180 to 180 if necessary (to be compatible with area mask)
if (lon[0] >= 0.0) and (lon[-1] > 180):
    lons_orig = lon[:]
    temp_obs, lon = shiftgrid(180., temp_obs, lon, start = False)

#if any region defined, read region extent from file and cut data into region
if region!=None:
        print 'Region is %s' %(region)
        area = region
        mask=np.loadtxt('/home/rlorenz/scripts/plot_scripts/areas_txt/%s.txt' %(region))                    
        temp_obs_reg_nan = np.ndarray((temp_obs.shape))
        for ilat in xrange(len(lat)):
                for ilon in xrange(len(lon)):
                        if (point_inside_polygon(lat[ilat], lon[ilon], np.fliplr(mask))):
                                temp_obs_reg_nan[:, ilat, ilon] = temp_obs[:, ilat, ilon]
                        else:
                                temp_obs_reg_nan[:, ilat, ilon] = np.NaN
        idx_lat = np.where((lat > np.min(mask[:, 1])) & (lat < np.max(mask[:, 1])))
        lat = lat[idx_lat]
        tmp1_obs_reg = np.empty((len(temp_obs_reg_nan), len(lat), len(lon)))
        idx_lon = np.where((lon > np.min(mask[:, 0])) & (lon < np.max(mask[:, 0])))
        lon = lon[idx_lon]
        temp_obs_reg = np.empty((len(temp_obs_reg_nan), len(lat), len(lon)))
        tmp1_obs_reg = np.squeeze(temp_obs_reg_nan[:, idx_lat, :], 1)
        temp_obs_reg = np.squeeze(tmp1_obs_reg[:, :, idx_lon], 2)

        #del temp_obs
else:
        area = 'GLOBAL'
        temp_obs_reg = temp_obs

#calculate obs climatology, variability, trend
print "Calculate obs climatology, variability, trend"
if (res[0] == None) and (var_kind[0] == 'cyc'):
        temp_obs_ts = ann_avg_mon_TLL(temp_obs_reg, obsyears)
        time_out = temp['time'][0::12]
elif (res[0] == None) and (var_kind[0] != 'cyc'):
        temp_obs_ts = temp_obs_reg
        time_out = temp['time']
else:
        if var_kind[0] == 'mon':
                ind = np.where(obsmonths == res[0])
                temp_obs_ts = np.squeeze(temp_obs_reg[ind, :, :])
                time_out = temp['time'][ind]
        elif var_kind[0] == 'seas':
                temp_obs_seas_ts = clim_seas_mon_TLL(temp_obs_reg, obsmonths, obsyears)
                temp_obs_ts = temp_obs_seas_ts[res[0], :, :, :]
                time_out = temp['time'][0::12]
#save ts data
outfile = '%s%s_%s_%s_%s_%s_%s-%s.nc' %(outdir, variable[0], 'ts', var_kind[0],
                                        dataset, area, syear, eyear)
func_write_netcdf(outfile, temp_obs_ts, variable[0], lon, lat, 
                  timevar = time_out, time_unit = temp['tunit'],
                  time_cal = temp['tcal'], var_units = obs_unit,
                  Description = '%s timeseries over %s %s' %(variable[0],
                                                             var_kind[0],
                                                             res[0]))

t_ind = var_file.index('clim')
if res[t_ind] == None:
        temp_obs_clim = clim_mon_TLL(temp_obs_reg, obsmonths)
        time_out = range(0, 12)
elif var_kind[t_ind] == 'mon':
        ind=np.where(obsmonths == res[t_ind])
        temp_obs_clim = np.nanmean(np.squeeze(temp_obs_reg[ind, :, :]),
                                   axis = 0, keepdims = True)
        time_out = [1]
elif var_kind[t_ind] == 'seas':
        temp_obs_clim = np.empty((1, len(lat), len(lon)))
        temp_obs_clim_seas = seas_avg_mon_TLL(temp_obs_reg, obsmonths)
        temp_obs_clim[0, :, :] = temp_obs_clim_seas[res[t_ind], :, :]
        time_out = [1]
#save clim data
outfile = '%s%s_%s_%s_%s_%s_%s-%s.nc' %(outdir, variable[t_ind], 'clim',
                                        var_kind[t_ind], dataset, area, syear,
                                        eyear)
if len(time_out) > 1:
        func_write_netcdf(outfile, temp_obs_clim, variable[t_ind], lon, lat,
                          timevar = time_out, time_unit = 'month',
                          var_units = obs_unit,
                          Description = '%s climatology over %s %s' %(
                                  variable[t_ind], var_kind[t_ind], res[t_ind]))
else:
        func_write_netcdf(outfile, temp_obs_clim, variable[t_ind], lon, lat,
                          var_units = obs_unit,
                          Description = '%s climatology over %s %s' %(
                                  variable[t_ind], var_kind[t_ind], res[t_ind]))

t_ind = var_file.index('std')
#detrend ts before calculating variability etc.
if np.isnan(temp_obs_ts).any():
        temp_obs_reg_dtr = np.empty((temp_obs_ts.shape))
        for ilat in xrange(len(lat)):
                for ilon in xrange(len(lon)):
                        tmp = temp_obs_ts[:, ilat, ilon]
                        if np.isnan(tmp).any():
                                temp_obs_reg_dtr[:, ilat, ilon] = np.NaN
                        else:
                                temp_obs_reg_dtr[:, ilat, ilon] = signal.detrend(tmp, axis = 0, type = 'linear', bp = 0)
else:
        temp_obs_reg_dtr = signal.detrend(temp_obs_ts, axis = 0,
                                          type = 'linear', bp = 0)

if var_kind[t_ind] == 'ann':
        temp_obs_std = np.nanstd(temp_obs_reg_dtr, axis = 0, keepdims = True)
elif (var_kind[t_ind] == 'mon') or (var_kind[t_ind] == 'seas'):
        temp_obs_std = np.nanstd(np.squeeze(temp_obs_reg_dtr),
                                 axis = 0, keepdims = True)

#save std data
outfile = '%s%s_%s_%s_%s_%s_%s-%s.nc' %(outdir, variable[t_ind], 'std',
                                        var_kind[t_ind], dataset, area, syear,
                                        eyear)
if len(time_out) > 1:
        func_write_netcdf(outfile, temp_obs_std, variable[t_ind], lon, lat,
                          timevar = time_out, time_unit = 'month',
                          var_units = obs_unit,
                          Description = '%s variability over %s %s' %(variable[t_ind], var_kind[t_ind], res[t_ind]))
else:
        func_write_netcdf(outfile, temp_obs_std, variable[t_ind], lon, lat,
                          var_units = obs_unit,
                          Description = '%s variability over %s %s' %(variable[t_ind], var_kind[t_ind], res[t_ind]))

#calculate trend over time defined in res
if any('trnd' in s for s in var_file):
        t_ind = var_file.index('trnd')
        if res[t_ind] == None:
                temp_obs_trnd = trend_TLL(temp_obs_ts, drop = False)
        else:
            if (var_kind[t_ind] == 'mon'):
                    ind = np.where(obsmonths == res[t_ind])
                    temp_obs_trnd = trend_TLL(np.squeeze(temp_obs_reg[ind, :, :]), drop = False)
            elif (var_kind[t_ind] == 'seas'):
                    temp_obs_trnd = trend_TLL(temp_obs_seas_ts[res[t_ind], :, :, :], drop = False)

#save trnd data
outfile = '%s%s_%s_%s_%s_%s_%s-%s.nc' %(outdir, variable[t_ind], 'trnd',
                                        var_kind[t_ind], dataset, area, syear,
                                        eyear)
if len(time_out) > 1:
        func_write_netcdf(outfile, temp_obs_trnd, variable[t_ind], lon, lat,
                          timevar = time_out, time_unit = 'month',
                          var_units = obs_unit,
                          Description = '%s trend over %s %s' %(variable[t_ind],
                                                                var_kind[t_ind],
                                                                res[t_ind]))
else:
        func_write_netcdf(outfile, temp_obs_trnd, variable[t_ind], lon, lat,
                          var_units = obs_unit,
                          Description = '%s trend over %s %s' %(variable[t_ind],
                                                                var_kind[t_ind],
                                                                res[t_ind]))

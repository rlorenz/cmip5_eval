#!/usr/bin/python
'''
File Name : calc_DTR_obs_cdo.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 24-08-2017
Modified: Thu 24 Aug 2017 05:39:33 PM CEST
Purpose: calculate DTR from observations


'''
import netCDF4 as nc # to work with NetCDF files
import numpy as np
from cdo import *   # load cdo functionality
cdo = Cdo()
from os.path import expanduser
home = expanduser("~") # Get users home directory
import glob

###
# Define input
###
variable = 'tasmax'
outvar = 'TXx'
var_kind = 'ann'    #kind is ann: annual, mon: monthly, seas: seasonal
seasons = ['ANN'] #choose data for particular season?
region = ['EUR', 'CNEU']          #cut data over region?
obs = ['Obs'] # 'ERAint', 'MERRA2', 'Obs'

workdir = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/%s/work/' %(
    outvar)

syear = 1950
eyear = 1999
nyears = eyear - syear + 1
grid = 'g025'
masko = True

pathtrop = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/'
if masko:
    outdir = '%s/%s/ann/maskT/' %(pathtrop, outvar)
    oceanmask = "%s/scripts/plot_scripts/areas_txt/seamask_%s.nc" %(home, grid)
else:
    outdir = '%s/%s/ann/maskF/' %(pathtrop, outvar)

if (os.access(workdir,os.F_OK) == False):
    os.makedirs(workdir)
if (os.access(outdir, os.F_OK) == False):
        os.makedirs(outdir)

#read obs data
for dataset in obs:
    if dataset == 'ERAint':
        print "Read ERAint data"
        path = '/net/tropo/climphys/rlorenz/Datasets/ERAint/ERAint_daily'
        varname1 = 'mx2t'
        filename1 = '%s/%s_ERAint_daily_19790101_20151231_%s.nc' %(
            path, varname1, grid)
    elif dataset == 'MERRA2':
        print "Read MERRA2 data"
        path = '/net/tropo/climphys/rlorenz/Datasets/MERRA2/daily'
        varname1 = 'T2MMAX'
        lev = 'slv'
        filename1 = '%s/%s.tavgday_2d_%s_Nx.%s.1980-2015_%s.nc' %(
            path, dataset, lev, varname1, grid)
    elif (dataset == 'Obs'):
        print "Read Obs data"
        path = '/net/tropo/climphys/rlorenz/Datasets/'
        if ((region[0] == 'EUR') or (region[0] == 'CNEU')):
            varname1 = 'tx'
            filename1 = '%s/E-OBS/tx_day_reg_v16.0_%s.nc' %(path, grid)
        else:
            varname1 = 'tmax'
            filename1 = '%s/HadGHCND/daily/HadGHCND_TX_acts_19500101-20141231_remapbil_%s.nc' %(path, grid)

    fh = nc.Dataset(filename1, mode = 'r')
    try:
        unit = fh.variables[varname1].units
    except AttributeError:
        unit = "unknown"
    fh.close()

    infile1 = '%s_day' %(variable)
    for res in seasons:
        tmpfile1 = '%s/%s_%s_%s-%s_%s' %(workdir, infile1, dataset, syear,
                                         eyear, res)

        outfile = '%s/%s_ann_%s_%s-%s_%s' %(outdir, outvar, dataset, syear,
                                            eyear, res)

        datefile = cdo.seldate('%s-01-01,%s-12-31' %(syear, eyear),
                               input = '%s' %(filename1))
        boxfile = cdo.sellonlatbox(-180,180,-90,90, input = datefile)
        varfile = cdo.selname(varname1, input = boxfile)
        namefile = cdo.chname(varname1,variable, input = varfile)
        cdo.yearmax(input = namefile, output = '%s.nc' %(tmpfile1))

        os.system("ncatted -h -O -a valid_min,,d,, %s.nc" %(tmpfile1))
        os.system("ncatted -h -O -a valid_max,,d,, %s.nc" %(tmpfile1))

        if ((variable == 'tasmax') and (unit == 'K')):
            unitfile = cdo.subc(273.15, options = '-b 64',
                                input = '%s.nc' %(tmpfile1))
            cdo.chunit('"K","degC"', input = unitfile,
                       output = '%s.nc' %(tmpfile1))
        if masko:
            maskfile = '%s_masko.nc' %(tmpfile1)
            cdo.setmissval(0, input = "-mul -eqc,1 %s %s.nc" %(oceanmask,
                                                               tmpfile1),
                           output = '%s' %(maskfile))
            os.system("mv %s %s.nc" %(maskfile, tmpfile1))

        cdo.chname('tasmax,TXx', input = '%s.nc' %(tmpfile1),
                   output = '%sMAX.nc' %(outfile))

        cdo.timmean(input = '%sMAX.nc' %(outfile),
                    output = '%sMAX_CLIM.nc' %(outfile))

        cdo.detrend(input = '%sMAX.nc' %(outfile),
                    output = '%sMAX_DETREND.nc' %(tmpfile1))
        cdo.regres(input = '%sMAX.nc' %(outfile),
                   output = '%sMAX_TREND.nc' %(outfile))
        cdo.timstd(input = '%sMAX_DETREND.nc' %(tmpfile1),
                   output = '%sMAX_STD.nc' %(outfile))

        if region != None:
            # loop over regions
            for reg in region:
                print 'Region is %s' %(reg)
                area = reg
                mask = np.loadtxt(
                    '/home/rlorenz/scripts/plot_scripts/areas_txt/%s.txt' %(reg))

                lonmax = np.max(mask[:, 0])
                lonmin = np.min(mask[:, 0])
                latmax = np.max(mask[:, 1]) 
                latmin = np.min(mask[:, 1])
                cdo.sellonlatbox(lonmin,lonmax,latmin,latmax,
                                 input = '%sMAX_CLIM.nc' %(outfile),
                                 output = '%sMAX_CLIM_%s.nc' %(outfile, reg))
                cdo.sellonlatbox(lonmin,lonmax,latmin,latmax,
                                 input = '%sMAX_STD.nc' %(outfile),
                                 output = '%sMAX_STD_%s.nc' %(outfile, reg))
                cdo.sellonlatbox(lonmin,lonmax,latmin,latmax,
                                 input = '%sMAX_TREND.nc' %(outfile),
                                 output = '%sMAX_TREND_%s.nc' %(outfile, reg))
                cdo.sellonlatbox(lonmin,lonmax,latmin,latmax,
                                 input = '%sMAX.nc' %(outfile),
                                 output = '%sMAX_%s.nc' %(outfile, reg))

# clean up workdir
filelist = glob.glob(workdir + '*')
for f in filelist:
    os.remove(f)

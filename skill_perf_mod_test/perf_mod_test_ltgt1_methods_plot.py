#!/usr/bin/python
'''
File Name : perf_mod_test_ltgt1_methods_plot.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 18-12-2017
Modified: Mon 18 Dec 2017 02:57:27 PM CET
Purpose: plot fraction RMSE between methods lt/gt 1
         from perfect model test

'''
import numpy as np
import os # operating system interface
import matplotlib.pyplot as plt

###
# Define input & output
###
target_var = 'TXx'
# climatology: CLIM, variability: STD, trend: TREND
target_file = 'CLIM'
res_name_target = 'ANN'
target_mask = 'maskT'
res_time_target = 'MAX'

diag_var = ['TXx', 'TXx']
var_file = ['TREND', 'CLIM']
res_name = ['ANN', 'ANN']
res_time = ['MAX', 'MAX']
masko = ['maskT', 'maskT']
#freq_v = 'mon'

nvar = len(diag_var)

# cut data over region?
region = 'CNEU'
if region != None:
    area = region
else:
    area = 'GLOBAL'

syear_fut = [2015, 2035, 2065]
eyear_fut = [2049, 2069, 2099]

# define directories
path = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/'
outdir = '%sweight_perf_mod_test/%s/%s/' %(path, target_var, area)
if (os.access(outdir, os.F_OK) == False):
    os.makedirs(outdir)

###
# Read data
###
lt1 = list()
gt1 = list()
lt1_perc = np.empty(len(syear_fut))
lt1_perc.fill(np.NaN)
gt1_perc = np.empty(len(syear_fut))
gt1_perc.fill(np.NaN)
for p in xrange(len(syear_fut)):
    lt1.append(np.genfromtxt('%sRMSE_ltgt1_%s_%s_%s_%s%s_%s_%s_%s-%s.txt' %(
        outdir, target_var, target_file, target_mask, len(diag_var), 
        diag_var[0], var_file[0], res_name[0], syear_fut[p], eyear_fut[p]),
                             delimiter = '', dtype = None)[0])
    gt1.append(np.genfromtxt('%sRMSE_ltgt1_%s_%s_%s_%s%s_%s_%s_%s-%s.txt' %(
        outdir, target_var, target_file, target_mask, len(diag_var), 
        diag_var[0], var_file[0], res_name[0], syear_fut[p], eyear_fut[p]),
                             delimiter = '', dtype = None)[1])
    lt1_perc[p] = lt1[p] * 100 / (lt1[p] + gt1[p])
    gt1_perc[p] = gt1[p] * 100 / (lt1[p] + gt1[p])

fig = plt.figure(figsize = (10, 5), dpi = 300)
plt.plot(eyear_fut, lt1_perc, 'blue', label = '< 1', linewidth = 3.)
plt.plot(eyear_fut, gt1_perc, 'red', label = '> 1', linewidth = 3.)

plt.xlabel('end year period')
plt.ylabel('Percent of model runs better/worse than IPCC MMM [%]')
plt.ylim([0, 100])
plt.grid(False)
leg = plt.legend(loc = 'upper left')  ## leg defines legend -> can be modified
leg.draw_frame(False)

plt.savefig('%sltgt1_%s_%s_%s_%s%s_%s_%s_%speriods.pdf' %(
    outdir, target_var, target_file, target_mask, len(diag_var),
    diag_var[0], var_file[0], res_name[0], len(syear_fut)))

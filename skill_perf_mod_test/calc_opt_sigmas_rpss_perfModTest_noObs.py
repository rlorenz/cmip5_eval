#!/usr/bin/python
'''
File Name : calc_opt_sigmas_rpss_perfModTest_noObs.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 24-04-2018
Modified: Tue 24 Apr 2018 04:18:22 PM CEST
Purpose: Determine optimal sigmas based on RPSS
         Calcualte RPSS for all sigma combination and determine
         the one with the highest skill
'''
import numpy as np
import netCDF4 as nc
from netcdftime import utime
import datetime as dt
import glob
import sys
import os # operating system interface
from os.path import expanduser
home = expanduser("~") # Get users home directory
sys.path.insert(0, '%s/scripts/plot_scripts/utils/' %home)
sys.path.insert(0,'%s/scripts/plot_scripts/CMIP5/' %home)
from func_read_data import func_read_netcdf
from func_calc_wu_wq import calc_wu, calc_wq, calc_weights_approx
from calc_RMSE_obs_mod_3D import rmse_3D
import properscoring as ps
from OrderedSet import OrderedSet
import math
import operator
import json

import logging
from info_utils import set_logger
logger = logging.getLogger(__name__)
set_logger(level = logging.DEBUG)
logger.info('Start')
###
# Define input & output
###
experiment = 'rcp85'
archive = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/'
grid = 'g025'

target_var = 'tas'
# climatology: CLIM, variability: STD, trend: TREND
target_file = 'CLIM'
res_name_target = 'JJA'
target_mask = 'maskT'
res_time_target = 'MEAN'
freq = 'mon'

diag_var = ['tasclt', 'pr', 'tas', 'pr', 'rnet', 'tas', 'hfls']
var_file = ['CORR', 'CLIM', 'STD', 'STD', 'TREND', 'CLIM', 'CLIM']
res_name = ['JJA', 'JJA', 'JJA', 'JJA', 'JJA', 'JJA', 'JJA', 'JJA']
res_time = ['MEAN', 'MEAN', 'MEAN', 'MEAN', 'MEAN', 'MEAN', 'MEAN']
masko =  ['maskT', 'maskT', 'maskT', 'maskT', 'maskT', 'maskT', 'maskT']
freq_v =   ['mon', 'mon', 'mon', 'mon', 'mon', 'mon', 'mon']

# weight of individual fields, all equal weight 1 at the moment
fields_weight = [1, 1, 1, 1, 1, 1, 1]

nvar = len(diag_var)

# cut data over region?
region = 'CNEU'
if region != None:
    area = region
else:
    area = 'GLOBAL'

obsdata = 'NoObs' #ERAint, MERRA2, Obs
err = 'RMSE' #'perkins_SS', 'RMSE'
err_var = 'rmse' #'SS', 'rmse'

# include initial conditions ensemble members or not?
ensembles = False
# time ranges
syear = 1950
eyear = 2100

split_ratio = 0.2

if (split_ratio == 0.33):
    syear_eval = [1950, 1950, 1950, 1950, 1950, 1950, 1950]
    eyear_eval = [1999, 1999, 1999, 1999, 1999, 1999, 1999]
    syear_hist = 1950
    eyear_hist = 1999
    syear_fut = [1975, 2000, 2025, 2050]
    eyear_fut = [2024, 2049, 2074, 2099]
elif (split_ratio == 0.2):
    syear_eval = [1950, 1950, 1950, 1950, 1950, 1950, 1950]
    eyear_eval = [1979, 1979, 1979, 1979, 1979, 1979, 1979]
    syear_hist = 1950
    eyear_hist = 1979
    syear_fut = [1980, 2010, 2040, 2070]
    eyear_fut = [2009, 2039, 2069, 2099]

nyears = eyear_hist - syear_hist + 1
ntim_tot = eyear - syear + 1
# define directories
path = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/'
outdir = '%sweight_perf_mod_test/%s/%s/sigmas/' %(path, target_var, area)
if (os.access(outdir, os.F_OK) == False):
    os.makedirs(outdir)

sigma_size = 41

###
# Read data
###
## read rmse for var_file
## first read model names for all var_file from text file
## select all models that occur for all var_files,
## or if testing select number of simulations defined in test
rmse_models = dict()
overlap = list()
rmse_file_target = '%s%s/%s/%s/%s_%s_all_%s_%s-%s' %(
    path, target_var, freq, target_mask, target_var, res_name_target,
    experiment, syear_hist, eyear_hist)
overlap = np.genfromtxt(rmse_file_target + '.txt', delimiter = '',
                        dtype = None).tolist()

for v in xrange(len(var_file)):
    rmsefile = '%s%s/%s/%s/%s_%s_%s_all_%s_%s-%s' %(
        path, diag_var[v], freq_v[v], masko[v], diag_var[v], var_file[v],
        res_name[v], experiment, syear_eval[v], eyear_eval[v])
    if (os.access(rmsefile + '.txt', os.F_OK) == True):
        rmse_models[var_file[v]] = np.genfromtxt(rmsefile + '.txt',
                                                 delimiter = '',
                                                 dtype = None).tolist()
        overlap = list(OrderedSet(rmse_models[var_file[v]]) & OrderedSet(overlap))
        try:
            overlap = overlap[0:test]
        except (NameError):
            pass
nfiles = len(overlap)
print '%s matching files' %(str(nfiles))

### read model data
print "Read model data"
d_temp_mod = dict()
d_temp_mod1 = dict()
d_temp_mod1_std = dict()
d_temp_mod_ts = dict()
models_1ens = list()
model_noens = list()
for f in xrange(len(overlap)):
    model = overlap[f].split('_', 1)[0]
    ens = overlap[f].split('_', 1)[1]
    if ensembles:
        models_1ens = overlap
    else:
        if model not in model_noens:
            model_noens.append(model)
            models_1ens.append(model + '_' + ens)
        else:
            print '%s already used, not using multiple ensemble members' %model
            continue
    path_target = '%s%s/%s/%s/' %(path, target_var, freq, target_mask)
    if region:
        modfile_ts = '%s%s_%s_%s_%s_%s_%s-%s_%s%s_%s.nc' %(
            path_target, target_var, freq, model, experiment, ens, syear, eyear,
            res_name_target, res_time_target, region)
    else:
        modfile_ts = '%s%s_%s_%s_%s_%s_%s-%s_%s%s.nc' %(
            path_target, target_var, freq, model, experiment, ens, syear, eyear,
            res_name_target, res_time_target)

    fh = nc.Dataset(modfile_ts, mode = 'r')
    tmp = fh.variables[target_var]
    unit = tmp.units
    temp_mod_ts = fh.variables[target_var][:]
    lat = fh.variables['lat'][:]
    lon = fh.variables['lon'][:]
    time = fh.variables['time']
    cdftime = utime(time.units, calendar = time.calendar)
    dates = cdftime.num2date(time[:])
    years = np.asarray([dates[i].year for i in xrange(len(dates))])
    fh.close()
    if isinstance(temp_mod_ts, np.ma.core.MaskedArray):
        d_temp_mod_ts[model + '_' + ens] = temp_mod_ts.filled(np.nan)
    else:
        d_temp_mod_ts[model + '_' + ens] = temp_mod_ts

# calculate area average
rad = 4.0 * math.atan(1.0) / 180
w_lat = np.cos(lat * rad) # weight for latitude differences in area

d_temp_mod_ts_areaavg = dict()
tmp_latweight = np.ma.empty((len(years), len(lon)))
for key, value in d_temp_mod_ts.iteritems():
    for ilon in xrange(len(lon)):
        tmp_latweight[:, ilon] = np.ma.average(value[:, :, ilon],
                                               axis = 1, weights = w_lat)
    d_temp_mod_ts_areaavg[key] = np.nanmean(tmp_latweight.filled(
        np.nan), axis = 1)


sigma_S2_end = dict()
sigma_D2_end = dict()
for ndiag in xrange(1, nvar + 1):
    logger.info('Diag loop')
    for per in xrange(len(syear_fut)):
        logger.info('Period loop')
        print 'Perfect model test for %s diagnostics and period %s' %(ndiag, syear_fut[per])
        ###
        # Choose one model to be excluded, use as truth in perfect model test
        ###
        d_mm_ens_ts = dict()
        d_wmm_ens_ts = dict()
        #delta_wmm = np.zeros((len(models_1ens) - 1))
        delta_mm = np.zeros((len(models_1ens) - 1))
        weights_mod = np.zeros((len(models_1ens) - 1))
        skill_wmm2 = np.zeros((len(models_1ens), sigma_size, sigma_size))
        ind_hist1 = np.where(years == syear_hist)[0][0]
        ind_hist2 = np.where(years == eyear_hist)[0][0] + 1
        ind_fut1 = np.where(years == syear_fut[per])[0][0]
        ind_fut2 = np.where(years == eyear_fut[per])[0][0] + 1
        rpss_sigmas = np.empty((sigma_size, sigma_size))
        rpss_sigmas.fill(np.NaN)
        for rint in xrange(len(models_1ens)):
            #logger.info('Perfect Model Loop')
            #rint = randint(0, nfiles)
            model_subset = models_1ens[: rint] + models_1ens[rint + 1 :]

            indices = dict()
            rmse_d = np.ndarray((ndiag, len(model_subset), len(model_subset)))
            rmse_q = np.ndarray((ndiag, len(model_subset)))
            for v in xrange(ndiag):
                rmsepath = '%s%s/%s/%s/' %(
                    path, diag_var[v], freq_v[v], masko[v])
                rmsefile = '%s%s_%s_%s_all_%s_%s-%s' %(
                    rmsepath, diag_var[v], var_file[v], res_name[v],
                    experiment, syear_eval[v], eyear_eval[v])
                ## find indices of model_subset in rmse_file
                rmse_models[var_file[v]] = np.genfromtxt(rmsefile + '.txt',
                                                         delimiter = '',
                                                         dtype = None).tolist()
                for m in xrange(len(model_subset)):
                    indices[model_subset[m]] = rmse_models[var_file[v]].index(
                        model_subset[m])
                sorted_ind = sorted(indices.items(),
                                    key = operator.itemgetter(1))
                ind = [x[1] for x in sorted_ind]
                model_names = [x[0] for x in sorted_ind]

                ncfile = '%s%s_%s_%s_all_%s_%s-%s_%s_%s_%s.nc' %(
                    rmsepath, diag_var[v], var_file[v], res_name[v], experiment,
                    syear_eval[v], eyear_eval[v], area, obsdata, err)
                if (os.access(ncfile, os.F_OK) == True):
                    #print err + ' already exist, read from netcdf'
                    fh = nc.Dataset(ncfile, mode = 'r')
                    rmse_all = fh.variables[err_var]
                    rmse_d[v, :, :] = rmse_all[ind, ind]
                    rmse_q[v, :] = rmse_all[rint, ind]
                    fh.close()
                else:
                    logging.warning('RMSE delta matrix does not exist yet, exiting')
                    sys.exit
            delta_u = np.ndarray((ndiag, len(model_names),
                                  len(model_names)))
            delta_q = np.ndarray((ndiag, len(model_names)))
            for v in xrange(ndiag):
                ## normalize rmse by median
                med = np.nanmedian(rmse_d[v, :, :])
                delta_u[v, :, :] = rmse_d[v, :, :] / med
                delta_q[v, :] = rmse_q[v, :] / med
            ## average deltas over fields,
            ## taking field weight into account (all 1 at the moment)
            fields_weight_tmp = fields_weight[0 : ndiag]
            field_w_extend_u = np.reshape(np.repeat(
                fields_weight_tmp, len(model_names) * len(model_names)),
                                          (ndiag, len(model_names),
                                           len(model_names)))
            delta_u = np.sqrt(np.nansum(field_w_extend_u * delta_u, axis = 0)
                              / np.nansum(fields_weight_tmp))

            field_w_extend_q = np.reshape(np.repeat(fields_weight_tmp,
                                                    len(model_names)),
                                          (ndiag, len(model_names)))
            delta_q = np.sqrt(np.nansum(field_w_extend_q * delta_q, axis = 0)
                              / np.nansum(fields_weight_tmp))

            # subset model data, exclude one model
            d_temp_mod_sub_ts = dict(d_temp_mod_ts)
            del d_temp_mod_sub_ts[models_1ens[rint]]
            # calculate area average
            d_ts_areaavg = dict()
            for key, value in d_temp_mod_sub_ts.iteritems():
                ma_value = np.ma.masked_array(value, np.isnan(value))
                tmp_latweight = np.ma.empty((len(years), len(lon)))
                for ilon in xrange(len(lon)):
                    tmp_latweight[:, ilon] = np.ma.average(ma_value[:, :, ilon],
                                                           axis = 1,
                                                           weights = w_lat)
                d_ts_areaavg[key] = np.nanmean(tmp_latweight.filled(np.nan),
                                               axis = 1)
            # average over initial conditions ensembles per model
            # if ensemble = True
            if ensembles:
                for key, value in d_ts_areaavg.iteritems():
                    if key.split('_')[0] in list_with_mult_ens:
                        #find other ensemble members
                        ens_mem = [value2 for key2, value2 in sorted(
                            d_ts_areaavg.iteritems()) if key.split('_')[0] in key2]
                        d_mm_ens_ts[key.split('_')[0]] = np.nanmean(
                            ens_mem, axis = 0)
                    else:
                        d_mm_ens_ts[key.split('_')[0]] = value
                del key, value, key2, value2
            else:
                d_mm_ens_ts = d_ts_areaavg
            # Calculate delta change for excluded model as truth
            truth = d_temp_mod_ts_areaavg[models_1ens[rint]]
            delta_true = np.nanmean(truth[ind_fut1 : ind_fut2]) - np.nanmean(truth[ind_hist1 : ind_hist2])
            # Calculate unweighted mm mean delta change
            mod = 0
            for key, value in d_mm_ens_ts.iteritems():
                delta_mm[mod] =  np.nanmean(value[ind_fut1 : ind_fut2]) - np.nanmean(value[ind_hist1 : ind_hist2])
                mod = mod + 1

            # calculate crps using properscoring
            baseline_score_2 = ps.crps_ensemble(delta_true, delta_mm)

            tmp = np.mean(delta_u)
            #wu
            sigma_S2 = np.linspace(tmp - 0.9 * tmp, tmp + 0.9 * tmp, sigma_size)
            #wq
            sigma_D2 = np.linspace(tmp - 0.9 * tmp, tmp + 0.9 * tmp, sigma_size)
            #logger.info('Start Sigma Loops')
            for s2 in xrange(len(sigma_S2)):
                for d2 in xrange(len(sigma_D2)):
                    wu_end = calc_wu(delta_u, model_names, sigma_S2[s2])
                    wq_end = calc_wq(delta_q, model_names, sigma_D2[d2])
                    approx_wmm_ts = calc_weights_approx(wu_end, wq_end,
                                                        model_names,
                                                        d_temp_mod_sub_ts)
                    ###
                    # "Evaluate" mmm with excluded model as truth
                    ###
                    mod = 0
                    for key in d_mm_ens_ts:
                        weights_mod[mod] = approx_wmm_ts['weights'][key]
                        mod = mod + 1

                    if ((np.min(weights_mod) == 0.0) and
                        (np.max(weights_mod) == 0.0)):
                        logging.warning('All weights zero for S2: %s, D2: %s') %(s2, d2)

                    # calculate crps using properscoring
                    forecast_score_wmm2 = ps.crps_ensemble(delta_true, delta_mm,
                                                           weights= weights_mod)
                    skill_wmm2[rint, s2, d2] = 1 - (forecast_score_wmm2 / baseline_score_2)
                    if skill_wmm2[rint, s2, d2] > 1:
                        logging.warning('Warning, RPSS larger than 1')
                        
        logging.debug('Median skill: %s' %(np.nanmedian(skill_wmm2, axis = 0)))
        rpss_sigmas = np.nanmedian(skill_wmm2, axis = 0)

        ## calcuate indices of smallest largest RPSS to determine optimal sigmas
        logging.debug('%s' %rpss_sigmas)
        ind1, ind2 = np.unravel_index(np.argmax(rpss_sigmas), rpss_sigmas.shape)
        sigma_S2_end[syear_fut[per]] = round(sigma_S2[ind1], 3)
        sigma_D2_end[syear_fut[per]] = round(sigma_D2[ind2], 3)
        logging.info('Max. RPSS is %s, median RPSS is %s' %(
            np.nanmax(rpss_sigmas), np.nanmedian(rpss_sigmas)))
        logging.info("Optimal sigmas are %s and %s" %(
            str(sigma_S2_end[syear_fut[per]]),
            str(sigma_D2_end[syear_fut[per]])))

    logging.info("save optimal sigmas to json file")
    sigma_S2_out = "%ssigma_S2_%s_%s_%s_%s%s_%s_%s_%s_%s-%s_split%s.txt" %(
        outdir, target_var, target_file, target_mask, ndiag, diag_var[0],
        var_file[0], res_name[0], region, syear_hist, eyear_hist,
        str(split_ratio))
    with open(sigma_S2_out, "w") as text_file_S2:
        json.dump(sigma_S2_end, text_file_S2)

    sigma_D2_out = "%ssigma_D2_%s_%s_%s_%s%s_%s_%s_%s_%s-%s_split%s.txt" %(
        outdir, target_var, target_file, target_mask, ndiag, diag_var[0],
        var_file[0], res_name[0], region, syear_hist, eyear_hist,
        str(split_ratio))
    with open(sigma_D2_out, "w") as text_file_D2:
        json.dump(sigma_D2_end, text_file_D2)

#!/usr/bin/python
'''
File Name : plot_nr_diag_med_rpss_diffdiag_combinations.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 07-06-2018
Modified: Thu 07 Jun 2018 03:27:37 PM CEST
Purpose: plot number of diagnostics versus RPSS for different diagnostics combinations


'''
import numpy as np
import os # operating system interface
import json
import matplotlib
import matplotlib.pyplot as plt
import sys
from os.path import expanduser
home = expanduser("~") # Get users home directory
sys.path.insert(0, home + '/scripts/plot_scripts/utils/')

###
# Define input & output
###
target_var = 'tas'
# climatology: CLIM, variability: STD, trend: TREND
target_file = 'CLIM'
res_name_target = 'JJA'
target_mask = 'maskT'
res_time_target = 'MEAN'

diag_var = ['tasclt', 'pr', 'tas', 'pr', 'rnet', 'tas', 'hfls']
var_file = ['CORR', 'CLIM', 'STD', 'STD', 'TREND', 'CLIM', 'CLIM']
res_name = ['JJA', 'JJA', 'JJA', 'JJA', 'JJA', 'JJA', 'JJA', 'JJA']
res_time = ['MEAN', 'MEAN', 'MEAN', 'MEAN', 'MEAN', 'MEAN', 'MEAN']
masko =  ['maskT', 'maskT', 'maskT', 'maskT', 'maskT', 'maskT', 'maskT']
freq_v =   ['mon', 'mon', 'mon', 'mon', 'mon', 'mon', 'mon']

nvar = len(diag_var)

# cut data over region?
region = 'CNEU'
if region != None:
    area = region
else:
    area = 'GLOBAL'

sigma_type = "fixsigmas0.35_0.5"

split_ratio = 0.2

if split_ratio == 0.33:
    syear_eval = 1950
    eyear_eval = 1999
    syear_fut = [1975, 2000, 2025, 2050]
    eyear_fut = [2024, 2049, 2074, 2099]
elif split_ratio == 0.2:
    syear_eval = 1950
    eyear_eval = 1979
    syear_fut = [1980, 2010, 2040, 2070]
    eyear_fut = [2009, 2039, 2069, 2099]

# define directories
path = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/'
outdir = '%sweight_perf_mod_test/%s/%s/%s/' %(path, target_var, area, sigma_type)
if (os.access(outdir, os.F_OK) == False):
    os.makedirs(outdir)

###
# Read data and Plot data
###
# read all data for weighted mm
mu = np.empty((len(diag_var), len(diag_var)))
mu.fill(np.NaN)
nd = 0
comb = 0
for d in xrange(1, len(diag_var) + 1, 1):
    for comb in xrange(0, len(diag_var), 1):
        file_in = "%s%s_%s_%s_%s%s_%s_%s_%s_%s-%s_linARIMA_rpss_ps.txt" %(
            outdir, target_var, target_file, target_mask, str(d),
            diag_var[comb], var_file[comb], res_name[comb], area,
            str(syear_fut[-1]), str(eyear_fut[-1]))
        with open(file_in, 'r') as fp:
            tmp = json.load(fp)
        mu[nd, comb] = np.median(tmp['rpss_wmm'].values())
        comb += 1
    nd += 1

# plot scatter, number of diagnostics versus median rpss
rainbow = plt.get_cmap('viridis_r')
col = matplotlib.colors.Normalize(vmin = 0.0, vmax = 8.0)

fig, ax = plt.subplots()
for comb in xrange(0, len(diag_var), 1):
    plt.plot(range(1, len(diag_var) + 1, 1), mu[:, comb], 'o', 
             color = rainbow(col(comb)))

ax.set_xlabel('Number of diagnostics')
ax.set_ylabel('CRPSS')

plt.savefig('%srpss_wmm_nrdiag_vs_medskill_%s_%s_%s_max%sdiags_%s-%s_split%s.pdf' %(
    outdir, target_var, target_file, target_mask, str(d),
    str(syear_fut[-1]), str(eyear_fut[-1]), str(split_ratio)))


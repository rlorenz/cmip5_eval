#!/usr/bin/python
'''
File Name : perf_mod_test_weight_mm_ARIMA_cdo.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 28-03-2018
Modified: Mon Apr  9 09:31:04 2018
Purpose: calculate weighted multi-model mean of cmip5 data,
         predict change, evaluate using perfect model test
         baseline is statistical model ARIMA (p, d, q)
         calculated in folder stat_mod_pred
'''
import numpy as np
import netCDF4 as nc
from netcdftime import utime
import datetime as dt
import pandas as pd
from random import randint, sample
import glob
import sys
import os # operating system interface
from os.path import expanduser
home = expanduser("~") # Get users home directory
sys.path.insert(0, '%s/scripts/plot_scripts/utils/' %home)
from func_read_data import func_read_netcdf
sys.path.insert(0, '%s/scripts/plot_scripts/CMIP5/' %home)
from func_calc_wu_wq import calc_wu, calc_wq, calc_weights_approx
from calc_RMSE_obs_mod_3D import rmse_3D
from func_eval_wmm_nonwmm_error_indexI import error_indexI
from ranks_from_scores import ranks_from_scores
import properscoring as ps
from rank_prob_score import rank_prob_score
from OrderedSet import OrderedSet
import operator
import math
import matplotlib.pyplot as plt

import json
#import pickle
###
# Define input & output
###
experiment = 'rcp85'
archive = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/'
freq = 'mon'
grid = 'g025'

target_var = 'tas'
# climatology: CLIM, variability: STD, trend: TREND
target_file = 'CLIM'
res_name_target = 'JJA'
target_mask = 'maskT'
res_time_target = 'MEAN'

diag_var = ['tasclt', 'pr', 'tas', 'pr', 'rnet', 'tas', 'hfls']
var_file = ['CORR', 'CLIM', 'STD', 'STD', 'TREND', 'CLIM', 'CLIM']
res_name = ['JJA', 'JJA', 'JJA', 'JJA', 'JJA', 'JJA', 'JJA', 'JJA']
res_time = ['MEAN', 'MEAN', 'MEAN', 'MEAN', 'MEAN', 'MEAN', 'MEAN']
masko =  ['maskT', 'maskT', 'maskT', 'maskT', 'maskT', 'maskT', 'maskT']
freq_v =   ['mon', 'mon', 'mon', 'mon', 'mon', 'mon', 'mon']

# weight of individual fields, all equal weight 1 at the moment
fields_weight = [1, 1, 1, 1, 1, 1, 1]

nvar = len(diag_var)

# cut data over region?
region = 'CNEU'
if region != None:
    area = region
else:
    area = 'GLOBAL'

obsdata = 'NoObs' #ERAint, MERRA2, Obs
err = 'RMSE' #'perkins_SS', 'RMSE'
err_var = 'rmse' #'SS', 'rmse'

# include initial conditions ensemble members or not?
ensembles = False

# time ranges
split_ratio = 0.33

syear = 1950
eyear = 2100
if split_ratio == 0.33:
    syear_eval = [1950, 1950, 1950, 1950, 1950, 1950, 1950]
    eyear_eval = [1999, 1999, 1999, 1999, 1999, 1999, 1999]
    syear_hist = 1950
    eyear_hist = 1999
    syear_fut = [1975, 2000, 2025, 2050]
    eyear_fut = [2024, 2049, 2074, 2099]
elif split_ratio == 0.2:
    syear_eval = [1950, 1950, 1950, 1950, 1950, 1950, 1950]
    eyear_eval = [1979, 1979, 1979, 1979, 1979, 1979, 1979]
    syear_hist = 1950
    eyear_hist = 1979
    syear_fut = [1980, 2010, 2040, 2070]
    eyear_fut = [2009, 2039, 2069, 2099]

nyears = eyear_hist - syear_hist + 1
ntim_tot = eyear - syear + 1

## parameters for weighting
## free parameter "radius of similarity", read from file if not given here
sigma_S2 = {'1975': 0.53, '2000': 0.53, '2025': 0.53, '2050': 0.53}
#sigma_S2 = {'1980': 0.53, '2010': 0.53, '2040': 0.53, '2070': 0.53}
## free parameter "radius of model quality", read from file if not given here
sigma_D2 = {'1975': 0.46, '2000': 0.46, '2025': 0.46, '2050': 0.46}
#sigma_D2 = {'1980': 0.46, '2010': 0.46, '2040': 0.46, '2070': 0.46}
sigma_read = False
#sigma_method = 'inpercentile' # or 'max_rpss'

## ARIMA parameters
p = 0
d = 1
q = 1

## parameter for bestXY models
# how many models do you want to select?
nr_models = 10

# define directories
path = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/'
if sigma_read:
    outdir = '%sweight_perf_mod_test/%s/%s/optsigmas/%s/' %(
        path, target_var, area, sigma_method)
else:
    outdir = '%sweight_perf_mod_test/%s/%s/fixsigmas%s_%s/' %(
        path, target_var, area, sigma_S2[str(syear_fut[0])],
        sigma_D2[str(syear_fut[0])])
if (os.access(outdir, os.F_OK) == False):
    os.makedirs(outdir)

###
# Read data
###
## read rmse for var_file
## first read model names for all var_file from text file
## select all models that occur for all var_files,
## or if testing select number of simulations defined in test
rmse_models = dict()
overlap = list()
rmse_file_target = '%s%s/%s/%s/%s_%s_all_%s_%s-%s' %(
    path, target_var, freq, target_mask, target_var, res_name_target,
    experiment, syear_hist, eyear_hist)
overlap = np.genfromtxt(rmse_file_target + '.txt', delimiter = '',
                        dtype = None).tolist()

for v in xrange(len(var_file)):
    rmsefile = '%s%s/%s/%s/%s_%s_%s_all_%s_%s-%s' %(
        path, diag_var[v], freq_v[v], masko[v], diag_var[v], var_file[v],
        res_name[v], experiment, syear_eval[v], eyear_eval[v])
    if (os.access(rmsefile + '.txt', os.F_OK) == True):
        rmse_models[var_file[v]] = np.genfromtxt(rmsefile + '.txt',
                                                 delimiter = '',
                                                 dtype = None).tolist()
        overlap = list(OrderedSet(rmse_models[var_file[v]]) & OrderedSet(overlap))
        try:
            overlap = overlap[0:test]
        except (NameError):
            pass
nfiles = len(overlap)
print '%s matching files' %(str(nfiles))

### read model data
print "Read model data"
d_temp_mod = dict()
d_temp_mod1 = dict()
d_temp_mod1_std = dict()
d_temp_mod_ts = dict()
models_1ens = list()
model_noens = list()
for f in xrange(len(overlap)):
    model = overlap[f].split('_', 1)[0]
    ens = overlap[f].split('_', 1)[1]
    if ensembles:
        models_1ens = overlap
    else:
        if model not in model_noens:
            model_noens.append(model)
            models_1ens.append(model + '_' + ens)
        else:
            print '%s already used, not using multiple ensemble members' %model
            continue
    path_target = '%s%s/%s/%s/' %(path, target_var, freq, target_mask)
    if region:
        modfile_ts = '%s%s_%s_%s_%s_%s_%s-%s_%s%s_%s.nc' %(
            path_target, target_var, freq, model, experiment, ens, syear, eyear,
            res_name_target, res_time_target, region)
    else:
        modfile_ts = '%s%s_%s_%s_%s_%s_%s-%s_%s%s.nc' %(
            path_target, target_var, freq, model, experiment, ens, syear, eyear,
            res_name_target, res_time_target)

    fh = nc.Dataset(modfile_ts, mode = 'r')
    tmp = fh.variables[target_var]
    unit = tmp.units
    temp_mod_ts = fh.variables[target_var][:]
    lat = fh.variables['lat'][:]
    lon = fh.variables['lon'][:]
    time = fh.variables['time']
    cdftime = utime(time.units, calendar = time.calendar)
    dates = cdftime.num2date(time[:])
    years = np.asarray([dates[i].year for i in xrange(len(dates))])
    fh.close()
    if isinstance(temp_mod_ts, np.ma.core.MaskedArray):
        d_temp_mod_ts[model + '_' + ens] = temp_mod_ts.filled(np.nan)
    else:
        d_temp_mod_ts[model + '_' + ens] = temp_mod_ts

# calculate area average
rad = 4.0 * math.atan(1.0) / 180
w_lat = np.cos(lat * rad) # weight for latitude differences in area

d_temp_mod_ts_areaavg = dict()
tmp_latweight = np.ma.empty((len(years), len(lon)))
for key, value in d_temp_mod_ts.iteritems():
    for ilon in xrange(len(lon)):
        tmp_latweight[:, ilon] = np.ma.average(value[:, :, ilon],
                                               axis = 1, weights = w_lat)
    d_temp_mod_ts_areaavg[key] = np.nanmean(tmp_latweight.filled(
        np.nan), axis = 1)

for ndiag in xrange(1, nvar + 1):
    print 'Number of diagnostics: %s' %ndiag
    ### read optimal sigma values if not given above
    if (sigma_read and sigma_method == 'max_rpss'):
        with open("%sweight_perf_mod_test/%s/%s/sigmas/sigma_S2_%s_%s_%s_%s%s_%s_%s_%s_%s-%s_split%s.txt" %(
                path, target_var, region, target_var, target_file, target_mask,
                ndiag, diag_var[0], var_file[0], res_name[0], area, syear_hist,
                eyear_hist, str(split_ratio))) as file_S2:
            sigma_S2 = json.load(file_S2)
        with open("%sweight_perf_mod_test/%s/%s/sigmas/sigma_D2_%s_%s_%s_%s%s_%s_%s_%s_%s-%s_split%s.txt" %(
                path, target_var, region, target_var, target_file, target_mask,
                ndiag, diag_var[0], var_file[0], res_name[0], area, syear_hist,
                eyear_hist, str(split_ratio))) as file_D2:
            sigma_D2 = json.load(file_D2)
    elif (sigma_read and sigma_method == 'inpercentile'):
        with open("%sEval_Weight/%s/sigmas/sigma_S2_inperc_%s_%s_%s_%s%s_%s_%s_%s_%s-%s.txt" %(
                path, target_var, target_var, target_file, target_mask, ndiag,
                diag_var[0], var_file[0], res_name[0], area, syear_hist,
                eyear_hist)) as file_S2:
            sigma_S2_tmp = json.load(file_S2)
        with open("%sEval_Weight/%s/sigmas/sigma_D2_inperc_%s_%s_%s_%s%s_%s_%s_%s_%s-%s.txt" %(
                path, target_var, target_var, target_file, target_mask, ndiag,
                diag_var[0], var_file[0], res_name[0], area, syear_hist,
                eyear_hist)) as file_D2:
            sigma_D2_tmp = json.load(file_D2)
        sigma_S2 = dict()
        sigma_D2 = dict()
        for t in xrange(len(syear_fut)):
            sigma_S2[str(syear_fut[t])] = sigma_S2_tmp
            sigma_D2[str(syear_fut[t])] = sigma_D2_tmp
    print 'sigma_S2: %s' %(sigma_S2)
    print 'sigma_D2: %s' %(sigma_D2)
    for per in xrange(len(syear_fut)):
        ###
        # Choose one model to be excluded, use as truth in perfect model test
        ###
        #lin_pred_aa = np.ndarray((len(models_1ens), len(years)), dtype = float)

        d_mm_ens_ts = dict()
        d_wmm_ens_ts = dict()
        d_best_mm_ts = dict()
        d_rand_mm_ts = dict()
        d_lin_pred_aa = dict()

        delta_true = np.zeros((len(models_1ens) - 1))
        delta_lin = np.zeros((len(models_1ens) - 1))
        delta_wmm = np.zeros((len(models_1ens) - 1))
        delta_mm = np.zeros((len(models_1ens) - 1))
        delta_best = np.zeros((nr_models))
        delta_rand = np.zeros((nr_models))
        weights_mod = np.zeros((len(models_1ens) - 1))

        skill_best = np.zeros((len(models_1ens)))
        skill_wmm = np.zeros((len(models_1ens)))
        skill_lin = np.zeros((len(models_1ens)))
        skill_mm = np.zeros((len(models_1ens)))
        skill_rand = np.zeros((len(models_1ens)))

        skill_best2 = np.zeros((len(models_1ens)))
        skill_wmm2 = np.zeros((len(models_1ens)))
        skill_lin2 = np.zeros((len(models_1ens)))
        skill_mm2 = np.zeros((len(models_1ens)))
        skill_rand2 = np.zeros((len(models_1ens)))
        skill_wrand2 = np.zeros((len(models_1ens)))

        ind_hist1 = np.where(years == syear_hist)[0][0]
        ind_hist2 = np.where(years == eyear_hist)[0][0] + 1
        ind_fut1 = np.where(years == syear_fut[per])[0][0]
        ind_fut2 = np.where(years == eyear_fut[per])[0][0] + 1

        for rint in xrange(len(models_1ens)):
            #rint = randint(0, nfiles)
            model_subset = models_1ens[: rint] + models_1ens[rint + 1 :]

            indices = dict()
            rmse_d = np.ndarray((ndiag, len(model_subset),
                                 len(model_subset)))
            rmse_q = np.ndarray((ndiag, len(model_subset)))
            for v in xrange(ndiag):
                rmsepath = '%s%s/%s/%s/' %(path, diag_var[v], freq_v[v],
                                           masko[v])
                rmsefile = '%s%s_%s_%s_all_%s_%s-%s' %(rmsepath, diag_var[v],
                                                       var_file[v], res_name[v],
                                                       experiment,
                                                       syear_eval[v],
                                                       eyear_eval[v])
                ## find indices of model_subset in rmse_file
                rmse_models[var_file[v]] = np.genfromtxt(rmsefile + '.txt',
                                                         delimiter = '',
                                                         dtype = None).tolist()
                for m in xrange(len(model_subset)):
                    indices[model_subset[m]] = rmse_models[var_file[v]].index(
                        model_subset[m])
                sorted_ind = sorted(indices.items(), key = operator.itemgetter(1))
                ind = [x[1] for x in sorted_ind]
                model_names = [x[0] for x in sorted_ind]

                ncfile = '%s%s_%s_%s_all_%s_%s-%s_%s_%s_%s.nc' %(
                    rmsepath, diag_var[v], var_file[v], res_name[v], experiment,
                    syear_eval[v], eyear_eval[v], area, obsdata, err)
                if (os.access(ncfile, os.F_OK) == True):
                    #print err + ' already exist, read from netcdf'
                    fh = nc.Dataset(ncfile, mode = 'r')
                    rmse_all = fh.variables[err_var]
                    rmse_d[v, :, :] = rmse_all[ind, ind]
                    rmse_q[v, :] = rmse_all[rint, ind]
                    fh.close()
                else:
                    print "RMSE delta matrix does not exist yet, exiting"
                    sys.exit
            delta_u = np.ndarray((ndiag, len(model_names),
                                  len(model_names)))
            delta_q = np.ndarray((ndiag, len(model_names)))
            for v in xrange(ndiag):
                ## normalize rmse by median
                med = np.nanmedian(rmse_d[v, :, :])
                delta_u[v, :, :] = rmse_d[v, :, :] / med
                delta_q[v, :] = rmse_q[v, :] / med
            ## average deltas over fields,
            ## taking field weight into account (all 1 at the moment)
            fields_weight_tmp = fields_weight[0 : ndiag]
            field_w_extend_u = np.reshape(np.repeat(
                fields_weight_tmp, len(model_names) * len(model_names)),
                                          (ndiag, len(model_names),
                                           len(model_names)))
            delta_u = np.sqrt(np.nansum(field_w_extend_u * delta_u, axis = 0)
                              / np.nansum(fields_weight_tmp))

            field_w_extend_q = np.reshape(np.repeat(fields_weight_tmp,
                                                    len(model_names)),
                                          (ndiag, len(model_names)))
            delta_q = np.sqrt(np.nansum(field_w_extend_q * delta_q, axis = 0)
                              / np.nansum(fields_weight_tmp))

            # subset model data, exclude one model
            d_temp_mod_sub_ts = dict(d_temp_mod_ts)
            del d_temp_mod_sub_ts[models_1ens[rint]]
            # calculate area average
            d_ts_areaavg = dict()
            for key, value in d_temp_mod_sub_ts.iteritems():
                ma_value = np.ma.masked_array(value, np.isnan(value))
                tmp_latweight = np.ma.empty((len(years), len(lon)))
                for ilon in xrange(len(lon)):
                    tmp_latweight[:, ilon] = np.ma.average(ma_value[:, :, ilon],
                                                           axis = 1,
                                                           weights = w_lat)
                d_ts_areaavg[key] = np.nanmean(tmp_latweight.filled(np.nan),
                                               axis = 1)

            ###
            # Calculate weights
            ###
            #print "Calculate weights for model uniqueness (u) and quality (q)"
            wu_end = calc_wu(delta_u, model_names,
                             float(sigma_S2[str(syear_fut[per])]))
            wq_end = calc_wq(delta_q, model_names,
                             float(sigma_D2[str(syear_fut[per])]))

            ###
            # Calculate weighted multi-model ensemble
            ###
            #print "Calculate weighted and non-weighted ensembles"
            model_keys = d_temp_mod_sub_ts.keys()
            if set(model_keys) != set(model_names):
                print 'Not the same model names in lists, exit'
                sys.exit
            # target_file       
            # time series
            approx_wmm_ts = calc_weights_approx(wu_end, wq_end, model_names,
                                                d_temp_mod_sub_ts)
            approx_wmm_ts_areaavg = calc_weights_approx(wu_end, wq_end,
                                                        model_names,
                                                        d_ts_areaavg)

            # weighted multi-model ensemble
            for key, value in d_ts_areaavg.iteritems():
                d_wmm_ens_ts[key] = approx_wmm_ts['weights'][key] * value

            # average over initial conditions ensembles per model if ensemble = True
            if ensembles:
                for key, value in d_ts_areaavg.iteritems():
                    if key.split('_')[0] in list_with_mult_ens:
                        #find other ensemble members
                        ens_mem = [value2 for key2, value2 in sorted(
                            d_ts_areaavg.iteritems()) if key.split('_')[0] in key2]
                        d_mm_ens_ts[key.split('_')[0]] = np.nanmean(ens_mem,
                                                                    axis = 0)
                    else:
                        d_mm_ens_ts[key.split('_')[0]] = value
                del key, value, key2, value2
            else:
                d_mm_ens_ts = d_ts_areaavg

            ###
            # Read linear regression of excluded model
            ###
            # linear regressions for area averages
            infile_ts = "%sstat_mod_cmip/%s/ARIMA/pred_ARIMA%s%s%s_model_%s-%s_shift_window_%s_%s_%s.txt"%(
                path_target, region, str(p), str(d), str(q), syear_hist, eyear_hist,
                target_var, res_name_target, region)
            #ts_areaavg = pickle.load(open(infile_ts, "rb"))
            with open(infile_ts, 'r') as fp:
                ts_areaavg = pd.read_json(fp)

            #lin_pred_aa[rint, :] = ts_areaavg[models_1ens[rint]]
            for key in model_subset:
                d_lin_pred_aa[key] = ts_areaavg[key]

            ###
            # determine XY "best" models
            ###
            RMSE_diag = dict(zip(model_names, delta_q))
            sort_RMSE = sorted(RMSE_diag.items(), key = operator.itemgetter(1))
            ranks = {}
            sort_ranks = ranks_from_scores(sort_RMSE)
            # choose XY best models:
            best_models = dict((key, value) for key, value in sort_ranks.items() if value <= nr_models)
            tmp = 0
            for key in best_models.keys():
                d_best_mm_ts[key] = d_ts_areaavg[key]

            ###
            # determine XY random models
            ###
            rand_mod = sample(model_names, nr_models)
            tmp = 0
            for key in rand_mod:
                d_rand_mm_ts[key] = d_ts_areaavg[key]

            ###
            # "Evaluate" mmm with excluded model as truth
            ###
            truth = d_temp_mod_ts_areaavg[models_1ens[rint]]
            delta_true = np.nanmean(truth[ind_fut1 : ind_fut2]) - np.nanmean(truth[ind_hist1 : ind_hist2])

            mod = 0
            for key, value in d_mm_ens_ts.iteritems():
                delta_mm[mod] =  np.nanmean(value[ind_fut1 : ind_fut2]) - np.nanmean(value[ind_hist1 : ind_hist2])
                weights_mod[mod] = approx_wmm_ts['weights'][key]
                mod = mod + 1

            mod1 = 0
            for key, value in d_best_mm_ts.iteritems():
                delta_best[mod1] =  np.nanmean(value[ind_fut1 : ind_fut2]) - np.nanmean(value[ind_hist1 : ind_hist2])
                mod1 = mod1 + 1
            mod2 = 0
            for key, value in d_rand_mm_ts.iteritems():
                delta_rand[mod2] =  np.nanmean(value[ind_fut1 : ind_fut2]) - np.nanmean(value[ind_hist1 : ind_hist2])
                mod2 = mod2 + 1
            del mod, mod1, mod2
            mod3 = 0
            for key, value in d_lin_pred_aa.iteritems():
                delta_lin[mod3] =  np.nanmean(value[ind_fut1 : ind_fut2]) - np.nanmean(value[ind_hist1 : ind_hist2])
                mod3 = mod3 + 1
            del mod3
            # evaluate using ranked probability skill score
            min_hist = np.nanmin([np.nanmin(delta_rand), np.nanmin(delta_best),
                                  np.nanmin(delta_mm), delta_true])
            max_hist = np.nanmax([np.nanmax(delta_rand), np.nanmax(delta_best),
                                  np.nanmax(delta_mm), delta_true])
            binwidth = 0.1
            bins = np.arange(min_hist, max_hist + binwidth, binwidth)
            delta_true_pdf = np.array([np.histogram(delta_true, bins = bins,
                                                    density = True)[0]])
            delta_lin_pdf = np.array([np.histogram(delta_lin, bins = bins,
                                                   density = True)[0]])
            delta_best_pdf = np.array([np.histogram(delta_best, bins = bins,
                                                    density = True)[0]])
            delta_mm_pdf = np.array([np.histogram(delta_mm, bins = bins,
                                                  density = True)[0]])
            delta_rand_pdf = np.array([np.histogram(delta_rand, bins = bins,
                                                    density = True)[0]])

            #baseline_score = rank_prob_score(delta_rand_pdf, delta_true_pdf)
            baseline_score = rank_prob_score(delta_mm_pdf, delta_true_pdf)
            #baseline_score = rank_prob_score(delta_lin_pdf, delta_true_pdf)
            forecast_score_best = rank_prob_score(delta_best_pdf, delta_true_pdf)
            forecast_score_wmm = rank_prob_score(delta_mm_pdf, delta_true_pdf)
            forecast_score_lin = rank_prob_score(delta_lin_pdf, delta_true_pdf)
            forecast_score_mm = rank_prob_score(delta_mm_pdf, delta_true_pdf)
            forecast_score_rand = rank_prob_score(delta_rand_pdf, delta_true_pdf)

            ###
            # save data for each model as truth
            ###
            prob = delta_mm_pdf[0] / np.sum(delta_mm_pdf)
            k = len(prob)
            D0 = 0
            pj_sum = np.cumsum(prob)
            for i in xrange(k - 1):
                D0 = D0 + prob[i] * (1 - prob[i] - 2 * pj_sum[i + 1])

            denominator = 0
            for key in d_mm_ens_ts.keys():
                w = approx_wmm_ts['weights'][key]
                denominator = denominator + w ** 2 / len(models_1ens) 
            Meff = 1 / denominator

            M = len(models_1ens)
            ###
            # save data for each model as truth
            ###
            skill_wmm[rint] = 1 - (forecast_score_wmm[0] / (baseline_score[0] + D0 / Meff))
            skill_best[rint] = 1 - (forecast_score_best[0] / (baseline_score[0] + D0 / M))
            skill_lin[rint] = 1 - (forecast_score_lin[0] / (baseline_score[0] + D0 / M))
            skill_mm[rint] = 1 - (forecast_score_mm[0] / (baseline_score[0] + D0 / M))
            skill_rand[rint] = 1 - (forecast_score_rand[0] / (baseline_score[0] + D0 / M))
            # calculate crps using properscoring
            baseline_score_2 = ps.crps_ensemble(delta_true, delta_mm)
            forecast_score_best2 = ps.crps_ensemble(delta_true, delta_best)
            forecast_score_rand2 = ps.crps_ensemble(delta_true, delta_rand)
            forecast_score_lin2 = ps.crps_ensemble(delta_true, delta_lin)
            forecast_score_wmm2 = ps.crps_ensemble(delta_true, delta_mm,
                                                   weights = weights_mod)
            forecast_score_wrand2 = ps.crps_ensemble(delta_true, delta_mm,
                                                     weights = np.random.permutation(weights_mod))
            forecast_score_mm2 = ps.crps_ensemble(delta_true, delta_mm)

            skill_best2[rint] = (baseline_score_2 - forecast_score_best2) / baseline_score_2
            skill_rand2[rint] = (baseline_score_2 - forecast_score_rand2) / baseline_score_2
            skill_lin2[rint] = (baseline_score_2 - forecast_score_lin2) / baseline_score_2
            skill_wmm2[rint] = (baseline_score_2 - forecast_score_wmm2) / baseline_score_2
            skill_mm2[rint] = (baseline_score_2 - forecast_score_mm2) / baseline_score_2
            skill_wrand2[rint] = (baseline_score_2 - forecast_score_wrand2) / baseline_score_2
            del d_lin_pred_aa, d_rand_mm_ts, d_best_mm_ts
            d_rand_mm_ts = dict()
            d_best_mm_ts = dict()
            d_lin_pred_aa = dict()

        skill_best_med = np.nanmedian(skill_best2)
        skill_wmm_med = np.nanmedian(skill_wmm2)
        skill_mm_med = np.nanmedian(skill_mm2)
        skill_lin_med = np.nanmedian(skill_lin2)
        skill_rand_med = np.median(skill_rand2)
        skill_wrand_med = np.median(skill_wrand2)

        print('Method "random" is on average %s%% better than mm') %(
            np.round(skill_rand_med, 3) * 100)
        print('Method "best" is on average %s%% better than mm') %(
            np.round(skill_best_med, 3) * 100)
        print('Method "lin" is on average %s%% better than mm') %(
            np.round(skill_lin_med, 3) * 100)
        print('Method "random wmm" is on average %s%% better than mm') %(
            np.round(skill_wrand_med, 3) * 100)
        print('Method "wmm" is on average %s%% better than mm') %(
            np.round(skill_wmm_med, 3) * 100)
        print('Method "mm" is on average %s%% better than mm') %(
            np.round(skill_mm_med, 3) * 100)

#        d_skill = {'rpss_lin': str(skill_lin), 'rpss_wmm': str(skill_wmm),
#                   'rpss_best': str(skill_best), 'rpss_mm': str(skill_mm), 
#                   'rpss_rand': str(skill_rand)}

#        with open("%s%s_%s_%s_%s%s_%s_%s_%s_%s-%s_linARIMA.txt" %(
#            outdir, target_var, target_file, target_mask, ndiag,
#            diag_var[0], var_file[0], res_name[0], region, syear_fut[per],
#            eyear_fut[per]), "wb") as skill_out:
#            json.dump(d_skill, skill_out)
        
#        d_skill2 = {'rpss_lin': str(skill_lin2), 'rpss_wmm': str(skill_wmm2),
#                    'rpss_best': str(skill_best2), 'rpss_mm': str(skill_mm2),
#                    'rpss_rand': str(skill_rand2),
#                    'rpss_wrand': str(skill_wrand2)}

        df_skill2 = pd.DataFrame(columns = ['rpss_lin', 'rpss_wmm',
                                            'rpss_best', 'rpss_mm',
                                            'rpss_rand', 'rpss_wrand'])
        df_skill2['rpss_lin'] = skill_lin2
        df_skill2['rpss_wmm'] = skill_wmm2
        df_skill2['rpss_best'] = skill_best2
        df_skill2['rpss_mm'] = skill_mm2
        df_skill2['rpss_rand'] = skill_rand2
        df_skill2['rpss_wrand'] = skill_wrand2

        diag_names = "" # empy string
        for di in xrange(0, ndiag + 1):
            diag_names += diag_var[di] + var_file[di] + "_"

        with open("%s%s_%s_%s_%s_%s%s%s_%s-%s_linARIMA_rpss_ps.txt" %(
            outdir, target_var, target_file, res_name_target, target_mask,
            ndiag, diag_names, region, syear_fut[per],
            eyear_fut[per]), "wb") as skill_out2:
            df_skill2.to_json(skill_out2)

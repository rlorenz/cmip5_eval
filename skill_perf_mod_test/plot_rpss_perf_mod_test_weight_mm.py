#!/usr/bin/python
'''
File Name : plot_rpss_perf_mod_test_weight_mm.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 03-04-2018
Modified: Tue Apr 10 09:12:49 2018
Purpose: plot values of ranked probability skill score
         calculated in perf_mod_test_weight_mm_ARIMA_cdo.py


'''
import numpy as np
import os # operating system interface
#import pickle
import json
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
import matplotlib
from scipy import stats
from sklearn.neighbors import KernelDensity
#from sklearn.grid_search import GridSearchCV
import sys
from os.path import expanduser
home = expanduser("~") # Get users home directory
sys.path.insert(0, home + '/scripts/plot_scripts/utils/')
import operator
from ranks_from_scores import ranks_from_scores
from boxplot_custom import boxplot, setBoxColors
import collections
###
# Define input & output
###
target_var = 'tas'
# climatology: CLIM, variability: STD, trend: TREND
target_file = 'CLIM'
res_name_target = 'JJA'
target_mask = 'maskT'
res_time_target = 'MEAN'

diag_var = ['pr', 'pr', 'tasclt', 'ef', 'rnet', 'pr', 'ef']
var_file = ['CLIM', 'TREND', 'CORR', 'CLIM', 'TREND', 'STD', 'STD']
res_name = ['JJA', 'JJA', 'JJA', 'JJA', 'JJA', 'JJA', 'JJA']
res_time = ['MEAN', 'MEAN', 'MEAN', 'MEAN', 'MEAN', 'MEAN', 'MEAN']
masko = ['maskT', 'maskT', 'maskT', 'maskT', 'maskT', 'maskT','maskT']
freq_v = ['mon', 'mon', 'mon', 'mon', 'mon', 'mon', 'mon'] #

nvar = len(diag_var)

# cut data over region?
region = 'CNEU'
if region != None:
    area = region
else:
    area = 'GLOBAL'

#optsigmas + method (max_rpss, inpercentile) or fixsigmasS.2_D.2
sigma_type = "optsigmas/inpercentile" #optsigmas/inpercentile

split_ratio = 0.33

if split_ratio == 0.33:
    syear_eval = 1950
    eyear_eval = 1999
    syear_fut = [1975, 2000, 2025, 2050]
    eyear_fut = [2024, 2049, 2074, 2099]
elif split_ratio == 0.2:
    syear_eval = 1950
    eyear_eval = 1979
    syear_fut = [1980, 2010, 2040, 2070]
    eyear_fut = [2009, 2039, 2069, 2099]

# define directories
path = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/'
outdir = '%sweight_perf_mod_test/%s/%s/%s/' %(path, target_var, area, sigma_type)
if (os.access(outdir, os.F_OK) == False):
    os.makedirs(outdir)

###
# Read data and Plot data
###
# read all data for weighted mm
rpss_all = dict()
rpss_randwmm_all = dict()
rpss_best_all = dict()
rpss_rand_all = dict()
rpss_lin_all = dict()
for p in xrange(len(eyear_fut)):
    tmp_rpss = list()
    tmp_rpss_randwmm = list()
    tmp_rpss_best = list()
    tmp_rpss_rand = list()
    tmp_rpss_lin = list()
    for d in xrange(1, nvar + 1):
        file_in = "%s%s_%s_%s_%s%s_%s_%s_%s_%s-%s_linARIMA_rpss_ps.txt" %(
            outdir, target_var, target_file, target_mask, d, diag_var[0],
            var_file[0], res_name[0], area, syear_fut[p], eyear_fut[p])
        with open(file_in, 'r') as fp:
            tmp = json.load(fp)
        tmp_rpss.append(tmp['rpss_wmm'].values())
        tmp_rpss_randwmm.append(tmp['rpss_wrand'].values())
        tmp_rpss_best.append(tmp['rpss_best'].values())
        tmp_rpss_rand.append(tmp['rpss_rand'].values())
        tmp_rpss_lin.append(tmp['rpss_lin'].values())
    rpss_all[str(eyear_fut[p])] = tmp_rpss
    rpss_randwmm_all[str(eyear_fut[p])] = tmp_rpss_randwmm
    rpss_best_all[str(eyear_fut[p])] = tmp_rpss_best
    rpss_rand_all[str(eyear_fut[p])] = tmp_rpss_rand
    rpss_lin_all[str(eyear_fut[p])] = tmp_rpss_lin

rpss_all_ord = collections.OrderedDict(sorted(rpss_all.items()))
rpss_randwmm_all_ord = collections.OrderedDict(sorted(rpss_randwmm_all.items()))
rpss_best_all_ord = collections.OrderedDict(sorted(rpss_best_all.items()))
rpss_rand_all_ord = collections.OrderedDict(sorted(rpss_rand_all.items()))
rpss_lin_all_ord = collections.OrderedDict(sorted(rpss_lin_all.items()))

blues = plt.get_cmap('Blues')
greens = plt.get_cmap('Greens')
purples = plt.get_cmap('Purples')
oranges = plt.get_cmap('Oranges')
greys = plt.get_cmap('Greys')
rainbow = plt.get_cmap('viridis_r')
col = matplotlib.colors.Normalize(vmin = 1.0, vmax = 7.0)

# put median rpss over all nr of diags into array
rpss_med_t = np.empty((5, len(eyear_fut)))
rpss_med_t.fill(np.NaN)
p = 0
for value in rpss_all_ord.values():
    rpss_med_t[0, p] = np.median(value)
    p = p + 1
p = 0
for value in rpss_randwmm_all_ord.values():
    rpss_med_t[1, p] = np.median(value)
    p = p + 1
p = 0
for value in rpss_best_all_ord.values():
    rpss_med_t[2, p] = np.median(value)
    p = p + 1
p = 0
for value in rpss_rand_all_ord.values():
    rpss_med_t[3, p] = np.median(value)
    p = p + 1
p = 0
for value in rpss_lin_all_ord.values():
    rpss_med_t[4, p] = np.median(value)
    p = p + 1

# plot median over all nr of diags for all methods
fig = plt.figure(figsize = (7, 7), dpi = 300)
eyear_new = [eyear_fut[0] + 4, eyear_fut[1] + 4, eyear_fut[2] + 4,
             eyear_fut[-1] + 4]
eyear_new1 = [eyear_fut[0] + 8, eyear_fut[1] + 8, eyear_fut[2] + 8,
              eyear_fut[-1] + 8]
eyear_new2 = [eyear_fut[0] + 12, eyear_fut[1] + 12, eyear_fut[2] + 12,
              eyear_fut[-1] + 12]
eyear_new3 = [eyear_fut[0] + 16, eyear_fut[1] + 16, eyear_fut[2] + 16,
              eyear_fut[-1] + 16]

plt.plot(eyear_fut, rpss_med_t[0, :], color = greens(col(6)),
         linestyle = "", marker = "o", label = 'weighted mm vs mm')
plt.plot(eyear_new, rpss_med_t[1, :], color = oranges(col(6)),
         linestyle = "", marker = "o", label = 'random weighted mm vs mm')
plt.plot(eyear_new1, rpss_med_t[2, :], color = blues(col(6)),
         linestyle = "", marker = "o", label = 'best vs mm')
plt.plot(eyear_new2, rpss_med_t[3, :], color = purples(col(6)),
         linestyle = "", marker = "o", label = 'random vs mm')
plt.plot(eyear_new3, rpss_med_t[4, :], color = greys(col(6)),
         linestyle = "", marker = "o", label = 'linear vs mm')

# adding horizontal grid lines
ax = plt.gca()
ax.yaxis.grid(True)
ax.axhline(y = 0.0, color = 'k', linestyle = '-')
ax.set_title('Median Scores for different time periods using up to %s diagnostics'%(str(nvar))) 
plt.ylim([-0.75, 0.5])
plt.ylabel('CRPSS')
#plt.xlabel('')
plt.xticks(eyear_fut, eyear_fut)
plt.grid(False)
leg = plt.legend(loc = 'lower right')  ## leg defines legend -> can be modified
leg.draw_frame(False)

plt.savefig('%srpss_medians_all_meth_%s_%s_%s_%s%s_%s_%s_%s.pdf' %(
    outdir, target_var, target_file, target_mask, str(d),
    diag_var[0], var_file[0], res_name[0], str(split_ratio)))

# plot values for all nr of diags as dots
fig = plt.figure(figsize = (6, 5), dpi = 300)
for d in xrange(1, len(diag_var) + 1, 1):
    rpss_wmm = list()
    for p in xrange(len(eyear_fut)):
        file_in = "%s%s_%s_%s_%s%s_%s_%s_%s_%s-%s_linARIMA_rpss_ps.txt" %(
            outdir, target_var, target_file, target_mask, d, diag_var[0],
            var_file[0], res_name[0], area, syear_fut[p], eyear_fut[p])
        with open(file_in, 'r') as fp:
            tmp = json.load(fp)
        rpss_wmm.append(np.median(tmp['rpss_wmm'].values()))
    if d == nvar:
        plt.plot(eyear_fut, rpss_wmm, color = greens(col(d)),
                 label = 'weighted mm vs mm', linestyle = "", marker = "o")
    else:
        plt.plot(eyear_fut, rpss_wmm, color = greens(col(d)), linestyle = "",
                 marker = "o")
plt.axhline(y = 0.0, color = 'k', linestyle = '--', linewidth = 2)
plt.title('Continuous Ranked Probability Skill scores for up to %s diagnostics' %(nvar))
plt.xlabel('end year period')
plt.ylabel('CRPSS')
plt.ylim([-0.55, 0.25])
plt.xticks(eyear_fut, eyear_fut)
plt.grid(False)
leg = plt.legend(loc = 'lower right')  ## leg defines legend -> can be modified
leg.draw_frame(False)

plt.savefig('%srpss_wmm_dots_%s_%s_%s_%s%s_%s_%s_%speriods_split%s.pdf' %(
    outdir, target_var, target_file, target_mask, len(diag_var),
    diag_var[0], var_file[0], res_name[0], len(syear_fut), str(split_ratio)))

fig = plt.figure(figsize = (6, 5), dpi = 300)
for d in xrange(1, nvar + 1, 1):
    rpss_best = list()
    rpss_wmm = list()
    rpss_wrand = list() 
    rpss_rand = list()
    rpss_lin = list() 
    for p in xrange(len(eyear_fut)):
        file_in = "%s%s_%s_%s_%s%s_%s_%s_%s_%s-%s_linARIMA_rpss_ps.txt" %(
            outdir, target_var, target_file, target_mask, d, diag_var[0],
            var_file[0], res_name[0], area, syear_fut[p], eyear_fut[p])
        with open(file_in, 'r') as fp:
            tmp = json.load(fp)
        rpss_wmm.append(np.median(tmp['rpss_wmm'].values()))
        rpss_wrand.append(np.median(tmp['rpss_wrand'].values()))
        rpss_best.append(np.median(tmp['rpss_best'].values())) 
	rpss_rand.append(np.median(tmp['rpss_rand'].values()))
	rpss_lin.append(np.median(tmp['rpss_lin'].values()))
    if d == 6: 
        plt.plot(eyear_fut, rpss_wmm, color = greens(col(d)),
		 label = 'weighted mm vs mm', linestyle = "", marker = "o")
        plt.plot(eyear_new, rpss_wrand, color = oranges(col(d)),
		 label = 'random weighted mm vs mm', linestyle = "",
                 marker = "o")
	plt.plot(eyear_new1, rpss_best, color = blues(col(d)),
                 label = 'best mm vs mm', linestyle = "", marker = "o")
        plt.plot(eyear_new2, rpss_rand, color = purples(col(d)),
                 label = 'rand mm vs mm', linestyle = "", marker = "o")
        plt.plot(eyear_new3, rpss_lin, color = greys(col(d)),
                 label = 'linear mm vs mm', linestyle = "", marker = "o")
    else:
        plt.plot(eyear_fut, rpss_wmm, color = greens(col(d)),
		 linestyle = "", marker = "o")
        plt.plot(eyear_new, rpss_wrand, color = oranges(col(d)),
		 linestyle = "", marker = "o")
	plt.plot(eyear_new1, rpss_best, color = blues(col(d)),
	 	 linestyle = "", marker = "o")
	plt.plot(eyear_new2, rpss_rand, color = purples(col(d)),
                 linestyle = "", marker = "o")
        plt.plot(eyear_new3, rpss_lin, color = greys(col(d)),
                 linestyle = "", marker = "o")

plt.axhline(y = 0.0, color = 'k', linestyle = '--', linewidth = 2)
plt.title('Continuous Ranked Probability Skill scores for up to %s diagnostics' %(nvar))
plt.xlabel('end year period')
plt.ylabel('CRPSS')
plt.ylim([-0.55, 0.25])
plt.xticks(eyear_fut, eyear_fut)
plt.grid(False)
leg = plt.legend(loc = 'lower right')  ## leg defines legend -> can be modified
leg.draw_frame(False)

plt.savefig('%srpss_dots_%s_%s_%s_%s%s_%s_%s_%speriods_split%s.pdf' %(
    outdir, target_var, target_file, target_mask, len(diag_var),
    diag_var[0], var_file[0], res_name[0], len(syear_fut), str(split_ratio)))

# plotshistogams and pdfs for wmm at end of century 
fig, ax = plt.subplots()
for d in xrange(1, len(diag_var) + 1, 1):
    file_in = "%s%s_%s_%s_%s%s_%s_%s_%s_%s-%s_linARIMA_rpss_ps.txt" %(
	outdir, target_var, target_file, target_mask, str(d), diag_var[0],
	var_file[0], res_name[0], area, str(syear_fut[-1]), str(eyear_fut[-1]))
    with open(file_in, 'r') as fp:
        tmp = json.load(fp)
    mu = np.median(tmp['rpss_wmm'].values())
    sigma = np.std(tmp['rpss_wmm'].values())
    num_bins = 50
    # the histogram of the data
    n, bins, patches = ax.hist(tmp['rpss_wmm'].values(), num_bins, normed = 1,
                               color = 'gainsboro')
    y = mlab.normpdf(bins, mu, sigma)
    ax.axvline(mu, linestyle = '-', linewidth = 2, color = rainbow(col(d)))
    ax.plot(bins, y, '-', color = rainbow(col(d)),
            label = '%s diag., std = %s' %(str(d), str(np.round(sigma, 2))))
    if target_var == 'tas':
        plt.ylim(0, 1)
    elif target_var == 'TXx':
        plt.ylim(0, 2)
    plt.xlim(-1.5, 1)
ax.set_xlabel('CRPSS')
ax.set_ylabel('frequency')

leg = plt.legend(loc = 'upper left')  ## leg defines legend -> can be modified
leg.draw_frame(False)

plt.savefig('%srpss_wmm_pdfs_%s_%s_%s_%s%s_%s_%s_%s-%s_split%s.pdf' %(
    outdir, target_var, target_file, target_mask, str(d),
    diag_var[0], var_file[0], res_name[0], str(syear_fut[-1]),
    str(eyear_fut[-1]), str(split_ratio)))
# plot distributions kernel density for wmm at end of century
fig, ax = plt.subplots()
x_grid = np.linspace(-1.5, 1, 500)
mu = np.empty((len(diag_var)))
mu.fill(np.NaN)
sigma = np.empty((len(diag_var)))
sigma.fill(np.NaN)
nd = 0
for d in xrange(1, len(diag_var) + 1, 1):
    file_in = "%s%s_%s_%s_%s%s_%s_%s_%s_%s-%s_linARIMA_rpss_ps.txt" %(
	outdir, target_var, target_file, target_mask, str(d), diag_var[0],
	var_file[0], res_name[0], area, str(syear_fut[-1]), str(eyear_fut[-1]))
    with open(file_in, 'r') as fp:
        tmp = json.load(fp)
    mu[nd] = np.median(tmp['rpss_wmm'].values())
    sigma[nd] = np.std(tmp['rpss_wmm'].values())
    # estimate bandwidth using sklearn GridSearch
    #grid = GridSearchCV(KernelDensity(),
    #                    {'bandwidth': np.linspace(0.1, 1.0, 30)}, cv = 20)
    #grid.fit(tmp['rpss_wmm'][:, None])
    #print grid.best_params_
    # use scipy.stats gaussian_kde using bandwidth learned from GridSearch,
    # default bandwidth estimate would be 'scott'
    #kernel = stats.gaussian_kde(tmp['rpss_wmm'],
    #                            bw_method = grid.best_params_['bandwidth'])
    kernel = stats.gaussian_kde(tmp['rpss_wmm'].values())
    pdf = kernel.evaluate(x_grid)
    ax.plot(x_grid, pdf, '-', color = rainbow(col(d)),
            label = '%s diag., std = %s' %(str(d), str(np.round(sigma[nd], 2))))
    ax.axvline(mu[nd], linestyle = '-', linewidth = 2, color = rainbow(col(d)))
    nd = nd + 1
ax.set_xlabel('CRPSS')
ax.set_ylabel('frequency')
  
leg = plt.legend(loc = 'upper left')  ## leg defines legend -> can be modified
leg.draw_frame(False)

plt.savefig('%srpss_wmm_kde_%s_%s_%s_%s%s_%s_%s_%s-%s_split%s.pdf' %(
    outdir, target_var, target_file, target_mask, str(d),
    diag_var[0], var_file[0], res_name[0], str(syear_fut[-1]),
    str(eyear_fut[-1]), str(split_ratio)))

# plot scatter, number of diagnostics versus median rpss
fig, ax = plt.subplots()
plt.plot(mu, range(1, len(diag_var) + 1, 1), 'o',  color = 'darkgreen') 
ax.set_xlabel('CRPSS')
ax.set_ylabel('Number of diagnostics')

plt.savefig('%srpss_wmm_scatter_%s_%s_%s_%s%s_%s_%s_%s-%s_split%s.pdf' %(
    outdir, target_var, target_file, target_mask, str(d),
    diag_var[0], var_file[0], res_name[0], str(syear_fut[-1]),
    str(eyear_fut[-1]), str(split_ratio)))

# plot boxplot of weighted mm 
fig = plt.figure(figsize = (7, 7), dpi = 300)
plt.boxplot([rpss_all[str(eyear_fut[0])], rpss_all[str(eyear_fut[1])],
             rpss_all[str(eyear_fut[2])], rpss_all[str(eyear_fut[3])]],
            showmeans = True,
            whis = [5, 95], vert = True,
            labels = ['%s-%s' %(syear_fut[0], eyear_fut[0]), 
                      '%s-%s' %(syear_fut[1], eyear_fut[1]),
                      '%s-%s' %(syear_fut[2], eyear_fut[2]),
                      '%s-%s' %(syear_fut[3], eyear_fut[3])])
# adding horizontal grid lines
ax = plt.gca()
ax.yaxis.grid(True)
ax.axhline(y = 0.0, color = 'k', linestyle = '-')
ax.set_title('Scores for different time periods using up to %s diagnostics'%(str(nvar))) 
plt.ylim([-2, 1])
plt.ylabel('CRPSS')
#plt.xlabel('')
plt.savefig('%srpss_wmm_boxplot_%s_%s_%s_%s%s_%s_%s_%s-%s_%s.pdf' %(
    outdir, target_var, target_file, target_mask, str(d),
    diag_var[0], var_file[0], res_name[0], str(syear_fut[-1]),
    str(eyear_fut[-1]), str(split_ratio)))

## plot boxplot for change from hist to futureall methods
fig = plt.figure(figsize = (12, 7), dpi = 300)

ax = plt.gca()
ax.axhline(y = 0.0, color = 'k', linestyle = '-')
#plt.hold(True)
#1st period
bp = plt.boxplot([[rpss_all[str(eyear_fut[0])]],
                  [rpss_randwmm_all[str(eyear_fut[0])]],
                  [rpss_best_all[str(eyear_fut[0])]],
                  [rpss_rand_all[str(eyear_fut[0])]],
                  [rpss_lin_all[str(eyear_fut[0])]]],
                 showmeans = True,
                 whis = [5, 95], vert = True,
                 positions = [1, 2, 3, 4, 5], widths = 0.6)
setBoxColors(bp, 'GrOrBlPuGy')
#2nd period
bp = plt.boxplot([[rpss_all[str(eyear_fut[1])]],
                  [rpss_randwmm_all[str(eyear_fut[1])]],
                  [rpss_best_all[str(eyear_fut[1])]],
                  [rpss_rand_all[str(eyear_fut[1])]],
                  [rpss_lin_all[str(eyear_fut[1])]]],
                 showmeans = True, whis = [5, 95], vert = True,
                 positions = [7, 8, 9, 10, 11], widths = 0.6)
setBoxColors(bp, 'GrOrBlPuGy')
#3rd period
bp = plt.boxplot([[rpss_all[str(eyear_fut[2])]],
                  [rpss_randwmm_all[str(eyear_fut[2])]],
                  [rpss_best_all[str(eyear_fut[2])]],
                  [rpss_rand_all[str(eyear_fut[2])]],
                  [rpss_lin_all[str(eyear_fut[2])]]], showmeans = True,
                 whis = [5, 95], vert = True, positions = [13, 14, 15, 16, 17],
                 widths = 0.6)
setBoxColors(bp, 'GrOrBlPuGy')
#4rd period
bp = plt.boxplot([[rpss_all[str(eyear_fut[3])]],
                  [rpss_randwmm_all[str(eyear_fut[3])]],
                  [rpss_best_all[str(eyear_fut[3])]],
                  [rpss_rand_all[str(eyear_fut[3])]],
                  [rpss_lin_all[str(eyear_fut[3])]]], showmeans = True,
                 whis = [5, 95], vert = True, positions = [19, 20, 21, 22, 23],
                 widths = 0.6)
setBoxColors(bp, 'GrOrBlPuGy')
# set axes limits and labels
plt.xlim(0, 24)
ax.set_xticklabels(['%s-%s' %(str(syear_fut[0]), str(eyear_fut[0])),
                    '%s-%s' %(str(syear_fut[1]), str(eyear_fut[1])),
                    '%s-%s' %(str(syear_fut[2]), str(eyear_fut[2])),
                    '%s-%s' %(str(syear_fut[3]), str(eyear_fut[3]))])
ax.set_xticks([3, 9, 15, 21])

ax.set_title('Continuous Ranked Probability Skill Scores for different Methods and up to %s diagnostics' %nvar) 
plt.ylabel('CRPSS')
plt.xlabel('Time period')

hGr, = plt.plot([1, 1], color = 'green', linestyle = '-')
hOr, = plt.plot([1, 1], color = 'darkorange', linestyle = '-')
hBl, = plt.plot([1, 1], color = 'royalblue', linestyle = '-')
hPu, = plt.plot([1, 1], color = 'mediumorchid', linestyle = '-')
hGy, = plt.plot([1, 1], color = 'grey', linestyle = '-')
plt.legend((hGr, hOr, hBl, hPu, hGy),
           ('weighted', 'random weighted', 'best', 'random', 'linear'),
           loc = 'lower left', fontsize = 12)
hGr.set_visible(False)
hOr.set_visible(False)
hBl.set_visible(False)
hPu.set_visible(False)
hGy.set_visible(False)

plt.savefig('%srpss_boxplot_%s_%s_%s_%s%s_%s_%s_%speriods_%s.pdf' %(
    outdir, target_var, target_file, target_mask, str(d),
    diag_var[0], var_file[0], res_name[0], len(syear_fut), str(split_ratio)))

# determine how many diagnostics do best, high mu, low sigma
def pts_to_dict(sorted_ranks):
    rank_pts = dict()
    if (isinstance(sorted_ranks, list)):
        points = [7, 6, 5, 4, 3, 2, 1]
        for rank in xrange(len(sorted_ranks)):
            key = sorted_ranks[rank][0]
            if (rank < len(points)):
                rank_pts[key] = points[rank]
            else:
                rank_pts[key] = 0
    if (isinstance(sorted_ranks, dict)):
        for key, value in sorted_ranks.items():
            if (value == 1):
                rank_pts[key] = 7.
            elif (value == 2):
                rank_pts[key] = 6.
            elif (value == 3):
                rank_pts[key] = 5.
            elif (value == 4):
                rank_pts[key] = 4.
            elif (value == 5):
                rank_pts[key] = 3.
            elif (value == 6):
                rank_pts[key] = 2.
            elif (value == 7):
                rank_pts[key] = 1.
            else:
                rank_pts[key] = 0.
    return rank_pts

# print info about mu ranks
mu_diag = dict(zip(range(1, len(diag_var) + 1), mu))
mu_sort = sorted(mu_diag.items(), key = operator.itemgetter(1), reverse = True)
print 'Median RPSS per diag nr: %s' %mu_sort
sort_ranks_mu = ranks_from_scores(mu_sort)
pts_mu = pts_to_dict(sort_ranks_mu)

sigma_diag = dict(zip(range(1, len(diag_var) + 1), sigma))
sigma_sort = sorted(sigma_diag.items(), key = operator.itemgetter(1))
sort_ranks_std = ranks_from_scores(sigma_sort)
pts_std = pts_to_dict(sort_ranks_std)

pts_sum = dict()
for key in pts_mu:
    pts_sum[key] = pts_mu[key] + pts_std[key]
pts_sum_sort = sorted(pts_sum.items(), key = operator.itemgetter(1), reverse = True)
print 'Sorted diags after points: %s' %pts_sum_sort

#!/usr/bin/python
'''
File Name : plot_opt_sigmas_rpss_perfModTest.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 22-05-2018
Modified: Tue 22 May 2018 09:51:30 AM CEST
Purpose: Visualize optimal sigmas calibrated for highest RPSS
         in perfect model test
'''
import numpy as np
import os # operating system interface
import json
import collections
import matplotlib.pyplot as plt
import matplotlib
from matplotlib.legend import Legend

# Define input & output
###
target_var = 'tas'
# climatology: CLIM, variability: STD, trend: TREND
target_file = 'CLIM'
res_name_target = 'JJA'
target_mask = 'maskT'
res_time_target = 'MEAN'

diag_var = ['tasclt', 'pr', 'tas', 'pr', 'rnet', 'tas', 'hfls']
var_file = ['CORR', 'CLIM', 'STD', 'STD', 'TREND', 'CLIM', 'CLIM']
res_name = ['JJA', 'JJA', 'JJA', 'JJA', 'JJA', 'JJA', 'JJA']
res_time = ['MEAN', 'MEAN', 'MEAN', 'MEAN', 'MEAN', 'MEAN', 'MEAN']
masko =    ['maskT', 'maskT', 'maskT', 'maskT', 'maskT', 'maskT', 'maskT']
freq_v =   ['mon', 'mon', 'mon', 'mon', 'mon', 'mon', 'mon']

nvar = len(diag_var)

# cut data over region?
region = 'CNEU'
if region != None:
    area = region
else:
    area = 'GLOBAL'

split_ratio = 0.33

if split_ratio == 0.33:
    syear_eval = 1950
    eyear_eval = 1999
    syear_fut = [1975, 2000, 2025, 2050]
    eyear_fut = [2024, 2049, 2074, 2099]
elif split_ratio == 0.2:
    syear_eval = 1950
    eyear_eval = 1979
    syear_fut = [1980, 2010, 2040, 2070]
    eyear_fut = [2009, 2039, 2069, 2099]

# define directories
path = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/'
outdir = '%sweight_perf_mod_test/%s/%s/sigmas/' %(path, target_var, area)
if (os.access(outdir, os.F_OK) == False):
    os.makedirs(outdir)

###
# Read data and Plot data
###
blues = plt.get_cmap('Blues')
greens = plt.get_cmap('Greens')
purples = plt.get_cmap('Purples')
oranges = plt.get_cmap('Oranges')
greys = plt.get_cmap('Greys')
rainbow = plt.get_cmap('viridis')
col = matplotlib.colors.Normalize(vmin = 1.0, vmax = 7.0)

# plot time periods on x-axis
eyear_fut_tick1 = [x - 1 for x in eyear_fut]
eyear_fut_tick2 = [x + 1 for x in eyear_fut]
sigma_S2 = list()
sigma_D2 = list()
fig = plt.figure(figsize = (6, 5), dpi = 300)
for ndiag in xrange(1, nvar + 1):
    print 'Number of diagnostics: %s' %ndiag
    ### read optimal sigma values
    with open("%ssigma_S2_%s_%s_%s_%s%s_%s_%s_%s_%s-%s_split%s.txt" %(
            outdir, target_var, target_file, target_mask, ndiag,
            diag_var[0], var_file[0], res_name[0], area, syear_eval,
            eyear_eval, str(split_ratio))) as file_S2:
        sigma_S2.append(json.load(file_S2))
    with open("%ssigma_D2_%s_%s_%s_%s%s_%s_%s_%s_%s-%s_split%s.txt" %(
            outdir, target_var, target_file, target_mask, ndiag,
            diag_var[0], var_file[0], res_name[0], area, syear_eval,
            eyear_eval, str(split_ratio))) as file_D2:
        sigma_D2.append(json.load(file_D2))
    sigma_S2_ord = collections.OrderedDict(sorted(sigma_S2[ndiag - 1].items()))
    sigma_D2_ord = collections.OrderedDict(sorted(sigma_D2[ndiag - 1].items()))
    if ndiag == nvar:
        plt.plot(eyear_fut_tick1, sigma_S2_ord.values(),
                 color = oranges(col(ndiag)), linestyle = "", marker = "o",
                 label = '$\sigma_S$ (independence)')
        plt.plot(eyear_fut_tick2, sigma_D2_ord.values(),
                 color = purples(col(ndiag)), linestyle = "", marker = "o",
                 label = '$\sigma_D$ (performance)')
    else:
        plt.plot(eyear_fut_tick1, sigma_S2_ord.values(),
                 color = oranges(col(ndiag)),
                 linestyle = "", marker = "o")
        plt.plot(eyear_fut_tick2, sigma_D2_ord.values(),
                 color = purples(col(ndiag)),
                 linestyle = "", marker = "o")

plt.title('Sigmas for up to %s diagnostics' %(nvar))
plt.xlabel('end year period')
plt.ylabel('$\sigma$')
plt.xticks(eyear_fut, eyear_fut)
plt.grid(False)
leg = plt.legend(loc = 'lower right')  ## leg defines legend -> can be modified
leg.draw_frame(False)

plt.savefig('%ssigmas_dots_%s_%s_%s_%s%s_%s_%s_%speriods_split%s.pdf' %(
    outdir, target_var, target_file, target_mask, len(diag_var),
    diag_var[0], var_file[0], res_name[0], len(syear_fut), str(split_ratio)))

# reorder sigma values for plotting (sigma_?2 nested list (diags), dict per period)
ar_sigma_S2 = np.empty((len(syear_fut), nvar))
ar_sigma_S2.fill(np.NaN)
ar_sigma_D2 = np.empty((len(syear_fut), nvar))
ar_sigma_D2.fill(np.NaN)
for ndiag in xrange(nvar):
    sigma_S2_ord = collections.OrderedDict(sorted(sigma_S2[ndiag].items()))
    sigma_D2_ord = collections.OrderedDict(sorted(sigma_D2[ndiag].items()))
    for p in xrange(len(syear_fut)):
        ar_sigma_S2[p, ndiag] = sigma_S2_ord[str(syear_fut[p])]
        ar_sigma_D2[p, ndiag] = sigma_D2_ord[str(syear_fut[p])]

# calculate median over all sigma values:
med_sigma_S2 = np.median(ar_sigma_S2)
med_sigma_D2 = np.median(ar_sigma_D2)
print('Median S2: %s' %(med_sigma_S2))
print('Median D2: %s' %(med_sigma_D2))

# plot nr diagnostics on x-axis
colorbar = ['orange', 'seagreen', 'purple', 'royalblue']
markers = [15, 12, 9, 6]
fig = plt.figure(figsize = (6, 5), dpi = 300)
ax = fig.add_subplot(1,1,1)
for p in xrange(len(syear_fut)):
    dot, = plt.plot(np.arange(0.8, len(diag_var), 1), ar_sigma_S2[p, :], 'o', 
                    markersize = markers[p], color = colorbar[p],
                    label = '%s' %(syear_fut[p]))
    triangle, = plt.plot(np.arange(1.2, len(diag_var) + 1, 1),
                         ar_sigma_D2[p, :], '<', markersize = markers[p],
                         color = colorbar[p])

plt.ylim(0, 2.0)

plt.title('Sigmas for up to %s diagnostics' %(nvar))
plt.xlabel('Number of diagnostics')
plt.ylabel('$\sigma$')
plt.xticks(range(1, nvar + 1, 1), range(1, nvar + 1, 1))
plt.grid(False)
leg = plt.legend(loc = 'upper right')  ## leg defines legend -> can be modified
leg.draw_frame(False)

leg2 = Legend(ax, [dot, triangle], ['$\sigma_S$ (independence)',
                                    '$\sigma_D$ (performance)'],
              loc = 'upper center', frameon = False)
ax.add_artist(leg2)
 
plt.savefig('%ssigmas_%s_%s_%s_%s%s_%s_%s_%speriods_split%s.pdf' %(
    outdir, target_var, target_file, target_mask, len(diag_var),
    diag_var[0], var_file[0], res_name[0], len(syear_fut), str(split_ratio)))

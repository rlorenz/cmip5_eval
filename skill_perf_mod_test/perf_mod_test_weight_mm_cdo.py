#!/usr/bin/python
'''
File Name : perf_mod_test_weight_mm_cdo.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 15-11-2017
Modified: Wed 15 Nov 2017 02:36:30 PM CET
Purpose: calculate weighted multi-model mean of cmip5 data,
         predict change, evaluate using perfect model test

'''
import numpy as np
import netCDF4 as nc
from netcdftime import utime
import datetime as dt
from random import randint
#from scipy import stats
#import statsmodels.api as sm
#from sklearn import linear_model
#from sklearn.preprocessing import PolynomialFeatures
#from sklearn.pipeline import Pipeline
import glob
import sys
import os # operating system interface
from os.path import expanduser
home = expanduser("~") # Get users home directory
sys.path.insert(0, home + '/scripts/plot_scripts/utils/')
from func_read_data import func_read_netcdf
from func_calc_wu_wq import calc_wu, calc_wq, calc_weights_approx
from calc_RMSE_obs_mod_3D import rmse_3D
from func_eval_wmm_nonwmm_error_indexI import error_indexI
from ranks_from_scores import ranks_from_scores
#import properscoring as ps
from rank_prob_score import rank_prob_score

import operator
import math
import matplotlib.pyplot as plt

import pickle
###
# Define input & output
###
experiment = 'rcp85'
archive = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/'
freq = 'ann'
grid = 'g025'

target_var = 'TXx'
# climatology: CLIM, variability: STD, trend: TREND
target_file = 'CLIM'
res_name_target = 'ANN'
target_mask = 'maskT'
res_time_target = 'MAX'

diag_var = ['TXx', 'TXx']
var_file = ['TREND', 'CLIM']
res_name = ['ANN', 'ANN']
res_time = ['MAX', 'MAX']
masko = ['maskT', 'maskT']
freq_v = 'ann'
# weight of individual fields, all equal weight 1 at the moment
fields_weight = [1, 1] #, 1, 1, 1, 1]

nvar = len(diag_var)

# cut data over region?
region = 'CNEU'
if region != None:
    area = region
else:
    area = 'GLOBAL'

obsdata = 'NoObs' #ERAint, MERRA2, Obs
err = 'RMSE' #'perkins_SS', 'RMSE'
err_var = 'rmse' #'SS', 'rmse'

# include initial conditions ensemble members or not?
ensembles = False

# time ranges
syear_eval = [1950, 1950]
eyear_eval = [1999, 1999]
syear_hist = 1950
eyear_hist = 1999
syear_fut = 2050
eyear_fut = 2099
syear = 1950
eyear = 2100

nyears = eyear_hist - syear_hist + 1
ntim_tot = eyear - syear + 1

# degree for linear fit
degree = 1

## parameters for weighting
## free parameter "radius of similarity"
sigma_S2 = 0.6
## free parameter "radius of model quality"
sigma_D2 = 0.5

## parameter for bestXY models
# how many models do you want to select?
nr_models = 10

# define directories
path = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/'
outdir = '%sweight_perf_mod_test/%s/%s/' %(path, target_var, area)
if (os.access(outdir, os.F_OK) == False):
    os.makedirs(outdir)

###
# Read data
###
## read rmse for var_file
## first read model names for all var_file from text file
## select all models that occur for all var_files,
## or if testing select number of simulations defined in test
rmse_models = dict()
overlap = list()
rmse_file_target = '%s%s/%s/%s/%s_%s_all_%s_%s-%s' %(
    path, target_var, freq, target_mask, target_var, res_name_target,
    experiment, syear_hist, eyear_hist)
overlap = np.genfromtxt(rmse_file_target + '.txt', delimiter = '',
                        dtype = None).tolist()

for v in xrange(len(var_file)):
    rmsefile = '%s%s/%s/%s/%s_%s_%s_all_%s_%s-%s' %(
        path, diag_var[v], freq_v, masko[v], diag_var[v], var_file[v],
        res_name[v], experiment, syear_eval[v], eyear_eval[v])
    if (os.access(rmsefile + '.txt', os.F_OK) == True):
        rmse_models[var_file[v]] = np.genfromtxt(rmsefile + '.txt',
                                                 delimiter = '',
                                                 dtype = None).tolist()
        overlap = list(set(rmse_models[var_file[v]]) & set(overlap))
        try:
            overlap = overlap[0:test]
        except (NameError):
            pass
nfiles = len(overlap)
print '%s matching files' %(str(nfiles))

### read model data
print "Read model data"
d_temp_mod = dict()
d_temp_mod1 = dict()
d_temp_mod1_std = dict()
d_temp_mod_ts = dict()
models_1ens = list()
model_noens = list()
for f in xrange(len(overlap)):
    model = overlap[f].split('_', 1)[0]
    ens = overlap[f].split('_', 1)[1]
    if ensembles:
        models_1ens = overlap
    else:
        if model not in model_noens:
            model_noens.append(model)
            models_1ens.append(model + '_' + ens)
        else:
            print '%s already used, not using multiple ensemble members' %model
            continue
    path_target = '%s%s/%s/%s/' %(path, target_var, freq, target_mask)
    if region:
        modfile_ts = '%s%s_%s_%s_%s_%s_%s-%s_%s%s_%s.nc' %(
            path_target, target_var, freq, model, experiment, ens, syear, eyear,
            res_name_target, res_time_target, region)
        modfile1 = '%s%s_%s_%s_%s_%s_%s-%s_%s%s_%s_%s.nc' %(
            path_target, target_var, freq, model, experiment, ens, syear_hist, 
            eyear_hist, res_name_target, res_time_target, target_file, region)
        modfile1_std = '%s%s_%s_%s_%s_%s_%s-%s_%s%s_%s_%s.nc' %(
            path_target, target_var, freq, model, experiment, ens, syear_hist, 
            eyear_hist, res_name_target, res_time_target, 'STD', region)
        modfile2 = '%s%s_%s_%s_%s_%s_%s-%s_%s%s_%s_%s.nc' %(
            path_target, target_var, freq, model, experiment, ens, syear_fut,
            eyear_fut, res_name_target, res_time_target, target_file, region)
    else:
        modfile_ts = '%s%s_%s_%s_%s_%s_%s-%s_%s%s.nc' %(
            path_target, target_var, freq, model, experiment, ens, syear, eyear,
            res_name_target, res_time_target)
        modfile1 = '%s%s_%s_%s_%s_%s_%s-%s_%s%s_%s.nc' %(
            path_target, target_var, freq, model, experiment, ens, syear_hist, 
            eyear_hist, res_name_target, res_time_target, target_file)
        modfile1_std = '%s%s_%s_%s_%s_%s_%s-%s_%s%s_%s.nc' %(
            path_target, target_var, freq, model, experiment, ens, syear_hist, 
            eyear_hist, res_name_target, res_time_target, 'STD')
        modfile2 = '%s%s_%s_%s_%s_%s_%s-%s_%s%s_%s.nc' %(
            path_target, target_var, freq, model, experiment, ens, syear_fut,
            eyear_fut, res_name_target, res_time_target, target_file)

    fh = nc.Dataset(modfile_ts, mode = 'r')
    tmp = fh.variables[target_var]
    unit = tmp.units
    temp_mod_ts = fh.variables[target_var][:]
    lat = fh.variables['lat'][:]
    lon = fh.variables['lon'][:]
    time = fh.variables['time']
    cdftime = utime(time.units, calendar = time.calendar)
    dates = cdftime.num2date(time[:])
    years = np.asarray([dates[i].year for i in xrange(len(dates))])
    fh.close()
    if isinstance(temp_mod_ts, np.ma.core.MaskedArray):
        d_temp_mod_ts[model + '_' + ens] = temp_mod_ts.filled(np.nan)
    else:
        d_temp_mod_ts[model + '_' + ens] = temp_mod_ts

    fh = nc.Dataset(modfile1, mode = 'r')
    temp_mod1 = fh.variables[target_var][:]
    fh.close()

    fh = nc.Dataset(modfile1_std, mode = 'r')
    temp_mod1_std = fh.variables[target_var][:]
    fh.close()

    fh = nc.Dataset(modfile2, mode = 'r')
    tmp = fh.variables[target_var]
    unit = tmp.units
    temp_mod2 = fh.variables[target_var][:]
    fh.close()

    if isinstance(temp_mod1, np.ma.core.MaskedArray):
        d_temp_mod1[model + '_' + ens] = temp_mod1.filled(np.nan)
        d_temp_mod1_std[model + '_' + ens] = temp_mod1_std.filled(np.nan)
        d_temp_mod[model + '_' + ens] = temp_mod2.filled(np.nan) - temp_mod1.filled(np.nan)
    else:
        d_temp_mod1[model + '_' + ens] = temp_mod1
        d_temp_mod1_std[model + '_' + ens] = temp_mod1_std
        d_temp_mod[model + '_' + ens] = temp_mod2 - temp_mod1
    del temp_mod1, temp_mod2

###
# Choose one model to be excluded, use as truth in perfect model test
###
I2_all = np.ndarray(len(models_1ens))
approx = np.ndarray((len(models_1ens), len(years), len(lat), len(lon)),
                    dtype = float)
lin_pred_aa = np.ndarray((len(models_1ens), len(years)), dtype = float)
mm_ts = np.ndarray((len(models_1ens), len(years), len(lat), len(lon)),
                        dtype = float)
best_mm_ts = np.ndarray((len(models_1ens), len(years), len(lat), len(lon)),
                        dtype = float)
rmse_frac = np.ndarray(len(models_1ens))
rmse_ts_frac = np.ndarray(len(models_1ens))
rmse_lin_frac_mm = np.ndarray(len(models_1ens))
rmse_lin_frac_wmm = np.ndarray(len(models_1ens))
rmse_best_frac_mm = np.ndarray(len(models_1ens))
for rint in xrange(len(models_1ens)):
    #rint = randint(0, nfiles)
    model_subset = models_1ens[: rint] + models_1ens[rint + 1 :]

    indices = dict()
    rmse_d = np.ndarray((len(var_file), len(model_subset), len(model_subset)))
    rmse_q = np.ndarray((len(var_file), len(model_subset)))
    for v in xrange(len(var_file)):
        rmsepath = '%s%s/%s/%s/' %(path, diag_var[v], freq_v, masko[v])
        rmsefile = '%s%s_%s_%s_all_%s_%s-%s' %(rmsepath, diag_var[v],
                                               var_file[v], res_name[v],
                                               experiment, syear_eval[v],
                                               eyear_eval[v])
        ## find indices of model_subset in rmse_file
        rmse_models[var_file[v]] = np.genfromtxt(rmsefile + '.txt',
                                                 delimiter = '',
                                                 dtype = None).tolist()
        for m in xrange(len(model_subset)):
            indices[model_subset[m]] = rmse_models[var_file[v]].index(
                model_subset[m])
        sorted_ind = sorted(indices.items(), key = operator.itemgetter(1))
        ind = [x[1] for x in sorted_ind]
        model_names = [x[0] for x in sorted_ind]

        ncfile = '%s%s_%s_%s_all_%s_%s-%s_%s_%s_%s.nc' %(
            rmsepath, diag_var[v], var_file[v], res_name[v], experiment,
            syear_eval[v], eyear_eval[v], area, obsdata, err)
        if (os.access(ncfile, os.F_OK) == True):
            print err + ' already exist, read from netcdf'
            fh = nc.Dataset(ncfile, mode = 'r')
            rmse_all = fh.variables[err_var]
            rmse_d[v, :, :] = rmse_all[ind, ind]
            rmse_q[v, :] = rmse_all[rint, ind]
            fh.close()
        else:
            print "RMSE delta matrix does not exist yet, exiting"
            sys.exit
    delta_u = np.ndarray((len(var_file), len(model_names), len(model_names)))
    delta_q = np.ndarray((len(var_file), len(model_names)))
    for v in xrange(len(var_file)):
        ## normalize rmse by median
        med = np.nanmedian(rmse_d[v, :, :])
        delta_u[v, :, :] = rmse_d[v, :, :] / med
        delta_q[v, :] = rmse_q[v, :] / med
    ## average deltas over fields,
    ## taking field weight into account (all 1 at the moment)
    field_w_extend_u = np.reshape(np.repeat(
        fields_weight, len(model_names) * len(model_names)),
                                  (len(fields_weight), len(model_names),
                                   len(model_names)))
    delta_u = np.sqrt(np.nansum(field_w_extend_u * delta_u, axis = 0)
                      / np.nansum(fields_weight))

    field_w_extend_q = np.reshape(np.repeat(fields_weight, len(model_names)),
                                  (len(fields_weight), len(model_names)))
    delta_q = np.sqrt(np.nansum(field_w_extend_q * delta_q, axis = 0)
                      / np.nansum(fields_weight))

    # subset model data, exclude one model
    d_temp_mod_sub = dict(d_temp_mod)
    del d_temp_mod_sub[models_1ens[rint]]

    d_temp_mod1_sub = dict(d_temp_mod1)
    del d_temp_mod1_sub[models_1ens[rint]]

    d_temp_mod_sub_ts = dict(d_temp_mod_ts)
    del d_temp_mod_sub_ts[models_1ens[rint]]

    ###
    # Calculate weights
    ###
    print "Calculate weights for model uniqueness (u) and quality (q)"
    wu_end = calc_wu(delta_u, model_names, sigma_S2)
    wq_end = calc_wq(delta_q, model_names, sigma_D2)

    ###
    # Calculate weighted multi-model mean
    ###
    print "Calculate weighted and non-weighted model means"
    model_keys = d_temp_mod_sub.keys()
    if set(model_keys) != set(model_names):
        print 'Not the same model names in lists, exit'
        sys.exit
    # target_file       
    approx_wmm = calc_weights_approx(wu_end, wq_end, model_names, 
                                     d_temp_mod_sub, var_file = target_file)
    #model_keys1 = sorted(d_temp_mod1_sub.keys())
    approx_wmm_eval = calc_weights_approx(wu_end, wq_end, model_names,
                                          d_temp_mod1_sub,
                                          var_file = target_file)
    # time series
    approx_wmm_ts = calc_weights_approx(wu_end, wq_end, model_names,
                                        d_temp_mod_sub_ts)
    # calculate unweighted mean
    # first average over initial conditions ensembles per model if exist
    if ensembles:
        models = [x.split('_')[0] for x in d_temp_mod_sub.keys()]
        mult_ens = []
        seen = set()
        for m in models:
            if m not in seen:
                seen.add(m)
            else:
                mult_ens.append(m)
        # make sure every model only once in list
        list_with_mult_ens = set(mult_ens)
        d_avg_ens = dict()
        for key, value in d_temp_mod_sub.iteritems():
            if key.split('_')[0] in list_with_mult_ens:
                #find other ensemble members
                ens_mem = [value2 for key2, value2 in sorted(
                    d_temp_mod_sub.iteritems()) if key.split('_')[0] in key2]
                d_avg_ens[key.split('_')[0]] = np.nanmean(ens_mem, axis = 0)
            else:
                d_avg_ens[key.split('_')[0]] = value
        del key, value, key2, value2
    else:
        d_avg_ens = d_temp_mod_sub
        models = model_noens
    tmp2 = 0
    if (target_file == 'STD'):
        for key, value in d_avg_ens.iteritems():
            tmp_pow = np.power(value, 2)
            tmp2 = tmp2 + tmp_pow
        mm = np.sqrt(tmp2 / len(set(models)))
    else: 
        for key, value in d_avg_ens.iteritems():
            tmp2 = tmp2 + value
        mm = tmp2 / len(set(models))
    del tmp2
    del key, value
    # calculate unweighted mean over evaluation period
    d_avg_ens_eval = dict()
    if ensembles:
        for key, value in d_temp_mod1_sub.iteritems():
            if key.split('_')[0] in list_with_mult_ens:
                #find other ensemble members
                ens_mem = [value2 for key2, value2 in sorted(
                    d_temp_mod1_sub.iteritems()) if key.split('_')[0] in key2]
                d_avg_ens_eval[key.split('_')[0]] = np.nanmean(ens_mem,
                                                               axis = 0)
            else:
                d_avg_ens_eval[key.split('_')[0]] = value
        del key, value, key2, value2
    else:
        d_avg_ens_eval = d_temp_mod1_sub

    tmp2 = 0
    if (target_file == 'STD'):
        for key, value in d_avg_ens_eval.iteritems():
            tmp_pow = np.power(value, 2)
            tmp2 = tmp2 + tmp_pow
        mm_eval = np.sqrt(tmp2 / len(set(models)))
    else: 
        for key, value in d_avg_ens_eval.iteritems():
            tmp2 = tmp2 + value
        mm_eval = tmp2 / len(set(models))
    del tmp2
    del key, value
    # calculate unweighted mean over time series
    d_avg_ens_ts = dict()
    if ensembles:
        for key, value in d_temp_mod_sub_ts.iteritems():
            if key.split('_')[0] in list_with_mult_ens:
                #find other ensemble members
                ens_mem = [value2 for key2, value2 in sorted(
                    d_temp_mod_sub_ts.iteritems()) if key.split('_')[0] in key2]
                d_avg_ens_ts[key.split('_')[0]] = np.nanmean(ens_mem, axis = 0)
            else:
                d_avg_ens_ts[key.split('_')[0]] = value
        del key, value, key2, value2
    else:
        d_avg_ens_ts = d_temp_mod_sub_ts
    tmp2 = 0
    for key, value in d_avg_ens_ts.iteritems():
        tmp2 = tmp2 + value
    mm_ts[rint, :, :, :] = tmp2 / len(set(models))
    del tmp2
    del key, value

    ###
    # Calculate linear regression of excluded model from historical period
    ###
    ind_eval1 = np.where(years == syear_eval[0])[0][0]
    ind_eval2 = np.where(years == eyear_eval[0])[0][0] + 1
    ts = d_temp_mod_ts[models_1ens[rint]]
    
    ts_hist = ts[ind_eval1 : ind_eval2, :, :]
    years_hist = years[ind_eval1 : ind_eval2]

#    years_hist2 = sm.add_constant(years_hist)
    extra_x_pred = np.ndarray((len(years), len(lat), len(lon)), dtype = float)

    for y in xrange(len(lat)):
        for x in xrange(len(lon)):
            if np.isnan(ts_hist[:, y, x]).any():
                extra_x_pred[:, y, x] = np.NaN
            else:
                z_pred = np.polyfit(years_hist, ts_hist[:, y, x], degree)
                f_pred = np.poly1d(z_pred)
                extra_x_pred[:, y, x] = f_pred(years)

    # linear regressions for area averages    
    ma_ts = np.ma.masked_array(ts, np.isnan(ts))
    w_lat = np.cos(lat * (4.0 * math.atan(1.0) / 180))
    tmp_latweight = np.ma.average(np.squeeze(ma_ts), axis = 1,
                                  weights = w_lat)
    ts_areaavg = np.nanmean(tmp_latweight.filled(np.nan), axis = 1)
    ts_areaavg_hist = ts_areaavg[ind_eval1 : ind_eval2]

    #result_aa = sm.OLS(ts_areaavg_hist, years_hist2).fit()
    #y_pred_aa = result_aa.predict(years_hist2)
    z_pred_aa = np.polyfit(years_hist, ts_areaavg_hist, degree)
    f_pred_aa = np.poly1d(z_pred_aa)
    lin_pred_aa[rint, :] = f_pred_aa(years)

    ###
    # determine XY "best" models
    ###
    RMSE_diag = dict(zip(model_names, delta_q))
    sort_RMSE = sorted(RMSE_diag.items(), key = operator.itemgetter(1))
    ranks = {}
    sort_ranks = ranks_from_scores(sort_RMSE)
    # choose XY best models:
    best_models = dict((key, value) for key, value in sort_ranks.items() if value <= nr_models)
    tmp = 0
    for key in best_models.keys():
        tmp = tmp + d_temp_mod_sub_ts[key]
    best_mm_ts[rint, :, :, :] = tmp / nr_models

    ###
    # "Evaluate" mmm with excluded model as truth
    ###
    ind_fut1 = np.where(years == syear_fut)[0][0]
    ind_fut2 = np.where(years == eyear_fut)[0][0] + 1
    rmse_mm = rmse_3D(mm_eval, d_temp_mod1[models_1ens[rint]], lat, lon)
    rmse_mm_ts = rmse_3D(mm_ts[rint, ind_fut1 : ind_fut2, :, :],
                         d_temp_mod_ts[models_1ens[rint]][ind_fut1 : ind_fut2,
                                                          :, :], lat, lon)

    rmse_wmm = rmse_3D(approx_wmm_eval['approx'],
                       d_temp_mod1[models_1ens[rint]], lat, lon)
    rmse_wmm_ts = rmse_3D(approx_wmm_ts['approx'][ind_fut1 : ind_fut2, :, :],
                          d_temp_mod_ts[models_1ens[rint]][ind_fut1:ind_fut2,
                                                           :, :], lat, lon)
    rmse_frac[rint] = rmse_wmm / rmse_mm
    #print 'Frac RMSE WMM-MM = ' + str(round(rmse_frac, 3))
    rmse_ts_frac[rint] = rmse_wmm_ts / rmse_mm_ts
    #print 'Frac RMSE_TS WMM-MM = ' + str(round(rmse_ts_frac, 3))

    I2 = error_indexI(np.squeeze(approx_wmm_eval['approx']),
                      np.squeeze(mm_eval),
                      np.squeeze(d_temp_mod1[models_1ens[rint]]),
                      np.squeeze(d_temp_mod1_std[models_1ens[rint]]),
                      lat, lon, res_name[0])
    #print 'I2 = ' + str(round(I2, 3))
    I2_all[rint] = I2
    approx[rint, :, :, :] = approx_wmm_ts['approx']

    # evaluate linear regression
    lin_reg_fut = extra_x_pred[ind_fut1 : ind_fut2, :, :]
    ts_fut = ts[ind_fut1 : ind_fut2, :, :]
    rmse_lin_ts = rmse_3D(ts_fut, lin_reg_fut, lat, lon)
    rmse_lin_frac_mm[rint] = rmse_lin_ts / rmse_mm_ts
    rmse_lin_frac_wmm[rint] = rmse_lin_ts / rmse_wmm_ts

    # evaluate best
    best_mm_ts_fut = best_mm_ts[rint, ind_fut1 : ind_fut2, :, :]
    rmse_best_ts = rmse_3D(ts_fut, best_mm_ts_fut, lat, lon)
    rmse_best_frac_mm[rint] = rmse_best_ts / rmse_mm_ts
    #rmse_best_frac_wmm[rint] = rmse_best_ts / rmse_wmm_ts
    
gt_1 = (I2_all > 1).sum()
lt_1 = (I2_all < 1).sum()
print 'I2 > 1 : ' + str(gt_1) + ', I2 < 1 : ' + str(lt_1)
rmse_gt1 = (rmse_ts_frac > 1).sum()
rmse_lt1 = (rmse_ts_frac < 1).sum()
print 'RMSE_frac > 1 : ' + str(rmse_gt1) + ', RMSE_frac < 1 : ' + str(rmse_lt1)

rmse_lin_mm_gt1 = (rmse_lin_frac_mm > 1).sum()
rmse_lin_mm_lt1 = (rmse_lin_frac_mm < 1).sum()
print 'RMSE_lin_mm_frac > 1 : ' + str(rmse_lin_mm_gt1)
print 'RMSE_lin_frac < 1 : ' + str(rmse_lin_mm_lt1)
rmse_lin_wmm_gt1 = (rmse_lin_frac_wmm > 1).sum()
rmse_lin_wmm_lt1 = (rmse_lin_frac_wmm < 1).sum()
print 'RMSE_lin_wmm_frac > 1 : ' + str(rmse_lin_wmm_gt1)
print 'RMSE_lin_wmm_frac < 1 : ' + str(rmse_lin_wmm_lt1)

rmse_best_mm_gt1 = (rmse_best_frac_mm > 1).sum()
rmse_best_mm_lt1 = (rmse_best_frac_mm < 1).sum()
print 'RMSE_best_mm_frac > 1 : ' + str(rmse_best_mm_gt1)
print 'RMSE_best_frac < 1 : ' + str(rmse_best_mm_lt1)

## save data to textfile
with open('%sRMSE_ltgt1_%s_%s_%s_%s%s_%s_%s_%s-%s.txt' %(
    outdir, target_var, target_file, target_mask, len(diag_var), diag_var[0],
    var_file[0], res_name[0], syear_fut, eyear_fut), "w") as text_file:
        text_file.write('%d' %rmse_lin_mm_lt1 + "\n")
        text_file.write('%d' %rmse_lin_mm_gt1)

## average over area
rad = 4.0 * math.atan(1.0) / 180
w_lat = np.cos(lat * rad) # weight for latitude differences in area

tmp_latweight = np.ma.empty((len(models_1ens), len(years), len(lon)))
ma_mm_ts = np.ma.masked_array(mm_ts, np.isnan(mm_ts))
for ilon in xrange(len(lon)):
    tmp_latweight[:, :, ilon] = np.ma.average(ma_mm_ts[:, :, :, ilon],
                                           axis = 2, weights = w_lat)
mm_ts_areaavg = np.nanmean(tmp_latweight.filled(np.nan), axis = 2)
del tmp_latweight

tmp_latweight2 = np.ma.empty((len(models_1ens), len(years), len(lon)))
ma_approx_ts = np.ma.masked_array(approx, np.isnan(approx))
for ilon in xrange(len(lon)):
    tmp_latweight2[:, :, ilon] = np.ma.average(ma_approx_ts[:, :, :, ilon],
                                               axis = 2, weights = w_lat)
approx_ts_areaavg = np.nanmean(tmp_latweight2.filled(np.nan), axis = 2)

tmp_latweight3 = np.ma.empty((len(models_1ens), len(years), len(lon)))
ma_best_ts = np.ma.masked_array(best_mm_ts, np.isnan(best_mm_ts))
for ilon in xrange(len(lon)):
    tmp_latweight3[:, :, ilon] = np.ma.average(ma_best_ts[:, :, :, ilon],
                                               axis = 2, weights = w_lat)
best_mm_ts_areaavg = np.nanmean(tmp_latweight3.filled(np.nan), axis = 2)

d_temp_mod_ts_areaavg = dict()
tmp_latweight = np.ma.empty((len(years), len(lon)))
for key, value in d_temp_mod_ts.iteritems():
    for ilon in xrange(len(lon)):
        tmp_latweight[:, ilon] = np.ma.average(value[:, :, ilon],
                                               axis = 1, weights = w_lat)
    d_temp_mod_ts_areaavg[key] = np.nanmean(tmp_latweight.filled(
        np.nan), axis = 1)

ind_hist1 = np.where(years == syear_hist)[0][0]
ind_hist2 = np.where(years == eyear_hist)[0][0] + 1
diff_wu_end = np.ndarray((len(models_1ens)))
pred_true = np.ndarray((len(models_1ens)))
lin_true = np.ndarray((len(models_1ens)))
mm_true = np.ndarray((len(models_1ens)))
best_true = np.ndarray((len(models_1ens)))

delta_true = np.ndarray((len(models_1ens)))
delta_lin = np.ndarray((len(models_1ens)))
delta_wmm = np.ndarray((len(models_1ens)))
delta_mm = np.ndarray((len(models_1ens)))
delta_best = np.ndarray((len(models_1ens)))

delta_pred_true = np.ndarray((len(models_1ens)))
delta_mm_true = np.ndarray((len(models_1ens)))
delta_lin_true = np.ndarray((len(models_1ens)))
delta_best_true = np.ndarray((len(models_1ens)))
for mod in xrange(len(models_1ens)):
    diff_wu_end[mod] = (np.nanmean(approx_ts_areaavg[mod,
                                                     ind_fut1 : ind_fut2]) - 
                        np.nanmean(mm_ts_areaavg[mod, ind_fut1 : ind_fut2]))
    pred_true[mod] = (np.nanmean(approx_ts_areaavg[mod, ind_fut1 : ind_fut2]) - 
                      np.nanmean(d_temp_mod_ts_areaavg[models_1ens[mod]][ind_fut1 : ind_fut2]))
    lin_true[mod] = (np.nanmean(lin_pred_aa[mod, ind_fut1 : ind_fut2]) -
                     np.nanmean(d_temp_mod_ts_areaavg[models_1ens[mod]][ind_fut1 : ind_fut2]))
    mm_true[mod] = (np.nanmean(mm_ts_areaavg[mod, ind_fut1 : ind_fut2]) - 
                    np.nanmean(d_temp_mod_ts_areaavg[models_1ens[mod]]
                                 [ind_fut1 : ind_fut2]))
    best_true[mod] = (np.nanmean(best_mm_ts_areaavg[mod, ind_fut1 : ind_fut2]) -
                      np.nanmean(d_temp_mod_ts_areaavg[models_1ens[mod]]
                                 [ind_fut1 : ind_fut2]))
    delta_mm[mod] = (np.nanmean(mm_ts_areaavg[mod, ind_fut1 : ind_fut2]) - 
                np.nanmean(mm_ts_areaavg[mod, ind_hist1 : ind_hist2]))
    delta_wmm[mod] = (np.nanmean(approx_ts_areaavg[mod, ind_fut1 : ind_fut2]) - 
                 np.nanmean(approx_ts_areaavg[mod, ind_hist1 : ind_hist2]))
    delta_true[mod] = np.nanmean(d_temp_mod_ts_areaavg[models_1ens[mod]][ind_fut1 : ind_fut2]) - np.nanmean(d_temp_mod_ts_areaavg[models_1ens[mod]][ind_hist1 : ind_hist2])
    delta_pred_true[mod] = delta_wmm[mod] - delta_true[mod]
    delta_mm_true[mod] = delta_mm[mod] - delta_true[mod]
    
    delta_lin[mod] = (np.nanmean(lin_pred_aa[mod, ind_fut1 : ind_fut2]) - 
                 np.nanmean(lin_pred_aa[mod, ind_hist1 : ind_hist2]))
    delta_lin_true[mod] = delta_lin[mod] - delta_true[mod]

    delta_best[mod] = (np.nanmean(best_mm_ts_areaavg[mod, ind_fut1 : ind_fut2])-
                       np.nanmean(best_mm_ts_areaavg[mod, ind_hist1 : ind_hist2]))
    delta_best_true[mod] = delta_best[mod] - delta_true[mod]

if unit == 'degC':
    degree_sign = u'\N{DEGREE SIGN}'
    unit = degree_sign + "C"

# evaluate using ranked probability skill score
min_hist = np.nanmin([delta_true, delta_lin, delta_best, delta_wmm, delta_mm])
max_hist = np.nanmax([delta_true, delta_lin, delta_best, delta_wmm, delta_mm])
binwidth = 0.2
bins = np.arange(min_hist, max_hist + binwidth, binwidth)
delta_true_pdf = np.array([np.histogram(delta_true, bins = bins,
                                        density = True)[0]])
delta_lin_pdf = np.array([np.histogram(delta_lin, bins = bins,
                                       density = True)[0]])
delta_best_pdf = np.array([np.histogram(delta_best, bins = bins,
                                        density = True)[0]])
delta_wmm_pdf = np.array([np.histogram(delta_wmm, bins = bins,
                                       density = True)[0]])
delta_mm_pdf = np.array([np.histogram(delta_mm, bins = bins,
                                      density = True)[0]])

#baseline_score = ps.crps_ensemble(delta_true, delta_lin)
#forecast_score_best = ps.crps_ensemble(delta_true, delta_best)
#forecast_score_wmm = ps.crps_ensemble(delta_true, delta_wmm)
#forecast_score_mm = ps.crps_ensemble(delta_true, delta_mm)

#skill_best = np.mean((baseline_score - forecast_score_best) / baseline_score)
#skill_wmm = np.mean((baseline_score - forecast_score_wmm) / baseline_score)
#skill_mm = np.mean((baseline_score - forecast_score_mm) / baseline_score)
#print('Method "best" is %s%% better than lin reg') %(np.round(skill_best, 3) * 100)
#print('Method "mm" is %s%% better than lin reg') %(np.round(skill_mm, 3) * 100)
#print('Method "wmm" is %s%% better than lin reg') %(np.round(skill_wmm, 3) * 100)

baseline_score = rank_prob_score(delta_lin_pdf, delta_true_pdf)
forecast_score_best = rank_prob_score(delta_best_pdf, delta_true_pdf)
forecast_score_wmm = rank_prob_score(delta_wmm_pdf, delta_true_pdf)
forecast_score_mm = rank_prob_score(delta_mm_pdf, delta_true_pdf)

skill_best = (baseline_score[0] - forecast_score_best[0]) / baseline_score[0]
skill_wmm = (baseline_score[0] - forecast_score_wmm[0]) / baseline_score[0]
skill_mm = (baseline_score[0] - forecast_score_mm[0]) / baseline_score[0]
print('Method "best" is %s%% better than lin reg') %(np.round(skill_best, 3) * 100)
print('Method "mm" is %s%% better than lin reg') %(np.round(skill_mm, 3) * 100)
print('Method "wmm" is %s%% better than lin reg') %(np.round(skill_wmm, 3) * 100)
d_skill = {'rpss_mm': skill_mm, 'rpss_wmm': skill_wmm, 'rpss_best': skill_best}
skill_out = open("%s%s_%s_%s_%s%s_%s_%s_%s_%s-%s.pkl" %(
    outdir, target_var, target_file, target_mask, len(diag_var),
    diag_var[0], var_file[0], res_name[0], region, syear_fut, eyear_fut), "wb")
pickle.dump(d_skill, skill_out)
skill_out.close()

# save data into netcdf
ncdir = '%s/ncdf/' %outdir
if (os.access('%s' %ncdir, os.F_OK) == False):
    os.makedirs('%s' %ncdir)
    print 'created directory %s' %ncdir

outfile = '%sdata_boxplot_%s_%s_%s_%s%s_%s_%s_%s_%s-%s.nc' %(
    ncdir, target_var, target_file, target_mask, len(diag_var),
    diag_var[0], var_file[0], res_name[0], region, syear_fut, eyear_fut)
fout = nc.Dataset(outfile, mode = 'w')

fout.createDimension('models', len(models_1ens))
modout = fout.createVariable('models', 'f8', ('models'), fill_value = 1e20)

linout = fout.createVariable('delta_lin_true', 'f8', ('models'),
                             fill_value = 1e20)
setattr(linout, 'Longname', 'delta linear minus true')
setattr(linout, 'units', unit)
setattr(linout, 'description', 'difference in change to future for linear regression and true from perfect model test')

mmout = fout.createVariable('delta_mm_true', 'f8', ('models'),
                             fill_value = 1e20)
setattr(mmout, 'Longname', 'delta mm minus true')
setattr(mmout, 'units', unit)
setattr(mmout, 'description', 'difference in change to future for arithmetric mult model mean and true from perfect model test')

wmmout = fout.createVariable('delta_wmm_true', 'f8', ('models'),
                             fill_value = 1e20)
setattr(wmmout, 'Longname', 'delta wmm minus true')
setattr(wmmout, 'units', unit)
setattr(wmmout, 'description', 'difference in change to future for weighted mult model mean and true from perfect model test')

bestout = fout.createVariable('delta_best_true', 'f8', ('models'),
                             fill_value = 1e20)
setattr(bestout, 'Longname', 'delta best minus true')
setattr(bestout, 'units', unit)
setattr(bestout, 'description', 'difference in change to future for best %s models and true from perfect model test' %nr_models)

modout[:] = range(len(models_1ens))[:]
linout[:] = delta_lin_true[:]
mmout[:] = delta_mm_true[:]
wmmout[:] = delta_pred_true[:]
bestout[:] = delta_best_true[:]

# Set global attributes
setattr(fout, "author", "Ruth Lorenz @IAC, ETH Zurich, Switzerland")
setattr(fout, "contact", "ruth.lorenz@env.ethz.ch")
setattr(fout, "creation date", dt.datetime.today().strftime('%Y-%m-%d'))
setattr(fout, "Script", "perf_mod_test_weight_mm_cdo.py")
setattr(fout, "Input files located in:", archive)
fout.close()

# plot boxplot for future 
fig = plt.figure(figsize = (7, 7), dpi = 300)
plt.boxplot([lin_true, mm_true, pred_true, best_true],  showmeans = True,
            whis = [5, 95], vert = True,
            labels = ['lin - true', 'mm - true', 'wmm - true', 'best - true'])
# adding horizontal grid lines
ax = plt.gca()
ax.yaxis.grid(True)
ax.axhline(y = 0.0, color = 'k', linestyle = '-')
ax.set_title('%s-%s average' %(syear_fut, eyear_fut)) 
plt.ylim([-10, 10])
plt.ylabel('%s %s %s [%s]' %(region, target_var, res_name_target, unit))
plt.savefig('%sboxplot_%s_%s_%s_%s%s_%s_%s_%s-%s.pdf' %(
    outdir, target_var, target_file, target_mask, len(diag_var),
    diag_var[0], var_file[0], res_name[0], syear_fut, eyear_fut))

## plot boxplot for change from hist to future
fig = plt.figure(figsize = (7, 7), dpi = 300)
plt.boxplot([delta_lin_true, delta_mm_true, delta_pred_true, delta_best_true],
            showmeans = True,
            whis = [5, 95], vert = True,
            labels = ['$\Delta$lin - $\Delta$true', '$\Delta$mm - $\Delta$true', '$\Delta$wmm - $\Delta$true', '$\Delta$best - $\Delta$true'])
# adding horizontal grid lines
ax = plt.gca()
ax.yaxis.grid(True)
ax.axhline(y = 0.0, color = 'k', linestyle = '-')
ax.set_title('%s-%s $-$ %s-%s' %(syear_fut, eyear_fut, syear_hist, eyear_hist)) 
plt.ylim([-6, 6])
plt.ylabel('%s $\Delta$%s %s [%s]' %(region, target_var, res_name_target, unit))
plt.savefig('%sboxplot_delta_%s_%s_%s_%s%s_%s_%s_%s-%s--%s-%s.pdf' %(
    outdir, target_var, target_file, target_mask, len(diag_var), diag_var[0], 
    var_file[0], res_name[0], syear_fut, eyear_fut, syear_hist, eyear_hist))

## read linear regression data
#infile = '%s%s/Lin_Reg/%s/ncdf/timeseries_lin_reg_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s.nc' %(
#    path, target_var, region, target_var, target_file, target_mask,
#    diag_var[0], var_file[0], res_name[0], syear_hist, eyear_hist, syear_fut, eyear_fut, len(years))
#fh = nc.Dataset(infile, mode = 'r')
#linreg_ts_areaavg = fh.variables['linreg_ts_areaavg'][:]

## plot data
colors = plt.matplotlib.cm.YlOrRd(np.linspace(0, 1, nfiles + 20))
fig = plt.figure(figsize = (10, 5), dpi = 300)
for key, value in d_temp_mod_ts.iteritems():
    tmp_latweight = np.ma.empty((len(years), len(lon)))
    ma_value = np.ma.masked_array(value, np.isnan(value))
    for ilon in xrange(len(lon)):
        tmp_latweight[:, ilon] = np.ma.average(ma_value[:, :, ilon],
                                               axis = 1, weights = w_lat)
    value_areaavg = np.nanmean(tmp_latweight.filled(np.nan), axis = 1)
    plt.plot(years, value_areaavg, color = 'grey', linewidth = 0.5)

sort_I2 = sorted(I2_all, reverse = True)
for mod in xrange(len(models_1ens)):
    indc = sort_I2.index(I2_all[mod])
    if mod == 0:
        plt.plot(years, np.squeeze(approx_ts_areaavg[mod, :]),
                 color = colors[indc], linewidth = 0.5,
                 label = "weighted MMM model-as-truth")
        plt.plot(years, np.squeeze(lin_pred_aa[mod, :]), color = 'blue',
                 linewidth = 0.5, label = "lin trend")
    else:
        plt.plot(years, np.squeeze(approx_ts_areaavg[mod, :]),
                 color = colors[indc], linewidth = 0.5)
        plt.plot(years, np.squeeze(lin_pred_aa[mod, :]), color = 'blue',
                 linewidth = 0.5)
#plt.plot(years, linreg_ts_areaavg, 'blue', label = 'OLS trend', linewidth = 2.0)

plt.plot(years, np.nanmean(mm_ts_areaavg, axis = 0), 'black',
         label = "non-weighted MMM", linewidth = 3.0)

plt.xlabel('Year')
plt.ylabel('%s %s %s [%s]' %(region, target_var, res_name_target, unit))
plt.grid(False)
leg = plt.legend(loc = 'upper left')  ## leg defines legend -> can be modified
leg.draw_frame(False)

plt.savefig('%stimeseries_%s_%s_%s_%s%s_%s_%s.pdf' %(
    outdir, target_var, target_file, target_mask, len(diag_var),
    diag_var[0], var_file[0], res_name[0]))

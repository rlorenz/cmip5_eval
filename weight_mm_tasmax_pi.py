#!/usr/bin/python
'''
File Name : weight_mm_tasmax_pi.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 11-05-2016
Modified: Wed 11 May 2016 02:29:05 PM CEST
Purpose: calculate weighted multi-model for tasmax depending on tasmax and pi


'''
import netCDF4 as nc # to work with NetCDF files
import numpy as np
from netcdftime import utime
import datetime as dt
from scipy import signal
import os # operating system interface
from os.path import expanduser
home = expanduser("~") # Get users home directory
workdir = os.getcwd()
import glob
import sys
sys.path.insert(0,home+'/scripts/plot_scripts/utils/')
sys.path.insert(0,workdir+'/functions/')
#from calc_RMSE_obs_mod_3D import rmse_3D
from func_read_data import func_read_netcdf
from func_calc_rmse import func_calc_rmse
from clim_seas_TLL import clim_mon_TLL, std_mon_TLL, seas_avg_mon_TLL, clim_seas_mon_TLL, seas_std_mon_TLL, ann_avg_mon_TLL
from point_inside_polygon import point_inside_polygon
from func_calc_wu_wq import calc_wu, calc_wq, calc_weights_approx 
from func_calc_corr import calc_corr
from func_eval_wmm_nonwmm_error_indexI import error_indexI
from draw_utils import draw
import matplotlib.pyplot as plt
import operator
#import pdb
###
# Define input
###
variable = ['tasmax','tasmax','tasmax','pi_seas']   #  ['tas','tas','tas']         #multiple variables possible but deltas need to be available
                                                          #and first one determines plotting and titles in plots
var_file = ['clim','std','trnd','clim']  # ['clim','std','trnd']         #climatology:clim, variability:std, trend:trnd
var_kind = ['seas','seas','seas','seas'] #   ['seas','seas','seas']        #kind is cyc: annual cycle, or mon: monthly values
fields_weight = [1,1,1,1]  #[1,1,1]              #weight of individual fields, all equal weight 1 at the moment
res = 2                         #choose data for particular month or season?
region = 'CNA_flip'
obsdata = 'MERRA2'

if region!=None:
        area = region
else:
        area = 'GLOBAL'

method= 'correlation' #'correlation'  #method to calculate optimal sigmas, correlation or IError or fix (if sigmas fixed)
#test = 20 #fore testing only use 10 models to fasten things up

archive = '/net/atmos/data/cmip5-ng'
indir = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/'
outdir = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/Eval_Weight/%s/' %(variable[0])
if (os.access(outdir,os.F_OK)==False):
        os.makedirs(outdir)

syear_eval = 1980
eyear_eval = 2015
s_year = 1951
e_year = 2100

experiment = 'rcp85'
grid = 'g025'

err = 'RMSE' #'perkins_SS'
err_var = 'rmse' #'SS'
# free parameter "radius of similarity" , minimum: internal variability ~0.04
# the larger the more distant models are considered similar
#sigma_S2 = 0.4 #1.0963
# free parameter "radius of model quality"
# minimum is smallest obs. bias seen in ensemble ~+-1.8**2
# wide: mean intermodel distance in CMIP5
#sigma_D2 = 0.6 #1.8353
# or define number of simgas to be tested if sigmas are to be calculated dynamically
sigma_size = 41

###
# Read data
###
#read rmse for var_file
#first read model names for all var_file from text file
#select all models that occur for all var_files, or if testing select number of simulations defined in test
rmse_models = dict()
overlap = list()
for v in xrange(len(var_file)):
        rmsefile = '%s%s/%s_%s_%s_%s_%s_%s_all_%s_%s-%s' %(indir,variable[v],variable[v],var_file[v],var_kind[v],area,obsdata,err,experiment,syear_eval,eyear_eval)
        if (os.access(rmsefile+'.txt',os.F_OK)==True):
                rmse_models[var_file[v]] = np.genfromtxt(rmsefile+'.txt', delimiter='', dtype=None).tolist()[:-1]
                if v>0:
                        overlap = list(set(rmse_models[var_file[v]]) & set(overlap))
                else:
                        overlap = rmse_models[var_file[v]]
                try:
                        overlap = overlap[0:test]
                except (NameError):
                        pass

indices = dict()
rmse_d = np.ndarray((len(var_file),len(overlap),len(overlap)))
rmse_q = np.ndarray((len(var_file),len(overlap)))
for v in xrange(len(var_file)):
    rmsefile = '%s%s/%s_%s_%s_%s_%s_%s_all_%s_%s-%s' %(indir,variable[v],variable[v],var_file[v],var_kind[v],area,obsdata,err,experiment,syear_eval,eyear_eval)
    #find indices of model_names in rmse_file
    rmse_models[var_file[v]] = np.genfromtxt(rmsefile+'.txt', delimiter='', dtype=None).tolist()[:-1]
    for m in xrange(len(overlap)):
            indices[overlap[m]] = rmse_models[var_file[v]].index(overlap[m])
    sorted_ind = sorted(indices.items(), key = operator.itemgetter(1))
    ind = [x[1] for x in sorted_ind]
    model_names = [x[0] for x in sorted_ind]
    if (os.access(rmsefile+'.nc',os.F_OK)==True):
            print err+' already exist, read from netcdf'
            fh = nc.Dataset(rmsefile+'.nc',mode='r')
            rmse_all = fh.variables[err_var]
            rmse_d[v,:,:] = rmse_all[ind,ind]
            rmse_q[v,:] = rmse_all[-1,ind]
            fh.close()
    else:
            print "RMSE delta matrix does not exist yet, exiting"
            sys.exit
delta_u_tmp = np.ndarray((len(var_file),len(model_names),len(model_names)))
delta_q_tmp = np.ndarray((len(var_file),len(model_names)))
for v in xrange(len(var_file)):
    #normalize rmse by median
    med = np.median(rmse_d[v,:,:])
    delta_u_tmp[v,:,:] = rmse_d[v,:,:]/med
    delta_q_tmp[v,:] = rmse_q[v,:]/med
    
#average deltas over fields, taking field weight into account (all 1 at the moment)
field_w_extend_u = np.reshape(np.repeat(fields_weight,len(model_names)*len(model_names)),(len(fields_weight),len(model_names),len(model_names)))
delta_u = np.sqrt(np.sum(field_w_extend_u*delta_u_tmp,axis=0)/np.sum(fields_weight))
    
field_w_extend_q = np.reshape(np.repeat(fields_weight,len(model_names)),(len(fields_weight),len(model_names)))
delta_q = np.sqrt(np.sum(field_w_extend_q*delta_q_tmp,axis=0)/np.sum(fields_weight))

#read obs data
print "Read "+obsdata+" data"
obsfile_ts = '%s%s/%s_%s_%s_%s_%s_%s_%s-%s.nc' %(indir,variable[0],variable[0],'ts',var_kind[0],obsdata,area,experiment,syear_eval,eyear_eval)
fh = nc.Dataset(obsfile_ts,mode='r')
temp_obs_ts = fh.variables[variable[0]][:]
time = fh.variables['time']
cdftime = utime(time.units,calendar=time.calendar)
obsdates=cdftime.num2date(time[:])
obsyears=np.asarray([obsdates[i].year for i in xrange(len(obsdates))])
fh.close()

obsfile = '%s%s/%s_%s_%s_%s_%s_%s_%s-%s.nc' %(indir,variable[0],variable[0],'clim',var_kind[0],obsdata,area,experiment,syear_eval,eyear_eval)
fh = nc.Dataset(obsfile,mode='r')
temp_obs_clim = fh.variables[variable[0]][:]
fh.close()
obsfile = '%s%s/%s_%s_%s_%s_%s_%s_%s-%s.nc' %(indir,variable[0],variable[0],'std',var_kind[0],obsdata,area,experiment,syear_eval,eyear_eval)
fh = nc.Dataset(obsfile,mode='r')
temp_obs_std = fh.variables[variable[0]][:]
fh.close()

obsfile = '%s%s/%s_%s_%s_%s_%s_%s_%s-%s.nc' %(indir,variable[0],variable[0],var_file[0],var_kind[0],obsdata,area,experiment,syear_eval,eyear_eval)
fh = nc.Dataset(obsfile,mode='r')
temp_obs_var = fh.variables[variable[0]][:]
lat = fh.variables['lat'][:]
lon = fh.variables['lon'][:]
fh.close()

###read model data
#same models as in rmse file
print "Read model data"
d_temp_mod_ts = dict()
d_temp_mod = dict()
nfiles = len(model_names)
print '%s matching files' %(str(nfiles))
for f in xrange(len(model_names)):
    model = model_names[f].split('_',1)[0]
    ens = model_names[f].split('_',1)[1]
    # need to re-read data over full time period and recalculate clim, std, trnd
    filename = '%s/%s/%s_mon_%s_%s_%s_%s.nc' %(archive,variable[0],variable[0],model,experiment,ens,grid)
    print "Read %s data" %(filename)
    model_data = func_read_netcdf(filename,variable[0],var_units=True,syear=s_year,eyear=e_year)
    temp_mod = model_data['data']
    lat = model_data['lat']
    lon = model_data['lon']
    dates = model_data['dates']
    unit = model_data['unit']
    months = np.asarray([dates[i].month for i in xrange(len(dates))])
    years = np.asarray([dates[i].year for i in xrange(len(dates))])

    if isinstance(temp_mod, np.ma.core.MaskedArray):
            temp_mod = temp_mod.filled(np.NaN)
    if (variable[0] == 'sic') and  (model=="EC-EARTH"):
            with np.errstate(invalid='ignore'):
                    temp_mod[temp_mod<0.0] = np.NaN

    #if any region defined, cut data into region
    if region != None:
        mask=np.loadtxt('/home/rlorenz/scripts/plot_scripts/areas_txt/%s.txt' %(region))
        temp_mod_reg_nan = np.ndarray((temp_mod.shape))
        for ilat in xrange(len(lat)):
                for ilon in xrange(len(lon)):
                        if (point_inside_polygon(lat[ilat],lon[ilon],np.fliplr(mask))):
                                temp_mod_reg_nan[:,ilat,ilon] = temp_mod[:,ilat,ilon]
                        else:
                                temp_mod_reg_nan[:,ilat,ilon] = np.NaN
        idx_lat = np.where((lat>mask[2,1]) & (lat<mask[0,1]))
        lat = lat[idx_lat]
        tmp1_mod_reg = np.empty((len(temp_mod_reg_nan),len(lat),len(lon)))
        tmp1_mod_reg.fill(np.NaN)
        idx_lon = np.where((lon>mask[0,0]) & (lon<mask[1,0]))
        lon = lon[idx_lon]
        temp_mod_reg = np.empty((len(temp_mod_reg_nan),len(lat),len(lon)))
        temp_mod_reg.fill(np.NaN)
        tmp1_mod_reg = np.squeeze(temp_mod_reg_nan[:,idx_lat,:],1)
        temp_mod_reg = np.squeeze(tmp1_mod_reg[:,:,idx_lon],2)
        del temp_mod
    else:
        temp_mod_reg = temp_mod

    #calculate timeseries and save for each model
    if (res==None) and (var_kind[0]=='cyc'):
            temp_mod_ts = ann_avg_mon_TLL(temp_mod_reg,years)
            d_temp_mod_ts[model+'_'+ens] = temp_mod_ts
            years = years[0::12]
            time_out = model_data['time'][0::12]
    elif (res==None) and (var_kind[0]!='cyc'):
            d_temp_mod_ts[model+'_'+ens] = temp_mod_reg
    else:
            if var_kind[0]=='mon':
                ind=np.where(months==res)
                d_temp_mod_ts[model+'_'+ens] = np.squeeze(temp_mod_reg[ind,:,:])
                years = years[ind]
            elif var_kind[0]=='seas':
                temp_mod_clim = np.empty((1,len(lat),len(lon)))
                temp_mod_clim.fill(np.NaN)
                temp_mod_clim_seas = seas_avg_mon_TLL(temp_mod_reg,months)
                d_temp_mod_ts[model+'_'+ens] = clim_seas_mon_TLL(temp_mod_reg,months,years)[res,:,:,:]
                years = years[0::12]    
    #calculate climatology
    if var_file[0]=='clim':
            if res==None:
                    d_temp_mod[model+'_'+ens] = clim_mon_TLL(temp_mod_reg,months)
            else:
                    if var_kind[0]=='mon':
                            ind=np.where(months==res)
                            d_temp_mod[model+'_'+ens] = np.nanmean(np.squeeze(temp_mod_reg[ind,:,:]),axis=0,keepdims=True)
                    elif var_kind[0]=='seas':
                            temp_mod_clim = np.empty((1,len(lat),len(lon)))
                            temp_mod_clim.fill(np.NaN)
                            temp_mod_clim_seas = seas_avg_mon_TLL(temp_mod_reg,months)
                            temp_mod_clim[0,:,:] = temp_mod_clim_seas[res,:,:]
                            d_temp_mod[model+'_'+ens] = temp_mod_clim
    if var_file[0]=='std':
            #detrend before calculating variability
            if np.isnan(temp_mod_reg).any():
                    temp_mod_reg_dtr = np.empty((temp_mod_reg.shape))
                    temp_mod_reg_dtr.fill(np.NaN)
                    for ilat in xrange(len(lat)):
                            for ilon in xrange(len(lon)):
                                    tmp = temp_mod_reg[:,ilat,ilon]
                                    if np.isnan(tmp).any():
                                            temp_mod_reg_dtr[:,ilat,ilon] = np.NaN
                                    else:
                                            temp_mod_reg_dtr[:,ilat,ilon] = signal.detrend(tmp, axis=0, type='linear',bp=0)
            else:
                    temp_mod_reg_dtr = signal.detrend(temp_mod_reg, axis=0, type='linear',bp=0)
            if res==None:
                    d_temp_mod[model+'_'+ens] = std_mon_TLL(temp_mod_reg_dtr,months)
            else:
                    if var_kind[0]=='mon':
                            ind=np.where(months==res)
                            d_temp_mod[model+'_'+ens] = np.nanstd(np.squeeze(temp_mod_reg_dtr[ind,:,:]),axis=0,keepdims=True)
                    elif var_kind[0]=='seas':
                            temp_mod_std = np.empty((1,len(lat),len(lon)))
                            temp_mod_std.fill(np.NaN)
                            temp_mod_std_seas =seas_std_mon_TLL(temp_mod_reg,months)
                            temp_mod_std[0,:,:] = temp_mod_std_seas[res,:,:]
                            d_temp_mod[model+'_'+ens] = temp_mod_std
    if var_file[0]=='trnd':
            if res==None:
                    d_temp_mod[model+'_'+ens] = trend_TLL(np.squeeze(d_temp_mod_ts[model+'_'+ens]),drop=False)
            else:
                    if var_kind[0]=='mon':
                            ind=np.where(months==res)
                            d_temp_mod[model+'_'+ens] = trend_TLL(np.squeeze(temp_mod_reg[ind,:,:]),drop=False)
                    elif var_kind[0]=='seas':
                            d_temp_mod[model+'_'+ens] = trend_TLL(np.squeeze(d_temp_mod_ts[model+'_'+ens]),drop=False)

###
# Calculate weights
###
print "Calculate weights for model dependence (u:uniqueness) and quality (q)"

try: sigma_S2
except NameError: sigma_S2 = None
if sigma_S2 is None:
    print "Find optimal sigmas"
#get sigmas, test over range of sigmas and find ideal correlation of weighted mean with original values
    tmp = np.mean(delta_u)
    sigma_S2 = np.linspace(tmp-1.9*tmp,tmp+1.9*tmp,sigma_size) #wu
    sigma_D2 = np.linspace(tmp-1.9*tmp,tmp+1.9*tmp,sigma_size) #wq

    #for perfect model approach only use one ensemble per model
    ind_ens1 = [i for i in range(len(model_names)) if not model_names[i].endswith('r1i1p1')]
    delta_u_ens1 = np.delete(delta_u,ind_ens1,axis=0)
    model_names_ens1 = np.delete(model_names,ind_ens1,axis=0)
    w_u = calc_wu(delta_u_ens1,model_names_ens1,sigma_S2)
    w_q = calc_wq(delta_u_ens1,model_names_ens1,sigma_D2)
    print "wu and wq calculated for all sigmas"

    #subset obs years in model timeseries
    d_temp_mod_obsts = dict()
    if (len(years)!=len(obsyears)):
        sind = years.tolist().index(obsyears[0])
        eind = years.tolist().index(obsyears[-1])
        for key, value in d_temp_mod_ts.iteritems():
                d_temp_mod_obsts[key] = value[sind:eind+1,:,:]
    else:
        for key, value in d_temp_mod_ts.iteritems():
                d_temp_mod_obsts[key] = value
            
    temp_mod_ens1 = [value for key, value in d_temp_mod_obsts.iteritems() if 'r1i1p1' in key]

if ((type(sigma_D2) is np.ndarray) | (type(sigma_S2) is np.ndarray)):
    print "Calculate approximations and weights"
    tmp_wmm = calc_weights_approx(w_u,w_q,model_names_ens1,temp_mod_ens1)
    
    #reshape approx to len(sigma_S2),len(sigma_D2),len(model_names),time*lat*lon
    ntim = temp_mod_ens1[0].shape[0]
    approx_flat = np.reshape(tmp_wmm['approx'],(len(sigma_S2),len(sigma_D2),len(model_names_ens1),ntim*len(lat)*len(lon)),order='A')
    temp_mod_ens1_flat = np.reshape(temp_mod_ens1,(len(model_names_ens1),ntim*len(lat)*len(lon)),order='A')
    if (method== 'correlation'):
        print "Calculate correlations to determine optimal sigmas"
        corr = calc_corr(sigma_S2,sigma_D2,approx_flat,temp_mod_ens1_flat,model_names_ens1)
        #calculate indices of optimal sigmas
        ind1, ind2 = np.unravel_index(np.argmax(corr),corr.shape)
        sigma_S2_end = sigma_S2[ind1]
        sigma_D2_end = sigma_D2[ind2]
        #plot optimal sigmas in correlation sigma space
        levels = np.arange(np.min(corr),np.max(corr),0.01)
        fig = plt.figure(figsize=(10, 8),dpi=300)
        cax = plt.contourf(sigma_D2,sigma_S2, corr, levels, cmap=plt.cm.YlOrRd, extend='both')
        cbar = plt.colorbar(cax, orientation='vertical')
        lx = plt.xlabel('$\sigma_D$', fontsize=18)
        ly = plt.ylabel('$\sigma_S$', fontsize=18)
        sax = plt.scatter(sigma_D2_end,sigma_S2_end,marker='o', c='k',s=5)
        plt.savefig('%s/corr_sigmas_%s_%s_%s_%s.pdf' %(outdir,variable[0],variable[-1],obsdata,area))    
    elif (method == 'IError'):
            print "Calculate error Index I to determine optimal sigmas"
            tmp_mm = np.nanmean(temp_mod_ens1,axis=0)
            I2_sigmas = np.empty((len(sigma_S2),len(sigma_D2)))
            I2_sigmas.fill(np.NaN)
            for s2 in xrange(len(sigma_S2)):
                    for d2 in xrange(len(sigma_D2)):
                            #calculate average weight from perfect model approach
                            weights = np.nanmean(tmp_wmm['weights'][s2,d2,:,:],axis = 0)
                            #calculate wmm for this sigma pair
                            tmp_mod = 0
                            for m in xrange(len(model_names_ens1)):
                                    tmp_mod= tmp_mod + tmp_wmm['approx'][s2,d2,m,:,:,:]*weights[m]
                            tmp_wmm_clim = tmp_mod/np.sum(weights)
                            I2_sigmas[s2,d2] = error_indexI(tmp_wmm_clim,tmp_mm,temp_obs_clim,temp_obs_std,lat,lon)
            #calcuate indices of smalles I2 error to determine optimal sigmas
            ind1, ind2 = np.unravel_index(np.argmin(I2_sigmas),I2_sigmas.shape)
            sigma_S2_end = sigma_S2[ind1]
            sigma_D2_end = sigma_D2[ind2]

    print "Optimal sigmas are %s and %s" %(str(sigma_S2_end), str(sigma_D2_end))

try: sigma_S2_end 
except NameError: sigma_S2_end = sigma_S2
try: sigma_D2_end
except NameError: sigma_D2_end = sigma_D2
wu_end = calc_wu(delta_u,model_names,sigma_S2_end)
wq_end = calc_wq(delta_q,model_names,sigma_D2_end)

###
# Calculate weighted multi-model climatologies
###
print "Calculate weighted and non-weighted model means"        
approx_wmm = calc_weights_approx(wu_end,wq_end,model_names,d_temp_mod)
#for time series calculate average mean first
d_temp_mod_ts_glob=dict()
for key, value in d_temp_mod_ts.iteritems():
        d_temp_mod_ts_glob[key] = np.apply_over_axes(np.nanmean, value, (1, 2))
d_temp_mod_glob=dict()
for key, value in d_temp_mod.iteritems():
        d_temp_mod_glob[key] = np.apply_over_axes(np.nanmean, value, (1, 2))

approx_wmm_ts = calc_weights_approx(wu_end,wq_end,model_names,d_temp_mod_ts_glob)
    
#test if sum of all weights equals one
weights = approx_wmm['weights']
if (type(weights) is dict):
        dims_w = len(weights.values())
        if (dims_w == len(model_names)):
            if (np.sum(weights.values()) != 1.0):
                    print "Warning: Sum of all weights does not equal 1 but "+str(np.sum(weights.values()))
else:
        dims_w = weights.shape
        if (dims_w[0] == len(model_names)):
                if (np.sum(weights) != 1.0):
                        print "Warning: Sum of all weights does not equal 1 but "+str(np.sum(weights))
        else:
            if (np.sum(weights[0,0,:]) != 1.0):
                    print "Warning: Sum of all weights does not equal 1 but "+str(np.sum(weights[0,0,:]))

wmm = approx_wmm['approx']
wmm_ts = approx_wmm_ts['approx']
print wmm.shape, np.nanmax(wmm), np.nanmin(wmm)
print wmm_ts.shape, np.nanmax(wmm_ts), np.nanmin(wmm_ts)

# area mean non-weighted
if len(wmm.shape)==3:
        wmm_glob = np.apply_over_axes(np.nanmean, wmm, (1, 2))
else:
        wmm_glob = np.apply_over_axes(np.nanmean, wmm, (0, 1))

# calculate unweighted mean
tmp2 = 0
for key, value in d_temp_mod.iteritems():
        tmp2 = tmp2 + value

mm = tmp2/len(model_names)
print mm.shape, np.nanmax(mm), np.nanmin(mm)

tmp3 = 0
for key, value in d_temp_mod_ts_glob.iteritems():
        tmp3 = tmp3 + value

mm_ts = tmp3/len(model_names)
print mm_ts.shape, np.nanmax(mm_ts), np.nanmin(mm_ts)

# area mean non-weighted
if len(mm.shape)==3:
        mm_glob = np.apply_over_axes(np.nanmean, mm, (1, 2))
else:
        mm_glob = np.apply_over_axes(np.nanmean, mm, (0, 1))

#calculate area mean for obs
if len(temp_obs_var.shape)==3:
        obs_glob = np.apply_over_axes(np.nanmean, temp_obs_var, (1, 2))
else:
        obs_glob = np.apply_over_axes(np.nanmean, temp_obs_var, (0, 1))
obs_ts_glob = np.apply_over_axes(np.nanmean, temp_obs_ts, (1, 2))

#average over months
#wmm_ann = np.nanmean(wmm,axis=0)
#mm_ann = np.nanmean(mm,axis=0)
#temp_obs_clim_ann = np.nanmean(temp_obs_clim,axis=0)
#temp_obs_std_ann = np.nanmean(temp_obs_std,axis=0)
#evaluate weighted multi-model mean with error index I^2
#I2 = error_indexI(wmm_ann,mm_ann,temp_obs_clim_ann,temp_obs_std_ann,lat,lon)
I2 = error_indexI(np.squeeze(wmm),np.squeeze(mm),temp_obs_clim,temp_obs_std,lat,lon,var_kind[0])
print I2

###
# Plotting
###
print "Plot data"
swu_txt = str(np.round(sigma_S2_end,3))
swq_txt = str(np.round(sigma_D2_end,3))
# plot area average of annual cycle
if res==None:
    fig = plt.figure(figsize=(10, 5),dpi=300)
    colors = plt.matplotlib.cm.YlOrRd(np.linspace(0,1,nfiles+20))
    sorted_weights = sorted(weights.items(), key=operator.itemgetter(1))
    for key, value in d_temp_mod_glob.iteritems():
            indc = sorted_weights.index((key, weights[key]))
            plt.plot(range(12),value[:,0,0],color=colors[indc],linewidth=0.5)
    plt.plot(range(12),obs_glob[:,0,0],'k-',label=obsdata,linewidth=3.0)
    plt.plot(range(12),mm_glob[:,0,0],color="mediumblue",linestyle='-',label="non-weighted MMM",linewidth=3.0)
    plt.plot(range(12),wmm_glob[:,0,0],color="red",linestyle='-',label="weighted MMM",linewidth=3.0)

    plt.xlabel('Month')
    plt.ylabel('%s mean %s [%s]' %(area,variable[0],unit)) #can use LaTeX for subscripts etc
    plt.grid(True)
    leg=plt.legend(loc='upper left') #leg defines legend -> can be modified
    leg.draw_frame(False)

    plt.savefig('%s%s_%s_%s_%s_%s_%s_%s_swu%s_swq%s_%s_mean_wmm.pdf' %(outdir,variable[0],variable[-1],var_file[0],var_kind[0],obsdata,area,err,swu_txt,swq_txt,method))

# plot area average of annual timeseries
fig = plt.figure(figsize=(10, 5),dpi=300)
colors = plt.matplotlib.cm.YlOrRd(np.linspace(0,1,nfiles+20))
sorted_weights = sorted(weights.items(), key=operator.itemgetter(1))
for key, value in d_temp_mod_ts_glob.items():
        indc = sorted_weights.index((key, weights[key]))
        plt.plot(years,value[:,0,0],color=colors[indc],linewidth=0.5)

plt.plot(obsyears,obs_ts_glob[:,0,0],'k-',label=obsdata,linewidth=3.0)
plt.plot(years,mm_ts[:,0,0],color="mediumblue",linestyle='-',label="non-weighted MMM",linewidth=3.0)
plt.plot(years,wmm_ts[:,0,0],color="red",linestyle='-',label="weighted MMM",linewidth=3.0)

plt.xlabel('Year')
plt.ylabel(area+' '+variable[0]+' '+var_file[0]+' '+var_kind[0]+' ['+unit+'] timeseries') #can use LaTeX for subscripts etc
plt.grid(True)
leg=plt.legend(loc='upper left') #leg defines legend -> can be modified
leg.draw_frame(False)

plt.savefig('%s%s_%s_%s_%s_%s_%s_%s_swu%s_swq%s_%s_mean_wmm_ts.pdf' %(outdir,variable[0],variable[-1],var_file[0],var_kind[0],obsdata,area,err,swu_txt,swq_txt,method))

#plot maps
#lev = np.arange(271,303,3)
if len(wmm.shape)==3:
        plot_wmm = np.mean(wmm, axis = 0)
elif len(wmm.shape)==2:
        plot_wmm = wmm
draw(plot_wmm,lat, lon, title="%s [%s] weighted multi-model mean" %(variable[0],unit), region=region) #, levels = lev)
plt.savefig('%s%s_%s_%s_%s_%s_%s_wu%s_wq%s_%s_map_wmm.pdf' %(outdir,variable[0],variable[-1],var_file[0],var_kind[0],obsdata,area,swu_txt,swq_txt,method))

if len(mm.shape)==3:
        plot_mm = np.mean(mm, axis = 0)
elif len(mm.shape)==2:
        plot_mm = mm
draw(plot_mm,lat, lon, title=variable[0]+" ["+unit+"] multi-model mean", region=region) #, levels = lev)
plt.savefig('%s%s_%s_%s_%s_%s_%s_%s_swu%s_swq%s_%s_map_mm.pdf' %(outdir,variable[0],variable[-1],var_file[0],var_kind[0],obsdata,area,err,swu_txt,swq_txt,method))

diff = plot_wmm-plot_mm
min_diff = np.nanmin(diff)
max_diff = np.nanmax(diff)
end = np.mean([abs(round(max_diff)),abs(round(min_diff))])
delta = (end*2)/9.0
if (end == end+delta):
        lev_d = [-0.09,-0.07,-0.05,-0.03,-0.01,0.01,0.03,0.05,0.07,0.09]
else:
        lev_d = np.arange(-end,end+delta,delta)
        
draw(diff,lat, lon, title="%s [%s] weighted - non-weighted, I$^2$=%s" %(variable[0],unit,str(round(I2,3))), levels = lev_d, colors = "RdBu_r", region=region)
plt.savefig('%s%s_%s_diff_%s_%s_%s_%s_%s_swu%s_swq%s_%s_map_wmm-mm.pdf' %(outdir,variable[0],variable[-1],var_file[0],var_kind[0],obsdata,area,err,swu_txt,swq_txt,method))

#!/usr/bin/python
'''
File Name : rmse_modmod_vs_modobs_11.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 27-04-2016
Modified: Wed 27 Apr 2016 11:52:14 AM CEST
Purpose: plot rmse model-model versus model-obs


'''
import netCDF4 as nc # to work with NetCDF files
import numpy as np
import os # operating system interface
from os.path import expanduser
home = expanduser("~") # Get users home directory
import sys
sys.path.insert(0,home+'/scripts/plot_scripts/utils/')
from func_read_data import func_read_netcdf
import matplotlib.pyplot as plt

###
# Define input
###
delta_file = ["sic_clim","sic_trend","tas_rmse_clim","tas_rmse_std"]
delta_kind = ["mon","mon","cyc","cyc"]
fields_weight = [1.,1.,1.,1.]
delta_time = [9,9,9,9]

nmodel = 114
model_names = ["ACCESS1-0","ACCESS1-3","bcc-csm1-1-m","bcc-csm1-1","CanESM2","CanESM2","CanESM2","CanESM2","CanESM2","CCSM4","CCSM4","CCSM4","CCSM4","CCSM4","CCSM4","CESM1-BGC","CESM1-CAM5","CESM1-CAM5","CESM1-CAM5","CMCC-CM","CMCC-CMS","CNRM-CM5","CSIRO-Mk3-6-0","CSIRO-Mk3-6-0","CSIRO-Mk3-6-0","CSIRO-Mk3-6-0","CSIRO-Mk3-6-0","CSIRO-Mk3-6-0","CSIRO-Mk3-6-0","CSIRO-Mk3-6-0","CSIRO-Mk3-6-0","CSIRO-Mk3-6-0","FGOALS-g2","FIO-ESM","FIO-ESM","FIO-ESM","GFDL-CM3","GFDL-ESM2G","GFDL-ESM2M","GISS-E2-H-CC","GISS-E2-H","GISS-E2-H","GISS-E2-H","GISS-E2-H","GISS-E2-H","GISS-E2-H","GISS-E2-H","GISS-E2-H","GISS-E2-H","GISS-E2-H","GISS-E2-H","GISS-E2-H","GISS-E2-H","GISS-E2-H","GISS-E2-H","GISS-E2-R-CC","GISS-E2-R","GISS-E2-R","GISS-E2-R","GISS-E2-R","GISS-E2-R","GISS-E2-R","GISS-E2-R","GISS-E2-R","GISS-E2-R","GISS-E2-R","GISS-E2-R","GISS-E2-R","GISS-E2-R","GISS-E2-R","GISS-E2-R","GISS-E2-R","GISS-E2-R","HadGEM2-AO","HadGEM2-CC","HadGEM2-ES","HadGEM2-ES","HadGEM2-ES","HadGEM2-ES","inmcm4","IPSL-CM5A-LR","IPSL-CM5A-LR","IPSL-CM5A-LR","IPSL-CM5A-LR","IPSL-CM5A-MR","IPSL-CM5B-LR","MIROC5","MIROC5","MIROC5","MIROC-ESM-CHEM","MIROC-ESM","MPI-ESM-LR","MPI-ESM-LR","MPI-ESM-LR","MPI-ESM-MR","MPI-ESM-MR","MPI-ESM-MR","MRI-CGCM3","NorESM1-ME","NorESM1-M","bccr_bcm2_0","cccma_cgcm3_1","cccma_cgcm3_1_t63","csiro_mk3_0","gfdl_cm2_1","giss_aom","iap_fgoals1_0_g","inmcm3_0","miroc3_2_medres","miub_echo_g","mpi_echam5","mri_cgcm2_3_2a","ncar_ccsm3_0","ukmo_hadcm3","Obs"]

archive = '/net/atmos/climphys/sedjan/Eval_Weight/Use_multiple_vars_obs_2016/'
outdir = '/net/tropo/climphys/rlorenz/Eval_Weight/'

###
# Read data
###
delta = np.ndarray((len(delta_file),nmodel,nmodel))
med_obs = np.ndarray((len(delta_file)))
med_mod = np.ndarray((len(delta_file)))
for j in range(len(delta_file)):
    filename = archive+delta_file[j]+'_cmip5_cmip3_obs_all_2050.nc'
    ifile = nc.Dataset(filename,mode='r')

    if (delta_kind[j]=="mon"):
        delta_tmp = ifile.variables['delta_mon'][delta_time[j]-1,:,:]
    elif (delta_kind[j]=="cyc"):
        delta_tmp0 = ifile.variables['delta_mon'][:]
        delta_tmp = np.mean(delta_tmp0,axis=0)
    #exclude "iap_fgoals1_0_g"
    ind = model_names.index('iap_fgoals1_0_g')
    delta_tmp1 = np.delete(delta_tmp,ind,axis=0)
    delta_tmp2 = np.delete(delta_tmp1,ind,axis=1)
    med_obs[j] = np.nanmedian(delta_tmp2[-1,:])
    med_mod[j] = np.nanmedian(delta_tmp2[:-1,:-1])
    delta[j,:-1,:-1] = delta_tmp2[:-1,:-1]/med_mod[j]
    delta[j,-1,:] = delta_tmp2[-1,:]/med_obs[j]
    delta[j,:,-1] = delta_tmp2[:,-1]/med_obs[j]

model_names = np.delete(model_names,ind,axis=0)

#average deltas over fields, taking field weight into account (all 1 at the moment)
field_w_extend_u = np.reshape(np.repeat(fields_weight,nmodel*nmodel),(len(fields_weight),nmodel,nmodel))
delta_avg = np.sqrt(np.sum(field_w_extend_u*delta,axis=0)/np.sum(fields_weight))
print np.nanmin(delta_avg[delta_avg.nonzero()]), np.nanmax(delta_avg)

delta_sic_clim_mod =  np.triu(delta[0,:-1,:-1])
delta_sic_clim_mod[delta_sic_clim_mod==0.0] = np.NaN
delta_sic_trend_mod = np.triu(delta[1,:-1,:-1])
delta_sic_trend_mod[delta_sic_trend_mod==0.0] = np.NaN
delta_tas_clim_mod = np.triu(delta[2,:-1,:-1])
delta_tas_clim_mod[delta_tas_clim_mod==0.0] = np.NaN
delta_tas_std_mod = np.triu(delta[3,:-1,:-1])
delta_tas_std_mod[delta_tas_std_mod==0.0] = np.NaN

delta_sic_clim_obs = delta[0,-1,:-1]
delta_sic_clim_obs[delta_sic_clim_obs==0.0] = np.NaN
delta_sic_trend_obs = delta[1,-1,:-1]
delta_sic_trend_obs[delta_sic_trend_obs==0.0] = np.NaN
delta_tas_clim_obs = delta[2,-1,:-1]
delta_tas_clim_obs[delta_tas_clim_obs==0.0] = np.NaN
delta_tas_std_obs = delta[3,-1,:-1]
delta_tas_std_obs[delta_tas_std_obs==0.0] = np.NaN

sic_clim_mod_med = np.ndarray((delta_sic_clim_obs.shape))
sic_trend_mod_med = np.ndarray((delta_sic_trend_obs.shape))
tas_clim_mod_med = np.ndarray((delta_tas_clim_obs.shape))
tas_std_mod_med = np.ndarray((delta_tas_std_obs.shape))
for m in xrange(len(model_names)-1):
    sic_clim_mod_med[m] = np.nanmedian(delta_sic_clim_mod[m,:])
    sic_trend_mod_med[m] = np.nanmedian(delta_sic_trend_mod[m,:])
    tas_clim_mod_med[m] = np.nanmedian(delta_tas_clim_mod[m,:])
    tas_std_mod_med[m] = np.nanmedian(delta_tas_std_mod[m,:])

print np.nanmin(delta_sic_clim_obs), np.nanmax(delta_sic_clim_obs)
print np.nanmin(sic_clim_mod_med), np.nanmax(sic_clim_mod_med)

xymax = np.nanmax(sic_clim_mod_med)
fig = plt.figure()
plt.scatter(delta_sic_clim_obs,sic_clim_mod_med,marker = 'o',c='gold',label='sic_clim')
plt.scatter(delta_sic_trend_obs,sic_trend_mod_med, marker = 'o',c='grey',label='sic_trend')
plt.scatter(delta_tas_clim_obs,tas_clim_mod_med,marker = 'o',c='crimson',label='tas_clim')
plt.scatter(delta_tas_std_obs,tas_std_mod_med,marker = 'o',c='darkcyan',label='tas_std')

plt.xlim(0, round(xymax))
plt.ylim(0, round(xymax))

plt.plot(range(0,int(round(xymax))+1),range(0,int(round(xymax))+1),'k--') # identity line

plt.legend()
plt.xlabel('RMSE Model-Obs')
plt.ylabel('RMSE Model-Model')
plt.title('')
plt.savefig(outdir+'rmse_modmod_vs_modobs_medObsMed.pdf')

xymax = np.nanmax(sic_clim_mod_med)
fig = plt.figure()
plt.scatter(np.repeat(delta_sic_clim_obs,113),delta_sic_clim_mod,marker = 'o',c='gold',label='sic_clim')
plt.scatter(np.repeat(delta_sic_trend_obs,113),delta_sic_trend_mod, marker = 'o',c='grey',label='sic_trend')
plt.scatter(np.repeat(delta_tas_clim_obs,113),delta_tas_clim_mod,marker = 'o',c='crimson',label='tas_clim')
plt.scatter(np.repeat(delta_tas_std_obs,113),delta_tas_std_mod,marker = 'o',c='darkcyan',label='tas_std')

plt.xlim(0, round(xymax))
plt.ylim(0, round(xymax))

plt.plot(range(0,int(round(xymax))+1),range(0,int(round(xymax))+1),'k--') # identity line

plt.legend()
plt.xlabel('RMSE Model-Obs')
plt.ylabel('RMSE Model-Model')
plt.title('')
plt.savefig(outdir+'rmse_modmod_vs_modobsblowup_medObsMed.pdf')

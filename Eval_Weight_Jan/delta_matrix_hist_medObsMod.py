#!/usr/bin/python
'''
File Name : delta_matrix_hist.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 07-04-2016
Modified: 
Purpose: investigate delta matrix calculated by Jan
	plot histogram, all model to model diffs and obs to model diffs 


'''
import netCDF4 as nc # to work with NetCDF files
import numpy as np
import os # operating system interface
from os.path import expanduser
home = expanduser("~") # Get users home directory
import sys
sys.path.insert(0,home+'/scripts/plot_scripts/utils/')
from func_read_data import func_read_netcdf
import matplotlib.pyplot as plt
###
# Define input
###
delta_file = ["sic_clim","sic_trend","tas_rmse_clim","tas_rmse_std"]
delta_kind = ["mon","mon","cyc","cyc"]
fields_weight = [1.,1.,1.,1.]
delta_time = [9,9,9,9]
nmodel = 114
model_names = ["ACCESS1-0","ACCESS1-3","bcc-csm1-1-m","bcc-csm1-1","CanESM2","CanESM2","CanESM2","CanESM2","CanESM2","CCSM4","CCSM4","CCSM4","CCSM4","CCSM4","CCSM4","CESM1-BGC","CESM1-CAM5","CESM1-CAM5","CESM1-CAM5","CMCC-CM","CMCC-CMS","CNRM-CM5","CSIRO-Mk3-6-0","CSIRO-Mk3-6-0","CSIRO-Mk3-6-0","CSIRO-Mk3-6-0","CSIRO-Mk3-6-0","CSIRO-Mk3-6-0","CSIRO-Mk3-6-0","CSIRO-Mk3-6-0","CSIRO-Mk3-6-0","CSIRO-Mk3-6-0","FGOALS-g2","FIO-ESM","FIO-ESM","FIO-ESM","GFDL-CM3","GFDL-ESM2G","GFDL-ESM2M","GISS-E2-H-CC","GISS-E2-H","GISS-E2-H","GISS-E2-H","GISS-E2-H","GISS-E2-H","GISS-E2-H","GISS-E2-H","GISS-E2-H","GISS-E2-H","GISS-E2-H","GISS-E2-H","GISS-E2-H","GISS-E2-H","GISS-E2-H","GISS-E2-H","GISS-E2-R-CC","GISS-E2-R","GISS-E2-R","GISS-E2-R","GISS-E2-R","GISS-E2-R","GISS-E2-R","GISS-E2-R","GISS-E2-R","GISS-E2-R","GISS-E2-R","GISS-E2-R","GISS-E2-R","GISS-E2-R","GISS-E2-R","GISS-E2-R","GISS-E2-R","GISS-E2-R","HadGEM2-AO","HadGEM2-CC","HadGEM2-ES","HadGEM2-ES","HadGEM2-ES","HadGEM2-ES","inmcm4","IPSL-CM5A-LR","IPSL-CM5A-LR","IPSL-CM5A-LR","IPSL-CM5A-LR","IPSL-CM5A-MR","IPSL-CM5B-LR","MIROC5","MIROC5","MIROC5","MIROC-ESM-CHEM","MIROC-ESM","MPI-ESM-LR","MPI-ESM-LR","MPI-ESM-LR","MPI-ESM-MR","MPI-ESM-MR","MPI-ESM-MR","MRI-CGCM3","NorESM1-ME","NorESM1-M","bccr_bcm2_0","cccma_cgcm3_1","cccma_cgcm3_1_t63","csiro_mk3_0","gfdl_cm2_1","giss_aom","iap_fgoals1_0_g","inmcm3_0","miroc3_2_medres","miub_echo_g","mpi_echam5","mri_cgcm2_3_2a","ncar_ccsm3_0","ukmo_hadcm3","Obs"]

archive = '/net/atmos/climphys/sedjan/Eval_Weight/Use_multiple_vars_obs_2016/'
outdir = '/net/tropo/climphys/rlorenz/Eval_Weight/'

###
# Read data
###
delta = np.ndarray((len(delta_file),nmodel,nmodel))
med_obs = np.ndarray((len(delta_file)))
med_mod = np.ndarray((len(delta_file)))
for j in xrange(len(delta_file)):
    filename = archive+delta_file[j]+'_cmip5_cmip3_obs_all_2050.nc'
    ifile = nc.Dataset(filename,mode='r')

    if (delta_kind[j]=="mon"):
        delta_tmp = ifile.variables['delta_mon'][delta_time[j]-1,:,:]
    elif (delta_kind[j]=="cyc"):
        delta_tmp0 = ifile.variables['delta_mon'][:]
        delta_tmp = np.mean(delta_tmp0,axis=0)
    print np.max(delta_tmp), np.min(delta_tmp)
    #exclude "iap_fgoals1_0_g"
    ind = model_names.index('iap_fgoals1_0_g')
    delta_tmp1 = np.delete(delta_tmp,ind,axis=0)
    delta_tmp2 = np.delete(delta_tmp1,ind,axis=1)
    med_obs[j] = np.nanmedian(delta_tmp2[-1,:])
    med_mod[j] = np.nanmedian(delta_tmp2[:-1,:-1])
    delta[j,:-1,:-1] = delta_tmp2[:-1,:-1]/med_mod[j]
    delta[j,-1,:] = delta_tmp2[-1,:]/med_obs[j]
    delta[j,:,-1] = delta_tmp2[:,-1]/med_obs[j]

model_names = np.delete(model_names,ind,axis=0)
#average deltas over fields, taking field weight into account (all 1 at the moment)
field_w_extend_u = np.reshape(np.repeat(fields_weight,nmodel*nmodel),(len(fields_weight),nmodel,nmodel))
delta_avg = np.sqrt(np.sum(field_w_extend_u*delta,axis=0)/np.sum(fields_weight))

print delta_avg.shape
print np.nanmin(delta_avg[delta_avg.nonzero()]), np.nanmax(delta_avg)
fout=nc.Dataset(outdir+'delta_avg_medObsMod_sep_sic_climtrend_cyc_tas_climstd.nc',mode='w')
fout.createDimension('x',nmodel)
fout.createDimension('y',nmodel)
xout = fout.createVariable('x','f8',('x'),fill_value = 1e20)
setattr(xout,'Longname','x')
yout = fout.createVariable('y','f8',('y'),fill_value = 1e20)
setattr(yout,'Longname','y')
deltaout = fout.createVariable('delta_avg','f8',('x','y'),fill_value = 1e20)
setattr(deltaout,'Longname','delta matrix')
setattr(deltaout,'units','-')
setattr(deltaout,'description','delta matrix averaged over fields sic_clim, sic_trend in sept, tas_rmse_clim and tas_rmse_std averaged over all months and using median from obs for obs, models for models') 
xout[:] = range(nmodel)[:]
yout[:] = range(nmodel)[:]
deltaout[:] = delta_avg[:]

fout.close()

#plot histograms
delta_sic_clim_mod =  np.triu(delta[0,:-1,:-1]) #np.triu extract one half of symmetric matrix
delta_sic_trend_mod = np.triu(delta[1,:-1,:-1])
delta_tas_clim_mod = np.triu(delta[2,:-1,:-1])
delta_tas_std_mod = np.triu(delta[3,:-1,:-1])

delta_sic_clim_obs = np.repeat(delta[0,-1,:],56)
delta_sic_trend_obs = np.repeat(delta[1,-1,:],56)
delta_tas_clim_obs = np.repeat(delta[2,-1,:],56)
delta_tas_std_obs = np.repeat(delta[3,-1,:],56)

binwidth = 0.5
binnr = 20
fig = plt.figure()
n, bins, patches = plt.hist([delta_sic_clim_mod[delta_sic_clim_mod!=0],
                             delta_sic_clim_obs[delta_sic_clim_obs!=0]],
                            #bins=np.arange(np.amin(delta_sic_clim_mod), np.amax(delta_sic_clim_mod) + binwidth, binwidth),
                            binnr,
                            histtype='bar',
                            color = ['blue','green'], label=["model_model","model_obs"])
plt.legend()
plt.xlabel('Delta')
plt.ylabel('Count')
plt.title('Histograms of sic_clim delta matrix')
plt.savefig(outdir+'hist_delta_matrix_sicclim_medObsMod.pdf')

fig = plt.figure()
n, bins, patches = plt.hist([delta_sic_trend_mod[delta_sic_trend_mod!=0],
                             delta_sic_trend_obs[delta_sic_trend_obs!=0]],
                            #bins=np.arange(np.amin(delta_sic_trend_mod), np.amax(delta_sic_trend_obs) + binwidth, binwidth),
                            binnr,
                            histtype='bar',
                            color = ['blue','green'], label=["model_model","model_obs"])
plt.legend()
plt.xlabel('Delta')
plt.ylabel('Count')
plt.title('Histograms of sic_trend delta matrixes')
plt.savefig(outdir+'hist_delta_matrix_sictrend_medObsMod.pdf')

fig = plt.figure()
n, bins, patches = plt.hist([delta_tas_clim_mod[delta_tas_clim_mod!=0],
                             delta_tas_clim_obs[delta_tas_clim_obs!=0]],
                            #bins=np.arange(np.amin(delta_tas_clim_mod), np.amax(delta_tas_clim_mod) + binwidth, binwidth),
                            binnr,
                            histtype='bar',
                            color = ['blue','green'], label=["model_model","model_obs"])
plt.legend()
plt.xlabel('Delta')
plt.ylabel('Count')
plt.title('Histograms of tas_clim delta matrix')
plt.savefig(outdir+'hist_delta_matrix_tasclim_medObsMod.pdf')


fig = plt.figure()
n, bins, patches = plt.hist([delta_tas_std_mod[delta_tas_std_mod!=0],
                             delta_tas_std_obs[delta_tas_std_obs!=0]],
                            #bins=np.arange(np.amin(delta_tas_std_mod), np.amax(delta_tas_std_obs) + binwidth, binwidth),
                            binnr,
                            histtype='bar',
                            color = ['blue','green'], label=["model_model","model_obs"])
plt.legend()
plt.xlabel('Delta')
plt.ylabel('Count')
plt.title('Histograms of tas_std delta matrixes')
plt.savefig(outdir+'hist_delta_matrix_tasstd_medObsMod.pdf')

#plot average delta hist
delta_mod_1 = np.triu(delta_avg[:-1,:-1]) #extract one half of symmetric matrix
delta_obs = delta_avg[-1,:]

fig = plt.figure()
hist, bins = np.histogram(delta_mod_1[delta_mod_1 != 0], bins=50)
width = 0.7 * (bins[1] - bins[0])
center = (bins[:-1] + bins[1:]) / 2
plt.bar(center, hist, align='center', width=width)
plt.xlabel('Delta')
plt.ylabel('Count')
plt.title('Histogram of averaged delta matrix for models-models')
plt.savefig(outdir+'hist_deltaAvg_matrix_noobs_medMod.pdf')

fig = plt.figure()
hist, bins = np.histogram(delta_obs[delta_obs!=0], bins=50)
width = 0.7 * (bins[1] - bins[0])
center = (bins[:-1] + bins[1:]) / 2
plt.bar(center, hist, align='center', width=width)
plt.xlabel('Delta')
plt.ylabel('Count')
plt.title('Histogram of averaged delta matrix for models-observations')
plt.savefig(outdir+'hist_deltaAvg_matrix_obs_medObs.pdf')

fig = plt.figure()
delta_mod_1 = np.triu(delta_avg[:-1,:-1]) #extract one half of symmetric matrix
delta_obs_1 = delta_avg[-1,:]
n, bins, patches = plt.hist([delta_mod_1[delta_mod_1 != 0],np.repeat(delta_obs_1[delta_obs_1!=0],56)],20,
                 histtype='bar', color = ['blue','green'], label=["model_model","model_obs"])
plt.legend()
plt.xlabel('Delta')
plt.ylabel('Count')
plt.title('Histogram of averaged delta matrix')
plt.savefig(outdir+'hist_deltaAvg_matrix_medObsMod.pdf')

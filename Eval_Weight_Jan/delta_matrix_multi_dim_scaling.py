#!/usr/bin/python
'''
File Name : delta_matrix_multi_dim_scaling.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 31-03-2016
Modified: Thu 31 Mar 2016 09:53:39 AM CEST
Purpose: investigate delta matrix calculated by Jan
	visualize in 2D using multi-dimensional scaling


'''
import netCDF4 as nc # to work with NetCDF files
import numpy as np
import os # operating system interface
from os.path import expanduser
home = expanduser("~") # Get users home directory
import sys
sys.path.insert(0,home+'/scripts/plot_scripts/utils/')
from func_read_data import func_read_netcdf
from sklearn.manifold import MDS
import matplotlib.pyplot as plt
###
# Define input
###
delta_file = ["sic_clim","sic_trend","tas_rmse_clim","tas_rmse_std"]
delta_kind = ["mon","mon","cyc","cyc"]
fields_weight = [1.,1.,1.,1.]
delta_time = [9,9,9,9]
nmodel = 114
model_names = ["ACCESS1-0","ACCESS1-3","bcc-csm1-1-m","bcc-csm1-1","CanESM2","CanESM2","CanESM2","CanESM2","CanESM2","CCSM4","CCSM4","CCSM4","CCSM4","CCSM4","CCSM4","CESM1-BGC","CESM1-CAM5","CESM1-CAM5","CESM1-CAM5","CMCC-CM","CMCC-CMS","CNRM-CM5","CSIRO-Mk3-6-0","CSIRO-Mk3-6-0","CSIRO-Mk3-6-0","CSIRO-Mk3-6-0","CSIRO-Mk3-6-0","CSIRO-Mk3-6-0","CSIRO-Mk3-6-0","CSIRO-Mk3-6-0","CSIRO-Mk3-6-0","CSIRO-Mk3-6-0","FGOALS-g2","FIO-ESM","FIO-ESM","FIO-ESM","GFDL-CM3","GFDL-ESM2G","GFDL-ESM2M","GISS-E2-H-CC","GISS-E2-H","GISS-E2-H","GISS-E2-H","GISS-E2-H","GISS-E2-H","GISS-E2-H","GISS-E2-H","GISS-E2-H","GISS-E2-H","GISS-E2-H","GISS-E2-H","GISS-E2-H","GISS-E2-H","GISS-E2-H","GISS-E2-H","GISS-E2-R-CC","GISS-E2-R","GISS-E2-R","GISS-E2-R","GISS-E2-R","GISS-E2-R","GISS-E2-R","GISS-E2-R","GISS-E2-R","GISS-E2-R","GISS-E2-R","GISS-E2-R","GISS-E2-R","GISS-E2-R","GISS-E2-R","GISS-E2-R","GISS-E2-R","GISS-E2-R","HadGEM2-AO","HadGEM2-CC","HadGEM2-ES","HadGEM2-ES","HadGEM2-ES","HadGEM2-ES","inmcm4","IPSL-CM5A-LR","IPSL-CM5A-LR","IPSL-CM5A-LR","IPSL-CM5A-LR","IPSL-CM5A-MR","IPSL-CM5B-LR","MIROC5","MIROC5","MIROC5","MIROC-ESM-CHEM","MIROC-ESM","MPI-ESM-LR","MPI-ESM-LR","MPI-ESM-LR","MPI-ESM-MR","MPI-ESM-MR","MPI-ESM-MR","MRI-CGCM3","NorESM1-ME","NorESM1-M","bccr_bcm2_0","cccma_cgcm3_1","cccma_cgcm3_1_t63","csiro_mk3_0","gfdl_cm2_1","giss_aom","iap_fgoals1_0_g","inmcm3_0","miroc3_2_medres","miub_echo_g","mpi_echam5","mri_cgcm2_3_2a","ncar_ccsm3_0","ukmo_hadcm3","Obs"]

archive = '/net/atmos/climphys/sedjan/Eval_Weight/Use_multiple_vars_obs_2016/'
outdir = '/net/tropo/climphys/rlorenz/Eval_Weight/'

###
# Read data
###
delta = np.ndarray((len(delta_file),nmodel,nmodel))
med = np.ndarray((len(delta_file)))
for j in xrange(len(delta_file)):
    filename = archive+delta_file[j]+'_cmip5_cmip3_obs_all_2050.nc'
    ifile = nc.Dataset(filename,mode='r')

    if (delta_kind[j]=="mon"):
        delta_tmp = ifile.variables['delta_mon'][delta_time[j]-1,:,:]
    elif (delta_kind[j]=="cyc"):
        delta_tmp0 = ifile.variables['delta_mon'][:]
        delta_tmp = np.mean(delta_tmp0,axis=0)
    #exclude "iap_fgoals1_0_g"
    ind = model_names.index('iap_fgoals1_0_g')
    delta_tmp1 = np.delete(delta_tmp,ind,axis=0)
    delta_tmp2 = np.delete(delta_tmp1,ind,axis=1)
    med[j] = np.nanmedian(delta_tmp2[:,:])
    delta[j,:,:] = delta_tmp2/med[j]

model_names = np.delete(model_names,ind,axis=0)
#average deltas over fields, taking field weight into account (all 1 at the moment)
field_w_extend_u = np.reshape(np.repeat(fields_weight,nmodel*nmodel),(len(fields_weight),nmodel,nmodel))
delta_avg = np.sqrt(np.sum(field_w_extend_u*delta,axis=0)/np.sum(fields_weight))

print delta_avg.shape
fout=nc.Dataset(outdir+'delta_avg_sep_sic_climtrend_cyc_tas_climstd.nc',mode='w')
fout.createDimension('x',nmodel)
fout.createDimension('y',nmodel)
xout = fout.createVariable('x','f8',('x'),fill_value = 1e20)
setattr(xout,'Longname','x')
yout = fout.createVariable('y','f8',('y'),fill_value = 1e20)
setattr(yout,'Longname','y')
deltaout = fout.createVariable('delta_avg','f8',('x','y'),fill_value = 1e20)
setattr(deltaout,'Longname','delta matrix')
setattr(deltaout,'units','-')
setattr(deltaout,'description','delta matrix averaged over fields sic_clim, sic_trend in sept, tas_rmse_clim and tas_rmse_std averaged over all months, normalized with median over all deltas') 
xout[:] = range(nmodel)[:]
yout[:] = range(nmodel)[:]
deltaout[:] = delta_avg[:]

fout.close()

#multi-dimensional scaling
mds = MDS(n_components=2, dissimilarity="precomputed", random_state=5)
results = mds.fit(delta_avg)
coords = results.embedding_
plt.subplots_adjust(bottom = 0.1)
plt.scatter(coords[:, 0], coords[:, 1], marker = 'o')
#for label,x,y in zip(model_names,coords[:,0],coords[:,1]):
#    plt.annotate(label,xy = (x, y), xytext = (-20, 20),textcoords = 'offset points', ha = 'right', va = 'bottom',arrowprops = dict(arrowstyle = '->', connectionstyle = 'arc3,rad=0'))

label = model_names[-1]
x = coords[-1,0]
y = coords[-1,1]
plt.annotate(label,xy = (x, y), xytext = (-20, 20),textcoords = 'offset points', ha = 'right', va = 'bottom',arrowprops = dict(arrowstyle = '->', connectionstyle = 'arc3,rad=0'))

#fgoals if included
#label = model_names[-9]
#x = coords[-9,0]
#y = coords[-9,1]
#plt.annotate(label,xy = (x, y), xytext = (-20, 20),textcoords = 'offset points', ha = 'right', va = 'bottom',arrowprops = dict(arrowstyle = '->', connectionstyle = 'arc3,rad=0'))

plt.title('Multi-dimensional scaling of delta matrix')

plt.savefig(outdir+'MDS_deltas_med_eq_all.pdf')
